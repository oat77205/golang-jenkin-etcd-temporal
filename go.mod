module hl-order-api

go 1.19

require (
	github.com/go-playground/validator/v10 v10.10.1
	github.com/golang-jwt/jwt/v4 v4.4.2
	github.com/google/uuid v1.3.0
	github.com/spf13/viper v1.13.0
	gitlab.com/true-itsd/iservicemax/omni/api/ms/cms v0.0.1-dev-2
	gitlab.com/true-itsd/iservicemax/omni/api/ms/com-platform v0.0.2-dev-10
	gitlab.com/true-itsd/iservicemax/omni/api/ms/doc-system v0.0.1-dev-36
	gitlab.com/true-itsd/iservicemax/omni/api/ms/gcs v0.0.3-dev-14
	gitlab.com/true-itsd/iservicemax/omni/api/ms/hl-otp v0.0.1-dev-11
	gitlab.com/true-itsd/iservicemax/omni/api/ms/intx v0.0.7-dev-16
	gitlab.com/true-itsd/iservicemax/omni/api/ms/inventory v0.0.1-dev-30
	gitlab.com/true-itsd/iservicemax/omni/api/ms/omx v0.0.1-dev-18
	gitlab.com/true-itsd/iservicemax/omni/api/ms/partner v0.0.0-00010101000000-000000000000
	gitlab.com/true-itsd/iservicemax/omni/api/ms/payment v0.0.1-dev-2
	gitlab.com/true-itsd/iservicemax/omni/api/ms/pim v0.0.1-dev-4
	gitlab.com/true-itsd/iservicemax/omni/api/ms/privilege v0.0.1-dev-3
	gitlab.com/true-itsd/iservicemax/omni/api/ms/psa v0.0.1-dev-14
	gitlab.com/true-itsd/iservicemax/omni/api/ms/sale-profile v0.0.1-dev-6
	gitlab.com/true-itsd/iservicemax/omni/pkg/common-utils-backend v1.0.1
	go.mongodb.org/mongo-driver v1.10.1
	go.temporal.io/sdk v1.21.1
)

require (
	github.com/KyleBanks/depth v1.2.1 // indirect
	github.com/PuerkitoBio/purell v1.1.1 // indirect
	github.com/PuerkitoBio/urlesc v0.0.0-20170810143723-de5bf2ad4578 // indirect
	github.com/andybalholm/brotli v1.0.4 // indirect
	github.com/beorn7/perks v1.0.1 // indirect
	github.com/cespare/xxhash/v2 v2.1.2 // indirect
	github.com/coreos/go-semver v0.3.0 // indirect
	github.com/coreos/go-systemd/v22 v22.3.2 // indirect
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/denisenkom/go-mssqldb v0.12.2 // indirect
	github.com/dgryski/go-rendezvous v0.0.0-20200823014737-9f7001d12a5f // indirect
	github.com/dustin/go-humanize v1.0.0 // indirect
	github.com/facebookgo/clock v0.0.0-20150410010913-600d898af40a // indirect
	github.com/go-openapi/jsonpointer v0.19.5 // indirect
	github.com/go-openapi/jsonreference v0.19.6 // indirect
	github.com/go-openapi/spec v0.20.4 // indirect
	github.com/go-openapi/swag v0.19.15 // indirect
	github.com/go-playground/locales v0.14.0 // indirect
	github.com/go-playground/universal-translator v0.18.0 // indirect
	github.com/go-redis/redis v6.15.9+incompatible // indirect
	github.com/go-redis/redis/v8 v8.11.5 // indirect
	github.com/gogo/googleapis v1.4.1 // indirect
	github.com/gogo/protobuf v1.3.2 // indirect
	github.com/gogo/status v1.1.1 // indirect
	github.com/golang-sql/civil v0.0.0-20190719163853-cb61b32ac6fe // indirect
	github.com/golang-sql/sqlexp v0.1.0 // indirect
	github.com/golang/protobuf v1.5.2 // indirect
	github.com/golang/snappy v0.0.4 // indirect
	github.com/grpc-ecosystem/go-grpc-middleware v1.3.0 // indirect
	github.com/h2non/filetype v1.1.3 // indirect
	github.com/josharian/intern v1.0.0 // indirect
	github.com/json-iterator/go v1.1.12 // indirect
	github.com/klauspost/compress v1.15.7 // indirect
	github.com/klauspost/cpuid v1.3.1 // indirect
	github.com/leodido/go-urn v1.2.1 // indirect
	github.com/mailru/easyjson v0.7.6 // indirect
	github.com/mattn/go-colorable v0.1.13 // indirect
	github.com/mattn/go-isatty v0.0.16 // indirect
	github.com/mattn/go-runewidth v0.0.14 // indirect
	github.com/matttproud/golang_protobuf_extensions v1.0.1 // indirect
	github.com/minio/md5-simd v1.1.0 // indirect
	github.com/minio/minio-go/v7 v7.0.26 // indirect
	github.com/minio/sha256-simd v0.1.1 // indirect
	github.com/mitchellh/go-homedir v1.1.0 // indirect
	github.com/modern-go/concurrent v0.0.0-20180306012644-bacd9c7ef1dd // indirect
	github.com/modern-go/reflect2 v1.0.2 // indirect
	github.com/montanaflynn/stats v0.0.0-20171201202039-1bf9dbcd8cbe // indirect
	github.com/newrelic/go-agent/v3 v3.15.2 // indirect
	github.com/pborman/uuid v1.2.1 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/prometheus/client_golang v1.13.1 // indirect
	github.com/prometheus/client_model v0.2.0 // indirect
	github.com/prometheus/common v0.37.0 // indirect
	github.com/prometheus/procfs v0.8.0 // indirect
	github.com/rivo/uniseg v0.2.0 // indirect
	github.com/robfig/cron v1.2.0 // indirect
	github.com/rs/xid v1.2.1 // indirect
	github.com/sijms/go-ora/v2 v2.4.27 // indirect
	github.com/sirupsen/logrus v1.8.1 // indirect
	github.com/stretchr/objx v0.5.0 // indirect
	github.com/stretchr/testify v1.8.1 // indirect
	github.com/swaggo/files v0.0.0-20210815190702-a29dd2bc99b2 // indirect
	github.com/swaggo/swag v1.8.1 // indirect
	github.com/valyala/bytebufferpool v1.0.0 // indirect
	github.com/valyala/fasthttp v1.40.0 // indirect
	github.com/valyala/tcplisten v1.0.0 // indirect
	github.com/xdg-go/pbkdf2 v1.0.0 // indirect
	github.com/xdg-go/scram v1.1.1 // indirect
	github.com/xdg-go/stringprep v1.0.3 // indirect
	github.com/youmark/pkcs8 v0.0.0-20181117223130-1be2e3e5546d // indirect
	go.etcd.io/etcd/api/v3 v3.5.4 // indirect
	go.etcd.io/etcd/client/pkg/v3 v3.5.4 // indirect
	go.etcd.io/etcd/client/v3 v3.5.4 // indirect
	go.temporal.io/api v1.16.0 // indirect
	go.uber.org/atomic v1.9.0 // indirect
	go.uber.org/multierr v1.6.0 // indirect
	go.uber.org/zap v1.17.0 // indirect
	golang.org/x/crypto v0.0.0-20220622213112-05595931fe9d // indirect
	golang.org/x/net v0.5.0 // indirect
	golang.org/x/sync v0.1.0 // indirect
	golang.org/x/time v0.1.0 // indirect
	golang.org/x/tools v0.1.12 // indirect
	google.golang.org/genproto v0.0.0-20230127162408-596548ed4efa // indirect
	google.golang.org/grpc v1.52.3 // indirect
	google.golang.org/protobuf v1.28.1 // indirect
)

require (
	github.com/arsmn/fiber-swagger/v2 v2.31.1
	github.com/fsnotify/fsnotify v1.5.4 // indirect
	github.com/goccy/go-json v0.9.11
	github.com/gofiber/adaptor/v2 v2.1.29
	github.com/gofiber/fiber/v2 v2.39.0
	github.com/golang/mock v1.6.0
	github.com/hashicorp/hcl v1.0.0 // indirect
	github.com/heptiolabs/healthcheck v0.0.0-20211123025425-613501dd5deb
	github.com/magiconair/properties v1.8.6 // indirect
	github.com/mitchellh/mapstructure v1.5.0
	github.com/pelletier/go-toml v1.9.5 // indirect
	github.com/pelletier/go-toml/v2 v2.0.5 // indirect
	github.com/spf13/afero v1.8.2 // indirect
	github.com/spf13/cast v1.5.0 // indirect
	github.com/spf13/jwalterweatherman v1.1.0 // indirect
	github.com/spf13/pflag v1.0.5 // indirect
	github.com/subosito/gotenv v1.4.1 // indirect
	golang.org/x/sys v0.4.0 // indirect
	golang.org/x/text v0.6.0 // indirect
	gopkg.in/ini.v1 v1.67.0 // indirect
	gopkg.in/yaml.v2 v2.4.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)

replace (
	gitlab.com/true-itsd/iservicemax/omni/api/ms/cms => gitlab.com/true-itsd/iservicemax/omni/api/ms/cms.git v0.0.1-dev-2
	gitlab.com/true-itsd/iservicemax/omni/api/ms/com-platform => gitlab.com/true-itsd/iservicemax/omni/api/ms/com-platform.git v0.0.2-dev-10
	gitlab.com/true-itsd/iservicemax/omni/api/ms/doc-system => gitlab.com/true-itsd/iservicemax/omni/api/ms/doc-system.git v0.0.1-dev-36
	gitlab.com/true-itsd/iservicemax/omni/api/ms/gcs => gitlab.com/true-itsd/iservicemax/omni/api/ms/gcs.git v0.0.3-dev-14
	gitlab.com/true-itsd/iservicemax/omni/api/ms/hl-otp => gitlab.com/true-itsd/iservicemax/omni/api/ms/hl-otp.git v0.0.1-dev-11
	gitlab.com/true-itsd/iservicemax/omni/api/ms/intx => gitlab.com/true-itsd/iservicemax/omni/api/ms/intx.git v0.0.7-dev-16
	gitlab.com/true-itsd/iservicemax/omni/api/ms/inventory => gitlab.com/true-itsd/iservicemax/omni/api/ms/inventory.git v0.0.1-dev-30
	gitlab.com/true-itsd/iservicemax/omni/api/ms/omx => gitlab.com/true-itsd/iservicemax/omni/api/ms/omx.git v0.0.1-dev-18
	gitlab.com/true-itsd/iservicemax/omni/api/ms/partner => gitlab.com/true-itsd/iservicemax/omni/api/ms/partner.git v0.0.1-dev-2
	gitlab.com/true-itsd/iservicemax/omni/api/ms/payment => gitlab.com/true-itsd/iservicemax/omni/api/ms/payment.git v0.0.1-dev-2
	gitlab.com/true-itsd/iservicemax/omni/api/ms/pim => gitlab.com/true-itsd/iservicemax/omni/api/ms/pim.git v0.0.1-dev-4
	gitlab.com/true-itsd/iservicemax/omni/api/ms/privilege => gitlab.com/true-itsd/iservicemax/omni/api/ms/privilege.git v0.0.1-dev-3
	gitlab.com/true-itsd/iservicemax/omni/api/ms/psa => gitlab.com/true-itsd/iservicemax/omni/api/ms/psa.git v0.0.1-dev-14
	gitlab.com/true-itsd/iservicemax/omni/api/ms/sale-profile => gitlab.com/true-itsd/iservicemax/omni/api/ms/sale-profile.git v0.0.1-dev-6
	gitlab.com/true-itsd/iservicemax/omni/pkg/common-utils-backend => gitlab.com/true-itsd/iservicemax/omni/pkg/common-utils-backend.git v1.0.1
)
