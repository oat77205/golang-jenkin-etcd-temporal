run-wiremock:
	cp ./configs/mock.yml ./configs/local.yml && go test -v -failfast -cover -v ./routes/api -coverprofile=tests/cover.out | tee tests/test-results.out

run-wiremock-win10:
	copy ./configs/mock.yml ./configs/local.yml && go test -v -failfast -cover -v ./routes/api -coverprofile=tests/cover.out | tee tests/test-results.out