package configs

import "time"

type CreateEndpointRequest struct {
	Endpoint string `json:"endpoint" validate:"required"`
	Data     string `json:"data" validate:"required"`
}

type CreateEndpointResponse struct {
	Data string `json:"data"`
}

type EndpointDB struct {
	Endpoint string `json:"endpoint" bson:"endpoint"`
	Data     string `json:"data" bson:"data"`
}
type Endpoint struct {
	Endpoint           string      `json:"endpoint"`
	Url                string      `json:"url"`
	Username           string      `json:"username"`
	Password           string      `json:"password"`
	Timeout            int         `json:"timeout"`
	ParamList          []ParamList `json:"paramList"`
	InsecureSkipVerify bool        `json:"insecureSkipVerify"`
}

type ParamList struct {
	Name  string `json:"name"`
	Value string `json:"value"`
}

type GetEndpointRequest struct {
	Endpoint string `json:"endpoint" validate:"required"`
}
type GetEndpointArrayRequest struct {
	Endpoint []string `json:"endpoint" validate:"required"`
}

type ConsentResult struct {
	OrderNo  string      `json:"orderNo"`
	TranId   string      `json:"tranId"`
	Feature  string      `json:"feature"`
	Url      string      `json:"url"`
	Response interface{} `json:"response"`
	Time     time.Time   `json:"time"`
}

type LoadEndpointRequest struct {
	Endpoints          string
	InsecureSkipVerify bool
	Path               []string
}

type GetConfigRequest struct {
	RuleName     string `json:"ruleName" validate:"required"`
	ActivityName string `json:"activityName,omitempty"`
	RuleId       int64  `json:"ruleId,omitempty"`
	Type         string `json:"type,omitempty"`
	Value        string `json:"value,omitempty"`
}

type CommonConfigStruct struct {
	RuleName      string      `json:"ruleName" bson:"ruleName"`
	ActionList    []string    `json:"actionList,omitempty" bson:"actionList,omitempty"`
	RuleValueList []RuleValue `json:"ruleValueList,omitempty" bson:"ruleValueList,omitempty"`
}

type RuleValue struct {
	RuleId       int32      `json:"ruleId,omitempty" bson:"ruleId,omitempty"`
	RuleItemList []RuleItem `json:"ruleItem,omitempty" bson:"ruleItem,omitempty"`
}

type RuleItem struct {
	Type      string   `json:"type,omitempty" bson:"type,omitempty"`
	ValueList []string `json:"valueList,omitempty" bson:"valueList,omitempty"`
	Oper      string   `json:"oper,omitempty" bson:"oper,omitempty"`
}
