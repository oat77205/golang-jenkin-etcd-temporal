package configs

import (
	"context"
	"time"

	"gitlab.com/true-itsd/iservicemax/omni/pkg/common-utils-backend/databases"
	"gitlab.com/true-itsd/iservicemax/omni/pkg/common-utils-backend/services/logger"
	"go.mongodb.org/mongo-driver/bson"
)

var collectionName = "sale_config"

const redisTimeout = time.Duration(6) * time.Hour

func QueryCommonConfig(l *logger.LogModel, params GetConfigRequest, mogoUrl, dbName, collectionRequest string) ([]CommonConfigStruct, error) {
	if collectionRequest != "" {
		collectionName = collectionRequest
	}
	var commonStructs []CommonConfigStruct
	redisKey := "config_" + params.RuleName
	if params.Type != "" {
		redisKey = redisKey + "_" + params.Type
	}
	if params.Value != "" {
		redisKey = redisKey + "_" + params.Value
	}
	ctx, cancle := context.WithTimeout(context.Background(), 15*time.Minute)
	defer cancle()

	database, err := databases.GetMongoDB(mogoUrl, dbName, nil)
	if err != nil {
		return nil, err
	}
	query := []interface{}{}
	if params.RuleName != "" {
		query = append(query, bson.M{"ruleName": params.RuleName})
	}
	if params.ActivityName != "" {
		query = append(query, bson.M{"activityName": params.RuleName})
	}
	if params.RuleId == 0 {
		params.RuleId = 1
	}
	query = append(query, bson.M{"ruleValueList.ruleId": params.RuleId})
	if params.Type != "" {
		query = append(query, bson.M{"ruleValueList.ruleItem.type": params.Type})
	}
	if params.Value != "" {
		query = append(query, bson.M{"ruleValueList.ruleItem.valueList": params.Value})
	}
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	cursor, err := database.Collection(collectionName).Find(ctx, bson.M{"$and": query})
	if err != nil {
		return nil, err
	}
	defer cursor.Close(ctx)
	for cursor.Next(ctx) {
		var commonStruct CommonConfigStruct
		cursor.Decode(&commonStruct)
		commonStructs = append(commonStructs, commonStruct)
	}
	if err := cursor.Err(); err != nil {
		return nil, err
	}

	return commonStructs, nil
}
