package configs

import (
	"flag"
	"fmt"
	"os"

	"github.com/spf13/viper"
	"gitlab.com/true-itsd/iservicemax/omni/pkg/common-utils-backend/utils"
)

var (
	Conf Config
)

type Config struct {
	Env           string
	Log           log
	Endpoint      EndpointConfig
	Database      utils.SystemDatabase
	StockShopCode []string
}

type log struct {
	Suffix      string
	Product     string
	ServiceType serviceType
}

type serviceType struct {
	SaveOrder                    string
	UpdateOrder                  string
	SearchOrderList              string
	GetOrderDetails              string
	GetConfigOrderTrackingStatus string
	GoodsReceive                 string
	ValidateTracking             string
	CustomerPickup               string
	SendOTP                      string
	ValidateOTP                  string
	UpdatePartialOrder           string
	GetReceipt                   string
	GenerateDocument             string
	PreverifyExisting            string
	ReservePrivilege             string
	ValidateSerialNumber         string
	SubmitOrder                  string
	ValidateExistingCampaign     string
}

type EndpointConfig struct {
	Cms           utils.EtcdApiConfig
	Inventory     utils.EtcdApiConfig
	Partner       utils.EtcdApiConfig
	HLOTP         utils.EtcdApiConfig
	Platform      utils.EtcdApiConfig
	ComplatForm   utils.EtcdApiConfig
	Whatsup       utils.EtcdApiConfig
	HLDOC         utils.EtcdApiConfig
	NodeJSUtility utils.EtcdApiConfig
	Psa           utils.EtcdApiConfig
	TsmSale       utils.EtcdApiConfig
	TsmPayment    utils.EtcdApiConfig
	Report        utils.EtcdApiConfig
	GCS           utils.EtcdApiConfig
	SMU           utils.EtcdApiConfig
	Intx          utils.EtcdApiConfig
	Omx           utils.EtcdApiConfig
	Privilege     utils.EtcdApiConfig
}

func Getpwd() string {
	pwd, err := os.Getwd()
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	return pwd
}

func Viper() {
	pwd := Getpwd()
	a := os.Args
	conf := &Config{}
	path := pwd
	envInput := GetEnv()
	if envInput != "local" {
		path += "/configs/"
	} else if (len(a) > 1 && os.Args[1] != "-test.run") || flag.Lookup("test.v") != nil {
		path += "/../../configs/"
	} else {
		path += "/configs/"
	}

	fmt.Println("path = ", path)
	viper, err := ReadViper(path, conf)

	if err != nil {
		fmt.Println(err)
	}

	if viper != nil {
		var convert *Config = viper.(*Config)
		Conf = *convert
	}

}

func FetchEtcd() error {
	Viper()
	ReadConfigDB()
	return nil
}

func GetEnv() string {
	a := os.Args
	env := "local"
	if len(a) > 1 && os.Args[1] != "-test.run" && flag.Lookup("test.v") == nil {
		env = os.Args[1]
	}
	return env
}

func ReadViper(path string, model interface{}) (interface{}, error) {
	envInput := GetEnv()
	viper.SetConfigName(envInput)
	viper.AddConfigPath(path)

	if err := viper.ReadInConfig(); err != nil {
		return nil, err
	}

	err := viper.Unmarshal(model)
	if err != nil {
		return nil, err
	}
	return model, nil
}

func ReadConfigDB() {
	Conf.StockShopCode = GetConfigDB("StockShopCode", "", "", "", "common.hlOrderApi")

	Conf.Endpoint.SMU = GetEndpoint("hl-order-api-smu", "saveOrderOnline", Conf.Database)

	Conf.Endpoint.ComplatForm = GetEndpoint("hl-order-api-complat", "smsgTemplate", Conf.Database)
	confComPlatform := GetEndpoint("hl-order-api-complat", "smsgTemplate", Conf.Database)
	Conf.Endpoint.ComplatForm.Endpoints["smsgTemplate"] = confComPlatform.Endpoints["smsgTemplate"]

	Conf.Endpoint.Inventory = GetEndpoint("hl-order-api-inventory", "saveOmniTrackingEndpoint", Conf.Database)
	confGetOmniTracking := GetEndpoint("hl-order-api-inventory", "getTrackingDetailPath", Conf.Database)
	confGetPimInfo := GetEndpoint("hl-order-api-inventory", "getPimInfo", Conf.Database)
	confGetConfigOrderTrackingStatus := GetEndpoint("hl-order-api-inventory", "getConfigOrderTrackingStatus", Conf.Database)
	confGShopSearch := GetEndpoint("hl-order-api-inventory", "gShopSearch", Conf.Database)
	confGetOmniTrackingHistory := GetEndpoint("hl-order-api-inventory", "getTrackingHistoryPath", Conf.Database)
	confSaveOmniTrackingHistory := GetEndpoint("hl-order-api-inventory", "saveTrackingHistoryPath", Conf.Database)

	Conf.Endpoint.Inventory.Endpoints["getTrackingDetailPath"] = confGetOmniTracking.Endpoints["getTrackingDetailPath"]
	Conf.Endpoint.Inventory.Endpoints["getPimInfo"] = confGetPimInfo.Endpoints["getPimInfo"]
	Conf.Endpoint.Inventory.Endpoints["getConfigOrderTrackingStatus"] = confGetConfigOrderTrackingStatus.Endpoints["getConfigOrderTrackingStatus"]
	Conf.Endpoint.Inventory.Endpoints["gShopSearch"] = confGShopSearch.Endpoints["gShopSearch"]
	Conf.Endpoint.Inventory.Endpoints["getTrackingHistory"] = confGetOmniTrackingHistory.Endpoints["getTrackingHistoryPath"]
	Conf.Endpoint.Inventory.Endpoints["saveOmniTrackingHistory"] = confSaveOmniTrackingHistory.Endpoints["saveTrackingHistoryPath"]

	Conf.Endpoint.Whatsup = GetEndpoint("hl-order-api-whatsup", "whatsupReceiver", Conf.Database)
	confWhatsup := GetEndpoint("hl-order-api-whatsup", "whatsupReceiver", Conf.Database)
	Conf.Endpoint.Whatsup.Endpoints["whatsupReceiver"] = confWhatsup.Endpoints["whatsupReceiver"]

	Conf.Endpoint.Partner = GetEndpoint("hl-order-api-partner", "tokenEndpoint", Conf.Database)
	Conf.Endpoint.Platform = GetEndpoint("hl-order-api-platform", "updateOrderStatusEndpoint", Conf.Database)
	Conf.Endpoint.Cms = GetEndpoint("hl-order-api-cms", "getConfigOrderTrackingStatus", Conf.Database)

	Conf.Endpoint.HLOTP = GetEndpoint("hl-otp", "", Conf.Database)
	confRequestOTP := GetEndpoint("hl-otp", "requestOTP", Conf.Database)
	confValidateOTP := GetEndpoint("hl-otp", "validateOTP", Conf.Database)

	Conf.Endpoint.HLOTP.Endpoints["requestOTP"] = confRequestOTP.Endpoints["requestOTP"]
	Conf.Endpoint.HLOTP.Endpoints["validateOTP"] = confValidateOTP.Endpoints["validateOTP"]

	Conf.Endpoint.HLDOC = GetEndpoint("hl-doc", "", Conf.Database)
	confUploadType := GetEndpoint("hl-doc", "uploadType", Conf.Database)
	confUploadDoc := GetEndpoint("hl-doc", "uploadDoc", Conf.Database)
	confGetFileInfo := GetEndpoint("hl-doc", "getFileInfo", Conf.Database)
	confDocSystemPath := GetEndpoint("hl-doc", "docSystemPath", Conf.Database)

	Conf.Endpoint.HLDOC.Endpoints["uploadType"] = confUploadType.Endpoints["uploadType"]
	Conf.Endpoint.HLDOC.Endpoints["uploadDoc"] = confUploadDoc.Endpoints["uploadDoc"]
	Conf.Endpoint.HLDOC.Endpoints["getFileInfo"] = confGetFileInfo.Endpoints["getFileInfo"]
	Conf.Endpoint.HLDOC.Endpoints["docSystemPath"] = confDocSystemPath.Endpoints["docSystemPath"]

	Conf.Endpoint.Psa = GetEndpoint("hl-order-api-psa", "", Conf.Database)
	Conf.Endpoint.Psa.Endpoints["getDeviceServiceInfo"] = GetEndpoint("hl-order-api-psa", "getDeviceServiceInfo", Conf.Database).Endpoints["getDeviceServiceInfo"]
	Conf.Endpoint.Psa.Endpoints["apiKey"] = GetEndpoint("hl-order-api-psa", "apiKey", Conf.Database).Endpoints["apiKey"]
	Conf.Endpoint.Psa.Endpoints["updateWarrantyDevice"] = GetEndpoint("hl-order-api-psa", "updateWarrantyDevice", Conf.Database).Endpoints["updateWarrantyDevice"]

	Conf.Endpoint.TsmSale = GetEndpoint("hl-order-api-tsmsale", "", Conf.Database)
	Conf.Endpoint.TsmSale.Endpoints["submitOrderOnline"] = GetEndpoint("hl-order-api-tsmsale", "submitOrderOnline", Conf.Database).Endpoints["submitOrderOnline"]

	Conf.Endpoint.TsmPayment = GetEndpoint("hl-order-api-tsmpayment", "", Conf.Database)
	Conf.Endpoint.TsmPayment.Endpoints["addPaymentMethod"] = GetEndpoint("hl-order-api-tsmpayment", "addPaymentMethod", Conf.Database).Endpoints["addPaymentMethod"]
	Conf.Endpoint.TsmPayment.Endpoints["getReceipt"] = GetEndpoint("hl-order-api-tsmpayment", "getReceipt", Conf.Database).Endpoints["getReceipt"]

	Conf.Endpoint.Report = GetEndpoint("hl-order-api-report", "", Conf.Database)
	Conf.Endpoint.Report.Endpoints["app4in1"] = GetEndpoint("hl-order-api-report", "app4in1", Conf.Database).Endpoints["app4in1"]
	Conf.Endpoint.Report.Endpoints["claim"] = GetEndpoint("hl-order-api-report", "claim", Conf.Database).Endpoints["claim"]
	Conf.Endpoint.Report.Authentication.ApiKey = GetEndpoint("hl-order-api-report", "apiKey", Conf.Database).Endpoints["apiKey"]

	Conf.Endpoint.GCS = GetEndpoint("hl-order-api-gcs", "", Conf.Database)
	Conf.Endpoint.GCS.Endpoints["validatePreverifyExisting"] = GetEndpoint("hl-order-api-gcs", "validatePreverifyExisting", Conf.Database).Endpoints["validatePreverifyExisting"]
	Conf.Endpoint.GCS.Endpoints["validateExistingCampaign"] = GetEndpoint("hl-order-api-gcs", "validateExistingCampaign", Conf.Database).Endpoints["validateExistingCampaign"]

	Conf.Endpoint.Intx = GetEndpoint("hl-order-api-intx", "", Conf.Database)
	Conf.Endpoint.Intx.Endpoints["getPromotionListByBusinessLine"] = GetEndpoint("hl-order-api-intx", "getPromotionListByBusinessLine", Conf.Database).Endpoints["getPromotionListByBusinessLine"]
	Conf.Endpoint.Intx.Endpoints["getOfferDetailList"] = GetEndpoint("hl-order-api-intx", "getOfferDetailList", Conf.Database).Endpoints["getOfferDetailList"]

	Conf.Endpoint.Omx = GetEndpoint("hl-order-api-omx", "", Conf.Database)
	Conf.Endpoint.Omx.Endpoints["submitOMX"] = GetEndpoint("hl-order-api-omx", "submitOMX", Conf.Database).Endpoints["submitOMX"]

	Conf.Endpoint.Privilege = GetEndpoint("hl-order-api-privilege", "", Conf.Database)
	Conf.Endpoint.Privilege.Endpoints["getCampaignService"] = GetEndpoint("hl-order-api-privilege", "getCampaignService", Conf.Database).Endpoints["getCampaignService"]

	Conf.Endpoint.NodeJSUtility = GetEndpoint("hl-nodejs-utility", "", Conf.Database)
	confCustomerPickupForm := GetEndpoint("hl-nodejs-utility", "customerPickupForm", Conf.Database)
	Conf.Endpoint.NodeJSUtility.Endpoints["customerPickupForm"] = confCustomerPickupForm.Endpoints["customerPickupForm"]
}

func GetConfigDB(types, mogoUri, dbName, redis, ruleName string) []string {
	reqDB := GetConfigRequest{
		RuleName: ruleName, //common.hlOrderApi
	}

	if mogoUri == "" {
		mogoUri = Conf.Database.MongoDB.Uri
	}
	if dbName == "" {
		dbName = Conf.Database.MongoDB.DbName
	}

	dataResponse, err := QueryCommonConfig(nil, reqDB, mogoUri, dbName, "hl_common_config")
	if len(dataResponse) > 0 && err == nil {
		for _, d := range dataResponse {
			for _, rvl := range d.RuleValueList {
				for _, ril := range rvl.RuleItemList {
					if ril.Type == types {
						return ril.ValueList //StockShopCode
					}
				}
			}
		}
	}
	return []string{""}
}

func GetEndpoint(endpointConfig string, endpointData string, confDatabase utils.SystemDatabase) (response utils.EtcdApiConfig) {
	var err error
	endpointService := NewServiceEndpoint(confDatabase)
	getInventory := []string{endpointData}
	req := LoadEndpointRequest{
		Endpoints:          endpointConfig,
		InsecureSkipVerify: true,
		Path:               getInventory,
	}
	response, err = endpointService.LoadEndpointEtcdService(req, "hl_endpoint")
	if err != nil {
		panic(err)
	}

	return response
}
