package models

type RequestUpdateOrderState struct {
	CorrelationID string `json:"correlationId" validate:"required"`
	FlowID        string `json:"flowId" validate:"required"`
	OrderInfo     struct {
		OrderNumber    string `json:"orderNumber" validate:"required,alphanum"`
		OrderStateCode string `json:"orderStateCode" validate:"required,oneof=SHIPPED PACKED DELIVERY DELIVERY_FAILED CANCELLED_ORDER"`
		OrderItems     []struct {
			Sequence    int    `json:"sequence"`
			ProductCode string `json:"productCode"`
			TrackingID  string `json:"trackingId" validate:"omitempty,alphanum"`
			Serial      string `json:"serial"`
		} `json:"orderItems"`
	} `json:"orderInfo" validate:"required"`
	AirwayBills []AirwayBills `json:"airwayBills"`
}

type RequestUpdateOrderStateDelivery struct {
	CorrelationID string `json:"correlationId" validate:"required"`
	FlowID        string `json:"flowId" validate:"required"`
	OrderInfo     struct {
		OrderNumber    string `json:"orderNumber" validate:"required,alphanum"`
		OrderStateCode string `json:"orderStateCode" validate:"required"`
		OrderItems     []struct {
			Sequence    int    `json:"sequence"`
			ProductCode string `json:"productCode"`
			TrackingID  string `json:"trackingId" validate:"omitempty,alphanum"`
			Serial      string `json:"serial"`
		} `json:"orderItems"`
	} `json:"orderInfo" validate:"required"`
	AirwayBills []AirwayBills `json:"airwayBills"  validate:"required,gt=0,dive"`
}

type RequestUpdateOrderStatePacked struct {
	CorrelationID string `json:"correlationId" validate:"required"`
	FlowID        string `json:"flowId" validate:"required"`
	OrderInfo     struct {
		OrderNumber    string `json:"orderNumber" validate:"required,alphanum"`
		OrderStateCode string `json:"orderStateCode" validate:"required"`
		OrderItems     []struct {
			Sequence    int    `json:"sequence" validate:"required"`
			ProductCode string `json:"productCode" validate:"required"`
			TrackingID  string `json:"trackingId" validate:"omitempty,alphanum"`
			Serial      string `json:"serial"`
		} `json:"orderItems"   validate:"required,gt=0,dive"`
	} `json:"orderInfo"  validate:"required"`
	AirwayBills []AirwayBills `json:"airwayBills"`
}

type RequestUpdateOrderStateShipped struct {
	CorrelationID string `json:"correlationId" validate:"required"`
	FlowID        string `json:"flowId" validate:"required"`
	OrderInfo     struct {
		OrderNumber    string `json:"orderNumber" validate:"required,alphanum"`
		OrderStateCode string `json:"orderStateCode" validate:"required"`
		OrderItems     []struct {
			Sequence    int    `json:"sequence" validate:"required"`
			ProductCode string `json:"productCode" validate:"required"`
			TrackingID  string `json:"trackingId" validate:"required,alphanum"`
			Serial      string `json:"serial"`
		} `json:"orderItems"   validate:"required,gt=0,dive"`
	} `json:"orderInfo" validate:"required"`
	AirwayBills []AirwayBills `json:"airwayBills"  validate:"required,gt=0,dive"`
}
type AirwayBills struct {
	TrackingID             string     `json:"trackingId" validate:"required,alphanum"`
	PackageNumber          string     `json:"packageNumber"`
	Courier                string     `json:"courier"`
	CustomerName           string     `json:"customerName"`
	CustomerAddress        string     `json:"customerAddress"`
	CustomerContract       string     `json:"customerContract"`
	PaymentMethod          string     `json:"paymentMethod"`
	MerchantName           string     `json:"merchantName"`
	MerchantAddress        string     `json:"merchantAddress"`
	MerchantContract       string     `json:"merchantContract"`
	ReferenceOrderNumber   string     `json:"referenceOrderNumber"`
	NumberOfItems          string     `json:"numberOfItems"`
	ObNumber               string     `json:"obNumber"`
	LotID                  string     `json:"lotId"`
	PurchaseDate           CustomTime `json:"purchaseDate,omitempty" validate:"omitempty"`
	PrintDate              CustomTime `json:"printDate,omitempty" validate:"omitempty"`
	TargetDeliverStartDate CustomTime `json:"targetDeliverStartDate,omitempty" validate:"omitempty"`
	TargetDeliverEndDate   CustomTime `json:"targetDeliverEndDate,omitempty" validate:"omitempty"`
}
