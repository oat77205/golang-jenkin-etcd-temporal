package models

type RequestGoodsReceive struct {
	CorrelationId string         `json:"correlationId" validate:"required"`
	GoodsReceive  []GoodsReceive `json:"goodsReceive" validate:"required,dive"`
}

type GoodsReceive struct {
	OrderNumber    string  `json:"orderNumber" validate:"required,alphanum"`
	TrackingId     string  `json:"trackingId" validate:"required"`
	PickupName     string  `json:"pickupName"`
	UserId         string  `json:"userId"`
	TrackingStatus string  `json:"trackingStatus" validate:"required,oneof=READY_TO_PICK_UP REJECTED_TRACKING"`
	TrackingReason string  `json:"trackingReason" validate:"required"`
	ImageList      []Image `json:"imageList" validate:"required"`
}

type Image struct {
	RefId     string `json:"refId" validate:"required"`
	ImagePath string `json:"imagePath"`
}

type ResponseGoodsReceive struct {
	TrackingId string `json:"trackingId"`
	Status     string `json:"status"`
	Message    string `json:"message,omitempty"`
}

type DataGoodsReceive struct {
	GoodsReceive []ResponseGoodsReceive `json:"goodsReceive"`
}
