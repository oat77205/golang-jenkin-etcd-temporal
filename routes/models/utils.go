package models

import (
	"fmt"
	"time"
)

type ValidCorrelationRequest struct {
	Channel       string
	CorrelationID string
	HCorID        string
	HChannel      string
}

type ValidCorrelationResponse struct {
	Code        string
	Description string
}
type CustomTime struct {
	*time.Time
}

func (m *CustomTime) UnmarshalJSON(data []byte) error {
	// Ignore null, like in the main JSON package.
	if string(data) == "null" || string(data) == `""` {
		return nil
	}
	// Fractional seconds are handled implicitly by Parse.
	tt, err := time.Parse(`"`+time.RFC3339+`"`, string(data))
	*m = CustomTime{&tt}
	return err
}

func (m *CustomTime) MarshalJSON() ([]byte, error) {
	t := fmt.Sprintf("\"%s\"", m.Format(time.RFC3339))
	return []byte(t), nil
}
