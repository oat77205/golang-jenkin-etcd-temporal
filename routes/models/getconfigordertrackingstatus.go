package models

type RequestGetConfigOrderTrackingStatus struct {
	CorrelationID string `json:"correlationId" validate:"required"`
	Channel       string `json:"channel" validate:"required"`
	CMSType       string `json:"cmsType"`
	CMSName       string `json:"cmsName"`
}
