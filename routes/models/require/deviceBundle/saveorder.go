package requiredevicebundle

type RequestSaveOrderDeviceBundle struct {
	CorrelationID string `json:"correlationId" validate:"required"`
	FlowID        string `json:"flowId" validate:"required"`
	UserId        string `json:"userId" validate:"required"`
	Channel       string `json:"channel" validate:"required"`
	OrderInfo     struct {
		OrderNumber    string `json:"orderNumber" validate:"required"`
		OrderStateCode string `json:"orderStateCode" validate:"required"`
		PaymentInfo    struct {
			TotalAmount         *float64            `json:"totalAmount" validate:"required"`
			CpuId               string              `json:"cpuId" validate:"required"`
			RepCompanyObjectId  string              `json:"repCompanyObjectId" validate:"required"`
			OperCompanyObjectId string              `json:"operCompanyObjectId" validate:"required"`
			CashierInfo         CashierInfo         `json:"cashierInfo" validate:"required"`
			PaymentMethodInfo   []PaymentMethodInfo `json:"paymentMethodInfo" validate:"required,gt=0,dive"`
		} `json:"paymentInfo"`
	} `json:"orderInfo"`
	CustomerInfo CustomerInfo `json:"customerInfo" validate:"required"`
}
type CashierInfo struct {
	CashierId      string `json:"cashierId" validate:"required"`
	CashierName    string `json:"CashierName" validate:"required"`
	CashierLanUser string `json:"CashierLanUser" validate:"required"`
}
type PaymentMethodInfo struct {
	PaymentMethod  string `json:"paymentMethod"  validate:"required"`
	CreditCardNo   string `json:"creditCardNo"  validate:"required_if=PaymentMethod CC,required_if=PaymentMethod DD"`
	CreditCardType string `json:"creditCardType" validate:"required_if=PaymentMethod CC"`
	ExpireDate     string `json:"expireDate"  validate:"required_if=PaymentMethod CC,required_if=PaymentMethod DD"`
	ApproveCode    string `json:"approveCode"  validate:"required"`
	BankNo         string `json:"bankNo"   validate:"required_if=PaymentMethod CC,required_if=PaymentMethod DD"`
	BankName       string `json:"bankName"  validate:"required_if=PaymentMethod CC,required_if=PaymentMethod DD"`
	BinNum         int    `json:"binNum"  validate:"required_if=PaymentMethod CC,required_if=PaymentMethod DD"`
	RepCompanyId   string `json:"repCompanyId"  validate:"required"`
}

type CustomerInfo struct {
	Title              string          `json:"title" validate:"required"`
	FirstName          string          `json:"firstName"  validate:"required"`
	LastName           string          `json:"lastName"  validate:"required"`
	CustomerType       string          `json:"customerType"  validate:"required"`
	Identification     string          `json:"identification"  validate:"required"`
	IdentificationType string          `json:"identificationType"  validate:"required"`
	Gender             string          `json:"gender"  validate:"required"`
	ContactNo          string          `json:"contactNo"  validate:"required"`
	CustomerAddress    CustomerAddress `json:"customerAddress" validate:"required"`
}

type CustomerAddress struct {
	HouseNo string `json:"houseNo"  validate:"required"`
	Tumbon  string `json:"tumbon"  validate:"required"`
	Amphur  string `json:"amphur"  validate:"required"`
	City    string `json:"city"  validate:"required"`
	Zip     string `json:"zip"  validate:"required"`
}
