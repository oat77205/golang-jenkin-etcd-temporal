package requiredevicebundle

import "time"

type RequestReserveDeviceBundle struct {
	CorrelationID string `json:"correlationId" validate:"required"`
	Channel       string `json:"Channel" validate:"required"`
	FlowID        string `json:"flowId" validate:"required"`
	SaleInfo      struct {
		DealerCode  string `json:"dealerCode" validate:"required"`
		OwnerDealer string `json:"ownerDealer"  validate:"required"`
		SaleCode    string `json:"saleCode" validate:"required"`
		SaleName    string `json:"saleName" validate:"required"`
	} `json:"saleInfo"  validate:"required"`
	SubscriberInfo SubscriberInfo `json:"subscriberInfo" validate:"required"`
	OrderInfo      struct {
		OrderNumber string     `json:"orderNumber" validate:"required"`
		OrderDate   *time.Time `json:"orderDate"  validate:"required"`
		PaymentInfo struct {
			TotalAmount            *float64                      `json:"totalAmount" validate:"required"`
			BillDiscountAmount     *float64                      `json:"billDiscountAmount"  validate:"required"`
			BillDiscountInfo       []BillDiscountInfoReserve     `json:"billDiscountInfo" validate:"required,gt=0,dive"`
			BillOtherPaymentAmount *float64                      `json:"billOtherPaymentAmount"`
			BillOtherPaymentInfo   []BillOtherPaymentInfoReserve `json:"billOtherPaymentInfo"`
		} `json:"paymentInfo" validate:"required"`
		OrderItems []OrderItemsReserve `json:"orderItems"  validate:"required,gt=0,dive"`
	} `json:"orderInfo"`
	CustomerInfo CustomerInfoReserve `json:"customerInfo"  validate:"required"`
}
type CustomerInfoReserve struct {
	FirstName          string          `json:"firstName"  validate:"required"`
	LastName           string          `json:"lastName"  validate:"required"`
	Identification     string          `json:"identification"  validate:"required"`
	IdentificationType string          `json:"identificationType"  validate:"required"`
	CustomerAddress    CustomerAddress `json:"customerAddress" validate:"required"`
}
type BillDiscountInfoReserve struct {
	DiscountCode   string   `json:"discountCode" validate:"required"`
	DiscountAmount *float64 `json:"discountAmount" validate:"required"`
	CouponSerial   string   `json:"couponSerial"`
}
type BillOtherPaymentInfoReserve struct {
	OtherPaymentCode   string   `json:"OtherPaymentCode" validate:"required"`
	OtherPaymentAmount *float64 `json:"otherPaymentAmount" validate:"required"`
	CouponSerial       string   `json:"couponSerial"`
}

type OrderItemsReserve struct {
	Sequence    int    `json:"sequence"  validate:"required"`
	ProductCode string `json:"productCode"  validate:"required"`
	ProductType string `json:"productType" validate:"required"`
	AmountInfo  struct {
		Price            *float64                `json:"price" validate:"required"`
		Qty              *float64                `json:"qty" validate:"required"`
		DiscountInfo     DiscountInfoReserve     `json:"discountInfo" validate:"required"`
		OtherPaymentInfo OtherPaymentInfoReserve `json:"otherPaymentInfo" validate:"required"`
	} `json:"amountInfo"`
	CampaignInfo CampaignInfo `json:"campaignInfo"`
}
type CampaignInfo struct {
	CampaignCode string `json:"campaignCode" validate:"required"`
}
type DiscountInfoReserve struct {
	TotalDiscountAmount *float64          `json:"totalDiscountAmount" validate:"required"`
	Discount            []DiscountReserve `json:"discount" validate:"required,gt=0,dive"`
}
type DiscountReserve struct {
	DiscountCode   string   `json:"discountCode" validate:"required"`
	DiscountAmount *float64 `json:"discountAmount" validate:"required"`
	CounponSerial  string   `json:"counponSerial"`
}
type OtherPaymentInfoReserve struct {
	TotalOtherPaymentAmount *float64               `json:"totalOtherPaymentAmount" validate:"required"`
	OtherPayments           []OtherPaymentsReserve `json:"otherPayments" validate:"required,gt=0,dive"`
}
type OtherPaymentsReserve struct {
	OtherPaymentCode   string   `json:"otherPaymentCode" validate:"required"`
	OtherPaymentAmount *float64 `json:"otherPaymentAmount" validate:"required"`
	CounponSerial      string   `json:"counponSerial"`
}
type SubscriberInfo struct {
	SubscriberNumber string            `json:"subscriberNumber" validate:"required"`
	CompanyCode      string            `json:"companyCode" validate:"required"`
	AdditionPackage  []AdditionPackage `json:"additionPackage" validate:"required,gt=0,dive"`
}

type AdditionPackage struct {
	OfferName   string `json:"offerName" validate:"required"`
	ServiceType string `json:"serviceType" validate:"required"`
}
