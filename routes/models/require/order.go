package require

type OrderItems struct {
	Sequence    int        `json:"sequence"  validate:"required"`
	ProductCode string     `json:"productCode"  validate:"required"`
	ProductType string     `json:"productType" validate:"required"`
	AmountInfo  AmountInfo `json:"amountInfo" validate:"required,gt=0,dive"`
}
type AmountInfo struct {
	Price *float64 `json:"price" validate:"required"`
	Qty   *float64 `json:"qty" validate:"required"`
}
type CustomerInfo struct {
	Title           string           `json:"title" validate:"required"`
	FirstName       string           `json:"firstName" validate:"required"`
	LastName        string           `json:"lastName" validate:"required"`
	CustomerType    string           `json:"customerType" validate:"required"`
	Identification  string           `json:"identification" validate:"required"`
	ContactNo       string           `json:"contactNo" validate:"required"`
	CustomerAddress *CustomerAddress `json:"customerAddress" validate:"required"`
}

type CustomerAddress struct {
	HouseNo string `json:"houseNo" validate:"required"`
	Tumbon  string `json:"tumbon" validate:"required"`
	Amphur  string `json:"amphur" validate:"required"`
	City    string `json:"city" validate:"required"`
	Zip     string `json:"zip" validate:"required"`
}
