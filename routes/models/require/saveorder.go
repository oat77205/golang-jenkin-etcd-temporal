package require

import "time"

type RequestRequireSaleDevicePickup struct {
	SaleInfo     RequireSaleInfo     `validate:"required"`
	OrderInfo    RequireOrderInfo    `validate:"required"`
	CustomerInfo RequireCustomerInfo `validate:"required"`
	ContactInfo  *RequireContactInfo `validate:"omitempty"`
}

type RequireSaleInfo struct {
	DealerCode  string `validate:"required"`
	OwnerDealer string `validate:"required"`
}

type RequireOrderInfo struct {
	OrderNumber    string              `validate:"required,alphanum"`
	OrderDate      *time.Time          `validate:"required"`
	OrderStateCode string              `validate:"required"`
	PaymentInfo    RequirePaymentInfo  `validate:"required"`
	OrderItems     []RequireOrderItems `validate:"required,dive"`
}

type RequirePaymentInfo struct {
	TotalAmount            *float64 `validate:"required"`
	BillDiscountAmount     *float64 `validate:"required"`
	BillOtherPaymentAmount *float64 `validate:"required"`
	PaymentAmount          *float64 `validate:"required"`
	PaymentMethod          string   `validate:"required,oneof='Credit Card' 'True Money Wallet' 'Promptpay' 'True Pay Next' 'True Pay Next Extra'"`
}

type RequireOrderItems struct {
	Sequence           int               `validate:"required"`
	ProductCode        string            `validate:"required"`
	ProductType        string            `validate:"required,oneof='P' 'S'"`
	InventoryType      string            `validate:"required,oneof='IMEI' 'SIM' 'SERVICE' 'OTHER'"`
	StockShopCode      string            `validate:"required"`
	OrderItemStateCode string            `validate:"required"`
	AmountInfo         RequireAmountInfo `validate:"required"`
}

type RequireAmountInfo struct {
	Price *float64 `validate:"required"`
	Qty   *float64 `validate:"required"`
}

type RequireCustomerInfo struct {
	Title           string                  `validate:"required"`
	FirstName       string                  `validate:"required"`
	LastName        string                  `validate:"required"`
	BirthDate       *time.Time              `validate:"omitempty"`
	CustomerAddress *RequireCustomerAddress `validate:"omitempty"`
}

type RequireCustomerAddress struct {
	HouseNo string `validate:"required"`
	Tumbon  string `validate:"required"`
	Amphur  string `validate:"required"`
	City    string `validate:"required"`
	Zip     string `validate:"required"`
}

type RequireContactInfo struct {
	Title     string `validate:"required"`
	FirstName string `validate:"required"`
	LastName  string `validate:"required"`
}

type RequireIsPickup struct {
	TargetDealerCode string `validate:"required"`
}
