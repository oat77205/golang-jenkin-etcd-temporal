package require

type RequireClaim struct {
	PricePlan     string `validate:"required"`
	CitizenBase64 string `validate:"required"`
	IdType        string `validate:"required"`
}
