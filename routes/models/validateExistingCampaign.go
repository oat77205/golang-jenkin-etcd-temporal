package models

type RequestValidateExistingCampaign struct {
	CorrelationID string `json:"correlationId" validate:"required"`
	Channel       string `json:"channel" validate:"required"`
	FlowID        string `json:"flowId" validate:"required"`
	SaleInfo      struct {
		DealerCode string `json:"dealerCode" validate:"required"`
	} `json:"saleInfo" validate:"required"`
	SubscriberInfo struct {
		SubscriberNumber string `json:"subscriberNumber" validate:"required"`
		CompanyCode      string `json:"companyCode" validate:"required"`
		ProductCode      string `json:"productCode"`
		ServiceCode      string `json:"serviceCode"`
	} `json:"subscriberInfo"`
	CampaignInfo struct {
		CampaignCode string              `json:"campaignCode" validate:"required"`
		CampaignType string              `json:"campaignType"`
		VerifyKey    []map[string]string `json:"verifyKey"`
	} `json:"campaignInfo" validate:"required"`
	CustomerInfo struct {
		Identification string `json:"identification" validate:"required"`
		CustomerType   string `json:"customerType" validate:"required"`
	} `json:"customerInfo" validate:"required"`
}
