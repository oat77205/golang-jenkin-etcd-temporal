package models

import "time"

type RequestSaveOrder struct {
	CorrelationID string `json:"correlationId" validate:"required"`
	FlowID        string `json:"flowId" validate:"required"`
	Channel       string `json:"channel"`
	UserId        string `json:"userId"`
	SaleInfo      struct {
		DealerCode       string `json:"dealerCode"`
		OwnerDealer      string `json:"ownerDealer"`
		OwnerSaleId      string `json:"ownerSaleId"`
		WebMethodChannel string `json:"webMethodChannel"`
		SalePlatform     string `json:"salePlatform"`
		TargetDealerCode string `json:"targetDealerCode"`
		SaleCode         string `json:"saleCode"`
		SaleName         string `json:"saleName"`
	} `json:"saleInfo"`
	OrderInfo struct {
		OrderNumber    string     `json:"orderNumber"`
		OrderDate      *time.Time `json:"orderDate"`
		OrderStateCode string     `json:"orderStateCode"`
		PaymentInfo    struct {
			TotalAmount            *float64            `json:"totalAmount"`
			CpuId                  string              `json:"cpuId"`
			RepCompanyObjectId     string              `json:"repCompanyObjectId"`
			OperCompanyObjectId    string              `json:"operCompanyObjectId"`
			CashierInfo            CashierInfo         `json:"cashierInfo"`
			PaymentMethodInfo      []PaymentMethodInfo `json:"paymentMethodInfo"`
			BillDiscountAmount     *float64            `json:"billDiscountAmount"`
			BillOtherPaymentAmount *float64            `json:"billOtherPaymentAmount"`
			PaymentAmount          *float64            `json:"paymentAmount"`
			PaymentMethod          string              `json:"paymentMethod"`
			CreditCardNo           string              `json:"creditCardNo"`
			OwnerName              string              `json:"ownerName"`
			ExpireDate             *time.Time          `json:"expireDate"`
			BankName               string              `json:"bankName"`
			BankNo                 string              `json:"bankNo"`
		} `json:"paymentInfo"`
		OrderItems []OrderItems `json:"orderItems" `
	} `json:"orderInfo"`
	CustomerInfo CustomerInfo  `json:"customerInfo"`
	ContactInfo  *CustomerInfo `json:"contactInfo"`
	ShippingInfo CustomerInfo  `json:"shippingInfo"`
	DocumentInfo DocumentInfo  `json:"documentInfo"`
}

type SubscriberInfoOrder struct {
	SubscriberNumber string `json:"subscriberNumber"`
	CompanyCode      string `json:"companyCode"`
	Priceplan        string `json:"priceplan"`
	PropositionName  string `json:"propositionName"`
}
type BillDiscountInfo struct {
	DiscountCode   string   `json:"discountCode"`
	DiscountAmount *float64 `json:"discountAmount"`
	CouponSerial   string   `json:"couponSerial"`
}
type BillOtherPaymentInfo struct {
	OtherPaymentCode   string   `json:"OtherPaymentCode"`
	OtherPaymentAmount *float64 `json:"otherPaymentAmount"`
	CouponSerial       string   `json:"couponSerial"`
}

type CashierInfo struct {
	CashierId      string `json:"cashierId"`
	CashierName    string `json:"CashierName"`
	CashierLanUser string `json:"CashierLanUser"`
}
type PaymentMethodInfo struct {
	PaymentMethod            string   `json:"paymentMethod"`
	PaymentMethodDescription string   `json:"paymentMethodDescription"`
	Amount                   *float64 `json:"amount"`
	CreditCardNo             string   `json:"creditCardNo"`
	CreditCardType           string   `json:"creditCardType"`
	OwnerName                string   `json:"ownerName"`
	ExpireDate               string   `json:"expireDate"`
	ApproveCode              string   `json:"approveCode"`
	BankNo                   string   `json:"bankNo"`
	BankName                 string   `json:"bankName"`
	BankObjectId             string   `json:"bankObjectId"`
	BinNum                   int      `json:"binNum"`
	RepCompanyId             string   `json:"repCompanyId"`
}
type OrderItems struct {
	Sequence    int    `json:"sequence"`
	ProductCode string `json:"productCode"`
	ProductInfo struct {
		Code     string `json:"code"`
		Name     string `json:"name"`
		Brand    string `json:"brand"`
		Model    string `json:"model"`
		Color    string `json:"color"`
		Capacity string `json:"capacity"`
	} `json:"productInfo"`
	ProductType        string `json:"productType"`
	PromotionSet       string `json:"promotionSet"`
	PromotionType      string `json:"promotionType"`
	Proposition        string `json:"proposition"`
	GroupID            string `json:"groupId"`
	InventoryType      string `json:"inventoryType"`
	TrackingId         string `json:"trackingId"`
	Serial             string `json:"serial"`
	StockShopCode      string `json:"stockShopCode"`
	OrderItemStateCode string `json:"orderItemStateCode"`
	AmountInfo         *struct {
		DepositAmt       *float64          `json:"depositAmt"`
		Price            *float64          `json:"price"`
		Qty              *float64          `json:"qty"`
		DiscountInfo     *DiscountInfo     `json:"discountInfo"`
		OtherPaymentInfo *OtherPaymentInfo `json:"otherPaymentInfo"`
	} `json:"amountInfo"`
	CampaignInfo *CampaignInfo `json:"campaignInfo"`
}

type CustomerInfo struct {
	Title                  string                  `json:"title"`
	FirstName              string                  `json:"firstName"`
	LastName               string                  `json:"lastName"`
	CustomerType           string                  `json:"customerType"`
	BirthDate              *time.Time              `json:"birthDate"`
	Identification         string                  `json:"identification"`
	IdentificationType     string                  `json:"identificationType"`
	IdentificationCardInfo *IdentificationCardInfo `json:"identificationCardInfo"`
	Gender                 string                  `json:"gender"`
	ContactNo              string                  `json:"contactNo"`
	ContactEmail           string                  `json:"contactEmail"`
	CustomerAddress        *CustomerAddress        `json:"customerAddress"`
	InstallationAddress    InstallationAddressInfo `json:"installationAddress"` //db
	BillAddress            BillAddress             `json:"billAddress"`         //db
	TaxAddress             TaxAddress              `json:"taxAddress"`          //db
	AlternateContactNo1    string                  `json:"alternateContactNo1"` //db
	AlternateContactNo2    string                  `json:"alternateContactNo2"` //db
	ViaSMS                 bool                    `json:"viaSMS"`              //db
	ViaEmail               bool                    `json:"viaEmail"`            //db
	IvrLang                string                  `json:"ivrLang"`             //db
	UssdLang               string                  `json:"ussdLang"`            //db
	SmsEmailLang           string                  `json:"smsEmailLang"`        //db
}

type CustomerAddress struct {
	HouseNo      string `json:"houseNo"`
	BuildingName string `json:"buildingName"`
	Soi          string `json:"soi"`
	Moo          string `json:"moo"`
	RoomNo       string `json:"roomNo"`
	StreetName   string `json:"streetName"`
	Tumbon       string `json:"tumbon"`
	Amphur       string `json:"amphur"`
	City         string `json:"city"`
	Zip          string `json:"zip"`
}

type DiscountInfo struct {
	TotalDiscountAmount *float64   `json:"totalDiscountAmount"`
	Discount            []Discount `json:"discount"`
}

type Discount struct {
	DiscountCode   string   `json:"discountCode"`
	DiscountAmount *float64 `json:"discountAmount"`
	CounponSerial  string   `json:"counponSerial"`
}

type OtherPaymentInfo struct {
	TotalOtherPaymentAmount *float64        `json:"totalOtherPaymentAmount"`
	OtherPayments           []OtherPayments `json:"otherPayments"`
}

type OtherPayments struct {
	OtherPaymentCode   string   `json:"otherPaymentCode"`
	OtherPaymentAmount *float64 `json:"otherPaymentAmount"`
	CounponSerial      string   `json:"counponSerial"`
}

type DocumentInfo struct {
	DocumentType string `json:"documentType"`
	DocumentName string `json:"documentName"`
	FileName     string `json:"fileName"`
	Url          string `json:"url"`
}

type InstallationAddressInfo struct {
	LivingStyle     string `json:"livingStyle"`
	LivingType      string `json:"livingType"`
	OtherLivingType string `json:"otherLivingType"`
	HouseNo         string `json:"houseNo"`
	BuildingName    string `json:"buildingName"`
	Soi             string `json:"soi"`
	Moo             string `json:"moo"`
	RoomNo          string `json:"roomNo"`
	StreetName      string `json:"streetName"`
	Tumbon          string `json:"tumbon"`
	Amphur          string `json:"amphur"`
	City            string `json:"city"`
	Zip             string `json:"zip"`
}

type BillAddress struct {
	HouseNo      string `json:"houseNo"`
	BuildingName string `json:"buildingName"`
	Soi          string `json:"soi"`
	Moo          string `json:"moo"`
	RoomNo       string `json:"roomNo"`
	StreetName   string `json:"streetName"`
	Tumbon       string `json:"tumbon"`
	Amphur       string `json:"amphur"`
	City         string `json:"city"`
	Zip          string `json:"zip"`
}

type TaxAddress struct {
	HouseNo      string `json:"houseNo"`
	BuildingName string `json:"buildingName"`
	Soi          string `json:"soi"`
	Moo          string `json:"moo"`
	RoomNo       string `json:"roomNo"`
	StreetName   string `json:"streetName"`
	Tumbon       string `json:"tumbon"`
	Amphur       string `json:"amphur"`
	City         string `json:"city"`
	Zip          string `json:"zip"`
}

type CampaignInfo struct {
	CampaignCode *string             `json:"campaignCode"`
	CampaignName *string             `json:"campaignName"`
	VerifyKey    []map[string]string `json:"verifyKey"`
}
