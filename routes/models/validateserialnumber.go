package models

type ValidateSerialNumberResquest struct {
	CorrelationID  string                `json:"correlationId" validate:"required"`
	Channel        string                `json:"channel" validate:"required"`
	FlowID         string                `json:"flowId" validate:"required"`
	UserID         string                `json:"userId" validate:"required"`
	SubscriberInfo SubscriberInfoValidSN `json:"subscriberInfo"`
	OrderInfo      OrderInfoValidSN      `json:"orderInfo"`
}

type SubscriberInfoValidSN struct {
	ServiceCode string `json:"serviceCode" validate:"required"`
}

type OrderInfoValidSN struct {
	OrderNumber string             `json:"orderNumber" validate:"required"`
	OrderItem   []OrderItemValidSN `json:"orderItem"`
}

type OrderItemValidSN struct {
	Sequence      int    `json:"sequence" validate:"required"`
	ProductCode   string `json:"productCode" validate:"required"`
	Serial        string `json:"serial" validate:"required"`
	InventoryType string `json:"inventoryType" validate:"required,oneof=SIM IMEI MATCODE"`
	SerialStatus  string `json:"serialStatus" validate:"required_if=InventoryType SIM"`
}

type ValidateSerialNumberResponse struct {
	OrderItem []OrderItemSN `json:"orderItem"`
}
type OrderItemSN struct {
	Sequence      int    `json:"sequence"`
	ProductCode   string `json:"productCode"`
	Serial        string `json:"serial"`
	InventoryType string `json:"inventoryType"`
	SerialStatus  string `json:"serialStatus"`
	FoundFlag     bool   `json:"foundFlag"`
	ErrorMessage  string `json:"errorMessage"`
}
