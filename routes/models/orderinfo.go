package models

type RequestSearchOrderList struct {
	CorrelationId string
	SearchValue   string `json:"searchValue"`
	ShopCode      string `json:"shopCode"`
	StockType     string `json:"stockType"`
	OrderStatus   string `json:"orderStatus"`
	PageNo        int    `json:"pageNo" validate:"required"`
	PageSize      int    `json:"pageSize" validate:"required"`
	UserID        string `json:"userId" validate:"required"`
	Channel       string `json:"channel" validate:"required"`
}

type RequestGetOrderDetails struct {
	CorrelationId string
	SearchValue   string `json:"searchValue" validate:"required"`
	ShopCode      string `json:"shopCode"`
	UserID        string `json:"userId" validate:"required"`
	Channel       string `json:"channel" validate:"required"`
}
