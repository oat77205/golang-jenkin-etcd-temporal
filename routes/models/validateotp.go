package models

type RequestValidateOTP struct {
	CorrelationID string `json:"correlationId" validate:"required"`
	OrderNumber   string `json:"orderNumber" validate:"required"`
	FunctionName  string `json:"functionName" validate:"required"`
	OtpCode       string `json:"otpCode" validate:"required"`
	RefID         string `json:"refId" validate:"required"`
}

type ValidateOTPData struct {
	Function string `json:"function"`
	Msisdn   string `json:"msisdn"`
	Language string `json:"language"`
	OTPCode  string `json:"otpCode"`
	RefID    string `json:"refId"`
}
