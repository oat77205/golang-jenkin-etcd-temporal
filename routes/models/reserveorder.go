package models

type RequestReserve struct {
	CorrelationID string `json:"correlationId" validate:"required"`
	Channel       string `json:"Channel" validate:"required"`
	FlowID        string `json:"flowId" validate:"required"`
	SaleInfo      struct {
		DealerCode  string `json:"dealerCode"`
		OwnerDealer string `json:"ownerDealer"`
		SaleCode    string `json:"saleCode"`
		SaleName    string `json:"saleName"`
	} `json:"saleInfo" validate:"required"`
	SubscriberInfo SubscriberInfoReserve `json:"subscriberInfo"`
	OrderInfo      struct {
		OrderNumber string     `json:"orderNumber"`
		OrderDate   CustomTime `json:"orderDate"`
		// OrderStateCode string     `json:"orderStateCode"`
		PaymentInfo struct {
			TotalAmount            *float64                       `json:"totalAmount"`
			BillDiscountAmount     *float64                       `json:"billDiscountAmount"`
			BillDiscountInfo       *[]BillDiscountInfoReserve     `json:"billDiscountInfo"`
			BillOtherPaymentAmount *float64                       `json:"billOtherPaymentAmount"`
			BillOtherPaymentInfo   *[]BillOtherPaymentInfoReserve `json:"billOtherPaymentInfo"`
			PaymentAmount          *float64                       `json:"paymentAmount"`
			PaymentMethod          string                         `json:"paymentMethod"`
			CreditCardNo           string                         `json:"creditCardNo"`
			OwnerName              string                         `json:"ownerName"`
			ExpireDate             CustomTime                     `json:"expireDate"`
			BankName               string                         `json:"bankName"`
			BankNo                 string                         `json:"bankNo"`
		} `json:"paymentInfo"`
		OrderItems []OrderItems `json:"orderItems" `
	} `json:"orderInfo"`
	CustomerInfo CustomerInfoReserve  `json:"customerInfo"`
	ContactInfo  *CustomerInfoReserve `json:"contactInfo"`
	ShippingInfo CustomerInfoReserve  `json:"shippingInfo"`
	DocumentInfo DocumentInfoReserve  `json:"documentInfo"`
}

type SubscriberInfoReserve struct {
	SubscriberNumber string             `json:"subscriberNumber"`
	CompanyCode      string             `json:"companyCode"`
	Priceplan        string             `json:"priceplan"`
	AdditionPackage  *[]AdditionPackage `json:"additionPackage"`
}

type AdditionPackage struct {
	OfferName   string `json:"offerName"`
	ServiceType string `json:"serviceType"`
}

type BillDiscountInfoReserve struct {
	DiscountCode   string   `json:"discountCode"`
	DiscountAmount *float64 `json:"discountAmount"`
	CouponSerial   string   `json:"couponSerial"`
}
type BillOtherPaymentInfoReserve struct {
	OtherPaymentCode   string   `json:"OtherPaymentCode"`
	OtherPaymentAmount *float64 `json:"otherPaymentAmount"`
	CouponSerial       string   `json:"couponSerial"`
}

type CustomerInfoReserve struct {
	Identification     string                  `json:"identification"`
	IdentificationType string                  `json:"identificationType"`
	Title              string                  `json:"title"`
	FirstName          string                  `json:"firstName"`
	LastName           string                  `json:"lastName"`
	CustomerType       string                  `json:"customerType"`
	BirthDate          CustomTime              `json:"birthDate"`
	Gender             string                  `json:"gender"`
	ContactNo          string                  `json:"contactNo"`
	ContactEmail       string                  `json:"contactEmail"`
	CustomerAddress    *CustomerAddressReserve `json:"customerAddress"`
	// InstallationAddress InstallationAddressInfoReserve `json:"installationAddress"` //db
	// BillAddress         BillAddressReserve             `json:"billAddress"`         //db
	// TaxAddress          TaxAddressReserve              `json:"taxAddress"`          //db
	// AlternateContactNo1 string                         `json:"alternateContactNo1"` //db
	// AlternateContactNo2 string                         `json:"alternateContactNo2"` //db
	// ViaSMS              bool                           `json:"viaSMS"`              //db
	// ViaEmail            bool                           `json:"viaEmail"`            //db
	// IvrLang             string                         `json:"ivrLang"`             //db
	// UssdLang            string                         `json:"ussdLang"`            //db
	// SmsEmailLang        string                         `json:"smsEmailLang"`        //db
}

type CustomerAddressReserve struct {
	HouseNo      string `json:"houseNo"`
	BuildingName string `json:"buildingName"`
	Soi          string `json:"soi"`
	Moo          string `json:"moo"`
	RoomNo       string `json:"roomNo"`
	StreetName   string `json:"streetName"`
	Tumbon       string `json:"tumbon"`
	Amphur       string `json:"amphur"`
	City         string `json:"city"`
	Zip          string `json:"zip"`
}

type DiscountInfoReserve struct {
	TotalDiscountAmount *float64   `json:"totalDiscountAmount"`
	Discount            []Discount `json:"discount"`
}

type DiscountReserve struct {
	DiscountCode   string   `json:"discountCode"`
	DiscountAmount *float64 `json:"discountAmount"`
	CounponSerial  string   `json:"counponSerial"`
}

type OtherPaymentInfoReserve struct {
	TotalOtherPaymentAmount *float64         `json:"totalOtherPaymentAmount"`
	OtherPayments           *[]OtherPayments `json:"otherPayments"`
}

type OtherPaymentsReserve struct {
	OtherPaymentCode   string   `json:"otherPaymentCode"`
	OtherPaymentAmount *float64 `json:"otherPaymentAmount"`
	CounponSerial      string   `json:"counponSerial"`
}

type DocumentInfoReserve struct {
	DocumentType string `json:"documentType"`
	DocumentName string `json:"documentName"`
	FileName     string `json:"fileName"`
	Url          string `json:"url"`
}

type InstallationAddressInfoReserve struct {
	LivingStyle     string `json:"livingStyle"`
	LivingType      string `json:"livingType"`
	OtherLivingType string `json:"otherLivingType"`
	HouseNo         string `json:"houseNo"`
	BuildingName    string `json:"buildingName"`
	Soi             string `json:"soi"`
	Moo             string `json:"moo"`
	RoomNo          string `json:"roomNo"`
	StreetName      string `json:"streetName"`
	Tumbon          string `json:"tumbon"`
	Amphur          string `json:"amphur"`
	City            string `json:"city"`
	Zip             string `json:"zip"`
}

type BillAddressReserve struct {
	HouseNo      string `json:"houseNo"`
	BuildingName string `json:"buildingName"`
	Soi          string `json:"soi"`
	Moo          string `json:"moo"`
	RoomNo       string `json:"roomNo"`
	StreetName   string `json:"streetName"`
	Tumbon       string `json:"tumbon"`
	Amphur       string `json:"amphur"`
	City         string `json:"city"`
	Zip          string `json:"zip"`
}

type TaxAddressReserve struct {
	HouseNo      string `json:"houseNo"`
	BuildingName string `json:"buildingName"`
	Soi          string `json:"soi"`
	Moo          string `json:"moo"`
	RoomNo       string `json:"roomNo"`
	StreetName   string `json:"streetName"`
	Tumbon       string `json:"tumbon"`
	Amphur       string `json:"amphur"`
	City         string `json:"city"`
	Zip          string `json:"zip"`
}
