package models

type GenarateDocmentRequest struct {
	CorrelationID string `json:"correlationId" validate:"required"`
	FlowID        string `json:"flowId" validate:"required"`
	OrderInfo     struct {
		OrderNumber string `json:"orderNumber" validate:"required"`
	} `json:"orderInfo"`
	GenerateDocumentInfo struct {
		DocumentType  string `json:"documentType" validate:"required,oneof=app_form TBC"`
		Watermark     string `json:"watermark"`
		MaxSizeMB     string `json:"maxSizeMB"`
		Date          string `json:"date"`
		FullName      string `json:"fullName"`
		TitleName     string `json:"titleName"`
		IdNumber      string `json:"idNumber"`
		DobFormatTh   string `json:"dobFormatTh"`
		Status        string `json:"status"`
		Nationality   string `json:"nationality"`
		Job           string `json:"job"`
		WorkYear      string `json:"workYear"`
		WorkMonth     string `json:"workMonth"`
		ContactNo     string `json:"contactNo"`
		Email         string `json:"email"`
		SameAddress   string `json:"sameAddress"`
		PdpaConsent   string `json:"pdpaConsent"`
		TmnConsent    string `json:"tmnConsent"`
		StaffNo       string `json:"staffNo"`
		DealerNo      string `json:"dealerNo"`
		DealerName    string `json:"dealerName"`
		Msisdn        string `json:"msisdn"`
		Signature     string `json:"signature"`
		CitizenBase64 string `json:"citizenBase64"`
		DocClaim      struct {
			FinalPrice         string `json:"finalPrice"`
			AdvancePayment     string `json:"advancePayment"`
			ExtraAdvanceAmount string `json:"extraAdvanceAmount"`
			ProductPrice       string `json:"productPrice"`
			PricePlan          string `json:"pricePlan"`
			ContractTerm       string `json:"contractTerm"`
			ModelDesc          string `json:"modelDesc"`
			IdType             string `json:"idType"`
		} `json:"docClaim"`
		Address struct {
			HouseNo      string `json:"houseNo"`
			BuildingName string `json:"buildingName"`
			Soi          string `json:"soi"`
			Moo          string `json:"moo"`
			RoomNo       string `json:"roomNo"`
			StreetName   string `json:"streetName"`
			Tumbon       string `json:"tumbon"`
			Amphur       string `json:"amphur"`
			City         string `json:"city"`
			Zip          string `json:"zip"`
		} `json:"address"`
		BillingAddress struct {
			HouseNo      string `json:"houseNo"`
			BuildingName string `json:"buildingName"`
			Soi          string `json:"soi"`
			Moo          string `json:"moo"`
			RoomNo       string `json:"roomNo"`
			StreetName   string `json:"streetName"`
			Tumbon       string `json:"tumbon"`
			Amphur       string `json:"amphur"`
			City         string `json:"city"`
			Zip          string `json:"zip"`
		} `json:"billingAddress"`
		TrueOnline struct {
			IsNew           *bool  `json:"isNew" validate:"required"`
			IsInternet      *bool  `json:"isInternet" validate:"required"`
			InternetDetail  string `json:"internetDetail"`
			IsHomePhone     *bool  `json:"isHomePhone" validate:"required"`
			HomePhoneDetail string `json:"homePhoneDetail"`
			ServiceDetail   string `json:"serviceDetail"`
			PayMonthAmount  string `json:"payMonthAmount"`
			InstallDate     string `json:"installDate"`
			IncomingPrice   string `json:"incomingPrice"`
			MonthPay        string `json:"monthPay"`
		} `json:"trueOnline"`
		TrueVision struct {
			IsNew             *bool  `json:"isNew" validate:"required"`
			MemberNo          string `json:"memberNo"`
			Packages          string `json:"package"`
			AdditionalPackage string `json:"additionalPackage"`
			AddPoint          string `json:"addPoint"`
			InstallDate       string `json:"installDate"`
			MonthPay          string `json:"monthPay"`
		} `json:"trueVision"`
		TrueMoveH struct {
			IsChange       *bool  `json:"isChange" validate:"required"`
			IsNew          *bool  `json:"isNew" validate:"required"`
			Msisdn         string `json:"msisdn"`
			ChangeMobileNo string `json:"changeMobileNo"`
			ChangeRefNo    string `json:"changeRefNo"`
			ChangeFrom     string `json:"changeFrom"`
			Packages       string `json:"package"`
			MonthPay       string `json:"monthPay"`
		} `json:"trueMoveH"`
		Customer struct {
			IdNumber     string `json:"idNumber"`
			TitleTh      string `json:"titleTh"`
			FirstNameTh  string `json:"firstNameTh"`
			MiddleNameTh string `json:"middleNameTh"`
			LastNameTh   string `json:"lastNameTh"`
			TitleEn      string `json:"titleEn"`
			FirstNameEn  string `json:"firstNameEn"`
			MiddleNameEn string `json:"middleNameEn"`
			LastNameEn   string `json:"lastNameEn"`
			BirthDate    string `json:"birthDate"`
			Moo          string `json:"moo"`
			Soi          string `json:"soi"`
			Trok         string `json:"trok"`
			Road         string `json:"road"`
			Subdistrict  string `json:"subdistrict"`
			District     string `json:"district"`
			Province     string `json:"province"`
			IssueDate    string `json:"issueDate"`
			ExpireDate   string `json:"expireDate"`
			Photo        string `json:"photo"`
		} `json:"customer"`
	} `json:"generateDocumentInfo"`
}
