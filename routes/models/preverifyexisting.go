package models

type PreverifyExistingRequest struct {
	CorrelationID      string                          `json:"correlationId" validate:"required"`
	Channel            string                          `json:"channel" validate:"required"`
	FlowID             string                          `json:"flowId" validate:"required"`
	SubscriberInfo     SubscriberInfoPreverifyExisting `json:"subscriberInfo"`
	InquiryProfileInfo InquiryProfileInfo              `json:"inquiryProfileInfo"`
}

type SubscriberInfoPreverifyExisting struct {
	SubscriberNumber string `json:"subscriberNumber" validate:"required"`
}

type InquiryProfileInfo struct {
	InquiryProfileFlag bool `json:"inquiryProfileFlag"`
}

type PreverifyExistingResponse struct {
	MobileNo       string       `json:"mobileNo"`
	Identification string       `json:"identification"`
	CompanyCode    string       `json:"companyCode"`
	CustomerType   string       `json:"customerType"`
	ProductAging   string       `json:"productAging"`
	CurrentOffer   CurrentOffer `json:"currentOffer"`
	ProductInfo    ProductInfo  `json:"productInfo"`
}

type CurrentOffer struct {
	OfferInfo            OfferInfo            `json:"offerInfo"`
	ServiceAgreementInfo ServiceAgreementInfo `json:"serviceAgreementInfo"`
}

type OfferInfo struct {
	Code         string `json:"code"`
	Name         string `json:"name"`
	Description  string `json:"description"`
	Types        string `json:"type"`
	ServiceLevel string `json:"serviceLevel"`
	RcRate       string `json:"rcRate"`
}

type ServiceAgreementInfo struct {
	Code         string `json:"code"`
	Name         string `json:"name"`
	Description  string `json:"description"`
	Types        string `json:"type"`
	ServiceLevel string `json:"serviceLevel"`
	RcRate       string `json:"rcRate"`
}

type ProductInfo struct {
	SubscriberInfo SubscriberInfo `json:"subscriberInfo"`
}

type SubscriberInfo struct {
	ConvergenceCode     string `json:"convergenceCode"`
	CreateDate          string `json:"createDate"`
	EffectiveDate       string `json:"effectiveDate"`
	InstallationType    string `json:"installationType"`
	MultiSIMLevel       string `json:"multiSIMLevel"`
	ResourceType        string `json:"resourceType"`
	ResourceValue       string `json:"resourceValue"`
	RelatedSubscriberId string `json:"relatedSubscriberId"`
	StartDate           string `json:"startDate"`
	SubscriberId        string `json:"subscriberId"`
	Status              string `json:"status"`
}
