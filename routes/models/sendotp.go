package models

type RequestSendOTP struct {
	CorrelationID string `json:"correlationId" validate:"required"`
	OrderNumber   string `json:"orderNumber" validate:"required"`
	FunctionName  string `json:"functionName" validate:"required"`
}

type ResponseSendOTPData struct {
	RefID string `json:"refId,omitempty"`
}

type RequestOTPData struct {
	Function string `json:"function"`
	Msisdn   string `json:"msisdn"`
	Language string `json:"language"`
}
