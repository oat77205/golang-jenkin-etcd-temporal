package models

type RequestCustomerPickup struct {
	CorrelationID         string             `json:"correlationId" validate:"required"`
	OrderNumber           string             `json:"orderNumber" validate:"required"`
	OrderStateCode        string             `json:"orderStateCode" validate:"required"`
	OrderReasonCode       string             `json:"orderReasonCode"`
	OrderReasonName       string             `json:"orderReasonName"`
	PickupDate            *CustomTime        `json:"pickupDate"`
	PickupRemark          string             `json:"pickupRemark"`
	PickupTransactionDate *CustomTime        `json:"pickupTransactionDate"`
	PickupLaterFlag       bool               `json:"pickupLaterFlag"`
	Signature             string             `json:"signature"`
	PickupList            []PickupList       `json:"pickupList" validate:"dive"`
	FileList              []FileList         `json:"fileList" validate:"dive"`
	PickupCustomerInfo    PickupCustomerInfo `json:"pickupCustomerInfo" validate:"dive"`
}

type PickupList struct {
	TrackingID string      `json:"trackingId" validate:"required"`
	ImageList  []ImageList `json:"imageList"`
}

type ImageList struct {
	RefID     string `json:"refId" validate:"required"`
	ImagePath string `json:"imagePath"`
}

type FileList struct {
	RefID     string `json:"refId" validate:"required"`
	ImagePath string `json:"imagePath"`
	Type      string `json:"type" validate:"required"`
}

type PickupCustomerInfo struct {
	Title                      string                 `json:"title" validate:"required"`
	FirstName                  string                 `json:"firstName" validate:"required"`
	LastName                   string                 `json:"lastName" validate:"required"`
	BirthDate                  *CustomTime            `json:"birthDate"`
	Identification             string                 `json:"identification" validate:"required"`
	IdentificationType         string                 `json:"identificationType"`
	DocumentIdentificationType string                 `json:"documentIdentificationType"`
	Gender                     string                 `json:"gender"`
	ContactNo                  string                 `json:"contactNo"`
	ContactEmail               string                 `json:"contactEmail"`
	VerifyInfo                 VerifyInfo             `json:"verifyInfo"`
	IdentificationCardInfo     IdentificationCardInfo `json:"identificationCardInfo"`
	CustomerAddress            CustomerAddress        `json:"customerAddress"`
}

type VerifyInfo struct {
	SecondAuthenFlag   bool   `json:"secondAuthenFlag"`
	SecondAuthenBy     string `json:"secondAuthenBy"`
	SecondAuthenByName string `json:"secondAuthenByName"`
	SecondAuthenReason string `json:"secondAuthenReason"`
}

type IdentificationCardInfo struct {
	Bp1Number   string `json:"bp1Number"`
	ChipID      string `json:"chipId"`
	IssueDate   string `json:"issueDate"`
	IssuerPlace string `json:"issuerPlace"`
	ExpireDate  string `json:"expireDate"`
	LaserID     string `json:"laserId"`
}
