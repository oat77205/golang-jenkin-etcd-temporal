package models

type UpdatePartialOrderRequest struct {
	CorrelationID string                 `json:"correlationId" validate:"required,max=50"`
	Channel       string                 `json:"channel" validate:"required"`
	FlowID        string                 `json:"flowId" validate:"required"`
	UserId        string                 `json:"userId" validate:"required"`
	OrderInfo     OrderInfoUpdatePartial `json:"orderInfo"`
}

type OrderInfoUpdatePartial struct {
	OrderNumber string      `json:"orderNumber" validate:"required"`
	OrderItem   []OrderItem `json:"orderItem" validate:"required,dive"`
}

type OrderItem struct {
	Sequence      int    `json:"sequence" validate:"required"`
	ProductCode   string `json:"productCode" validate:"required"`
	Serial        string `json:"serial" validate:"required"`
	InventoryType string `json:"inventoryType" validate:"required,oneof=SIM IMEI"`
	SerialStatus  string `json:"serialStatus"`
}

type UpdatePartialOrderResponse struct {
	OrderItem []OrderItemResponse `json:"orderItem"`
}
type OrderItemResponse struct {
	Sequence      int    `json:"sequence"`
	ProductCode   string `json:"productCode"`
	Serial        string `json:"serial"`
	InventoryType string `json:"inventoryType"`
	SerialStatus  string `json:"serialStatus"`
	FoundFlag     bool   `json:"foundFlag"`
	ErrorMessage  string `json:"errorMessage"`
}
