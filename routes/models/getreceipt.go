package models

type GetReceiptRequest struct {
	CorrelationID string              `json:"correlationId" validate:"required"`
	OrderInfo     OrderInfoGetReceipt `json:"orderInfo" validate:"required"`
}

type OrderInfoGetReceipt struct {
	OrderNumber string `json:"orderNumber" validate:"required"`
}
