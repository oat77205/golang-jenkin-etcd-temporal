package models

type SubmitOrderRequest struct {
	CorrelationID string `json:"correlationId" validate:"required"`
	Channel       string `json:"channel" validate:"required"`
	FlowID        string `json:"flowId" validate:"required"`
	UserID        string `json:"userId" validate:"required"`
	OrderInfo     struct {
		OrderNumber     string     `json:"orderNumber" validate:"required"`
		SubmitOrderDate CustomTime `json:"submitOrderDate" validate:"required"`
	} `json:"orderInfo"`
}
