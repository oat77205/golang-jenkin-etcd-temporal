package models

type RequestValidateTracking struct {
	CorrelationID  string `json:"correlationId" validate:"required"`
	TrackingNumber string `json:"trackingNumber" validate:"required"`
	ShopCode       string `json:"shopCode" validate:"required"`
	UserID         string `json:"userId" validate:"required"`
	Channel        string `json:"channel" validate:"required"`
}

type ResponseValidateTracking struct {
	ValidateCode    string                        `json:"validateCode"`
	ValidateMessage string                        `json:"validateMessage"`
	OrderInfo       *OrderInfo                    `json:"orderInfo,omitempty"`
	CustomerInfo    *ValidateTrackingCustomerInfo `json:"customerInfo,omitempty"`
	TargetDealer    string                        `json:"targetDealer,omitempty"`
}

type OrderInfo struct {
	OrderNumber           string         `json:"orderNumber,omitempty"`
	OrderStatus           string         `json:"orderStatus,omitempty"`
	OrderDate             string         `json:"orderDate,omitempty"`
	TrackingList          []TrackingList `json:"trackingList,omitempty"`
	TotalTrackingReceived *int           `json:"totalTrackingReceived,omitempty"`
}

type ValidateTrackingCustomerInfo struct {
	FirstName string `json:"firstName,omitempty"`
	LastName  string `json:"lastName,omitempty"`
}

type TrackingList struct {
	TrackingNumber string `json:"trackingNumber"`
	TrackingReason string `json:"trackingReason"`
}
