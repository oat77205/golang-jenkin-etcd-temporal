package api

import (
	"hl-order-api/configs"
	"hl-order-api/routes/models"
	"hl-order-api/routes/valid"
	"hl-order-api/services/flowconfig"
	"hl-order-api/util"
	"time"

	"github.com/gofiber/fiber/v2"
	"gitlab.com/true-itsd/iservicemax/omni/pkg/common-utils-backend/services/logger"
	"gitlab.com/true-itsd/iservicemax/omni/pkg/common-utils-backend/utils"
)

// @Tags SaveOrder
// @Accept       json
// @Produce      json
// @Param request body models.RegisterRequest true "RegisterRequest"
// @Success 200 {object} models.Response200Register "Success"
// @Param request body models.RegisterRequest true "RegisterRequest"
// @Success 200 {object} models.Response200Register "Success"
// @Router /register/{correlationId} [post]
func (server *Server) SaveOrder(c *fiber.Ctx) error {
	start := time.Now()
	trackingID := utils.GetUUID()

	hAuth := c.Request().Header.Peek("Authorization")

	var fielderror string
	var request models.RequestSaveOrder
	var response utils.ResponseStandard
	var logModel logger.LogModel

	defer func() {
		logOrderRequest := logger.LogOrderRequest{
			Request:    string(c.Body()),
			Response:   string(c.Response().Body()),
			ResultCode: response.Code,
			ResultDesc: response.Message,
		}
		logger.LogOrder(logOrderRequest, logModel, start)
	}()

	if err := c.BodyParser(&request); err != nil {
		response.Code = util.Code400
		response.BizError = fiber.ErrBadRequest.Message
		response.Message = err.Error()
		response.System = util.System_HL_ORDER_API
		return c.Status(fiber.StatusBadRequest).JSON(response)
	}

	getLogModelRequest := logger.LogModelRequest{
		ServiceType:   configs.Conf.Log.ServiceType.SaveOrder,
		Channel:       "ALL",
		CorrelationID: request.CorrelationID + "|" + request.OrderInfo.OrderNumber,
		Method:        fiber.MethodPost,
		StepName:      configs.Conf.Log.ServiceType.SaveOrder,
		Start:         start,
		TrackingID:    trackingID,
		Suffix:        configs.Conf.Log.Suffix,
		Product:       configs.Conf.Log.Product,
	}
	logModel = logger.GetLogModel(getLogModelRequest)
	err := utils.ValidatorNew(request, &fielderror)
	if err != nil {
		response.Code = util.Code422
		response.BizError = util.ParameterRequired
		response.Message = util.ParameterRequired
		response.Error = utils.GetErrorResponse(err)
		response.System = util.System_HL_ORDER_API
		return c.Status(fiber.StatusUnprocessableEntity).JSON(response)
	}

	serviceDaoOrderFlowidConfig := flowconfig.NewIServiceDaoOrderFlowidConfig(configs.Conf.Database)
	err, orderFlowid := serviceDaoOrderFlowidConfig.GetOrderFlowidConfig(request.FlowID, logModel)
	if err != nil {
		response.Code = util.Code500
		response.BizError = fiber.ErrInternalServerError.Message
		response.Message = err.Error()
		response.System = util.System_HL_ORDER_API
		return c.Status(fiber.StatusInternalServerError).JSON(response)
	}
	if orderFlowid == nil && err == nil {
		response.Code = util.Code404
		response.BizError = fiber.ErrNotFound.Message
		response.Message = "FlowID " + fiber.ErrNotFound.Message
		response.System = util.System_HL_ORDER_API
		return c.Status(fiber.StatusNotFound).JSON(response)
	}

	aErr := RequireByFlowid(request, orderFlowid)
	if aErr != nil {
		response.Code = util.Code422
		response.BizError = util.ParameterRequired
		response.Message = util.ParameterRequired
		response.Error = util.GetArrayErrorResponse(aErr)
		response.System = util.System_HL_ORDER_API
		return c.Status(fiber.StatusUnprocessableEntity).JSON(response)
	}

	err, claims := util.GetJWT(string(hAuth))
	if err != nil {
		response.Code = util.Code401
		response.BizError = fiber.ErrUnauthorized.Message
		response.Message = err.Error()
		response.System = util.System_HL_ORDER_API
		return c.Status(fiber.StatusUnauthorized).JSON(response)
	}
	if claims.ChannelSystem != "" {
		logModel.Channel = claims.ChannelSystem
	}

	result, resp := server.Services.IServiceSaveOrder.ServiceSaveOrder(request, *claims, orderFlowid, logModel)
	response.Code = resp.Code
	response.Message = resp.Message
	if !result {
		return c.Status(util.GetHttpsStatus(resp.Code)).JSON(resp)
	}
	if resp.Code == util.Code200 {
		resp.HlTrackingId = trackingID
	}
	return c.Status(fiber.StatusOK).JSON(resp)
}

func RequireByFlowid(req models.RequestSaveOrder, orderFlowid *flowconfig.OrderFlowidConfig) []error {
	var errs []error

	if req.FlowID == "SALE-DEVICE-PICKUP" {
		err := valid.RequireSaleDevicePickup(req)
		if err != nil {
			errs = append(errs, err)
		}
	} else if req.FlowID == "DEVICE-BUNDLE-EXISTING-CUSTOMER-TOS" {
		err := valid.RequireDeviceBundle(req)
		if err != nil {
			errs = append(errs, err)
		}
	}

	if orderFlowid.IsPickup {
		err := valid.RequireIsPickup(req)
		if err != nil {
			errs = append(errs, err)
		}
	}

	return errs
}
