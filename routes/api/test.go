package api

import (
	"context"
	"fmt"
	"hl-order-api/configs"
	"hl-order-api/routes/models"
	"hl-order-api/services/flowconfig"
	"hl-order-api/services/order"
	"hl-order-api/services/orderhistory"
	"hl-order-api/util"
	"time"

	"github.com/gofiber/fiber/v2"
	"gitlab.com/true-itsd/iservicemax/omni/pkg/common-utils-backend/services/logger"
	"gitlab.com/true-itsd/iservicemax/omni/pkg/common-utils-backend/utils"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.temporal.io/sdk/client"
	"go.temporal.io/sdk/temporal"
	"go.temporal.io/sdk/workflow"
)

type RequestTest struct {
	Request  models.RequestSaveOrder
	LogModel logger.LogModel
	Conf     configs.Config
	Claims   *util.Claims
}

func StartMyWorkFlow(request RequestTest) utils.ResponseStandard {
	// Create a new Temporal client
	cd, err := client.Dial(client.Options{})
	if err != nil {
		fmt.Println("Error creating Temporal client:", err)
	}
	defer cd.Close()

	retrypolicy := &temporal.RetryPolicy{
		InitialInterval:    time.Second,
		BackoffCoefficient: 2.0,
		MaximumInterval:    time.Second * 120,
	}

	// Start a new workflow
	workflowOptions := client.StartWorkflowOptions{
		ID:                       "my-workflow_" + utils.GetUUID(),
		TaskQueue:                "my-task-queue2",
		WorkflowExecutionTimeout: time.Second * 120,
		RetryPolicy:              retrypolicy,
	}
	workflowRun, err := cd.ExecuteWorkflow(context.Background(), workflowOptions, "MyWorkflow", request)
	if err != nil {
		fmt.Println("Error starting workflow:", err)
	}

	// Wait for the workflow to complete
	var result utils.ResponseStandard
	err = workflowRun.Get(context.Background(), &result)
	if err != nil {
		fmt.Println("Error getting workflow result:", err)
	}

	fmt.Println("Workflow completed with result:", result)
	return result
}

func MyWorkflow(ctx workflow.Context, req RequestTest) (utils.ResponseStandard, error) {
	var response utils.ResponseStandard

	retryPolicy := &temporal.RetryPolicy{
		InitialInterval:        time.Second,
		BackoffCoefficient:     2.0,
		MaximumInterval:        30 * time.Second,
		MaximumAttempts:        3, // unlimited retries
		NonRetryableErrorTypes: []string{"ErrInvalidAccount", "ErrInsufficientFunds"},
	}
	ao := workflow.ActivityOptions{
		StartToCloseTimeout: 30 * time.Second,
		RetryPolicy:         retryPolicy,
	}
	ctx = workflow.WithActivityOptions(ctx, ao)

	var orderFlowid Response_MyActivity_GetOrderFlowidConfig
	err := workflow.ExecuteActivity(ctx, "MyActivity_GetOrderFlowidConfig", req).Get(ctx, &orderFlowid)
	if err != nil {
		response.Code = util.Code500
		response.BizError = "MyActivity_GetOrderFlowidConfig failed"
		response.Message = err.Error()
		return response, err
	}

	if orderFlowid.Response.Code != "200" {
		return orderFlowid.Response, nil
	}

	aErr := RequireByFlowid(req.Request, orderFlowid.OrderFlowId)
	if aErr != nil {
		response.Code = util.Code422
		response.BizError = util.ParameterRequired
		response.Message = util.ParameterRequired
		response.Error = util.GetArrayErrorResponse(aErr)
		response.System = util.System_HL_ORDER_API
		return response, nil
	}

	var orderData Response_MyActivity_GetOrder
	err = workflow.ExecuteActivity(ctx, "MyActivity_GetOrder", req).Get(ctx, &orderData)
	if err != nil {
		response.Code = util.Code500
		response.BizError = "MyActivity_GetOrder failed"
		response.Message = err.Error()
		return response, err
	}

	if orderFlowid.OrderFlowId.IsPickup {
		if orderData.OrderData != nil {
			response = util.GetResponse200(response)
			return response, nil
		}
	} else {
		if orderData.OrderData == nil {
			response.Code = util.Code404
			response.BizError = "NOTFOUND"
			response.Message = "Data not found"
			response.System = util.System_HL_ORDER_API
			return response, nil
		}
		if orderData.OrderData.OrderInfo.OrderStateCode == "COMPLETED" {
			response.Code = util.Code422
			response.BizError = "ORDERCOMPLETED"
			response.Message = "The order number is in Completed status."
			response.System = util.System_HL_ORDER_API
			return response, nil
		}
	}

	defer func() (utils.ResponseStandard, error) {
		if response.Code == "200" {
			var result string
			err = workflow.ExecuteActivity(ctx, "MyActivity_InsertOrderHistory", req).Get(ctx, &result)
			if err != nil {
				response.Code = util.Code500
				response.BizError = "MyActivity_InsertOrderHistory failed"
				response.Message = err.Error()
				return response, err
			}
		}
		return response, nil
	}()

	response = util.GetResponse200(response)
	// serviceSaveOrder := saveorder.NewIServiceSaveOrder(req.Conf.Endpoint.Inventory, req.Conf.Database)
	// _, resp := serviceSaveOrder.ServiceSaveOrder(req.Request, *req.Claims, orderFlowid.OrderFlowId, req.LogModel)
	fmt.Println("------------------ result ------------------")
	return response, nil
}

type Response_MyActivity_GetOrderFlowidConfig struct {
	OrderFlowId *flowconfig.OrderFlowidConfig
	Response    utils.ResponseStandard
}

type Response_MyActivity_GetOrder struct {
	OrderData *order.Order
	Response  utils.ResponseStandard
}

type Activities struct{}

func (a *Activities) MyActivity_GetOrderFlowidConfig(ctx context.Context, req RequestTest) (Response_MyActivity_GetOrderFlowidConfig, error) {
	var responseAct Response_MyActivity_GetOrderFlowidConfig
	serviceDaoOrderFlowidConfig := flowconfig.NewIServiceDaoOrderFlowidConfig(req.Conf.Database)
	err, orderFlowid := serviceDaoOrderFlowidConfig.GetOrderFlowidConfig(req.Request.FlowID, req.LogModel)
	if err != nil {
		responseAct.Response.Code = util.Code500
		responseAct.Response.BizError = fiber.ErrInternalServerError.Message
		responseAct.Response.Message = err.Error()
		responseAct.Response.System = util.System_HL_ORDER_API
		return responseAct, nil
	}
	if orderFlowid == nil && err == nil {
		responseAct.Response.Code = util.Code404
		responseAct.Response.BizError = fiber.ErrNotFound.Message
		responseAct.Response.Message = "FlowID " + fiber.ErrNotFound.Message
		responseAct.Response.System = util.System_HL_ORDER_API
		return responseAct, nil
	}

	responseAct.OrderFlowId = orderFlowid
	responseAct.Response.Code = util.Code200
	return responseAct, nil
}

func (a *Activities) MyActivity_GetOrder(ctx context.Context, req RequestTest) (Response_MyActivity_GetOrder, error) {
	var responseAct Response_MyActivity_GetOrder
	var response utils.ResponseStandard
	serviceDaoSaveOrder := order.NewIServiceDaoOrder(req.Conf.Database)
	err, orderData := serviceDaoSaveOrder.GetOrder(req.Request.OrderInfo.OrderNumber, req.Claims.ChannelSystem, req.LogModel)
	if err != nil {
		response.Code = util.Code500
		response.BizError = fiber.ErrInternalServerError.Message
		response.Message = err.Error()
		response.System = util.System_HL_ORDER_API
		responseAct.Response = response
		return responseAct, nil
	}

	responseAct.OrderData = orderData
	return responseAct, nil
}

func (a *Activities) MyActivity_InsertOrderHistory(ctx context.Context, req RequestTest) (string, error) {
	serviceDaoOrderHistory := orderhistory.NewIServiceDaoOrderHistory(req.Conf.Database)
	serviceDaoOrderHistory.InsertOrderHistory(orderhistory.OrderHistory{
		CorrelationID:       req.Request.CorrelationID,
		OrderNumber:         req.Request.OrderInfo.OrderNumber,
		UserID:              req.Claims.ChannelSystem,
		ActivityName:        "SaveOrder",
		ActivityDescription: "SaveOrder " + req.Request.OrderInfo.OrderStateCode,
		CreatedDate:         primitive.NewDateTimeFromTime(time.Now()),
		CreatedBy:           req.Claims.ChannelSystem,
	}, req.LogModel)
	return "200", nil
}

func (server *Server) TestTemporal(c *fiber.Ctx) error {
	start := time.Now()
	trackingID := utils.GetUUID()

	hAuth := c.Request().Header.Peek("Authorization")

	var fielderror string
	var request models.RequestSaveOrder
	var response utils.ResponseStandard
	var logModel logger.LogModel

	defer func() {
		logOrderRequest := logger.LogOrderRequest{
			Request:    string(c.Body()),
			Response:   string(c.Response().Body()),
			ResultCode: response.Code,
			ResultDesc: response.Message,
		}
		logger.LogOrder(logOrderRequest, logModel, start)
	}()

	if err := c.BodyParser(&request); err != nil {
		response.Code = util.Code400
		response.BizError = fiber.ErrBadRequest.Message
		response.Message = err.Error()
		response.System = util.System_HL_ORDER_API
		return c.Status(fiber.StatusBadRequest).JSON(response)
	}

	getLogModelRequest := logger.LogModelRequest{
		ServiceType:   configs.Conf.Log.ServiceType.SaveOrder,
		Channel:       "ALL",
		CorrelationID: request.CorrelationID + "|" + request.OrderInfo.OrderNumber,
		Method:        fiber.MethodPost,
		StepName:      configs.Conf.Log.ServiceType.SaveOrder,
		Start:         start,
		TrackingID:    trackingID,
		Suffix:        configs.Conf.Log.Suffix,
		Product:       configs.Conf.Log.Product,
	}
	logModel = logger.GetLogModel(getLogModelRequest)
	err := utils.ValidatorNew(request, &fielderror)
	if err != nil {
		response.Code = util.Code422
		response.BizError = util.ParameterRequired
		response.Message = util.ParameterRequired
		response.Error = utils.GetErrorResponse(err)
		response.System = util.System_HL_ORDER_API
		return c.Status(fiber.StatusUnprocessableEntity).JSON(response)
	}

	err, claims := util.GetJWT(string(hAuth))
	if err != nil {
		response.Code = util.Code401
		response.BizError = fiber.ErrUnauthorized.Message
		response.Message = err.Error()
		response.System = util.System_HL_ORDER_API
		return c.Status(fiber.StatusUnauthorized).JSON(response)
	}
	if claims.ChannelSystem != "" {
		logModel.Channel = claims.ChannelSystem
	}

	response = StartMyWorkFlow(RequestTest{
		Request:  request,
		LogModel: logModel,
		Conf:     configs.Conf,
		Claims:   claims,
	})

	if response.Code != "200" {
		return c.Status(util.GetHttpsStatus(response.Code)).JSON(response)
	}
	if response.Code == util.Code200 {
		response.HlTrackingId = trackingID
	}
	return c.Status(fiber.StatusOK).JSON(response)
}
