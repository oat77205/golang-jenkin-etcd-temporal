package api

import (
	"hl-order-api/configs"

	fiber "github.com/gofiber/fiber/v2"
)

func (server *Server) RegisterRoutes(app *fiber.App) {
	app.Post("/test", server.TestTemporal)

	app.Post("/saveOrder", server.SaveOrder)

	app.Get("/config/:key", func(c *fiber.Ctx) error {
		if c.Params("key") == configs.Conf.Env {
			return c.Status(fiber.StatusOK).JSON(fiber.Map{"config": configs.Conf})
		}
		return c.Status(fiber.StatusUnauthorized).JSON(fiber.Map{"message": "contract admin"})
	})

	app.Get("/fetchEtcd/:key", func(c *fiber.Ctx) error {
		if c.Params("key") == configs.Conf.Env {
			err := configs.FetchEtcd()
			NewServer()
			if err != nil {
				return c.Status(fiber.StatusOK).JSON(fiber.Map{"message": err.Error()})
			}
			return c.Status(fiber.StatusOK).JSON(fiber.Map{"message": "success"})
		}
		return c.Status(fiber.StatusUnauthorized).JSON(fiber.Map{"message": "contract admin"})
	})
}
