package api

import (
	"hl-order-api/configs"
	"hl-order-api/services/saveorder"
)

type Server struct {
	Services Services
}

type Services struct {
	IServiceSaveOrder saveorder.IServiceSaveOrder
}

func NewServerTest() *Server {
	return &Server{}
}

func NewServer() *Server {
	return &Server{
		Services: Services{
			IServiceSaveOrder: saveorder.NewIServiceSaveOrder(configs.Conf.Endpoint.Inventory, configs.Conf.Database),
		},
	}
}
