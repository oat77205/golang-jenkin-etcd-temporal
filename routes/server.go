package routes

import (
	"hl-order-api/routes/api"
	"hl-order-api/routes/models"
	"log"
	"strconv"
	"strings"
	"time"

	swagger "github.com/arsmn/fiber-swagger/v2"
	"github.com/goccy/go-json"
	"github.com/gofiber/adaptor/v2"
	fiber "github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/cors"
	"github.com/gofiber/fiber/v2/middleware/monitor"
	"github.com/gofiber/fiber/v2/middleware/recover"
	"github.com/heptiolabs/healthcheck"
	"gitlab.com/true-itsd/iservicemax/omni/pkg/common-utils-backend/services/logger"
	"gitlab.com/true-itsd/iservicemax/omni/pkg/common-utils-backend/utils"
)

// @title hl-order-api
// @version v1.0
// @description Description API
// @schemes http
// @host localhost:8080
// @securityDefinitions.basic BasicAuth
func StartServer() {
	app := fiber.New(ConfigFiber())

	app.Use(recover.New(
		recover.Config{
			EnableStackTrace: true,
		},
	))
	app.Use(cors.New(cors.Config{
		AllowOrigins: "*",
		AllowMethods: "GET,POST,HEAD,PUT,DELETE,PATCH",
		AllowHeaders: "access-control-allow-origin,authorization,content-type,language,webversion,x-channel,x-platform,x-subchannel,x-transaction-id",
	}))

	app.Get("/swagger/*", swagger.HandlerDefault)
	app.Get("/dashboard", monitor.New())

	health := healthcheck.NewHandler()
	app.Get("/live", adaptor.HTTPHandlerFunc(health.LiveEndpoint))
	app.Get("/ready", adaptor.HTTPHandlerFunc(health.ReadyEndpoint))
	app.Get("/health", func(c *fiber.Ctx) error {
		return c.Status(fiber.StatusOK).JSON(nil)
	})

	s := api.NewServer()
	s.RegisterRoutes(app)

	log.Fatal(app.Listen(":8080"))
}

func ConfigFiber() fiber.Config {

	return fiber.Config{
		// Override default error handler
		ErrorHandler: func(ctx *fiber.Ctx, errors error) error {
			var log *logger.LogModel
			start := time.Now()
			trackingID := utils.GetUUID()
			correlationID := GetCorrelationID(ctx)
			getLogModelRequest := logger.LogModelRequest{
				ServiceType:   "ErrorHandler",
				CorrelationID: correlationID,
				Start:         start,
				TrackingID:    trackingID,
			}

			logModel := logger.GetLogModel(getLogModelRequest)
			defer func() {
				logOrderRequest := logger.LogOrderRequest{
					Request:  string(ctx.Body()),
					Response: string(ctx.Response().Body()),
				}
				if log != nil && !strings.Contains(log.ResultDesc, "status code : 404") {
					logger.LogOrder(logOrderRequest, logModel, start)
				}
			}()

			msg := errors.Error()
			err := json.Unmarshal([]byte(msg), &log)

			if err == nil && log != nil {
				errorCode := ""
				message := ""

				switch log.ResultDesc {
				case "timeout":
					errorCode = strconv.Itoa(fiber.ErrGatewayTimeout.Code)
					message = fiber.ErrGatewayTimeout.Message
				default:
					errorCode = log.ResultCode
					message = log.ResultDesc
				}

				status := fiber.ErrInternalServerError
				return ctx.Status(status.Code).JSON(utils.ResponseStandard{
					Code:    errorCode,
					Message: message,
				})
			}

			// Return from handler
			status := fiber.ErrInternalServerError
			return ctx.Status(status.Code).JSON(utils.ResponseStandard{
				Code:    strconv.Itoa(status.Code),
				Message: errors.Error(),
			})
		},
	}
}

func GetCorrelationID(ctx *fiber.Ctx) string {
	correlationId := ""

	switch ctx.Route().Path {
	case "/saveOrder":
		var req *models.RequestSaveOrder
		json.Unmarshal([]byte(ctx.Request().Body()), &req)
		correlationId = req.CorrelationID
	case "/updateOrderState":
		var req *models.RequestUpdateOrderState
		json.Unmarshal([]byte(ctx.Request().Body()), &req)
		correlationId = req.CorrelationID
	case "/searchOrderList":
		var req *models.RequestSearchOrderList
		json.Unmarshal([]byte(ctx.Request().Body()), &req)
		correlationId = req.CorrelationId
	case "/getOrderDetails":
		var req *models.RequestGetOrderDetails
		json.Unmarshal([]byte(ctx.Request().Body()), &req)
		correlationId = req.CorrelationId
	case "/getConfigOrderTrackingStatus":
		var req *models.RequestGetConfigOrderTrackingStatus
		json.Unmarshal([]byte(ctx.Request().Body()), &req)
		correlationId = req.CorrelationID
	case "/validateTracking":
		var req *models.RequestValidateTracking
		json.Unmarshal([]byte(ctx.Request().Body()), &req)
		correlationId = req.CorrelationID
	case "/goodsReceive":
		var req *models.RequestGoodsReceive
		json.Unmarshal([]byte(ctx.Request().Body()), &req)
		correlationId = req.CorrelationId
	case "/sendOTP":
		var req *models.RequestSendOTP
		json.Unmarshal([]byte(ctx.Request().Body()), &req)
		correlationId = req.CorrelationID
	case "/validateOTP":
		var req *models.RequestValidateOTP
		json.Unmarshal([]byte(ctx.Request().Body()), &req)
		correlationId = req.CorrelationID

	case "/preverifyExisting":
		var req *models.PreverifyExistingRequest
		json.Unmarshal([]byte(ctx.Request().Body()), &req)
		correlationId = req.CorrelationID
	case "/validateExistingCampaign":
		var req *models.RequestValidateExistingCampaign
		json.Unmarshal([]byte(ctx.Request().Body()), &req)
		correlationId = req.CorrelationID
	case "/validateSerialNumber":
		var req *models.ValidateSerialNumberResquest
		json.Unmarshal([]byte(ctx.Request().Body()), &req)
		correlationId = req.CorrelationID
	case "/updatePartialOrder":
		var req *models.UpdatePartialOrderRequest
		json.Unmarshal([]byte(ctx.Request().Body()), &req)
		correlationId = req.CorrelationID
	case "/getReceipt":
		var req *models.GetReceiptRequest
		json.Unmarshal([]byte(ctx.Request().Body()), &req)
		correlationId = req.CorrelationID
	case "/submitOrder":
		var req *models.SubmitOrderRequest
		json.Unmarshal([]byte(ctx.Request().Body()), &req)
		correlationId = req.CorrelationID
	case "/generateDocument":
		var req *models.GenarateDocmentRequest
		json.Unmarshal([]byte(ctx.Request().Body()), &req)
		correlationId = req.CorrelationID
	case "/reservePrivilege":
		var req *models.RequestReserve
		json.Unmarshal([]byte(ctx.Request().Body()), &req)
		correlationId = req.CorrelationID

	}

	return correlationId
}
