package valid

import (
	"hl-order-api/routes/models"
	requiredevicebundle "hl-order-api/routes/models/require/deviceBundle"

	"gitlab.com/true-itsd/iservicemax/omni/pkg/common-utils-backend/utils"
)

func RequireDeviceBundleReserve(req models.RequestReserve) error {
	var fielderror string
	var validate requiredevicebundle.RequestReserveDeviceBundle
	discountAmount := 0.00
	validate.CorrelationID = req.CorrelationID
	validate.FlowID = req.FlowID
	validate.Channel = req.Channel
	validate.SaleInfo.DealerCode = req.SaleInfo.DealerCode
	validate.SaleInfo.SaleCode = req.SaleInfo.SaleCode
	validate.SaleInfo.OwnerDealer = req.SaleInfo.OwnerDealer
	validate.SaleInfo.SaleName = req.SaleInfo.SaleName

	validate.SubscriberInfo.SubscriberNumber = req.SubscriberInfo.SubscriberNumber
	validate.SubscriberInfo.CompanyCode = req.SubscriberInfo.CompanyCode

	validate.OrderInfo.OrderNumber = req.OrderInfo.OrderNumber
	validate.OrderInfo.OrderDate = req.OrderInfo.OrderDate.Time

	validate.OrderInfo.PaymentInfo.TotalAmount = req.OrderInfo.PaymentInfo.TotalAmount
	validate.OrderInfo.PaymentInfo.BillDiscountAmount = req.OrderInfo.PaymentInfo.BillDiscountAmount

	if req.SubscriberInfo.AdditionPackage != nil {
		for _, d := range *req.SubscriberInfo.AdditionPackage {
			validate.SubscriberInfo.AdditionPackage = append(validate.SubscriberInfo.AdditionPackage, requiredevicebundle.AdditionPackage{
				OfferName:   d.OfferName,
				ServiceType: d.ServiceType,
			})
		}
	} else {
		validate.SubscriberInfo.AdditionPackage = append(validate.SubscriberInfo.AdditionPackage, requiredevicebundle.AdditionPackage{
			OfferName:   "Notvalidate",
			ServiceType: "Notvalidate",
		})
	}
	if req.OrderInfo.PaymentInfo.BillDiscountInfo != nil {
		for _, d := range *req.OrderInfo.PaymentInfo.BillDiscountInfo {
			validate.OrderInfo.PaymentInfo.BillDiscountInfo = append(validate.OrderInfo.PaymentInfo.BillDiscountInfo, requiredevicebundle.BillDiscountInfoReserve{
				DiscountCode:   d.DiscountCode,
				DiscountAmount: d.DiscountAmount,
			})
		}
	} else {

		validate.OrderInfo.PaymentInfo.BillDiscountInfo = append(validate.OrderInfo.PaymentInfo.BillDiscountInfo, requiredevicebundle.BillDiscountInfoReserve{
			DiscountCode:   "Notvalidate",
			DiscountAmount: &discountAmount,
		})
	}
	if req.OrderInfo.PaymentInfo.BillOtherPaymentInfo != nil {
		for _, d := range *req.OrderInfo.PaymentInfo.BillOtherPaymentInfo {
			validate.OrderInfo.PaymentInfo.BillOtherPaymentInfo = append(validate.OrderInfo.PaymentInfo.BillOtherPaymentInfo, requiredevicebundle.BillOtherPaymentInfoReserve{
				OtherPaymentCode:   d.OtherPaymentCode,
				OtherPaymentAmount: d.OtherPaymentAmount,
			})
		}
	} else {
		validate.OrderInfo.PaymentInfo.BillOtherPaymentInfo = append(validate.OrderInfo.PaymentInfo.BillOtherPaymentInfo, requiredevicebundle.BillOtherPaymentInfoReserve{
			OtherPaymentCode:   "Notvalidate",
			OtherPaymentAmount: &discountAmount,
		})
	}

	validate.CustomerInfo.FirstName = req.CustomerInfo.FirstName
	validate.CustomerInfo.LastName = req.CustomerInfo.LastName
	validate.CustomerInfo.Identification = req.CustomerInfo.Identification
	validate.CustomerInfo.IdentificationType = req.CustomerInfo.IdentificationType

	if req.CustomerInfo.CustomerAddress != nil {
		validate.CustomerInfo.CustomerAddress = requiredevicebundle.CustomerAddress{
			HouseNo: req.CustomerInfo.CustomerAddress.HouseNo,
			Tumbon:  req.CustomerInfo.CustomerAddress.Tumbon,
			Amphur:  req.CustomerInfo.CustomerAddress.Amphur,
			City:    req.CustomerInfo.CustomerAddress.City,
			Zip:     req.CustomerInfo.CustomerAddress.Zip,
		}
	}

	if len(req.OrderInfo.OrderItems) > 0 {
		for i, d := range req.OrderInfo.OrderItems {
			validate.OrderInfo.OrderItems = append(validate.OrderInfo.OrderItems, requiredevicebundle.OrderItemsReserve{
				Sequence:    d.Sequence,
				ProductCode: d.ProductCode,
				ProductType: d.ProductType,
			})
			if d.AmountInfo.DiscountInfo != nil {
				validate.OrderInfo.OrderItems[i].AmountInfo.DiscountInfo.TotalDiscountAmount = d.AmountInfo.DiscountInfo.TotalDiscountAmount
				if d.AmountInfo.DiscountInfo.Discount != nil {
					for _, dataDiscount := range d.AmountInfo.DiscountInfo.Discount {
						validate.OrderInfo.OrderItems[i].AmountInfo.DiscountInfo.Discount = append(validate.OrderInfo.OrderItems[i].AmountInfo.DiscountInfo.Discount, requiredevicebundle.DiscountReserve{
							DiscountCode:   dataDiscount.DiscountCode,
							DiscountAmount: dataDiscount.DiscountAmount,
						})
					}
				} else {
					validate.OrderInfo.OrderItems[i].AmountInfo.DiscountInfo.Discount = append(validate.OrderInfo.OrderItems[i].AmountInfo.DiscountInfo.Discount, requiredevicebundle.DiscountReserve{
						DiscountCode:   "Notvalidate",
						DiscountAmount: &discountAmount,
					})
				}
			} else {
				validate.OrderInfo.OrderItems[i].AmountInfo.DiscountInfo.TotalDiscountAmount = &discountAmount
				validate.OrderInfo.OrderItems[i].AmountInfo.DiscountInfo.Discount = append(validate.OrderInfo.OrderItems[i].AmountInfo.DiscountInfo.Discount, requiredevicebundle.DiscountReserve{
					DiscountCode:   "Notvalidate",
					DiscountAmount: &discountAmount,
				})
			}
			if d.AmountInfo != nil {
				validate.OrderInfo.OrderItems[i].AmountInfo.Price = d.AmountInfo.Price
				validate.OrderInfo.OrderItems[i].AmountInfo.Qty = d.AmountInfo.Qty
				if d.AmountInfo.OtherPaymentInfo != nil {
					validate.OrderInfo.OrderItems[i].AmountInfo.OtherPaymentInfo.TotalOtherPaymentAmount = d.AmountInfo.OtherPaymentInfo.TotalOtherPaymentAmount
				} else {
					validate.OrderInfo.OrderItems[i].AmountInfo.OtherPaymentInfo.TotalOtherPaymentAmount = &discountAmount
				}
			} else {
				validate.OrderInfo.OrderItems[i].AmountInfo.OtherPaymentInfo.TotalOtherPaymentAmount = &discountAmount
			}
			if d.CampaignInfo != nil {
				if d.CampaignInfo.CampaignCode != nil {
					validate.OrderInfo.OrderItems[i].CampaignInfo.CampaignCode = *d.CampaignInfo.CampaignCode
				}
			} else {
				validate.OrderInfo.OrderItems[i].CampaignInfo.CampaignCode = "Notvalidate"
			}

			if req.OrderInfo.OrderItems[i].AmountInfo.OtherPaymentInfo != nil && req.OrderInfo.OrderItems[i].AmountInfo.OtherPaymentInfo.OtherPayments != nil {
				for _, data := range req.OrderInfo.OrderItems[i].AmountInfo.OtherPaymentInfo.OtherPayments {
					validate.OrderInfo.OrderItems[i].AmountInfo.OtherPaymentInfo.OtherPayments = append(validate.OrderInfo.OrderItems[i].AmountInfo.OtherPaymentInfo.OtherPayments, requiredevicebundle.OtherPaymentsReserve{
						OtherPaymentCode:   data.OtherPaymentCode,
						OtherPaymentAmount: data.OtherPaymentAmount,
					})
				}
			} else {
				validate.OrderInfo.OrderItems[i].AmountInfo.OtherPaymentInfo.OtherPayments = append(validate.OrderInfo.OrderItems[i].AmountInfo.OtherPaymentInfo.OtherPayments, requiredevicebundle.OtherPaymentsReserve{
					OtherPaymentCode:   "Notvalidate",
					OtherPaymentAmount: &discountAmount,
				})
			}
		}
	}

	err := utils.ValidatorNew(validate, &fielderror)
	return err
}
