package valid

import (
	"hl-order-api/routes/models"
	"hl-order-api/routes/models/require"

	"gitlab.com/true-itsd/iservicemax/omni/pkg/common-utils-backend/utils"
)

func RequireUpdatePartialOrder(req models.UpdatePartialOrderRequest) []error {
	var errs []error
	fielderror := new(string)
	requireSim := new(require.RequestRequireSIM)

	for _, d := range req.OrderInfo.OrderItem {
		if d.InventoryType == "SIM " {
			requireSim.SerialStatus = &d.SerialStatus
			err := utils.ValidatorNew(requireSim, fielderror)
			if errs != nil {
				errs = append(errs, err)
			}
		}
	}

	return errs
}
