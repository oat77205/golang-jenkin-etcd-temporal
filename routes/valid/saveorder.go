package valid

import (
	"hl-order-api/routes/models"
	"hl-order-api/routes/models/require"

	"gitlab.com/true-itsd/iservicemax/omni/pkg/common-utils-backend/utils"
)

func RequireSaleDevicePickup(req models.RequestSaveOrder) error {
	var fielderror string
	var requireOrderItems []require.RequireOrderItems
	for _, d := range req.OrderInfo.OrderItems {
		requireOrderItems = append(requireOrderItems, require.RequireOrderItems{
			Sequence:           d.Sequence,
			ProductCode:        d.ProductCode,
			ProductType:        d.ProductType,
			InventoryType:      d.InventoryType,
			StockShopCode:      d.StockShopCode,
			OrderItemStateCode: d.OrderItemStateCode,
			AmountInfo: require.RequireAmountInfo{
				Price: d.AmountInfo.Price,
				Qty:   d.AmountInfo.Qty,
			},
		})
	}

	saleDevicePickup := require.RequestRequireSaleDevicePickup{
		SaleInfo: require.RequireSaleInfo{
			DealerCode:  req.SaleInfo.DealerCode,
			OwnerDealer: req.SaleInfo.OwnerDealer,
		},
		OrderInfo: require.RequireOrderInfo{
			OrderNumber:    req.OrderInfo.OrderNumber,
			OrderDate:      req.OrderInfo.OrderDate,
			OrderStateCode: req.OrderInfo.OrderStateCode,
			PaymentInfo: require.RequirePaymentInfo{
				TotalAmount:            req.OrderInfo.PaymentInfo.TotalAmount,
				BillDiscountAmount:     req.OrderInfo.PaymentInfo.BillDiscountAmount,
				BillOtherPaymentAmount: req.OrderInfo.PaymentInfo.BillOtherPaymentAmount,
				PaymentAmount:          req.OrderInfo.PaymentInfo.PaymentAmount,
				PaymentMethod:          req.OrderInfo.PaymentInfo.PaymentMethod,
			},
			OrderItems: requireOrderItems,
		},
		CustomerInfo: require.RequireCustomerInfo{
			Title:     req.CustomerInfo.Title,
			FirstName: req.CustomerInfo.FirstName,
			LastName:  req.CustomerInfo.LastName,
		},
	}

	if req.CustomerInfo.BirthDate != nil {
		saleDevicePickup.CustomerInfo.BirthDate = req.CustomerInfo.BirthDate
	}

	if req.CustomerInfo.CustomerAddress != nil {
		saleDevicePickup.CustomerInfo.CustomerAddress = &require.RequireCustomerAddress{
			HouseNo: req.CustomerInfo.CustomerAddress.HouseNo,
			Tumbon:  req.CustomerInfo.CustomerAddress.Tumbon,
			Amphur:  req.CustomerInfo.CustomerAddress.Amphur,
			City:    req.CustomerInfo.CustomerAddress.City,
			Zip:     req.CustomerInfo.CustomerAddress.Zip,
		}
	}

	if req.ContactInfo != nil {
		saleDevicePickup.ContactInfo = &require.RequireContactInfo{
			Title:     req.ContactInfo.Title,
			FirstName: req.ContactInfo.FirstName,
			LastName:  req.ContactInfo.LastName,
		}
	}

	err := utils.ValidatorNew(saleDevicePickup, &fielderror)
	return err
}

func RequireIsPickup(req models.RequestSaveOrder) error {
	var fielderror string
	var requireIsPickup require.RequireIsPickup

	requireIsPickup.TargetDealerCode = req.SaleInfo.TargetDealerCode

	err := utils.ValidatorNew(requireIsPickup, &fielderror)
	return err
}
