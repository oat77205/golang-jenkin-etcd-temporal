package valid

import (
	"hl-order-api/routes/models"
	"hl-order-api/routes/models/require"

	"gitlab.com/true-itsd/iservicemax/omni/pkg/common-utils-backend/utils"
)

func RequireIsClaim(req models.GenarateDocmentRequest) error {
	var fielderror string
	var requireClaim require.RequireClaim

	requireClaim.CitizenBase64 = req.GenerateDocumentInfo.CitizenBase64
	requireClaim.IdType = req.GenerateDocumentInfo.DocClaim.IdType
	requireClaim.PricePlan = req.GenerateDocumentInfo.DocClaim.PricePlan

	err := utils.ValidatorNew(requireClaim, &fielderror)
	return err
}
