package valid

import (
	"hl-order-api/routes/models"
	requiredevicebundle "hl-order-api/routes/models/require/deviceBundle"

	"gitlab.com/true-itsd/iservicemax/omni/pkg/common-utils-backend/utils"
)

func RequireDeviceBundle(req models.RequestSaveOrder) error {
	var fielderror string
	var validate requiredevicebundle.RequestSaveOrderDeviceBundle
	validate.CorrelationID = req.CorrelationID
	validate.FlowID = req.FlowID
	validate.UserId = req.UserId
	validate.OrderInfo.OrderNumber = req.OrderInfo.OrderNumber
	validate.Channel = req.Channel

	validate.OrderInfo.OrderStateCode = "Notvalidate"
	if req.Channel == "TOS" {
		validate.OrderInfo.OrderStateCode = req.OrderInfo.OrderStateCode
	}

	validate.OrderInfo.PaymentInfo.TotalAmount = req.OrderInfo.PaymentInfo.TotalAmount
	validate.OrderInfo.PaymentInfo.CpuId = req.OrderInfo.PaymentInfo.CpuId
	validate.OrderInfo.PaymentInfo.RepCompanyObjectId = req.OrderInfo.PaymentInfo.RepCompanyObjectId
	validate.OrderInfo.PaymentInfo.OperCompanyObjectId = req.OrderInfo.PaymentInfo.OperCompanyObjectId

	validate.OrderInfo.PaymentInfo.CashierInfo.CashierId = req.OrderInfo.PaymentInfo.CashierInfo.CashierId
	validate.OrderInfo.PaymentInfo.CashierInfo.CashierName = req.OrderInfo.PaymentInfo.CashierInfo.CashierName
	validate.OrderInfo.PaymentInfo.CashierInfo.CashierLanUser = req.OrderInfo.PaymentInfo.CashierInfo.CashierLanUser

	if req.OrderInfo.PaymentInfo.PaymentMethodInfo != nil {
		for _, d := range req.OrderInfo.PaymentInfo.PaymentMethodInfo {
			validate.OrderInfo.PaymentInfo.PaymentMethodInfo = append(validate.OrderInfo.PaymentInfo.PaymentMethodInfo, requiredevicebundle.PaymentMethodInfo{
				PaymentMethod:  d.PaymentMethod,
				CreditCardNo:   d.CreditCardNo,
				CreditCardType: d.CreditCardType,
				ExpireDate:     d.ExpireDate,
				ApproveCode:    d.ApproveCode,
				BankNo:         d.BankNo,
				BankName:       d.BankName,
				BinNum:         d.BinNum,
				RepCompanyId:   d.RepCompanyId,
			})
		}
	} else {
		validate.OrderInfo.PaymentInfo.PaymentMethodInfo = append(validate.OrderInfo.PaymentInfo.PaymentMethodInfo, requiredevicebundle.PaymentMethodInfo{
			PaymentMethod: "Notvalidate",
			CreditCardNo:  "Notvalidate",
			ExpireDate:    "Notvalidate",
			ApproveCode:   "Notvalidate",
			BankNo:        "Notvalidate",
			BankName:      "Notvalidate",
			BinNum:        0,
			RepCompanyId:  "Notvalidate",
		})
	}

	validate.CustomerInfo.Title = req.CustomerInfo.Title
	validate.CustomerInfo.FirstName = req.CustomerInfo.FirstName
	validate.CustomerInfo.LastName = req.CustomerInfo.LastName
	validate.CustomerInfo.Identification = req.CustomerInfo.Identification
	validate.CustomerInfo.IdentificationType = req.CustomerInfo.IdentificationType
	validate.CustomerInfo.CustomerType = req.CustomerInfo.CustomerType
	validate.CustomerInfo.Gender = req.CustomerInfo.Gender
	validate.CustomerInfo.ContactNo = req.CustomerInfo.ContactNo
	if req.CustomerInfo.CustomerAddress != nil {

		validate.CustomerInfo.CustomerAddress.HouseNo = req.CustomerInfo.CustomerAddress.HouseNo
		validate.CustomerInfo.CustomerAddress.Tumbon = req.CustomerInfo.CustomerAddress.Tumbon
		validate.CustomerInfo.CustomerAddress.Amphur = req.CustomerInfo.CustomerAddress.Amphur
		validate.CustomerInfo.CustomerAddress.City = req.CustomerInfo.CustomerAddress.City
		validate.CustomerInfo.CustomerAddress.Zip = req.CustomerInfo.CustomerAddress.Zip
	}
	err := utils.ValidatorNew(validate, &fielderror)
	return err
}
