// Code generated by MockGen. DO NOT EDIT.
// Source: ./services/saveomnitracking/service.go

// Package mock is a generated GoMock package.
package mock

import (
	gomock "github.com/golang/mock/gomock"
	saveomnitracking "gitlab.com/true-itsd/iservicemax/omni/api/ms/inventory/services/saveomnitracking"
	logger "gitlab.com/true-itsd/iservicemax/omni/pkg/common-utils-backend/services/logger"
	utils "gitlab.com/true-itsd/iservicemax/omni/pkg/common-utils-backend/utils"
	reflect "reflect"
)

// MockIServiceSaveOmniTracking is a mock of IServiceSaveOmniTracking interface
type MockIServiceSaveOmniTracking struct {
	ctrl     *gomock.Controller
	recorder *MockIServiceSaveOmniTrackingMockRecorder
}

// MockIServiceSaveOmniTrackingMockRecorder is the mock recorder for MockIServiceSaveOmniTracking
type MockIServiceSaveOmniTrackingMockRecorder struct {
	mock *MockIServiceSaveOmniTracking
}

// NewMockIServiceSaveOmniTracking creates a new mock instance
func NewMockIServiceSaveOmniTracking(ctrl *gomock.Controller) *MockIServiceSaveOmniTracking {
	mock := &MockIServiceSaveOmniTracking{ctrl: ctrl}
	mock.recorder = &MockIServiceSaveOmniTrackingMockRecorder{mock}
	return mock
}

// EXPECT returns an object that allows the caller to indicate expected use
func (m *MockIServiceSaveOmniTracking) EXPECT() *MockIServiceSaveOmniTrackingMockRecorder {
	return m.recorder
}

// SaveOmniTrackingService mocks base method
func (m *MockIServiceSaveOmniTracking) SaveOmniTrackingService(arg0 saveomnitracking.SaveOmniTrackingRequest, arg1 logger.LogModel) utils.ResponseStandard {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "SaveOmniTrackingService", arg0, arg1)
	ret0, _ := ret[0].(utils.ResponseStandard)
	return ret0
}

// SaveOmniTrackingService indicates an expected call of SaveOmniTrackingService
func (mr *MockIServiceSaveOmniTrackingMockRecorder) SaveOmniTrackingService(arg0, arg1 interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "SaveOmniTrackingService", reflect.TypeOf((*MockIServiceSaveOmniTracking)(nil).SaveOmniTrackingService), arg0, arg1)
}
