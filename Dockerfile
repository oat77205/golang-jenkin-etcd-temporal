#=============================================================
#--------------------- build stage ---------------------------
#=============================================================
FROM golang:1.19.0-alpine3.15 AS build_stage


ENV http_proxy=http://pxys2s1.true.th:80
ENV https_proxy=http://pxys2s1.true.th:80

RUN apk update
RUN apk add git
RUN apk add build-base
RUN git config --global url."https://{GIT_NAME}:{GIT_TOKEN}@gitlab.com/true-itsd".insteadOf "https://gitlab.com/true-itsd"

ENV PACKAGE_PATH=hl-order-api/omni
RUN mkdir -p /go/src/
WORKDIR /go/src/$PACKAGE_PATH

ENV PATH="/usr/local/go/bin:$PATH"
COPY . /go/src/$PACKAGE_PATH/

COPY ./configs/mock.yml /go/src/$PACKAGE_PATH/configs/local.yml
RUN cd configs && pwd && ls && cat local.yml

RUN go env -w GONOSUMDB="gitlab.com/true-itsd"
RUN go env -w GOPRIVATE="gitlab.com/true-itsd"
RUN go clean -modcache
RUN go mod tidy -compat=1.19
RUN go build -o hl-order-api
ENV http_proxy=
ENV https_proxy=

#RUN cd configs && cat local.yml
#RUN go test -v -failfast -cover -v ./routes/api -coverprofile=tests/cover.out | tee tests/test-results.out
#WORKDIR /go/src/$PACKAGE_PATH
#RUN cd tests && ls -alh
#RUN cd tests && cat test-results.out

#RUN PASSED=$(cat tests/test-results.out | grep -c '\--- PASS' || true); echo $PASSED; \
#    FAILED=$(cat tests/test-results.out | grep -c '\--- FAIL' || true); echo $FAILED; \
#    if [ "$FAILED" != '0' ] || [ "$PASSED" == '0' ]; then exit 1 ; else exit 0 ; fi

#=============================================================
#--------------------- final stage ---------------------------
#=============================================================
FROM golang:1.19.0-alpine3.16 AS final_stage


ENV PACKAGE_PATH=hl-order-api/omni

ENV http_proxy=http://pxys2s1.true.th:80
ENV https_proxy=http://pxys2s1.true.th:80

ENV http_proxy=
ENV https_proxy=

COPY --from=build_stage /go/src/$PACKAGE_PATH/hl-order-api /go/src/$PACKAGE_PATH/
COPY --from=build_stage /go/src/$PACKAGE_PATH/configs /go/src/$PACKAGE_PATH/configs

WORKDIR /go/src/$PACKAGE_PATH/
ENTRYPOINT ./hl-order-api {ENV}
EXPOSE {PORT}