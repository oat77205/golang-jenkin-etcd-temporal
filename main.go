package main

import (
	"hl-order-api/configs"
	"hl-order-api/routes"
)

func main() {
	configs.FetchEtcd()
	routes.StartServer()
}
