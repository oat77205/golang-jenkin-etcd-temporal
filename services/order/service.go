package order

import "gitlab.com/true-itsd/iservicemax/omni/api/ms/sale-profile/services/gshopsearch"

func GetShopNameTh(partnerCode string, shopsearch *gshopsearch.GShopSearchResponse) string {
	var shopNameTh string

	if shopsearch != nil && shopsearch.Data != nil {
		for _, d := range shopsearch.Data.ShopInfoList {
			if d.PartnerCode == partnerCode {
				shopNameTh = d.ShopNameTh
				break
			}
		}
	}

	return shopNameTh
}
