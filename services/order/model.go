package order

import (
	"time"

	"go.mongodb.org/mongo-driver/bson/primitive"
)

type Order struct {
	CorrelationID      string          `bson:"correlationId,omitempty"`
	FlowID             string          `bson:"flowId,omitempty"`
	Channel            string          `bson:"channel,omitempty"`
	SaleInfo           SaleInfo        `bson:"saleInfo,omitempty"`
	SubscriberInfo     *SubscriberInfo `bson:"subscriberInfo,omitempty"`
	OrderInfo          OrderInfo       `bson:"orderInfo,omitempty"`
	CustomerInfo       CustomerInfo    `bson:"customerInfo,omitempty"`
	ContactInfo        CustomerInfo    `bson:"contactInfo,omitempty"`
	PickupCustomerInfo *CustomerInfo   `bson:"pickupCustomerInfo,omitempty"`
	DocumentInfo       []DocumentInfo  `bson:"documentInfo,omitempty"`
	AirwayBills        []AirwayBills   `bson:"airwayBills,omitempty"`
}
type SubscriberInfo struct {
	SubscriberNumber    string                `bson:"subscriberNumber" validate:"required"`
	CompanyCode         string                `bson:"companyCode" validate:"required"`
	Priceplan           string                `bson:"priceplan,omitempty"`
	PropositionName     string                `bson:"propositionName,omitempty"`
	ContractProposition []ContractProposition `bson:"contractPropositionList,omitempty"`
	AdditionPackage     []AdditionPackage     `bson:"additionPackage,omitempty"`
}

type AdditionPackage struct {
	OfferName   string `bson:"offerName,omitempty"`
	ServiceType string `bson:"serviceType,omitempty"`
}

type ContractProposition struct {
	ContractStartDate     string `bson:"contractStartDate"`
	ContractExpireDate    string `bson:"contractExpireDate"`
	SocName               string `bson:"socName"`
	Fee                   string `bson:"fee"`
	SocDescription        string `bson:"socDescription"`
	Term                  string `bson:"term"`
	OfferInstanceID       string `bson:"offerInstanceId"`
	OfferGroup            string `bson:"offerGroup"`
	SocType               string `bson:"socType"`
	ContractExpireDateStr string `bson:"contractExpireDateStr"`
}

type AirwayBills struct {
	TrackingID             string    `bson:"trackingId,omitempty"`
	PackageNumber          string    `bson:"packageNumber,omitempty"`
	Courier                string    `bson:"courier,omitempty"`
	CustomerName           string    `bson:"customerName,omitempty"`
	CustomerAddress        string    `bson:"customerAddress,omitempty"`
	CustomerContract       string    `bson:"customerContract,omitempty"`
	PaymentMethod          string    `bson:"paymentMethod,omitempty"`
	MerchantName           string    `bson:"merchantName,omitempty"`
	MerchantAddress        string    `bson:"merchantAddress,omitempty"`
	MerchantContract       string    `bson:"merchantContract,omitempty"`
	ReferenceOrderNumber   string    `bson:"referenceOrderNumber,omitempty"`
	NumberOfItems          string    `bson:"numberOfItems,omitempty"`
	ObNumber               string    `bson:"obNumber,omitempty"`
	LotID                  string    `bson:"lotId,omitempty"`
	PurchaseDate           time.Time `bson:"purchaseDate,omitempty"`
	PrintDate              time.Time `bson:"printDate,omitempty"`
	TargetDeliverStartDate time.Time `bson:"targetDeliverStartDate,omitempty"`
	TargetDeliverEndDate   time.Time `bson:"targetDeliverEndDate,omitempty"`
}

type SaleInfo struct {
	DealerCode       string `bson:"dealerCode,omitempty"`
	DealerName       string `bson:"dealerName,omitempty"`
	OwnerDealer      string `bson:"ownerDealer,omitempty"`
	OwnerSaleId      string `bson:"ownerSaleId,omitempty"`
	OwnerDealerName  string `bson:"ownerDealerName,omitempty"`
	WebMethodChannel string `bson:"webMethodChannel,omitempty"`
	SalePlatform     string `bson:"salePlatform,omitempty"`
	TargetDealerCode string `bson:"targetDealerCode,omitempty"`
	TargetDealerName string `bson:"targetDealerName,omitempty"`
	SaleCode         string `bson:"saleCode,omitempty"`
	SaleName         string `bson:"saleName,omitempty"`
}

type OrderInfo struct {
	OrderNumber           string             `bson:"orderNumber,omitempty"`
	OrderStatus           string             `bson:"orderStatus,omitempty"`
	OrderType             string             `bson:"orderType,omitempty"`
	OrderReasonName       string             `bson:"orderReasonName,omitempty"`
	OrderReasonCode       string             `bson:"orderReasonCode,omitempty"`
	OrderStateCode        string             `bson:"orderStateCode,omitempty"`
	OrderDate             primitive.DateTime `bson:"orderDate,omitempty"`
	CreatedDate           primitive.DateTime `bson:"createdDate,omitempty"`
	CreatedBy             string             `bson:"createdBy,omitempty"`
	UpdatedDate           primitive.DateTime `bson:"updatedDate,omitempty"`
	UpdatedBy             string             `bson:"updatedBy,omitempty"`
	PickupDate            primitive.DateTime `bson:"pickupDate,omitempty"`
	PaymentInfo           PaymentInfo        `bson:"paymentInfo,omitempty"`
	OrderItems            []OrderItems       `bson:"orderItems,omitempty"`
	PickupRemark          string             `bson:"pickupRemark,omitempty"`
	PickupTransactionDate primitive.DateTime `bson:"pickupTransactionDate,omitempty"`
	PickupLaterFlag       bool               `bson:"pickupLaterFlag,omitempty"`
}

type VerifyInfo struct {
	SecondAuthenFlag   bool   `bson:"secondAuthenFlag"`
	SecondAuthenBy     string `bson:"secondAuthenBy"`
	SecondAuthenReason string `bson:"secondAuthenReason"`
	SecondAuthenByName string `bson:"secondAuthenByName"`
}

type IdentificationCardInfo struct {
	Bp1Number   string `bson:"bp1Number"`
	ChipID      string `bson:"chipId"`
	IssueDate   string `bson:"issueDate"`
	IssuerPlace string `bson:"issuerPlace"`
	ExpireDate  string `bson:"expireDate"`
	LaserID     string `bson:"laserId"`
}

type PaymentInfo struct {
	TotalAmount            float64                 `bson:"totalAmount,omitempty"`
	CpuId                  string                  `bson:"cpuId,omitempty"`
	RepCompanyObjectId     string                  `bson:"repCompanyObjectId,omitempty"`
	OperCompanyObjectId    string                  `bson:"operCompanyObjectId,omitempty"`
	CashierInfo            CashierInfo             `bson:"cashierInfo,omitempty"`
	PaymentMethodInfo      *[]PaymentMethodInfo    `bson:"paymentMethodInfo,omitempty"`
	BillDiscountAmount     float64                 `bson:"billDiscountAmount,omitempty"`
	BillOtherPaymentAmount float64                 `bson:"billOtherPaymentAmount,omitempty"`
	BillDiscountInfo       *[]BillDiscountInfo     `bson:"billDiscountInfo,omitempty"`
	BillOtherPaymentInfo   *[]BillOtherPaymentInfo `bson:"billOtherPaymentInfo,omitempty"`
	PaymentAmount          float64                 `bson:"paymentAmount,omitempty"`
	PaymentMethod          string                  `bson:"paymentMethod,omitempty"`
	CreditCardNo           string                  `bson:"creditCardNo,omitempty"`
	OwnerName              string                  `bson:"ownerName,omitempty"`
	ExpireDate             primitive.DateTime      `bson:"expireDate,omitempty"`
	BankName               string                  `bson:"bankName,omitempty"`
	BankNo                 string                  `bson:"bankNo,omitempty"`
}

type CashierInfo struct {
	CashierId      string `bson:"cashierId,omitempty"`
	CashierName    string `bson:"CashierName,omitempty"`
	CashierLanUser string `bson:"CashierLanUser,omitempty"`
}
type PaymentMethodInfo struct {
	PaymentMethod            string   `bson:"paymentMethod,omitempty"`
	PaymentMethodDescription string   `bson:"paymentMethodDescription,omitempty"`
	Amount                   *float64 `bson:"amount,omitempty"`
	CreditCardNo             string   `bson:"creditCardNo,omitempty"`
	CreditCardType           string   `bson:"creditCardType,omitempty"`
	OwnerName                string   `bson:"ownerName,omitempty"`
	ExpireDate               string   `bson:"expireDate,omitempty"`
	ApproveCode              string   `bson:"approveCode,omitempty"`
	BankNo                   string   `bson:"bankNo,omitempty"`
	BankName                 string   `bson:"bankNam,omitempty"`
	BankObjectId             string   `bson:"bankObjectId,omitempty"`
	BinNum                   int      `bson:"binNum,omitempty"`
	RepCompanyId             string   `bson:"repCompanyId,omitempty"`
}
type BillDiscountInfo struct {
	DiscountCode   string   `bson:"discountCode,omitempty"`
	DiscountAmount *float64 `bson:"discountAmount,omitempty"`
	CouponSerial   string   `bson:"couponSerial,omitempty"`
}

type BillOtherPaymentInfo struct {
	OtherPaymentCode   string   `bson:"OtherPaymentCode"`
	OtherPaymentAmount *float64 `bson:"otherPaymentAmount"`
	CouponSerial       string   `bson:"couponSerial"`
}

type OrderItems struct {
	Sequence           int           `bson:"sequence,omitempty"`
	ProductCode        string        `bson:"productCode,omitempty"`
	Serial             string        `bson:"serial,omitempty"`
	ProductInfo        ProductInfo   `bson:"productInfo,omitempty"`
	ProductType        string        `bson:"productType,omitempty"`
	PromotionSet       string        `bson:"promotionSet,omitempty"`
	PromotionType      string        `bson:"promotionType,omitempty"`
	Proposition        string        `bson:"proposition,omitempty"`
	GroupID            string        `bson:"groupId,omitempty"`
	MobileNo           string        `bson:"mobileNo,omitempty"`
	CompanyCode        string        `bson:"companyCode,omitempty"`
	InventoryType      string        `bson:"inventoryType,omitempty"`
	SerialStatus       string        `bson:"serialStatus,omitempty"`
	StockShopCode      string        `bson:"stockShopCode,omitempty"`
	StockShopType      string        `bson:"stockShopType,omitempty"`
	OrderItemStateCode string        `bson:"orderItemStateCode,omitempty"`
	AmountInfo         AmountInfo    `bson:"amountInfo,omitempty"`
	CampaignInfo       *CampaignInfo `bson:"campaignInfo,omitempty"`
	TrackingId         string        `bson:"trackingId,omitempty"`
}

type ProductInfo struct {
	Code     string `bson:"code,omitempty"`
	Name     string `bson:"name,omitempty"`
	Brand    string `bson:"brand,omitempty"`
	Model    string `bson:"model,omitempty"`
	Color    string `bson:"color,omitempty"`
	Capacity string `bson:"capacity,omitempty"`
}

type AmountInfo struct {
	Price            float64           `bson:"price,omitempty"`
	Qty              float64           `bson:"qty,omitempty"`
	Total            float64           `bson:"total,omitempty"`
	DepositAmt       float64           `bson:"depositAmt,omitempty"`
	DiscountInfo     *DiscountInfo     `bson:"discountInfo,omitempty"`
	OtherPaymentInfo *OtherPaymentInfo `bson:"otherPaymentInfo,omitempty"`
}
type OtherPaymentInfo struct {
	TotalOtherPaymentAmount *float64         `bson:"totalOtherPaymentAmount"`
	OtherPayments           *[]OtherPayments `bson:"otherPayments"`
}
type OtherPayments struct {
	OtherPaymentCode   string   `bson:"otherPaymentCode"`
	OtherPaymentAmount *float64 `bson:"otherPaymentAmount"`
	CounponSerial      string   `bson:"counponSerial"`
}
type CampaignInfo struct {
	CampaignCode string              `bson:"campaignCode,omitempty"`
	CampaignName string              `bson:"campaignName,omitempty"`
	VerifyKey    []map[string]string `bson:"verifyKey,omitempty"`
}

type CustomerInfo struct {
	Title                  string                  `bson:"title,omitempty"`
	FirstName              string                  `bson:"firstName,omitempty"`
	LastName               string                  `bson:"lastName,omitempty"`
	CustomerType           string                  `bson:"customerType"`
	BirthDate              primitive.DateTime      `bson:"birthDate,omitempty"`
	Identification         string                  `bson:"identification,omitempty"`
	IdentificationType     string                  `bson:"identificationType,omitempty"`
	Gender                 string                  `bson:"gender,omitempty"`
	ContactNo              string                  `bson:"contactNo,omitempty"`
	ContactEmail           string                  `bson:"contactEmail,omitempty"`
	CustomerAddress        *CustomerAddress        `bson:"customerAddress,omitempty"`
	VerifyInfo             *VerifyInfo             `bson:"verifyInfo,omitempty"`
	IdentificationCardInfo *IdentificationCardInfo `bson:"identificationCardInfo, omitempty"`
}

type CustomerAddress struct {
	HouseNo      string `bson:"houseNo,omitempty"`
	BuildingName string `bson:"buildingName,omitempty"`
	Soi          string `bson:"soi,omitempty"`
	Moo          string `bson:"moo,omitempty"`
	RoomNo       string `bson:"roomNo,omitempty"`
	StreetName   string `bson:"streetName,omitempty"`
	Tumbon       string `bson:"tumbon,omitempty"`
	Amphur       string `bson:"amphur,omitempty"`
	City         string `bson:"city,omitempty"`
	Zip          string `bson:"zip,omitempty"`
}

type DiscountInfo struct {
	TotalDiscountAmount float64    `bson:"totalDiscountAmount,omitempty"`
	Discount            []Discount `bson:"discount,omitempty"`
}

type Discount struct {
	DiscountCode   string  `bson:"discountCode,omitempty"`
	DiscountAmount float64 `bson:"discountAmount,omitempty"`
	CounponSerial  string  `bson:"counponSerial,omitempty"`
}

type SearchOrderRequest struct {
	CorrelationId         string
	FilterType            string
	OrderNumber           string `json:"orderNumber"`
	OrderNumberArr        []string
	CustomerName          string `json:"customerName"`
	CustomerLastName      string `json:"customerLastName"`
	CustomerContactNumber string `json:"customerContactNumber"`
	ShopCode              string `json:"shopCode"`
	StockType             string `json:"stockType"`
	OrderStatus           string `json:"orderStatus"`
	TrackingNumber        string `json:"trackingNumber"`
	FindAll               bool
	PageNo                int    `json:"pageNo" validate:"required"`
	PageSize              int    `json:"pageSize" validate:"required"`
	UserID                string `json:"userId" validate:"required"`
	Channel               string `json:"channel" validate:"required"`
}

type DocumentInfo struct {
	DocumentType string `bson:"documentType"`
	DocumentName string `bson:"documentName"`
	FileName     string `bson:"fileName"`
	Path         string `bson:"path"`
	RefID        string `bson:"refId"`
}
