package order

import (
	"context"
	"encoding/json"
	"fmt"
	"hl-order-api/configs"
	"hl-order-api/routes/models"
	"math"
	"time"

	"gitlab.com/true-itsd/iservicemax/omni/pkg/common-utils-backend/databases"
	"gitlab.com/true-itsd/iservicemax/omni/pkg/common-utils-backend/services/logger"
	"gitlab.com/true-itsd/iservicemax/omni/pkg/common-utils-backend/utils"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

const CollectionDB = "order"

type IServiceDaoOrder interface {
	InsertOrder(req *Order, logModel logger.LogModel) error
	GetOrder(orderNumber string, channel string, logModel logger.LogModel) (error, *Order)
	GetOrderByOrderNumber(orderNumber string, logModel logger.LogModel) (error, *Order)
	GetOrderDeviceBundleExisting(orderStateCode string, orderNumber string, channel string, flowId string, logModel logger.LogModel) (error, *Order)
	GetOrderList(req SearchOrderRequest, logModel logger.LogModel) (resp []Order, totalPag int, err error)
	UpdateSerialOrder(orderNumber string, productCode string, channel string, serial string, sequence int, logModel logger.LogModel) error
	UpdateAirwayBillOrder(req models.RequestUpdateOrderState, channel string, logModel logger.LogModel) error
	UpdateOrderState(*Order, logger.LogModel) error
	UpdateTrackingId(orderNumber string, productCode string, channel string, tracking string, sequence int, logModel logger.LogModel) error
	UpdateOrder(*Order, logger.LogModel) error
	UpdateDocumentInfoOrder(string, []DocumentInfo, logger.LogModel) error
	UpdateOrderItems(string, OrderItems, logger.LogModel) error
	UpdateForSubmitOrder(string, string, time.Time, logger.LogModel) error
	UpdateContractProposition(string, []ContractProposition, logger.LogModel) error
}

type serviceDaoOrder struct {
	conf utils.SystemDatabase
}

func NewIServiceDaoOrder(conf utils.SystemDatabase) IServiceDaoOrder {
	return &serviceDaoOrder{
		conf: conf,
	}
}

func (in *serviceDaoOrder) InsertOrder(reqDB *Order, logModel logger.LogModel) error {
	startStep := time.Now()

	logStepRequest := logger.LogStepRequest{
		StepName:    "Create - Order",
		StartDate:   utils.ConvDatetimeFormatLog(startStep),
		StepRequest: "", //Query params
		Endpoint:    "Mongo DB",
		Method:      "Insert",
		System:      "Order",
	}

	defer func() {
		logger.LogStep(logStepRequest, logModel, startStep)
	}()

	//Set timeout query 5 secs
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	client, err := databases.GetMongoDB(in.conf.MongoDB.Uri, in.conf.MongoDB.DbName, nil)
	if err != nil {
		logStepRequest.ResultDesc = "Error : GetMongoDB"
		logStepRequest.StepResponse = err.Error()
		logStepRequest.ResultCode = "500"
		return err
	}
	_, err = client.Collection(CollectionDB).InsertOne(ctx, reqDB)
	if err != nil {
		logStepRequest.ResultDesc = "Error : Insert"
		logStepRequest.StepResponse = err.Error()
		logStepRequest.ResultCode = "500"
		return err
	}
	logStepRequest.ResultDesc = "SUCCESS"
	logStepRequest.ResultCode = "200"
	return nil
}

func (in *serviceDaoOrder) GetOrder(orderNumber string, channel string, logModel logger.LogModel) (error, *Order) {
	startStep := time.Now()
	var dataDB *Order
	var query primitive.D

	logStepRequest := logger.LogStepRequest{
		StepName:     "Get - Order",
		StartDate:    utils.ConvDatetimeFormatLog(startStep),
		StepRequest:  "",
		StepResponse: "Success",
		Endpoint:     "Mongo DB",
		Method:       "Select",
		System:       "Order",
	}

	defer func() {
		logStepRequest.StepRequest = fmt.Sprint(query)
		logger.LogStep(logStepRequest, logModel, startStep)
	}()

	//Set timeout query 5 secs
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	client, err := databases.GetMongoDB(in.conf.MongoDB.Uri, in.conf.MongoDB.DbName, nil)
	if err != nil {
		logStepRequest.ResultDesc = "Error : GetMongoDB"
		logStepRequest.StepResponse = err.Error()
		logStepRequest.ResultCode = "500"
		return err, nil
	}

	query = bson.D{{Key: "orderInfo.orderNumber", Value: orderNumber}, {Key: "channel", Value: channel}}
	cursor, err := client.Collection(CollectionDB).Find(ctx, query)

	if err != nil {
		logStepRequest.ResultDesc = "Error : QueryOrder"
		logStepRequest.StepResponse = err.Error()
		logStepRequest.ResultCode = "500"
		return err, nil
	}

	defer cursor.Close(ctx)
	for cursor.Next(ctx) {
		cursor.Decode(&dataDB)
	}
	if err = cursor.Err(); err != nil {
		return err, nil
	}

	logResponse, _ := json.Marshal(dataDB)
	logStepRequest.StepResponse = string(logResponse)
	logStepRequest.ResultDesc = "SUCCESS"
	logStepRequest.ResultCode = "200"
	return nil, dataDB
}

func (in *serviceDaoOrder) GetOrderByOrderNumber(orderNumber string, logModel logger.LogModel) (error, *Order) {
	startStep := time.Now()
	var dataDB *Order

	logStepRequest := logger.LogStepRequest{
		StepName:    "Get - GetOrderByOrderNumber",
		StartDate:   utils.ConvDatetimeFormatLog(startStep),
		StepRequest: "", //Query params
		Endpoint:    "Mongo DB",
		Method:      "Select",
		System:      "Order",
	}

	defer func() {
		logger.LogStep(logStepRequest, logModel, startStep)
	}()

	//Set timeout query 5 secs
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	client, err := databases.GetMongoDB(configs.Conf.Database.MongoDB.Uri, configs.Conf.Database.MongoDB.DbName, nil)
	if err != nil {
		logStepRequest.ResultDesc = "Error : GetMongoDB"
		logStepRequest.StepResponse = err.Error()
		logStepRequest.ResultCode = "500"
		return err, nil
	}

	query := bson.D{{Key: "orderInfo.orderNumber", Value: orderNumber}}
	cursor, err := client.Collection(CollectionDB).Find(ctx, query)

	if err != nil {
		logStepRequest.ResultDesc = "Error : QueryOrder"
		logStepRequest.StepResponse = err.Error()
		logStepRequest.ResultCode = "500"
		return err, nil
	}

	defer cursor.Close(ctx)
	for cursor.Next(ctx) {
		cursor.Decode(&dataDB)
	}
	if err = cursor.Err(); err != nil {
		return err, nil
	}

	logStepRequest.ResultDesc = "SUCCESS"
	logStepRequest.ResultCode = "200"
	return nil, dataDB
}

func (in *serviceDaoOrder) GetOrderDeviceBundleExisting(orderStateCode string, orderNumber string, channel string, flowId string, logModel logger.LogModel) (error, *Order) {
	startStep := time.Now()
	var dataDB *Order

	logStepRequest := logger.LogStepRequest{
		StepName:    "Get - GetOrderDeviceBundleExisting",
		StartDate:   utils.ConvDatetimeFormatLog(startStep),
		StepRequest: "", //Query params
		Endpoint:    "Mongo DB",
		Method:      "Select",
		System:      "Order",
	}

	defer func() {
		logger.LogStep(logStepRequest, logModel, startStep)
	}()

	//Set timeout query 5 secs
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	client, err := databases.GetMongoDB(configs.Conf.Database.MongoDB.Uri, configs.Conf.Database.MongoDB.DbName, nil)
	if err != nil {
		logStepRequest.ResultDesc = "Error : GetMongoDB"
		logStepRequest.StepResponse = err.Error()
		logStepRequest.ResultCode = "500"
		return err, nil
	}

	query := bson.D{
		{Key: "orderInfo.orderStateCode", Value: orderStateCode},
		{Key: "orderInfo.orderNumber", Value: orderNumber},
		{Key: "channel", Value: channel},
		{Key: "flowId", Value: flowId},
	}
	cursor, err := client.Collection(CollectionDB).Find(ctx, query)

	if err != nil {
		logStepRequest.ResultDesc = "Error : QueryOrder"
		logStepRequest.StepResponse = err.Error()
		logStepRequest.ResultCode = "500"
		return err, nil
	}

	defer cursor.Close(ctx)
	for cursor.Next(ctx) {
		cursor.Decode(&dataDB)
	}
	if err = cursor.Err(); err != nil {
		return err, nil
	}

	logStepRequest.ResultDesc = "SUCCESS"
	logStepRequest.ResultCode = "200"
	return nil, dataDB
}

func (i *serviceDaoOrder) GetOrderList(req SearchOrderRequest, logModel logger.LogModel) (resp []Order, totalPag int, err error) {
	startStep := time.Now()
	totalPag = 1
	logStepRequest := logger.LogStepRequest{
		StepName:    "OrderInfo - GetOrderList",
		StartDate:   utils.ConvDatetimeFormatLog(startStep),
		StepRequest: "GetOrderList",
		Endpoint:    "Mongo DB",
		Method:      "GetOrderList",
		System:      "Order",
	}

	defer func() {
		logger.LogStep(logStepRequest, logModel, startStep)
	}()

	//Set timeout query 5 secs
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	connect, err := databases.GetMongoDB(configs.Conf.Database.MongoDB.Uri, configs.Conf.Database.MongoDB.DbName, nil)
	if err != nil {
		logStepRequest.ResultDesc = "Error : GetMongoDB"
		logStepRequest.StepResponse = err.Error()
		logStepRequest.ResultCode = "500"
		return
	}

	var cursor *mongo.Cursor
	if req.FilterType == "SearchOrderList" {

		filArr := bson.A{}

		if req.CustomerContactNumber != "" {
			filArr = append(filArr, bson.M{"customerInfo.contactNo": bson.M{"$regex": req.CustomerContactNumber, "$options": "i"}})
		}
		if req.CustomerName != "" {
			filArr = append(filArr, bson.M{"customerInfo.firstName": bson.M{"$regex": req.CustomerName, "$options": "i"}})
		}
		if req.CustomerLastName != "" {
			filArr = append(filArr, bson.M{"customerInfo.lastName": bson.M{"$regex": req.CustomerLastName, "$options": "i"}})
		}
		if req.OrderNumber != "" && len(req.OrderNumberArr) == 0 {
			filArr = append(filArr, bson.M{"orderInfo.orderNumber": bson.M{"$regex": req.OrderNumber, "$options": "i"}})
		}
		if len(req.OrderNumberArr) > 0 {
			filArr = append(filArr, bson.M{"orderInfo.orderNumber": bson.M{"$in": req.OrderNumberArr}})
		}

		flowId := []string{"SALE-DEVICE-PICKUP"}
		filArrAnd := bson.A{bson.M{"flowId": bson.M{"$in": flowId}}}
		filArrAnd = append(filArrAnd, bson.M{"orderInfo.orderDate": bson.M{"$gte": primitive.NewDateTimeFromTime(time.Now().AddDate(-31, 0, 0))}})

		if req.ShopCode != "" {
			filArrAnd = append(filArrAnd, bson.M{"saleInfo.targetDealerCode": bson.M{"$regex": req.ShopCode, "$options": "i"}})
		}
		if req.OrderStatus != "" {
			filArrAnd = append(filArrAnd, bson.M{"orderInfo.orderStateCode": bson.M{"$regex": req.OrderStatus, "$options": "i"}})
		}
		if req.StockType != "" {
			filArrAnd = append(filArrAnd, bson.M{"orderInfo.orderItems.stockShopType": bson.M{"$regex": req.StockType, "$options": "i"}})
		}
		var filter bson.M
		if len(filArrAnd) > 0 {
			if len(filArr) > 0 {
				filArrAnd = append(filArrAnd, bson.M{
					"$or": filArr,
				})
			}
			filter = bson.M{
				"$and": filArrAnd,
			}
		} else {
			filter = bson.M{
				"$or": filArr,
			}
		}

		if req.FindAll {
			sort := bson.M{"orderInfo.orderDate": -1} // -1=Desc
			opts := options.Find().SetSort(sort)
			cursor, err = connect.Collection(CollectionDB).Find(ctx, filter, opts)
		} else {
			total, errCnt := connect.Collection(CollectionDB).CountDocuments(ctx, filter)
			if errCnt == nil {
				totalPag = int(math.Ceil(float64(total) / float64(req.PageSize)))
			}
			sort := bson.M{"orderInfo.orderDate": -1} // -1=Desc
			skipIndex := int64((req.PageNo - 1) * req.PageSize)
			opts := options.Find().SetSort(sort).SetSkip(skipIndex).SetLimit(int64(req.PageSize))
			cursor, err = connect.Collection(CollectionDB).Find(ctx, filter, opts)
		}

	} else {
		filter := bson.M{}
		if req.CustomerContactNumber != "" {
			filter["customerInfo.contactNo"] = bson.M{"$regex": req.CustomerContactNumber, "$options": "i"}
		}
		if req.CustomerName != "" {
			filter["customerInfo.firstName"] = bson.M{"$regex": req.CustomerName, "$options": "i"}
		}
		if req.OrderNumber != "" {
			filter["orderInfo.orderNumber"] = req.OrderNumber
		}
		if req.OrderStatus != "" {
			filter["orderInfo.orderStateCode"] = bson.M{"$regex": req.OrderStatus, "$options": "i"}
		}
		cursor, err = connect.Collection(CollectionDB).Find(ctx, filter)
	}
	if err != nil {

		logStepRequest.ResultDesc = "Error : GetOrderList"
		logStepRequest.StepResponse = err.Error()
		logStepRequest.ResultCode = "500"
		return
	}

	defer cursor.Close(ctx)
	for cursor.Next(ctx) {
		order := Order{}
		cursor.Decode(&order)
		resp = append(resp, order)
	}
	if err = cursor.Err(); err != nil {
		return
	}
	logResponse, _ := json.Marshal(resp)
	logStepRequest.StepResponse = string(logResponse)
	logStepRequest.ResultDesc = "SUCCESS"
	logStepRequest.ResultCode = "200"
	return
}

func (in *serviceDaoOrder) UpdateSerialOrder(orderNumber string, productCode string, channel string, serial string, sequence int, logModel logger.LogModel) error {
	startStep := time.Now()
	filterUpdate := bson.M{"orderInfo.orderNumber": orderNumber, "channel": channel, "orderInfo.orderItems.productCode": productCode, "orderInfo.orderItems.sequence": sequence}
	updatedDate := primitive.NewDateTimeFromTime(time.Now())
	update := bson.M{
		"$set": bson.M{"orderInfo.orderItems.$.serial": serial, "orderInfo.updatedDate": updatedDate, "orderInfo.updatedBy": channel},
	}
	logStepRequest := logger.LogStepRequest{
		StepName:  "Updaate - UpdateSerialOrder",
		StartDate: utils.ConvDatetimeFormatLog(startStep),
		Endpoint:  "Mongo DB",
		Method:    "Update",
		System:    "Order",
	}

	defer func() {
		logStepRequest.StepRequest = fmt.Sprint(filterUpdate)
		logger.LogStep(logStepRequest, logModel, startStep)
	}()

	//Set timeout query 5 secs
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	client, err := databases.GetMongoDB(configs.Conf.Database.MongoDB.Uri, configs.Conf.Database.MongoDB.DbName, nil)
	if err != nil {
		logStepRequest.ResultDesc = "Error : GetMongoDB"
		logStepRequest.StepResponse = err.Error()
		logStepRequest.ResultCode = "500"
		return err
	}

	collection := client.Collection(CollectionDB)
	opts := options.Update().SetUpsert(true)
	updateResult, errUpdateDB := collection.UpdateOne(ctx, filterUpdate, update, opts)

	if errUpdateDB != nil {
		logStepRequest.ResultDesc = errUpdateDB.Error()
		logStepRequest.ResultCode = "500"
		return errUpdateDB
	}
	resByte, _ := json.Marshal(updateResult)
	logStepRequest.StepResponse = string(resByte)
	logStepRequest.ResultDesc = "SUCCESS"
	logStepRequest.ResultCode = "200"
	return nil
}

func (in *serviceDaoOrder) UpdateAirwayBillOrder(req models.RequestUpdateOrderState, channel string, logModel logger.LogModel) error {
	startStep := time.Now()
	filterUpdate := bson.M{"orderInfo.orderNumber": req.OrderInfo.OrderNumber, "channel": channel}
	dataToDB := []AirwayBills{}
	updatedDate := primitive.NewDateTimeFromTime(time.Now())
	var purchaseDate, printDate, targetDeliverStartDate, targetDeliverEndDate time.Time
	if len(req.AirwayBills) > 0 {
		for _, a := range req.AirwayBills {
			if a.PurchaseDate.Time != nil {
				purchaseDate = *a.PurchaseDate.Time
			}
			if a.PrintDate.Time != nil {
				printDate = *a.PrintDate.Time
			}
			if a.TargetDeliverStartDate.Time != nil {
				targetDeliverStartDate = *a.TargetDeliverStartDate.Time
			}
			if a.TargetDeliverEndDate.Time != nil {
				targetDeliverEndDate = *a.TargetDeliverEndDate.Time
			}
			dataToDB = append(dataToDB, AirwayBills{
				TrackingID:             a.TrackingID,
				PackageNumber:          a.PackageNumber,
				Courier:                a.Courier,
				CustomerName:           a.CustomerName,
				CustomerAddress:        a.CustomerAddress,
				CustomerContract:       a.CustomerContract,
				PaymentMethod:          a.PaymentMethod,
				MerchantName:           a.MerchantName,
				MerchantAddress:        a.MerchantAddress,
				MerchantContract:       a.MerchantContract,
				ReferenceOrderNumber:   a.ReferenceOrderNumber,
				NumberOfItems:          a.NumberOfItems,
				ObNumber:               a.ObNumber,
				LotID:                  a.LotID,
				PurchaseDate:           purchaseDate,
				PrintDate:              printDate,
				TargetDeliverStartDate: targetDeliverStartDate,
				TargetDeliverEndDate:   targetDeliverEndDate,
			})
		}
	}

	update := bson.M{
		"$set": bson.M{
			"airwayBills":           dataToDB,
			"orderInfo.updatedDate": updatedDate,
			"orderInfo.updatedBy":   channel,
		},
	}
	logStepRequest := logger.LogStepRequest{
		StepName:  "Updaate - UpdateAirwayBillOrder",
		StartDate: utils.ConvDatetimeFormatLog(startStep),
		Endpoint:  "Mongo DB",
		Method:    "Update",
		System:    "Order",
	}

	defer func() {
		logStepRequest.StepRequest = fmt.Sprint(filterUpdate)
		logger.LogStep(logStepRequest, logModel, startStep)
	}()

	//Set timeout query 5 secs
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	client, err := databases.GetMongoDB(configs.Conf.Database.MongoDB.Uri, configs.Conf.Database.MongoDB.DbName, nil)
	if err != nil {
		logStepRequest.ResultDesc = "Error : GetMongoDB"
		logStepRequest.StepResponse = err.Error()
		logStepRequest.ResultCode = "500"
		return err
	}

	collection := client.Collection(CollectionDB)
	opts := options.Update().SetUpsert(true)
	updateResult, errUpdateDB := collection.UpdateOne(ctx, filterUpdate, update, opts)

	if errUpdateDB != nil {
		logStepRequest.ResultDesc = errUpdateDB.Error()
		logStepRequest.ResultCode = "500"
		return errUpdateDB
	}
	resByte, _ := json.Marshal(updateResult)
	logStepRequest.StepResponse = string(resByte)
	logStepRequest.ResultDesc = "SUCCESS"
	logStepRequest.ResultCode = "200"
	return nil
}

func (in *serviceDaoOrder) UpdateOrderState(order *Order, logModel logger.LogModel) error {

	startStep := time.Now()

	logStepRequest := logger.LogStepRequest{
		StepName:    "Update - Order state",
		StartDate:   utils.ConvDatetimeFormatLog(startStep),
		StepRequest: "", //Query params
		Endpoint:    "Mongo DB",
		Method:      "Update",
		System:      "Order",
	}

	defer func() {
		logger.LogStep(logStepRequest, logModel, startStep)
	}()

	//Set timeout query 5 secs
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	client, err := databases.GetMongoDB(configs.Conf.Database.MongoDB.Uri, configs.Conf.Database.MongoDB.DbName, nil)
	if err != nil {
		logStepRequest.ResultDesc = "Error : GetMongoDB"
		logStepRequest.StepResponse = err.Error()
		logStepRequest.ResultCode = "500"
		return err
	}
	updatedDate := primitive.NewDateTimeFromTime(time.Now())
	filter := bson.D{{Key: "orderInfo.orderNumber", Value: order.OrderInfo.OrderNumber}}
	update := map[string]interface{}{
		"$set": bson.D{{Key: "orderInfo.updatedDate", Value: updatedDate}, {Key: "orderInfo.updatedBy", Value: order.Channel}},
	}
	if order.OrderInfo.OrderStateCode == "READY_TO_PICK_UP" || order.OrderInfo.OrderStateCode == "CANCELLED_ORDER" {
		update = map[string]interface{}{
			"$set": bson.D{{Key: "orderInfo.orderStateCode", Value: order.OrderInfo.OrderStateCode}, {Key: "orderInfo.updatedDate", Value: updatedDate}, {Key: "orderInfo.updatedBy", Value: order.Channel}},
		}
	}

	opts := options.Update().SetUpsert(true)
	_, err = client.Collection(CollectionDB).UpdateOne(ctx, filter, update, opts)
	if err != nil {
		logStepRequest.ResultDesc = "Error : Update"
		logStepRequest.StepResponse = err.Error()
		logStepRequest.ResultCode = "500"
		return err
	}
	logStepRequest.ResultDesc = "SUCCESS"
	logStepRequest.ResultCode = "200"

	return nil
}
func (in *serviceDaoOrder) UpdateTrackingId(orderNumber string, productCode string, channel string, tracking string, sequence int, logModel logger.LogModel) error {
	startStep := time.Now()
	filterUpdate := bson.M{"orderInfo.orderNumber": orderNumber, "channel": channel, "orderInfo.orderItems.productCode": productCode, "orderInfo.orderItems.sequence": sequence}
	updatedDate := primitive.NewDateTimeFromTime(time.Now())
	update := bson.M{
		"$set": bson.M{"orderInfo.orderItems.$.trackingId": tracking, "orderInfo.updatedDate": updatedDate, "orderInfo.updatedBy": channel},
	}
	logStepRequest := logger.LogStepRequest{
		StepName:  "Updaate - UpdateTrackingId",
		StartDate: utils.ConvDatetimeFormatLog(startStep),
		Endpoint:  "Mongo DB",
		Method:    "Update",
		System:    "Order",
	}

	defer func() {
		logStepRequest.StepRequest = fmt.Sprint(filterUpdate)
		logger.LogStep(logStepRequest, logModel, startStep)
	}()

	//Set timeout query 5 secs
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	client, err := databases.GetMongoDB(configs.Conf.Database.MongoDB.Uri, configs.Conf.Database.MongoDB.DbName, nil)
	if err != nil {
		logStepRequest.ResultDesc = "Error : GetMongoDB"
		logStepRequest.StepResponse = err.Error()
		logStepRequest.ResultCode = "500"
		return err
	}

	collection := client.Collection(CollectionDB)
	opts := options.Update().SetUpsert(true)
	updateResult, errUpdateDB := collection.UpdateOne(ctx, filterUpdate, update, opts)

	if errUpdateDB != nil {
		logStepRequest.ResultDesc = errUpdateDB.Error()
		logStepRequest.ResultCode = "500"
		return errUpdateDB
	}
	resByte, _ := json.Marshal(updateResult)
	logStepRequest.StepResponse = string(resByte)
	logStepRequest.ResultDesc = "SUCCESS"
	logStepRequest.ResultCode = "200"
	return nil
}

func (in *serviceDaoOrder) UpdateOrder(order *Order, logModel logger.LogModel) error {

	startStep := time.Now()

	logStepRequest := logger.LogStepRequest{
		StepName:    "Update - Order state",
		StartDate:   utils.ConvDatetimeFormatLog(startStep),
		StepRequest: "", //Query params
		Endpoint:    "Mongo DB",
		Method:      "Update",
		System:      "Order",
	}

	defer func() {
		logger.LogStep(logStepRequest, logModel, startStep)
	}()

	//Set timeout query 5 secs
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	client, err := databases.GetMongoDB(configs.Conf.Database.MongoDB.Uri, configs.Conf.Database.MongoDB.DbName, nil)
	if err != nil {
		logStepRequest.ResultDesc = "Error : GetMongoDB"
		logStepRequest.StepResponse = err.Error()
		logStepRequest.ResultCode = "500"
		return err
	}

	filter := bson.D{{Key: "orderInfo.orderNumber", Value: order.OrderInfo.OrderNumber}}

	_, err = client.Collection(CollectionDB).ReplaceOne(ctx, filter, order)
	if err != nil {
		logStepRequest.ResultDesc = "Error : Update"
		logStepRequest.StepResponse = err.Error()
		logStepRequest.ResultCode = "500"
		return err
	}
	logStepRequest.ResultDesc = "SUCCESS"
	logStepRequest.ResultCode = "200"

	return nil
}

func (in *serviceDaoOrder) UpdateDocumentInfoOrder(orderNumber string, documentInfo []DocumentInfo, logModel logger.LogModel) error {
	startStep := time.Now()

	logStepRequest := logger.LogStepRequest{
		StepName:    "Update - UpdateDocumentInfoOrder",
		StartDate:   utils.ConvDatetimeFormatLog(startStep),
		StepRequest: "", //Query params
		Endpoint:    "Mongo DB",
		Method:      "Update",
		System:      "Order",
	}

	defer func() {
		logger.LogStep(logStepRequest, logModel, startStep)
	}()

	//Set timeout query 5 secs
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	client, err := databases.GetMongoDB(configs.Conf.Database.MongoDB.Uri, configs.Conf.Database.MongoDB.DbName, nil)
	if err != nil {
		logStepRequest.ResultDesc = "Error : GetMongoDB"
		logStepRequest.StepResponse = err.Error()
		logStepRequest.ResultCode = "500"
		return err
	}

	filter := bson.D{{Key: "orderInfo.orderNumber", Value: orderNumber}}

	update := bson.M{"$set": bson.M{"documentInfo": documentInfo}}

	opts := options.Update().SetUpsert(true)
	_, err = client.Collection(CollectionDB).UpdateOne(ctx, filter, update, opts)
	if err != nil {
		logStepRequest.ResultDesc = "Error : Update"
		logStepRequest.StepResponse = err.Error()
		logStepRequest.ResultCode = "500"
		return err
	}
	logStepRequest.ResultDesc = "SUCCESS"
	logStepRequest.ResultCode = "200"

	return nil
}

func (in *serviceDaoOrder) UpdateOrderItems(orderNumber string, orderItems OrderItems, logModel logger.LogModel) error {
	startStep := time.Now()

	logStepRequest := logger.LogStepRequest{
		StepName:    "Update - UpdateOrderItems",
		StartDate:   utils.ConvDatetimeFormatLog(startStep),
		StepRequest: "", //Query params
		Endpoint:    "Mongo DB",
		Method:      "Update",
		System:      "Order",
	}

	defer func() {
		logger.LogStep(logStepRequest, logModel, startStep)
	}()

	//Set timeout query 5 secs
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	client, err := databases.GetMongoDB(configs.Conf.Database.MongoDB.Uri, configs.Conf.Database.MongoDB.DbName, nil)
	if err != nil {
		logStepRequest.ResultDesc = "Error : GetMongoDB"
		logStepRequest.StepResponse = err.Error()
		logStepRequest.ResultCode = "500"
		return err
	}

	filter := bson.D{{Key: "orderInfo.orderNumber", Value: orderNumber}, {Key: "orderInfo.orderItems.sequence", Value: orderItems.Sequence}}

	update := bson.M{"$set": bson.M{
		"orderInfo.orderItems.$.productCode":   orderItems.ProductCode,
		"orderInfo.orderItems.$.serial":        orderItems.Serial,
		"orderInfo.orderItems.$.inventoryType": orderItems.InventoryType,
		"orderInfo.orderItems.$.serialStatus":  orderItems.SerialStatus,
	}}

	opts := options.Update().SetUpsert(true)
	_, err = client.Collection(CollectionDB).UpdateOne(ctx, filter, update, opts)
	if err != nil {
		logStepRequest.ResultDesc = "Error : Update"
		logStepRequest.StepResponse = err.Error()
		logStepRequest.ResultCode = "500"
		return err
	}
	logStepRequest.ResultDesc = "SUCCESS"
	logStepRequest.ResultCode = "200"

	return nil
}

func (in *serviceDaoOrder) UpdateForSubmitOrder(orderNumber string, orderStateCode string, submitOrderDate time.Time, logModel logger.LogModel) error {
	startStep := time.Now()

	logStepRequest := logger.LogStepRequest{
		StepName:    "Update - UpdateForSubmitOrder",
		StartDate:   utils.ConvDatetimeFormatLog(startStep),
		StepRequest: "", //Query params
		Endpoint:    "Mongo DB",
		Method:      "Update",
		System:      "Order",
	}

	defer func() {
		logger.LogStep(logStepRequest, logModel, startStep)
	}()

	//Set timeout query 5 secs
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	client, err := databases.GetMongoDB(configs.Conf.Database.MongoDB.Uri, configs.Conf.Database.MongoDB.DbName, nil)
	if err != nil {
		logStepRequest.ResultDesc = "Error : GetMongoDB"
		logStepRequest.StepResponse = err.Error()
		logStepRequest.ResultCode = "500"
		return err
	}

	filter := bson.D{{Key: "orderInfo.orderNumber", Value: orderNumber}}

	update := bson.M{"$set": bson.M{
		"orderInfo.orderStateCode":  orderStateCode,
		"orderInfo.submitOrderDate": primitive.NewDateTimeFromTime(submitOrderDate),
	}}

	opts := options.Update().SetUpsert(true)
	_, err = client.Collection(CollectionDB).UpdateOne(ctx, filter, update, opts)
	if err != nil {
		logStepRequest.ResultDesc = "Error : Update"
		logStepRequest.StepResponse = err.Error()
		logStepRequest.ResultCode = "500"
		return err
	}
	logStepRequest.ResultDesc = "SUCCESS"
	logStepRequest.ResultCode = "200"

	return nil
}

func (in *serviceDaoOrder) UpdateContractProposition(orderNumber string, contractProposition []ContractProposition, logModel logger.LogModel) error {
	startStep := time.Now()

	logStepRequest := logger.LogStepRequest{
		StepName:    "Update - UpdateContractProposition",
		StartDate:   utils.ConvDatetimeFormatLog(startStep),
		StepRequest: "", //Query params
		Endpoint:    "Mongo DB",
		Method:      "Update",
		System:      "Order",
	}

	defer func() {
		logger.LogStep(logStepRequest, logModel, startStep)
	}()

	//Set timeout query 5 secs
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	client, err := databases.GetMongoDB(configs.Conf.Database.MongoDB.Uri, configs.Conf.Database.MongoDB.DbName, nil)
	if err != nil {
		logStepRequest.ResultDesc = "Error : GetMongoDB"
		logStepRequest.StepResponse = err.Error()
		logStepRequest.ResultCode = "500"
		return err
	}

	filter := bson.D{{Key: "orderInfo.orderNumber", Value: orderNumber}}

	update := bson.M{"$set": bson.M{
		"subscriberInfo.contractPropositionList": contractProposition,
	}}

	opts := options.Update().SetUpsert(true)
	_, err = client.Collection(CollectionDB).UpdateOne(ctx, filter, update, opts)
	if err != nil {
		logStepRequest.ResultDesc = "Error : Update"
		logStepRequest.StepResponse = err.Error()
		logStepRequest.ResultCode = "500"
		return err
	}
	logStepRequest.ResultDesc = "SUCCESS"
	logStepRequest.ResultCode = "200"

	return nil
}
