package getconfigordertrackingstatus

import (
	"hl-order-api/routes/models"

	"hl-order-api/util"

	cms "gitlab.com/true-itsd/iservicemax/omni/api/ms/cms/services/getconfigordertrackingstatus"
	"gitlab.com/true-itsd/iservicemax/omni/pkg/common-utils-backend/services/logger"
	"gitlab.com/true-itsd/iservicemax/omni/pkg/common-utils-backend/utils"
)

type IServiceGetConfigOrderTrackingStatus interface {
	ServiceGetConfigOrderTrackingStatus(models.RequestGetConfigOrderTrackingStatus, util.Claims, logger.LogModel) (*utils.ResponseStandard, error)
}

type ServiceGetConfigOrderTrackingStatus struct {
	config utils.EtcdApiConfig
}

func NewIServiceGetConfigOrderTrackingStatus(config utils.EtcdApiConfig) IServiceGetConfigOrderTrackingStatus {
	return &ServiceGetConfigOrderTrackingStatus{config: config}
}

func (confs *ServiceGetConfigOrderTrackingStatus) ServiceGetConfigOrderTrackingStatus(req models.RequestGetConfigOrderTrackingStatus, claims util.Claims, logModel logger.LogModel) (*utils.ResponseStandard, error) {
	var response utils.ResponseStandard
	confGetConfigOrderTrackingStatus := util.ConvertGetEndpointAuthentication(confs.config, "getConfigOrderTrackingStatus", "")
	callGetConfigOrderTrackingStatus := cms.NewCallGetConfigOrderTrackingStatusCMS(confGetConfigOrderTrackingStatus)
	service := cms.NewServiceGetConfigOrderTrackingStatusCMS(callGetConfigOrderTrackingStatus)
	newReq := cms.GetConfigOrderTrackingStatusCMSRequest{
		CorrelationID: req.CorrelationID,
		Channel:       req.Channel,
		CMSType:       req.CMSType,
		CMSName:       req.CMSName,
	}
	resp, err := service.GetConfigOrderTrackingStatusCMSService(newReq, logModel)
	if err != nil {
		response.Code = "500"
		response.Message = ""
		return nil, nil
	}

	type StatusData struct {
		Status []cms.CMSValueDataStatus `json:"status"`
	}
	statusData := StatusData{
		Status: resp.Data[0].CmsValue.Data[0].Status,
	}
	response = utils.ResponseStandard{
		Code:         resp.Code,
		BizError:     "",
		Message:      "",
		TranID:       "",
		ApiCode:      "",
		HlTrackingId: "",
		Data:         statusData,
		Error:        &[]utils.ErrorParam{},
		Errors:       &[]utils.Errors{},
	}

	return &response, nil
}
