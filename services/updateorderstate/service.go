package updateorderstate

import (
	"hl-order-api/configs"
	"hl-order-api/routes/models"
	"hl-order-api/services/flowconfig"
	"hl-order-api/services/order"
	"hl-order-api/services/orderhistory"
	"hl-order-api/util"
	"strconv"
	"time"

	"github.com/gofiber/fiber/v2"
	"gitlab.com/true-itsd/iservicemax/omni/api/ms/inventory/services/saveomnitracking"
	"gitlab.com/true-itsd/iservicemax/omni/pkg/common-utils-backend/services/logger"
	"gitlab.com/true-itsd/iservicemax/omni/pkg/common-utils-backend/utils"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type IServiceUpdateOrderState interface {
	ServiceUpdateOrderState(models.RequestUpdateOrderState, util.Claims, logger.LogModel) (bool, utils.ResponseStandard)
	SaveOmniTracking(req models.RequestUpdateOrderState, orderData *order.Order, channel string, logModel logger.LogModel) (bool, utils.ResponseStandard)
	SaveOmniToTracking(s saveomnitracking.SaveOmniTrackingRequest, orderData *order.Order, OrderNumber string, logModel logger.LogModel) utils.ResponseStandard
	ValidateProductCodeAndSequence(productCode string, sequence int, order order.Order) (bool, string)
	ValidateAirwayBills(req models.RequestUpdateOrderState, trackingId string) (bool, string)
}

type ServiceUpdateOrderState struct {
	config    utils.EtcdApiConfig
	Databases utils.SystemDatabase
}

func NewIServiceUpdateOrderState(config utils.EtcdApiConfig, db utils.SystemDatabase) IServiceUpdateOrderState {
	return &ServiceUpdateOrderState{
		config:    config,
		Databases: db,
	}
}

func (confs *ServiceUpdateOrderState) ServiceUpdateOrderState(req models.RequestUpdateOrderState, claims util.Claims, logModel logger.LogModel) (bool, utils.ResponseStandard) {
	var response utils.ResponseStandard

	serviceDaoSaveOrder := order.NewIServiceDaoOrder(configs.Conf.Database)
	err, orderData := serviceDaoSaveOrder.GetOrder(req.OrderInfo.OrderNumber, claims.ChannelSystem, logModel)

	defer func() {
		serviceDaoOrderHistory := orderhistory.NewIServiceDaoOrderHistory(confs.Databases)
		serviceDaoOrderHistory.InsertOrderHistory(orderhistory.OrderHistory{
			CorrelationID:       req.CorrelationID,
			OrderNumber:         req.OrderInfo.OrderNumber,
			UserID:              claims.ChannelSystem,
			ActivityName:        "UpdateOrderState",
			ActivityDescription: "UpdateOrderState " + req.OrderInfo.OrderStateCode,
			CreatedDate:         primitive.NewDateTimeFromTime(time.Now()),
			CreatedBy:           claims.ChannelSystem,
		}, logModel)
	}()
	if err != nil {
		response.Code = "500"
		response.Message = err.Error()
		return false, response
	}

	if orderData == nil {
		response.Code = "404"
		response.Message = "Data not found"
		response.BizError = "NOTFOUND"
		return false, response
	}
	if len(req.OrderInfo.OrderItems) > 0 {
		for _, r := range req.OrderInfo.OrderItems {
			dataProductCode, msgError := confs.ValidateProductCodeAndSequence(r.ProductCode, r.Sequence, *orderData)
			if !dataProductCode {
				response.Code = "404"
				response.Message = msgError
				response.BizError = "NOTFOUND"
				return false, response
			}
			if len(req.AirwayBills) > 0 && r.TrackingID != "" {
				dataProductCode, msgError := confs.ValidateAirwayBills(req, r.TrackingID)
				if !dataProductCode {
					response.Code = "422"
					response.Message = msgError
					response.BizError = "NOTFOUND"
					return false, response
				}
			}
		}
	}

	flowConfig := flowconfig.NewIServiceDaoOrderFlowidConfig(configs.Conf.Database)
	err, flowConfigData := flowConfig.GetOrderFlowidConfig(req.FlowID, logModel)
	if err != nil {
		response.Code = "500"
		response.Message = err.Error()
		return false, response
	}
	if flowConfigData == nil || !flowConfigData.IsPickup {
		response.Code = "404"
		response.BizError = fiber.ErrNotFound.Message
		response.Message = "FlowID " + fiber.ErrNotFound.Message
		return false, response
	}
	result, response := confs.SaveOmniTracking(req, orderData, claims.ChannelSystem, logModel)
	if !result {
		return false, response
	}
	return true, response
}
func (confs *ServiceUpdateOrderState) ValidateProductCodeAndSequence(productCode string, sequence int, order order.Order) (bool, string) {
	msgError := "Product Code " + productCode + " Or sequence " + strconv.Itoa(sequence) + " not found"
	if len(order.OrderInfo.OrderItems) > 0 {
		for _, v := range order.OrderInfo.OrderItems {
			if productCode == v.ProductCode && sequence == v.Sequence {
				msgError = ""
				return true, msgError
			}
		}
	}
	return false, msgError
}
func (confs *ServiceUpdateOrderState) ValidateAirwayBills(req models.RequestUpdateOrderState, trackingId string) (bool, string) {
	msgError := "TrackingId " + trackingId + " not found in AirwayBills"
	if len(req.AirwayBills) > 0 {
		for _, v := range req.AirwayBills {
			if v.TrackingID == trackingId {
				msgError = ""
				return true, msgError
			}
		}
	}
	return false, msgError

}
func (confs *ServiceUpdateOrderState) SaveOmniTracking(req models.RequestUpdateOrderState, orderData *order.Order, channel string, logModel logger.LogModel) (bool, utils.ResponseStandard) {
	var response utils.ResponseStandard
	response.Code = util.Code200
	response.Message = "Success"
	response.BizError = "SUCCESS"
	saveOmniTrackingReq := SetStructToTracking(req, orderData, channel)
	serviceDaoSaveOrder := order.NewIServiceDaoOrder(configs.Conf.Database)
	if req.OrderInfo.OrderStateCode == "PACKED" {
		/*
			if len(req.OrderInfo.OrderItems) != len(orderData.OrderInfo.OrderItems) {
				response.Code = "400"
				response.Message = "orderitem invalid"
				response.BizError = "orderitem invalid"
				return false, response
			}
		*/
		if len(req.OrderInfo.OrderItems) > 0 {
			saveOmniTrackingReq.TotalItems = len(orderData.OrderInfo.OrderItems)
			saveOmniTrackingReq.PickupShopCode = orderData.SaleInfo.TargetDealerCode
			saveOmniTrackingReq.OrderNumber = req.OrderInfo.OrderNumber
			var arrStockShopCode []string
			for _, r := range req.OrderInfo.OrderItems {
				productCode := r.ProductCode
				stockShopCode := GetStockShopCode(productCode, r.Sequence, orderData)
				arrStockShopCode = append(arrStockShopCode, stockShopCode)
				if r.Serial != "" {
					err := serviceDaoSaveOrder.UpdateSerialOrder(req.OrderInfo.OrderNumber, productCode, channel, r.Serial, r.Sequence, logModel)
					if err != nil {
						response.Code = "400"
						response.Message = err.Error()
						response.BizError = err.Error()
						return false, response
					}
				}
			}

			stockShopCodeDup := util.RemoveDuplicateStr(arrStockShopCode)
			if len(stockShopCodeDup) > 0 {
				for _, s := range stockShopCodeDup {
					saveOmniTrackingReq.StockShopCode = s
					saveOmniTrackingReq.StockType = util.GetStockShopType(s)
					// saveOmniTrackingReq.TotalItemsbyTracking = CountStockShop(s, arrStockShopCode)
					resp := confs.SaveOmniToTracking(saveOmniTrackingReq, orderData, req.OrderInfo.OrderNumber, logModel)
					if resp.Code != "200" {
						return false, resp
					}
				}
			}
		}
	} else if req.OrderInfo.OrderStateCode == "DELIVERY" || req.OrderInfo.OrderStateCode == "DELIVERY_FAILED" {
		if len(req.AirwayBills) > 0 {
			for _, r := range req.AirwayBills {
				saveOmniTrackingReq.TrackingNumber = r.TrackingID
				resp := confs.SaveOmniToTracking(saveOmniTrackingReq, orderData, req.OrderInfo.OrderNumber, logModel)
				if resp.Code != "200" {
					return false, resp
				}
			}
		}
	} else if req.OrderInfo.OrderStateCode == "CANCELLED_ORDER" {
		saveOmniTrackingReq.OrderNumber = req.OrderInfo.OrderNumber
		resp := confs.SaveOmniToTracking(saveOmniTrackingReq, orderData, req.OrderInfo.OrderNumber, logModel)
		if resp.Code != "200" {
			return false, resp
		}
	} else if req.OrderInfo.OrderStateCode == "SHIPPED" {
		if len(req.OrderInfo.OrderItems) > 0 {
			saveOmniTrackingReq.TotalItems = len(orderData.OrderInfo.OrderItems)
			saveOmniTrackingReq.OrderNumber = req.OrderInfo.OrderNumber
			arrTrackingID := []string{}
			arrProductCode := []string{}
			arrSequence := []int{}
			checkDupTraking := make(map[string]bool)
			for _, r := range req.OrderInfo.OrderItems {
				arrTrackingID = append(arrTrackingID, r.TrackingID)
				arrProductCode = append(arrProductCode, r.ProductCode)
				arrSequence = append(arrSequence, r.Sequence)
			}
			if len(arrTrackingID) > 0 {

				for number, t := range arrTrackingID {
					if !checkDupTraking[t] {
						intTrackingId := CountTrackingID(t, arrTrackingID)
						saveOmniTrackingReq.TotalItemsbyTracking = intTrackingId
						saveOmniTrackingReq.TrackingNumber = t
						shopCode := GetStockShopCodeOne(arrProductCode[number], orderData)
						saveOmniTrackingReq.StockType = util.GetStockShopType(shopCode)
						saveOmniTrackingReq.StockShopCode = shopCode
						resp := confs.SaveOmniToTracking(saveOmniTrackingReq, orderData, req.OrderInfo.OrderNumber, logModel)
						if resp.Code != "200" {
							return false, resp
						}
						checkDupTraking[t] = true
					}
					err := serviceDaoSaveOrder.UpdateTrackingId(req.OrderInfo.OrderNumber, arrProductCode[number], channel, t, arrSequence[number], logModel)
					if err != nil {
						response.Code = "400"
						response.Message = err.Error()
						response.BizError = err.Error()
						return false, response
					}
				}
			}
			err := serviceDaoSaveOrder.UpdateAirwayBillOrder(req, channel, logModel)
			if err != nil {
				response.Code = "400"
				response.Message = err.Error()
				response.BizError = err.Error()
				return false, response
			}
		}
	}
	return true, response
}
func GetStockShopCode(productCode string, sequence int, orderData *order.Order) string {
	if len(orderData.OrderInfo.OrderItems) > 0 {
		for _, r := range orderData.OrderInfo.OrderItems {
			if r.ProductCode == productCode && r.Sequence == sequence {
				return r.StockShopCode
			}
		}
	}
	return ""
}
func GetStockShopCodeOne(productCode string, orderData *order.Order) string {
	stockShopCode := ""
	if len(orderData.OrderInfo.OrderItems) > 0 {
		for _, r := range orderData.OrderInfo.OrderItems {
			if r.ProductCode == productCode {
				return r.StockShopCode
			}
		}
	}
	return stockShopCode
}
func SetStructToTracking(req models.RequestUpdateOrderState, orderData *order.Order, channel string) saveomnitracking.SaveOmniTrackingRequest {
	saveOmniTrackingReq := saveomnitracking.SaveOmniTrackingRequest{}
	saveOmniTrackingReq.CorrelationID = req.CorrelationID
	saveOmniTrackingReq.Channel = channel
	saveOmniTrackingReq.TrackingStatus = req.OrderInfo.OrderStateCode
	if req.OrderInfo.OrderStateCode == "DELIVERY" {
		saveOmniTrackingReq.TrackingStatus = "DELIVERED_SUCCESS"
	} else if req.OrderInfo.OrderStateCode == "DELIVERY_FAILED" {
		saveOmniTrackingReq.TrackingStatus = "DELIVERED_FAILED"
	}
	saveOmniTrackingReq.UserID = channel
	saveOmniTrackingReq.OrderShopCode = orderData.SaleInfo.DealerCode
	return saveOmniTrackingReq
}
func (confs *ServiceUpdateOrderState) SaveOmniToTracking(s saveomnitracking.SaveOmniTrackingRequest, orderData *order.Order, OrderNumber string, logModel logger.LogModel) utils.ResponseStandard {
	confGetSaveOmniTracking := util.ConvertGetEndpointAuthentication(confs.config, "saveOmniTrackingEndpoint", "saveOmniTracking")
	newCallSaveOmniTracking := saveomnitracking.NewCallSaveOmniTracking(confGetSaveOmniTracking)
	servcieSaveOmniTracking := saveomnitracking.NewServiceSaveOmniTracking(newCallSaveOmniTracking)
	resp := servcieSaveOmniTracking.SaveOmniTrackingService(s, logModel)
	serviceDaoSaveOrder := order.NewIServiceDaoOrder(configs.Conf.Database)
	if resp.Code == "200" {
		orderData.OrderInfo.OrderStateCode = s.TrackingStatus
		orderData.OrderInfo.OrderNumber = OrderNumber
		err := serviceDaoSaveOrder.UpdateOrderState(orderData, logModel)
		if err != nil {
			resp.Code = "400"
			resp.Message = err.Error()
			resp.BizError = err.Error()
			return resp
		}
	}
	return resp
}

func CountTrackingID(trackingId string, strSlice []string) int {
	count := 0
	for _, item := range strSlice {
		if item == trackingId {
			count++
		}
	}
	return count
}

func CountStockShop(stockShop string, strSlice []string) int {
	count := 0
	for _, item := range strSlice {
		if item == stockShop {
			count++
		}
	}
	return count
}
