package generatedocument

import (
	"hl-order-api/configs"
	"hl-order-api/routes/models"
	"hl-order-api/services/docsystem"
	"hl-order-api/services/flowconfig"
	"hl-order-api/services/order"
	"hl-order-api/services/orderhistory"
	"hl-order-api/util"
	"time"

	"github.com/gofiber/fiber/v2"
	"gitlab.com/true-itsd/iservicemax/omni/pkg/common-utils-backend/services/logger"
	"gitlab.com/true-itsd/iservicemax/omni/pkg/common-utils-backend/utils"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type IServiceGenerateDocument interface {
	ServiceGenerateDocument(req models.GenarateDocmentRequest, claims util.Claims, orderFlowid *flowconfig.OrderFlowidConfig, logModel logger.LogModel) (bool, utils.ResponseStandard)
}

type ServiceGenerateDocument struct {
	Databases utils.SystemDatabase
}

func NewIServiceGenerateDocument(db utils.SystemDatabase) IServiceGenerateDocument {
	return &ServiceGenerateDocument{
		Databases: db,
	}
}

func (r *ServiceGenerateDocument) ServiceGenerateDocument(req models.GenarateDocmentRequest, claims util.Claims, orderFlowid *flowconfig.OrderFlowidConfig, logModel logger.LogModel) (bool, utils.ResponseStandard) {
	var response utils.ResponseStandard
	respFile := new(utils.ResponseStandard)

	serviceDaoSaveOrder := order.NewIServiceDaoOrder(configs.Conf.Database)
	err, orderData := serviceDaoSaveOrder.GetOrder(req.OrderInfo.OrderNumber, claims.ChannelSystem, logModel)
	if err != nil {
		response.Code = util.Code500
		response.BizError = fiber.ErrInternalServerError.Message
		response.Message = err.Error()
		response.System = util.System_HL_ORDER_API
		return false, response
	}

	if orderData == nil && err == nil {
		response.Code = util.Code404
		response.BizError = fiber.ErrNotFound.Message
		response.Message = fiber.ErrNotFound.Message
		response.System = util.System_HL_ORDER_API
		return false, response
	}

	defer func() {
		if response.Code == util.Code200 {
			serviceDaoOrderHistory := orderhistory.NewIServiceDaoOrderHistory(r.Databases)
			serviceDaoOrderHistory.InsertOrderHistory(orderhistory.OrderHistory{
				CorrelationID:       req.CorrelationID,
				OrderNumber:         req.OrderInfo.OrderNumber,
				UserID:              claims.ChannelSystem,
				ActivityName:        "GenerateDocument",
				ActivityDescription: "GenerateDocument " + orderData.OrderInfo.OrderStateCode,
				CreatedDate:         primitive.NewDateTimeFromTime(time.Now()),
				CreatedBy:           claims.ChannelSystem,
			}, logModel)
		}

	}()

	callDocSystem := docsystem.NewIServiceDocSystem()
	if req.GenerateDocumentInfo.DocumentType == "app_form" {
		result, respApp4in1 := callDocSystem.ServiceApp4in1(req, orderData, logModel)

		if result.Code != util.Code200 {
			response.Code = result.Code
			response.BizError = result.Description
			response.Message = result.Description
			response.System = util.System_HL_REPORT
			return false, response
		}
		respFile = respApp4in1
	} else if req.GenerateDocumentInfo.DocumentType == "TBC" {
		result, respClaim := callDocSystem.ServiceClaim(req, logModel)

		if result.Code != util.Code200 {
			response.Code = result.Code
			response.BizError = result.Description
			response.Message = result.Description
			response.System = util.System_HL_REPORT
			return false, response
		}
		respFile = respClaim
	}

	result, respUploadType := callDocSystem.ServiceUploadType(req, claims, respFile, orderFlowid, logModel)
	if result.Code != util.Code200 {
		response.Code = result.Code
		response.BizError = result.Description
		response.Message = result.Description
		response.System = util.System_DOC_SYSTEM
		return false, response
	}

	documentInfo := []order.DocumentInfo{}
	documentInfo = append(documentInfo, orderData.DocumentInfo...)
	documentInfo = append(documentInfo, order.DocumentInfo{DocumentType: req.GenerateDocumentInfo.DocumentType, RefID: respUploadType.RefID})

	err = serviceDaoSaveOrder.UpdateDocumentInfoOrder(req.OrderInfo.OrderNumber, documentInfo, logModel)
	if err != nil {
		response.Code = util.Code500
		response.BizError = fiber.ErrInternalServerError.Message
		response.Message = err.Error()
		response.System = util.System_DOC_SYSTEM
		return false, response
	}

	response = util.GetResponse200(response)
	return true, response
}
