package piminfo

import (
	"errors"
	"hl-order-api/util"
	"strings"

	"github.com/gofiber/fiber/v2"
	"gitlab.com/true-itsd/iservicemax/omni/api/ms/pim/services/getpiminfo"
	"gitlab.com/true-itsd/iservicemax/omni/pkg/common-utils-backend/services/logger"
	"gitlab.com/true-itsd/iservicemax/omni/pkg/common-utils-backend/utils"
)

type IServiceServiceGetPiminfo interface {
	ServiceGetPiminfo(req RequestPiminfo, claims util.Claims, logModel logger.LogModel) (*getpiminfo.GetPiminfoResponse, string, string, error)
}

type ServiceServiceGetPiminfo struct {
	config utils.EtcdApiConfig
}

func NewIServiceGetPiminfo(config utils.EtcdApiConfig) IServiceServiceGetPiminfo {
	return &ServiceServiceGetPiminfo{config: config}
}

func (confs *ServiceServiceGetPiminfo) ServiceGetPiminfo(req RequestPiminfo, claims util.Claims, logModel logger.LogModel) (*getpiminfo.GetPiminfoResponse, string, string, error) {
	confGetPiminfo := util.ConvertGetEndpointAuthentication(confs.config, "getPimInfo", "")
	newCallGetPiminfo := getpiminfo.NewCallGetPiminfo(confGetPiminfo)
	servcieGetPiminfo := getpiminfo.NewServiceGetPiminfo(newCallGetPiminfo)
	reqGetpiminfog := getpiminfo.GetPiminfoRequest{
		CorrelationID: req.CorrelationID,
		Channel:       claims.ChannelSystem, //
		UserID:        claims.ChannelSystem, //
	}

	ProductSlice := []string{}
	for _, d := range req.OrderItems {
		if strings.ToUpper(d.ProductType) == "P" {
			ProductSlice = append(ProductSlice, d.ProductCode)
		}
	}
	productDup := util.RemoveDuplicateStr(ProductSlice)
	reqGetpiminfog.ParameterList = []getpiminfo.ParameterList{
		{
			Name:   "matCodeList",
			Values: productDup,
		},
	}

	err, respGetpiminfo := servcieGetPiminfo.GetPiminfoService(reqGetpiminfog, logModel)
	if err != nil {
		return nil, "500", fiber.ErrInternalServerError.Message, err
	} else if len(productDup) != len(respGetpiminfo.Data.ProductList) {
		matCode := ""
		valuesArrayMatCode := make([]string, len(respGetpiminfo.Data.ProductList))
		for i, d := range respGetpiminfo.Data.ProductList {
			valuesArrayMatCode[i] = d.MatCode
		}
		for _, d := range productDup {
			if !util.Contains(valuesArrayMatCode, d) {
				matCode += d + ","
			}
		}
		return nil, "404", fiber.ErrNotFound.Message, errors.New("not found matCode (" + matCode[0:len(matCode)-1] + ")")
	}
	return respGetpiminfo, "200", "", nil
}
