package piminfo

import "hl-order-api/routes/models"

type RequestPiminfo struct {
	CorrelationID string
	OrderItems    []models.OrderItems
}
