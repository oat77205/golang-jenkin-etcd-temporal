package customerpickup

import (
	"fmt"
	"hl-order-api/configs"
	"hl-order-api/routes/models"
	"hl-order-api/services/flowconfig"
	"hl-order-api/services/order"
	"hl-order-api/services/orderhistory"
	"strings"
	"time"

	"hl-order-api/util"

	"github.com/google/uuid"
	"gitlab.com/true-itsd/iservicemax/omni/api/ms/com-platform/services/whatsupemail"
	"gitlab.com/true-itsd/iservicemax/omni/api/ms/com-platform/services/whatup"
	"gitlab.com/true-itsd/iservicemax/omni/api/ms/doc-system/services/customerpickupform"
	"gitlab.com/true-itsd/iservicemax/omni/api/ms/doc-system/services/getfileinfo"
	"gitlab.com/true-itsd/iservicemax/omni/api/ms/doc-system/services/uploaddoc"
	"gitlab.com/true-itsd/iservicemax/omni/api/ms/doc-system/services/uploadtype"
	"gitlab.com/true-itsd/iservicemax/omni/api/ms/inventory/services/gettrackingdetail"
	"gitlab.com/true-itsd/iservicemax/omni/api/ms/inventory/services/saveomnitracking"
	tostoken "gitlab.com/true-itsd/iservicemax/omni/api/ms/partner/services/token"

	"gitlab.com/true-itsd/iservicemax/omni/api/ms/partner/services/updateorderstatus"
	"gitlab.com/true-itsd/iservicemax/omni/pkg/common-utils-backend/services/logger"
	"gitlab.com/true-itsd/iservicemax/omni/pkg/common-utils-backend/utils"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type IServiceCustomerPickup interface {
	ServiceCustomerPickup(models.RequestCustomerPickup, util.Claims, logger.LogModel, string) (*utils.ResponseStandard, error)
}

type ServiceCustomerPickup struct {
	ServiceSaveOmniTracking   saveomnitracking.IServiceSaveOmniTracking
	ServiceGetTrackingDetail  gettrackingdetail.IServiceGetTrackingDetail
	ServiceToken              tostoken.IServiceGetToken
	ServiceUpdateOrderStatus  updateorderstatus.IServiceUpdateOrderStatus
	ServiceUploadType         uploadtype.IServiceUploadType
	ServiceUploadDoc          uploaddoc.IServiceUploadDoc
	ServiceGetFileInfo        getfileinfo.IServiceGetFileInfo
	ServiceCustomerPickupForm customerpickupform.IServiceCustomerPickupForm
	ServiceComplat            whatup.IServiceWhatUpSendSms
	ServiceWhatsup            whatsupemail.IServiceWhatsUpSendEmail
	Databases                 utils.SystemDatabase
}

func NewIServiceCustomerPickup(config utils.EtcdApiConfig, confsToken utils.EtcdApiConfig, confsPlatform utils.EtcdApiConfig, confComplatForm utils.EtcdApiConfig, confDocSystem utils.EtcdApiConfig, confComplatFormWhatsup utils.EtcdApiConfig, confNodeJSUtility utils.EtcdApiConfig, db utils.SystemDatabase) IServiceCustomerPickup {
	confGetSaveOmniTracking := util.ConvertGetEndpointAuthentication(config, "saveOmniTrackingEndpoint", "saveOmniTracking")
	newCallSaveOmniTracking := saveomnitracking.NewCallSaveOmniTracking(confGetSaveOmniTracking)
	servcieSaveOmniTracking := saveomnitracking.NewServiceSaveOmniTracking(newCallSaveOmniTracking)

	confGetTrackingDetail := util.ConvertGetEndpointAuthentication(config, "getTrackingDetailPath", "getTrackingDetail")
	newCallGetTrackingDetail := gettrackingdetail.NewCallGetTrackingDetail(confGetTrackingDetail)
	serviceGetTrackingDetail := gettrackingdetail.NewServiceGetTrackingDetail(newCallGetTrackingDetail)

	confToken := util.ConvertGetEndpointAuthentication(confsToken, "tokenEndpoint", "getToken")
	newCallGetToken := tostoken.NewCallGetToken(confToken)
	serviceToken := tostoken.NewServiceGetToken(newCallGetToken)

	confUpdateOrderStatus := util.ConvertGetEndpointAuthentication(confsPlatform, "updateOrderStatusEndpoint", "updateOrderStatus")
	newCallUpdateOrderStatus := updateorderstatus.NewCallUpdateOrderStatus(confUpdateOrderStatus)
	serviceUpdateOrderStatus := updateorderstatus.NewServiceUpdateOrderStatus(newCallUpdateOrderStatus)

	confUploadType := util.ConvertGetEndpointAuthentication(confDocSystem, "uploadType", "uploadType")
	newCallUploadType := uploadtype.NewCallUploadType(confUploadType)
	serviceUploadType := uploadtype.NewServiceUploadType(newCallUploadType)

	confUploadDoc := util.ConvertGetEndpointAuthentication(confDocSystem, "uploadDoc", "uploadDoc")
	newCallUploadDoc := uploaddoc.NewCallUploadDoc(confUploadDoc)
	serviceUploadDoc := uploaddoc.NewServiceUploadDoc(newCallUploadDoc)

	confGetFileInfo := util.ConvertGetEndpointAuthentication(confDocSystem, "getFileInfo", "getFileInfo")
	newCallGetFileInfo := getfileinfo.NewCallGetFileInfo(confGetFileInfo)
	serviceGetFileInfo := getfileinfo.NewServiceGetFileInfo(newCallGetFileInfo)

	confComplat := util.ConvertGetEndpointAuthentication(confComplatForm, "smsgTemplate", "smsgTemplate")
	newCallComplat := whatup.NewCalWhatUpSendSms(confComplat)
	serviceComplat := whatup.NewServiceWhatUpSendSms(newCallComplat)

	confCustomerPickupForm := util.ConvertGetEndpointAuthentication(confNodeJSUtility, "customerPickupForm", "customerPickupForm")
	newCallCustomerPickupForm := customerpickupform.NewCallCustomerPickupForm(confCustomerPickupForm)
	serviceCustomerPickupForm := customerpickupform.NewServiceCustomerPickupForm(newCallCustomerPickupForm)

	confWhatsup := util.ConvertGetEndpointAuthentication(confComplatFormWhatsup, "whatsupReceiver", "whatsupReceiver")
	newCallWhatsup := whatsupemail.NewCalWhatsUpSendEmail(confWhatsup)
	serviceWhatsup := whatsupemail.NewServiceWhatsUpSendEmail(newCallWhatsup)

	return &ServiceCustomerPickup{
		ServiceGetTrackingDetail:  serviceGetTrackingDetail,
		ServiceSaveOmniTracking:   servcieSaveOmniTracking,
		ServiceToken:              serviceToken,
		ServiceUpdateOrderStatus:  serviceUpdateOrderStatus,
		ServiceUploadType:         serviceUploadType,
		ServiceUploadDoc:          serviceUploadDoc,
		ServiceGetFileInfo:        serviceGetFileInfo,
		ServiceCustomerPickupForm: serviceCustomerPickupForm,
		ServiceComplat:            serviceComplat,
		ServiceWhatsup:            serviceWhatsup,
		Databases:                 db,
	}
}

func (confs *ServiceCustomerPickup) ServiceCustomerPickup(req models.RequestCustomerPickup, claims util.Claims, logModel logger.LogModel, token string) (*utils.ResponseStandard, error) {
	var response utils.ResponseStandard
	serviceDaoSaveOrder := order.NewIServiceDaoOrder(configs.Conf.Database)

	fmt.Printf("Token: %s\n", token)
	err, orderData := serviceDaoSaveOrder.GetOrderByOrderNumber(req.OrderNumber, logModel)
	if err != nil {
		response.Code = "500"
		response.Message = "Internal server error"
		return &response, err
	}

	if orderData == nil || (orderData != nil && orderData.CorrelationID == "") {
		response.Code = "400"
		response.BizError = "NOTFOUND"
		response.Message = "Data not found"
		return &response, err
	}

	getTrackingDetailReq := BuildGetTrackingDetail(req.OrderNumber)
	trackingDetailResp := confs.ServiceGetTrackingDetail.GetTrackingDetailService(getTrackingDetailReq, logModel, "OMNI - CustomerPickup")
	if trackingDetailResp.Code != "200" {
		response.Code = "500"
		response.Message = "Internal server error"
		return &response, err
	}

	if trackingDetailResp.Code == "200" && trackingDetailResp.Data == nil {
		response.Code = "400"
		response.BizError = "NOTFOUND"
		response.Message = "Data not found"
		return &response, err
	}

	pickupLen := len(req.PickupList)
	pickupFound := 0
	for _, pickup := range req.PickupList {
		for _, tracking := range trackingDetailResp.Data.TrackingList {
			if pickup.TrackingID == tracking.TrackingNumber && tracking.TrackingStatus == "READY_TO_PICK_UP" {
				pickupFound++
			}
		}
	}

	if pickupFound != pickupLen {
		response.Code = "422"
		response.BizError = "Tracking status is invalid"
		response.Message = "Tracking status is invalid"
		return &response, nil
	}

	orderNumberFound := false
	if req.OrderNumber == orderData.OrderInfo.OrderNumber && orderData.OrderInfo.OrderStateCode == "READY_TO_PICK_UP" {
		orderNumberFound = true
	}

	if !orderNumberFound {
		response.Code = "422"
		response.BizError = "Tracking status is invalid"
		response.Message = "Tracking status is invalid"
		return &response, nil
	}

	pickupLen2 := len(req.PickupList)
	pickupFound2 := 0
	for _, pickup := range req.PickupList {
		for _, item := range orderData.OrderInfo.OrderItems {
			if pickup.TrackingID == item.TrackingId && orderData.OrderInfo.OrderStateCode == "READY_TO_PICK_UP" {
				pickupFound2++
				break // one trackingId have many items
			}
		}
	}

	if pickupFound2 != pickupLen2 {
		response.Code = "404"
		response.BizError = "trackingId not equal in orderNumber"
		response.Message = "trackingId not equal in orderNumber"
		return &response, nil
	}

	serviceDaoOrderFlowidConfig := flowconfig.NewIServiceDaoOrderFlowidConfig(configs.Conf.Database)
	err, orderFlowID := serviceDaoOrderFlowidConfig.GetOrderFlowidConfig(orderData.FlowID, logModel)
	if err != nil {
		response.Code = "500"
		response.Message = "Internal server error"
		return &response, err
	}

	if orderFlowID == nil && err == nil {
		response.Code = "400"
		response.BizError = "NOTFOUND"
		response.Message = "Data not found"
		return &response, err
	}

	for _, pickup := range req.PickupList {
		//save omni tracking
		saveOmniTrackingReq := BuildSaveOmniTrackingRequest(req.CorrelationID, req, pickup, claims, orderFlowID)
		saveOmniTrackingRes := confs.ServiceSaveOmniTracking.SaveOmniTrackingService(saveOmniTrackingReq, logModel)
		if saveOmniTrackingRes.Code != "200" {
			response.Code = "500"
			response.Message = "Internal server error"
			return &response, err
		}
	}

	tokenRes := confs.ServiceToken.GetTokenService(logModel)

	if tokenRes.Code == "200" {
		tosOrderStatus := ""
		if req.OrderStateCode == "COMPLETED_PICK_UP" {
			tosOrderStatus = "COMPLETED"
		} else if req.OrderStateCode == "CANCELLED_ORDER" {
			tosOrderStatus = "REQUEST_REFUND"
		}
		updateOrderStatus := updateorderstatus.UpdateOrderStatusRequest{
			Token:       tokenRes.Data.AccessToken,
			OrderNumber: req.OrderNumber,
			OrderStatus: tosOrderStatus,
		}

		updateOrderResp := confs.ServiceUpdateOrderStatus.UpdateOrderStatusService(updateOrderStatus, logModel)

		serviceDaoOrderHistory := orderhistory.NewIServiceDaoOrderHistory(confs.Databases)
		serviceDaoOrderHistory.InsertOrderHistory(orderhistory.OrderHistory{
			CorrelationID:       req.CorrelationID,
			OrderNumber:         req.OrderNumber,
			UserID:              claims.ChannelSystem,
			ActivityName:        "CustomerPickup",
			ActivityDescription: "CustomerPickup " + orderData.OrderInfo.OrderStateCode + " TOS Code: " + updateOrderResp.Code + " Description: " + updateOrderResp.Message,
			CreatedDate:         primitive.NewDateTimeFromTime(time.Now()),
			CreatedBy:           claims.ChannelSystem,
		}, logModel)
	}

	_, _ = confs.CallUploadType(req, logModel, claims)

	if orderFlowID.CustomerPickup.SmsToCustomer.Action {
		currentDateTimeStr := ""
		currentDate := ""
		if req.PickupDate != nil {
			currentDateTime := util.ToThaiTime(*req.PickupDate.Time)
			currentDateTimeStr = currentDateTime.Format("2006-01-02T15:04:05+07:00")

			currentDate = currentDateTime.Format(orderFlowID.CustomerPickup.SmsToCustomer.CompletedSms.DateFormat)
		} else {
			currentDateTime := util.ToThaiTime(time.Now())
			currentDateTimeStr = currentDateTime.Format("2006-01-02T15:04:05+07:00")

			currentDate = currentDateTime.Format(orderFlowID.CustomerPickup.SmsToCustomer.CompletedSms.DateFormat)
		}

		msisdn := orderData.CustomerInfo.ContactNo
		if strings.HasPrefix(msisdn, "0") {
			msisdn = "66" + msisdn[1:]
		}

		var whatupReq whatup.WhatupSendSmsRequest
		templateContent := ""
		if req.OrderStateCode == "COMPLETED_PICK_UP" {
			destinationShopName := orderData.SaleInfo.TargetDealerName
			templateContent = orderFlowID.CustomerPickup.SmsToCustomer.CompletedSms.TemplateContent
			templateContent = strings.ReplaceAll(templateContent, "[orderid]", req.OrderNumber)
			templateContent = strings.ReplaceAll(templateContent, "[systemdate]", currentDate)
			templateContent = strings.ReplaceAll(templateContent, "[destinationshopname]", destinationShopName)

			whatupReq = whatup.WhatupSendSmsRequest{
				DateTime:          currentDateTimeStr,
				ServiceId:         orderFlowID.CustomerPickup.SmsToCustomer.CompletedSms.ServiceId,
				Msisdn:            msisdn,
				SourceAddressInfo: orderFlowID.CustomerPickup.SmsToCustomer.CompletedSms.ShortCode,
				TemplateContent:   templateContent,
				Language:          "TH",
			}
		} else if req.OrderStateCode == "CANCELLED_ORDER" {
			templateContent = orderFlowID.CustomerPickup.SmsToCustomer.CancelSms.TemplateContent
			templateContent = strings.ReplaceAll(templateContent, "[orderid]", req.OrderNumber)

			whatupReq = whatup.WhatupSendSmsRequest{
				DateTime:          currentDateTimeStr,
				ServiceId:         orderFlowID.CustomerPickup.SmsToCustomer.CancelSms.ServiceId,
				Msisdn:            msisdn,
				SourceAddressInfo: orderFlowID.CustomerPickup.SmsToCustomer.CancelSms.ShortCode,
				TemplateContent:   templateContent,
				Language:          "TH",
			}
		}

		confs.ServiceComplat.WhatUpSendSmsService(whatupReq, logModel)
	}

	pickupDateStr := ""
	if req.PickupDate == nil {
		pickupDate := util.ToThaiTime(time.Now())
		pickupDateStr = pickupDate.Format(orderFlowID.CustomerPickup.SmsToCustomer.CompletedSms.DateFormat)
	} else {
		pickupDate := util.ToThaiTime(*req.PickupDate.Time)
		pickupDateStr = pickupDate.Format(orderFlowID.CustomerPickup.SmsToCustomer.CompletedSms.DateFormat)
	}

	channel := orderData.Channel
	upperChannel := strings.ToUpper(orderData.Channel)
	if upperChannel == "TOS" {
		channel = "ทรูสโตร์"
	}

	orderItemList := []customerpickupform.OrderItemList{}
	for _, o := range orderData.OrderInfo.OrderItems {
		description := o.ProductInfo.Name + " " + o.ProductInfo.Color + " " + o.ProductInfo.Capacity + " (MATCODE:" + o.ProductCode + ")"
		orderItem := customerpickupform.OrderItemList{
			ItemDescription: description,
			Qty:             int(o.AmountInfo.Qty),
		}
		orderItemList = append(orderItemList, orderItem)
	}

	trackingIDList := []string{}
	for _, t := range orderData.AirwayBills {
		trackingIDList = append(trackingIDList, t.TrackingID)
	}

	isPickup := false
	if req.OrderStateCode == "COMPLETED_PICK_UP" {
		isPickup = true
	}

	if req.OrderStateCode == "COMPLETED_PICK_UP" {
		customerPickupReq := customerpickupform.CustomerPickupFormRequest{
			Watermark:         "",
			Signature:         req.Signature,
			CustomerFullName:  orderData.CustomerInfo.Title + orderData.CustomerInfo.FirstName + " " + orderData.CustomerInfo.LastName,
			CustomerContactNo: orderData.CustomerInfo.ContactNo,
			OrderNumber:       orderData.OrderInfo.OrderNumber,
			Channel:           channel,
			PickupDate:        pickupDateStr,
			TargetDealerName:  orderData.SaleInfo.TargetDealerName,
			PickupFullname:    req.PickupCustomerInfo.Title + req.PickupCustomerInfo.FirstName + " " + req.PickupCustomerInfo.LastName,
			PickupNo:          req.PickupCustomerInfo.ContactNo,
			OrderItemList:     orderItemList,
			TrackingIDList:    trackingIDList,
			IsPickup:          isPickup,
			PickupReason:      req.OrderReasonName,
			UserID:            claims.EmployeeID,
			UserFullName:      claims.ThaiName,
		}

		customerPickupFormResp, err := confs.ServiceCustomerPickupForm.CustomerPickupFormService(customerPickupReq, logModel, token)
		if err != nil {
			response.Code = "500"
			response.Message = "Internal server error"
			return &response, err
		}

		uploadDocUUID := uuid.NewString()

		submitDate := util.ToThaiTime(time.Now())
		submitDateStr := submitDate.Format("02/01/2006 17:18:33")

		uploadDocReq := uploaddoc.UploadDocRequest{
			CorrelationID:   uploadDocUUID,
			Channel:         claims.ChannelSystem,
			UserID:          claims.EmployeeID,
			OrderID:         orderData.OrderInfo.OrderNumber,
			UploadType:      "Y",
			SubmitDate:      submitDateStr,
			FileBase64:      customerPickupFormResp.Data,
			FileName:        "pickup_bill_" + uploadDocUUID + ".pdf",
			FileType:        "goods_receipt_document_no_watermark",
			ApplicationCode: orderFlowID.ApplicationCode,
			MimeType:        "application/pdf",
			Destination:     orderFlowID.Destination,
			Overwrite:       "1",
		}
		uploadDocResp, err := confs.ServiceUploadDoc.UploadDocService(uploadDocReq, logModel, logModel.StepName)
		if err != nil {
			response.Code = "500"
			response.Message = "Internal server error"
			return &response, err
		}

		refID := uploadDocResp.Data.RefID

		confDocSystemPath := configs.Conf.Endpoint.HLDOC.Endpoints["docSystemPath"]
		if orderFlowID.CustomerPickup.EmailToCustomer.Action {
			s3Urls := []whatsupemail.S3Urls{}
			for _, file := range req.FileList {
				if file.Type == "goods_receipt_document" {
					getFileInfoReq := getfileinfo.GetFileInfoRequest{
						RefID:           refID,
						ApplicationCode: orderFlowID.ApplicationCode,
					}
					getFileInfoResp, err := confs.ServiceGetFileInfo.GetFileInfoService(getFileInfoReq, logModel)
					if err != nil {
						response.Code = "500"
						response.Message = "Internal server error"
						return &response, err
					}

					s3Data := whatsupemail.S3Urls{
						FileName: "ใบรับสินค้า.pdf",
						URL:      confDocSystemPath + getFileInfoResp.Data.Document[0].TempLocation,
					}
					s3Urls = append(s3Urls, s3Data)
				}
			}

			var pickupDateStr string
			if req.PickupDate == nil {
				pickupDate := util.ToThaiTime(time.Now())
				pickupDateStr = pickupDate.Format(orderFlowID.CustomerPickup.SmsToCustomer.CompletedSms.DateFormat)
			} else {
				pickupDate := util.ToThaiTime(*req.PickupDate.Time)
				pickupDateStr = pickupDate.Format(orderFlowID.CustomerPickup.SmsToCustomer.CompletedSms.DateFormat)
			}

			templateContent := whatsupemail.Parameter{
				OrderID:    orderData.OrderInfo.OrderNumber,
				FirstName:  orderData.CustomerInfo.FirstName,
				LastName:   orderData.CustomerInfo.LastName,
				PickupDate: pickupDateStr,
			}

			whatsupEmailReq := whatsupemail.WhatsUpSendEmailRequest{
				RequestTransactionID: uuid.NewString(),
				TemplateID:           orderFlowID.CustomerPickup.EmailToCustomer.CompletedEmail.TemplateID,
				Version:              orderFlowID.CustomerPickup.EmailToCustomer.CompletedEmail.Version,
				Channel:              "EMAIL_ITO",
				Language:             orderFlowID.CustomerPickup.EmailToCustomer.CompletedEmail.Language,
				Parameters:           templateContent,
				To:                   []string{orderData.CustomerInfo.ContactEmail},
				Dro:                  true,
				S3Urls:               s3Urls,
			}

			confs.ServiceWhatsup.WhatsUpSendEmailService(whatsupEmailReq, logModel)
		}
	}

	var birthDate primitive.DateTime
	if req.PickupCustomerInfo.BirthDate != nil {
		birthDate = primitive.NewDateTimeFromTime(*req.PickupCustomerInfo.BirthDate.Time)
	}

	orderStateCode := req.OrderStateCode
	if req.OrderStateCode == "COMPLETED_PICK_UP" {
		orderStateCode = "COMPLETED"
	}
	orderData.OrderInfo.OrderStateCode = orderStateCode
	pickupCustomerInfo := order.CustomerInfo{
		Title:              req.PickupCustomerInfo.Title,
		FirstName:          req.PickupCustomerInfo.FirstName,
		LastName:           req.PickupCustomerInfo.LastName,
		Identification:     req.PickupCustomerInfo.Identification,
		IdentificationType: req.PickupCustomerInfo.IdentificationType,
		Gender:             req.PickupCustomerInfo.Gender,
		ContactNo:          req.PickupCustomerInfo.ContactNo,
		ContactEmail:       req.PickupCustomerInfo.ContactEmail,
		CustomerAddress:    &order.CustomerAddress{HouseNo: req.PickupCustomerInfo.CustomerAddress.HouseNo, Tumbon: req.PickupCustomerInfo.CustomerAddress.Tumbon, Amphur: req.PickupCustomerInfo.CustomerAddress.Amphur, City: req.PickupCustomerInfo.CustomerAddress.City, Zip: req.PickupCustomerInfo.CustomerAddress.Zip},
		VerifyInfo: &order.VerifyInfo{
			SecondAuthenFlag:   req.PickupCustomerInfo.VerifyInfo.SecondAuthenFlag,
			SecondAuthenBy:     req.PickupCustomerInfo.VerifyInfo.SecondAuthenBy,
			SecondAuthenReason: req.PickupCustomerInfo.VerifyInfo.SecondAuthenReason,
			SecondAuthenByName: req.PickupCustomerInfo.VerifyInfo.SecondAuthenByName,
		},
		IdentificationCardInfo: &order.IdentificationCardInfo{
			Bp1Number:   req.PickupCustomerInfo.IdentificationCardInfo.Bp1Number,
			ChipID:      req.PickupCustomerInfo.IdentificationCardInfo.ChipID,
			IssueDate:   req.PickupCustomerInfo.IdentificationCardInfo.IssueDate,
			IssuerPlace: req.PickupCustomerInfo.IdentificationCardInfo.IssuerPlace,
			ExpireDate:  req.PickupCustomerInfo.IdentificationCardInfo.ExpireDate,
			LaserID:     req.PickupCustomerInfo.IdentificationCardInfo.LaserID,
		},
	}
	if req.PickupCustomerInfo.BirthDate != nil {
		orderData.CustomerInfo.BirthDate = birthDate
	}
	orderData.PickupCustomerInfo = &pickupCustomerInfo

	var documentInfoList []order.DocumentInfo
	for _, file := range req.FileList {
		documentInfo := order.DocumentInfo{
			DocumentType: file.Type,
			DocumentName: "",
			FileName:     "",
			Path:         file.ImagePath,
			RefID:        file.RefID,
		}
		documentInfoList = append(documentInfoList, documentInfo)
	}
	orderData.DocumentInfo = documentInfoList
	orderData.OrderInfo.OrderReasonName = req.OrderReasonName
	orderData.OrderInfo.OrderReasonCode = req.OrderReasonCode
	orderData.OrderInfo.UpdatedDate = primitive.NewDateTimeFromTime(time.Now())
	if req.PickupDate != nil {
		orderData.OrderInfo.PickupDate = primitive.NewDateTimeFromTime(*req.PickupDate.Time)
	} else {
		orderData.OrderInfo.PickupDate = primitive.NewDateTimeFromTime(time.Now())
	}

	orderData.OrderInfo.PickupRemark = req.PickupRemark

	if req.PickupTransactionDate != nil {
		orderData.OrderInfo.PickupTransactionDate = primitive.NewDateTimeFromTime(*req.PickupTransactionDate.Time)
	}

	orderData.OrderInfo.PickupLaterFlag = req.PickupLaterFlag

	err = serviceDaoSaveOrder.UpdateOrder(orderData, logModel)
	if err != nil {
		response.Code = "400"
		response.Message = err.Error()
		response.BizError = err.Error()
		return &response, err
	}

	response = utils.ResponseStandard{
		Code:         "200",
		BizError:     "Success",
		Message:      "Success",
		TranID:       "",
		ApiCode:      "",
		HlTrackingId: "",
	}

	return &response, nil
}

func BuildGetTrackingDetail(orderNumber string) gettrackingdetail.GetTrackingDetailRequest {
	return gettrackingdetail.GetTrackingDetailRequest{
		CorrelationId:  uuid.New().String(),
		OrderNumber:    orderNumber,
		PickupShopCode: "",
		StockType:      "",
		TrackingStatus: "",
		TrackingNumber: "",
		StartDate:      "",
		EndDate:        "",
	}
}

func BuildSaveOmniTrackingRequest(correlationId string, req models.RequestCustomerPickup, pickup models.PickupList, claims util.Claims, flowId *flowconfig.OrderFlowidConfig) saveomnitracking.SaveOmniTrackingRequest {

	//READY_TO_PICK_UP due date > 14
	//REJECTED_TRACKING due date null
	userId := claims.EmployeeID
	if len(userId) == 0 {
		userId = claims.ChannelSystem
	}

	pickupName := claims.ThaiName
	if len(pickupName) == 0 {
		pickupName = claims.ChannelSystem
	}

	saveOmniTrackingReq := saveomnitracking.SaveOmniTrackingRequest{
		CorrelationID:      correlationId,
		OrderNumber:        req.OrderNumber,
		Channel:            claims.ChannelSystem, //jwt
		TrackingNumber:     pickup.TrackingID,
		TrackingStatus:     req.OrderStateCode,
		StatusReason:       req.OrderReasonName,
		CustomerPickupName: req.PickupCustomerInfo.FirstName + " " + req.PickupCustomerInfo.LastName,
		CustomerContact:    req.PickupCustomerInfo.ContactNo,
		PickupName:         pickupName, //jwt ชื่อ นามสกุล
		UserID:             userId,     //jwt id
		Remark:             req.PickupRemark,
	}

	pickupDateStr := ""
	var pickupDate time.Time
	if req.PickupDate != nil {
		pickupDate = util.ToThaiTime(*req.PickupDate.Time)
		pickupDateStr = pickupDate.Format("2006/01/02")
	}

	saveOmniTrackingReq.PickupLaterDate = pickupDateStr

	var imageList []saveomnitracking.ImagePathList
	for _, image := range pickup.ImageList {
		imagePath := image.ImagePath
		if image.ImagePath == "" {
			imagePath = "no path file"
		}
		imageList = append(imageList, saveomnitracking.ImagePathList{
			RefID:     image.RefID,
			ImagePath: imagePath,
			Type:      "IMAGE",
		})
	}

	for _, file := range req.FileList {
		imagePath := file.ImagePath
		if file.ImagePath == "" {
			imagePath = "no path file"
		}
		imageList = append(imageList, saveomnitracking.ImagePathList{
			RefID:     file.RefID,
			ImagePath: imagePath,
			Type:      file.Type,
		})
	}

	saveOmniTrackingReq.ImagePathList = imageList
	return saveOmniTrackingReq
}

func (confs *ServiceCustomerPickup) CallUploadType(req models.RequestCustomerPickup, logModel logger.LogModel, claims util.Claims) (*utils.ResponseStandard, error) {
	var response utils.ResponseStandard
	uploadTypReq := uploadtype.UploadTypeRequest{
		CorrelationID:   req.CorrelationID,
		Channel:         claims.ChannelSystem,
		UserID:          claims.ThaiName,
		RefID:           claims.RefID,
		OrderID:         req.OrderNumber,
		ApplicationCode: "tos",
		UploadType:      "Y",
	}

	uploadTypeResp, err := confs.ServiceUploadType.UploadTypeService(uploadTypReq, logModel, logModel.StepName)
	if err != nil {
		response.Code = "500"
		response.Message = "Internal server error"
		return &response, err
	}

	if uploadTypeResp.Code != "200" {
		response.Code = "500"
		response.Message = err.Error()
		return &response, err
	}

	for _, pickup := range req.PickupList {
		uploadTypReq := uploadtype.UploadTypeRequest{
			CorrelationID:   req.CorrelationID,
			Channel:         claims.ChannelSystem,
			UserID:          claims.ThaiName,
			RefID:           claims.RefID,
			OrderID:         pickup.TrackingID,
			ApplicationCode: "tos",
			UploadType:      "Y",
		}

		uploadTypeResp, err := confs.ServiceUploadType.UploadTypeService(uploadTypReq, logModel, logModel.StepName)
		if err != nil {
			response.Code = "500"
			response.Message = "Internal server error"
			return &response, err
		}

		if uploadTypeResp.Code != "200" {
			response.Code = "500"
			response.Message = err.Error()
			return &response, err
		}
	}

	return &response, nil
}
