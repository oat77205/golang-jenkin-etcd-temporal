package docsystem

import (
	"bytes"
	"encoding/base64"
	"encoding/gob"
	"hl-order-api/configs"
	"hl-order-api/routes/models"
	"hl-order-api/services/flowconfig"
	"hl-order-api/services/order"
	"hl-order-api/util"
	"strconv"
	"time"

	"github.com/mitchellh/mapstructure"

	"gitlab.com/true-itsd/iservicemax/omni/api/ms/doc-system/services/app4in1"
	"gitlab.com/true-itsd/iservicemax/omni/api/ms/doc-system/services/claim"
	"gitlab.com/true-itsd/iservicemax/omni/api/ms/doc-system/services/uploaddoc"
	"gitlab.com/true-itsd/iservicemax/omni/pkg/common-utils-backend/services/logger"
	"gitlab.com/true-itsd/iservicemax/omni/pkg/common-utils-backend/utils"
)

type IServiceDocSystem interface {
	ServiceApp4in1(req models.GenarateDocmentRequest, orderData *order.Order, logModel logger.LogModel) (utils.Result, *utils.ResponseStandard)
	ServiceClaim(req models.GenarateDocmentRequest, logModel logger.LogModel) (utils.Result, *utils.ResponseStandard)
	ServiceUploadType(req models.GenarateDocmentRequest, claims util.Claims, respFile *utils.ResponseStandard, orderFlowid *flowconfig.OrderFlowidConfig, logModel logger.LogModel) (utils.Result, *DocumentResponse)
}

type ServiceDocSystem struct {
}

func NewIServiceDocSystem() IServiceDocSystem {
	return &ServiceDocSystem{}
}

func (r *ServiceDocSystem) ServiceApp4in1(req models.GenarateDocmentRequest, orderData *order.Order, logModel logger.LogModel) (utils.Result, *utils.ResponseStandard) {
	callApp4in1 := app4in1.NewCallApp4in1(configs.Conf.Endpoint.Report)
	serviceApp4in1 := app4in1.NewServiceApp4in1(callApp4in1)

	sameAddress, _ := strconv.ParseBool(req.GenerateDocumentInfo.SameAddress)
	pdpaConsent, _ := strconv.ParseBool(req.GenerateDocumentInfo.PdpaConsent)
	tmnConsent, _ := strconv.ParseBool(req.GenerateDocumentInfo.TmnConsent)
	payMonthAmount, _ := strconv.ParseFloat(req.GenerateDocumentInfo.TrueOnline.PayMonthAmount, 64)
	incomingPrice, _ := strconv.Atoi(req.GenerateDocumentInfo.TrueOnline.IncomingPrice)
	monthPay, _ := strconv.Atoi(req.GenerateDocumentInfo.TrueOnline.MonthPay)
	addPoint, _ := strconv.Atoi(req.GenerateDocumentInfo.TrueVision.AddPoint)
	trueVisionMonthPay, _ := strconv.Atoi(req.GenerateDocumentInfo.TrueVision.MonthPay)
	trueMoveHMonthPay, _ := strconv.Atoi(req.GenerateDocumentInfo.TrueMoveH.MonthPay)

	documents := getDocuments(orderData.CustomerInfo.IdentificationType)
	result, respApp4in1 := serviceApp4in1.App4in1Service(app4in1.App4in1Request{
		IsResume:      false,
		Type:          "NOT",
		Watermark:     req.GenerateDocumentInfo.Watermark,
		MaxSizeMB:     req.GenerateDocumentInfo.MaxSizeMB,
		Date:          req.GenerateDocumentInfo.Date,
		OrderID:       req.OrderInfo.OrderNumber,
		FullName:      req.GenerateDocumentInfo.FullName,
		TitleName:     req.GenerateDocumentInfo.TitleName,
		Documents:     documents,
		IDNumber:      req.GenerateDocumentInfo.IdNumber,
		DobFormatTh:   req.GenerateDocumentInfo.DobFormatTh,
		ExpireDay:     req.GenerateDocumentInfo.Customer.ExpireDate,
		Status:        req.GenerateDocumentInfo.Status,
		Nationality:   req.GenerateDocumentInfo.Nationality,
		Job:           req.GenerateDocumentInfo.Job,
		WorkYear:      req.GenerateDocumentInfo.WorkYear,
		WorkMonth:     req.GenerateDocumentInfo.WorkMonth,
		ContactNo:     req.GenerateDocumentInfo.ContactNo,
		Email:         req.GenerateDocumentInfo.Email,
		SameAddress:   sameAddress,
		PdpaConsent:   pdpaConsent,
		TmnConsent:    tmnConsent,
		StaffNo:       req.GenerateDocumentInfo.StaffNo,
		DealerNo:      req.GenerateDocumentInfo.DealerNo,
		DealerName:    req.GenerateDocumentInfo.DealerName,
		CitizenBase64: req.GenerateDocumentInfo.CitizenBase64,
		Address: app4in1.Address{
			No:          req.GenerateDocumentInfo.Address.HouseNo,
			Building:    req.GenerateDocumentInfo.Address.BuildingName,
			Swine:       req.GenerateDocumentInfo.Address.Moo,
			Alley:       req.GenerateDocumentInfo.Address.Soi,
			Road:        req.GenerateDocumentInfo.Address.StreetName,
			Subdistrict: req.GenerateDocumentInfo.Address.Tumbon,
			District:    req.GenerateDocumentInfo.Address.Amphur,
			Province:    req.GenerateDocumentInfo.Address.City,
			PostalCode:  req.GenerateDocumentInfo.Address.Zip,
		},
		BillingAddress: app4in1.BillingAddress{
			No:          req.GenerateDocumentInfo.BillingAddress.HouseNo,
			Building:    req.GenerateDocumentInfo.BillingAddress.BuildingName,
			Swine:       req.GenerateDocumentInfo.BillingAddress.Moo,
			Alley:       req.GenerateDocumentInfo.BillingAddress.Soi,
			Road:        req.GenerateDocumentInfo.BillingAddress.StreetName,
			Subdistrict: req.GenerateDocumentInfo.BillingAddress.Tumbon,
			District:    req.GenerateDocumentInfo.BillingAddress.Amphur,
			Province:    req.GenerateDocumentInfo.BillingAddress.City,
			PostalCode:  req.GenerateDocumentInfo.BillingAddress.Zip,
		},
		TrueOnline: app4in1.TrueOnline{
			IsNew:           *req.GenerateDocumentInfo.TrueOnline.IsNew,
			IsInternet:      *req.GenerateDocumentInfo.TrueOnline.IsInternet,
			InternetDetail:  req.GenerateDocumentInfo.TrueOnline.InternetDetail,
			IsHomePhone:     *req.GenerateDocumentInfo.TrueOnline.IsHomePhone,
			HomePhoneDetail: req.GenerateDocumentInfo.TrueOnline.HomePhoneDetail,
			ServiceDetail:   req.GenerateDocumentInfo.TrueOnline.ServiceDetail,
			PayMonthAmount:  payMonthAmount,
			InstallDate:     req.GenerateDocumentInfo.TrueOnline.InstallDate,
			IncomingPrice:   incomingPrice,
			MonthPay:        monthPay,
		},
		TrueVision: app4in1.TrueVision{
			IsNew:             *req.GenerateDocumentInfo.TrueVision.IsNew,
			MemberNo:          req.GenerateDocumentInfo.TrueVision.MemberNo,
			Package:           req.GenerateDocumentInfo.TrueVision.Packages,
			AdditionalPackage: req.GenerateDocumentInfo.TrueVision.AdditionalPackage,
			AddPoint:          addPoint,
			InstallDate:       req.GenerateDocumentInfo.TrueVision.InstallDate,
			MonthPay:          trueVisionMonthPay,
		},
		TrueMoveH: app4in1.TrueMoveH{
			IsChange:       *req.GenerateDocumentInfo.TrueMoveH.IsChange,
			IsNew:          *req.GenerateDocumentInfo.TrueMoveH.IsNew,
			Msisdn:         req.GenerateDocumentInfo.TrueMoveH.Msisdn,
			ChangeMobileNo: req.GenerateDocumentInfo.TrueMoveH.ChangeMobileNo,
			ChangeRefNo:    req.GenerateDocumentInfo.TrueMoveH.ChangeRefNo,
			ChangeFrom:     req.GenerateDocumentInfo.TrueMoveH.ChangeFrom,
			Package:        req.GenerateDocumentInfo.TrueMoveH.Packages,
			MonthPay:       trueMoveHMonthPay,
		},
	}, logModel)

	return result, respApp4in1
}

func (r *ServiceDocSystem) ServiceClaim(req models.GenarateDocmentRequest, logModel logger.LogModel) (utils.Result, *utils.ResponseStandard) {
	callClaim := claim.NewCallClaim(configs.Conf.Endpoint.Report)
	serviceClaim := claim.NewServiceApp4in1(callClaim)

	finalPrice, _ := strconv.Atoi(req.GenerateDocumentInfo.DocClaim.FinalPrice)
	advancePayment, _ := strconv.Atoi(req.GenerateDocumentInfo.DocClaim.AdvancePayment)
	extraAdvanceAmount, _ := strconv.Atoi(req.GenerateDocumentInfo.DocClaim.ExtraAdvanceAmount)
	productPrice, _ := strconv.Atoi(req.GenerateDocumentInfo.DocClaim.ProductPrice)

	result, respClaim := serviceClaim.ClaimService(claim.ClaimRequest{
		FullName:           req.GenerateDocumentInfo.FullName,
		Date:               req.GenerateDocumentInfo.Date,
		IDNumber:           req.GenerateDocumentInfo.IdNumber,
		FinalPrice:         finalPrice,
		AdvancePayment:     advancePayment,
		ExtraAdvanceAmount: extraAdvanceAmount,
		ProductPrice:       productPrice,
		PricePlan:          req.GenerateDocumentInfo.DocClaim.PricePlan,
		ContractTerm:       req.GenerateDocumentInfo.DocClaim.ContractTerm,
		IsResume:           false,
		Type:               "NOT",
		Msisdn:             req.GenerateDocumentInfo.ContactNo,
		CitizenBase64:      req.GenerateDocumentInfo.CitizenBase64,
		Watermark:          req.GenerateDocumentInfo.Watermark,
		MaxSizeMB:          req.GenerateDocumentInfo.MaxSizeMB,
		ModelDesc:          req.GenerateDocumentInfo.DocClaim.ModelDesc,
		IDType:             req.GenerateDocumentInfo.DocClaim.IdType,
		Signature:          req.GenerateDocumentInfo.Signature,
	}, logModel)

	return result, respClaim
}

func (r *ServiceDocSystem) ServiceUploadType(req models.GenarateDocmentRequest, claims util.Claims, respFile *utils.ResponseStandard, orderFlowid *flowconfig.OrderFlowidConfig, logModel logger.LogModel) (utils.Result, *DocumentResponse) {
	result := new(utils.Result)
	response := new(DocumentResponse)

	callUploadDoc := uploaddoc.NewCallUploadDoc(configs.Conf.Endpoint.HLDOC)
	serviceUploadDoc := uploaddoc.NewServiceUploadDoc(callUploadDoc)

	fileBase64, err := convertInterfaceTobytes(respFile.Data)
	if err != nil {
		result.Code = "500"
		result.Description = err.Error()
		return *result, nil
	}
	resultUploadDoc, respUploadDoc := serviceUploadDoc.UploadDocService1Log(uploaddoc.UploadDocRequest{
		CorrelationID:   req.CorrelationID,
		Channel:         claims.ChannelSystem,
		UserID:          claims.ChannelSystem,
		OrderID:         req.OrderInfo.OrderNumber,
		UploadType:      orderFlowid.DocSystem.UploadType,
		SubmitDate:      time.Now().Format("02/01/2006 15:04:05"),
		FileBase64:      *fileBase64,
		FileName:        req.GenerateDocumentInfo.DocumentType + "_" + req.CorrelationID + ".pdf",
		FileType:        req.GenerateDocumentInfo.DocumentType,
		ApplicationCode: orderFlowid.DocSystem.ApplicationCode,
		MimeType:        "application/pdf",
		Destination:     orderFlowid.DocSystem.Destination,
		Overwrite:       strconv.FormatBool(orderFlowid.DocSystem.Overwrite),
	}, logModel, "")

	if resultUploadDoc.Code != "200" {
		result.Code = resultUploadDoc.Code
		result.Description = resultUploadDoc.Description
		return *result, nil
	}

	mapstructure.Decode(respUploadDoc.Data, &response)
	result.Code = "200"
	result.Description = "Success"
	return *result, response
}

func getDocuments(identificationType string) string {
	documents := ""
	if identificationType == "I" {
		documents = "บัตรประจำตัวประชาชน"
	} else if identificationType == "A" {
		documents = "บัตรประจำตัวคนต่างด้าว"
	} else if identificationType == "H" {
		documents = "หนังสือเดินทาง/Personal Identity"
	}
	return documents
}

func convertInterfaceTobytes(data interface{}) (*string, error) {
	buf := new(bytes.Buffer)
	enc := gob.NewEncoder(buf)
	if err := enc.Encode(data); err != nil {
		return nil, err
	}

	s := base64.StdEncoding.EncodeToString(buf.Bytes())
	return &s, nil
}
