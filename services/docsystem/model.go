package docsystem

type DocumentResponse struct {
	Document struct {
		DocumentType  string
		FileType      string
		Size          string
		TempETag      string
		TempLocation  string
		TruedocRefID  string
		TruedocStatus string
	}
	RefID string
}
