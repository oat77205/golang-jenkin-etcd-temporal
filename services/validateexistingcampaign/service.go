package validateexistingcampaign

import (
	"hl-order-api/routes/models"
	"hl-order-api/util"

	"gitlab.com/true-itsd/iservicemax/omni/api/ms/gcs/services/validateprivilegesubscriberbymobile"
	"gitlab.com/true-itsd/iservicemax/omni/api/ms/privilege/services/getcampaign"
	"gitlab.com/true-itsd/iservicemax/omni/pkg/common-utils-backend/services/logger"
	"gitlab.com/true-itsd/iservicemax/omni/pkg/common-utils-backend/utils"
)

type IServiceValidateExistingCampaign interface {
	ServiceValidateExistingCampaign(models.RequestValidateExistingCampaign, util.Claims, logger.LogModel) utils.ResponseStandard
	GetCampaignService(campaignCode string, logModel logger.LogModel) (string, utils.ResponseStandard)
}

type ServiceValidateExistingCampaign struct {
	config          utils.EtcdApiConfig
	configPrivilege utils.EtcdApiConfig
}

func NewIServiceValidateExistingCampaign(config utils.EtcdApiConfig, configPrivilege utils.EtcdApiConfig) IServiceValidateExistingCampaign {
	return &ServiceValidateExistingCampaign{
		config:          config,
		configPrivilege: configPrivilege,
	}
}

func (confs *ServiceValidateExistingCampaign) ServiceValidateExistingCampaign(req models.RequestValidateExistingCampaign, claims util.Claims, logModel logger.LogModel) utils.ResponseStandard {
	serviceCode := req.SubscriberInfo.ServiceCode
	response := utils.ResponseStandard{}
	response.Code = util.Code200
	response.BizError = "success"
	response.Message = "Success"
	if serviceCode == "" {
		code, respGetCampaign := confs.GetCampaignService(req.CampaignInfo.CampaignCode, logModel)
		if respGetCampaign.Code != util.Code200 {
			response.Code = respGetCampaign.Code
			response.BizError = respGetCampaign.BizError
			response.Message = respGetCampaign.Message
			response.System = util.System_PRIVILEGE
			return response
		}
		serviceCode = code
	}

	reqPrivilegeSubscriberByMobileService := SetStructToPrivilegeSubscriberByMobileService(req, serviceCode)
	confValidateExistingCampaign := util.ConvertGetEndpointAuthentication(confs.config, "validateExistingCampaign", "validateprivilegesubscriberbymobile")
	newValidateExistingCampaign := validateprivilegesubscriberbymobile.NewCalPrivilegeSubscriberByMobile(confValidateExistingCampaign)
	servciealidateExistingCampaign := validateprivilegesubscriberbymobile.NewServicePrivilegeSubscriberByMobile(newValidateExistingCampaign)
	resp := servciealidateExistingCampaign.PrivilegeSubscriberByMobileService(reqPrivilegeSubscriberByMobileService, logModel)
	if resp.Code != util.Code200 {
		response.Code = resp.Code
		response.BizError = resp.BizError
		response.Message = resp.Message
		response.System = util.System_GCS
		response.Data = resp.Data
	}
	return response
}

func SetStructToPrivilegeSubscriberByMobileService(req models.RequestValidateExistingCampaign, serviceCode string) validateprivilegesubscriberbymobile.ValidatePrivilegeSubscriberByMobileRequest {
	response := validateprivilegesubscriberbymobile.ValidatePrivilegeSubscriberByMobileRequest{
		CorrelationID: req.CorrelationID,
		Channel:       "headless",
		UserID:        req.Channel,
		ProductCode:   req.SubscriberInfo.ProductCode,
		ServiceCode:   serviceCode,
		ServiceID:     req.SubscriberInfo.SubscriberNumber,
		IDNumber:      req.CustomerInfo.Identification,
		PartnerCode:   req.SaleInfo.DealerCode,
		CompanyCode:   req.SubscriberInfo.CompanyCode,
		CustomerType:  req.CustomerInfo.CustomerType,
		CampaignCode:  req.CampaignInfo.CampaignCode,
		CampaignType:  req.CampaignInfo.CampaignType,
	}
	return response
}
func (confs *ServiceValidateExistingCampaign) GetCampaignService(campaignCode string, logModel logger.LogModel) (string, utils.ResponseStandard) {
	serviceCode := ""
	reqCampaign := getcampaign.GetCampaignRequest{}
	reqCampaign.CampaignCode = campaignCode
	resp := utils.ResponseStandard{}
	resp.Code = util.Code200
	resp.BizError = "SUCCESS"
	resp.Message = "Success"
	confGetCampaignService := util.ConvertGetEndpointAuthentication(confs.configPrivilege, "getCampaignService", "getCampaignService")
	newGetCampaign := getcampaign.NewGetCampaign(confGetCampaignService)
	respGetCampaign, err := newGetCampaign.GetCampaign(reqCampaign, logModel)
	if err != nil {
		resp.Code = util.Code500
		resp.BizError = "ERROR"
		resp.Message = err.Error()
	}
	if len(respGetCampaign) > 0 && respGetCampaign[0].Code != "" {
		serviceCode = respGetCampaign[0].Code
	}

	return serviceCode, resp
}
