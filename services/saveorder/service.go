package saveorder

import (
	"hl-order-api/routes/models"
	"hl-order-api/services/flowconfig"
	"hl-order-api/services/inventory"
	"hl-order-api/services/order"
	"hl-order-api/services/orderhistory"
	"hl-order-api/services/piminfo"
	"hl-order-api/services/shopsearch"
	"time"

	"hl-order-api/util"

	"github.com/gofiber/fiber/v2"
	"gitlab.com/true-itsd/iservicemax/omni/api/ms/pim/services/getpiminfo"
	"gitlab.com/true-itsd/iservicemax/omni/api/ms/sale-profile/services/gshopsearch"
	"gitlab.com/true-itsd/iservicemax/omni/pkg/common-utils-backend/services/logger"
	"gitlab.com/true-itsd/iservicemax/omni/pkg/common-utils-backend/utils"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type IServiceSaveOrder interface {
	ServiceSaveOrder(models.RequestSaveOrder, util.Claims, *flowconfig.OrderFlowidConfig, logger.LogModel) (bool, utils.ResponseStandard)
}

type ServiceSaveOrder struct {
	Config    utils.EtcdApiConfig
	Databases utils.SystemDatabase
}

func NewIServiceSaveOrder(config utils.EtcdApiConfig, db utils.SystemDatabase) IServiceSaveOrder {
	return &ServiceSaveOrder{
		Config:    config,
		Databases: db,
	}
}

func (in *ServiceSaveOrder) ServiceSaveOrder(Req models.RequestSaveOrder, Claims util.Claims, OrderFlowid *flowconfig.OrderFlowidConfig, LogModel logger.LogModel) (bool, utils.ResponseStandard) {
	var response utils.ResponseStandard
	serviceDaoSaveOrder := order.NewIServiceDaoOrder(in.Databases)

	err, orderData := serviceDaoSaveOrder.GetOrder(Req.OrderInfo.OrderNumber, Claims.ChannelSystem, LogModel)
	respShopSearchUse := gshopsearch.GShopSearchResponse{}
	if err != nil {
		response.Code = util.Code500
		response.BizError = fiber.ErrInternalServerError.Message
		response.Message = err.Error()
		response.System = util.System_HL_ORDER_API
		return false, response
	}

	if OrderFlowid.IsPickup {
		if orderData != nil {
			response = util.GetResponse200(response)
			return true, response
		}
	} else {
		if orderData == nil {
			response.Code = util.Code404
			response.BizError = "NOTFOUND"
			response.Message = "Data not found"
			response.System = util.System_HL_ORDER_API
			return false, response
		}
		if orderData.OrderInfo.OrderStateCode == "COMPLETED" {
			response.Code = util.Code422
			response.BizError = "ORDERCOMPLETED"
			response.Message = "The order number is in Completed status."
			response.System = util.System_HL_ORDER_API
			return false, response
		}
	}

	defer func() {
		if response.Code == "200" {
			serviceDaoOrderHistory := orderhistory.NewIServiceDaoOrderHistory(in.Databases)
			serviceDaoOrderHistory.InsertOrderHistory(orderhistory.OrderHistory{
				CorrelationID:       Req.CorrelationID,
				OrderNumber:         Req.OrderInfo.OrderNumber,
				UserID:              Claims.ChannelSystem,
				ActivityName:        "SaveOrder",
				ActivityDescription: "SaveOrder " + Req.OrderInfo.OrderStateCode,
				CreatedDate:         primitive.NewDateTimeFromTime(time.Now()),
				CreatedBy:           Claims.ChannelSystem,
			}, LogModel)
		}
	}()

	reqShopSearch := gshopsearch.GShopSearchRequest{}
	reqShopSearch.Channel = Claims.ChannelSystem
	reqShopSearch.CorrelationID = Req.CorrelationID
	reqShopSearch.UserID = Claims.ChannelSystem

	if OrderFlowid.IsPickup {
		reqShopSearch.SearchList = []gshopsearch.SearchList{
			{
				Type:  "partnerCode",
				Value: []string{Req.SaleInfo.DealerCode, Req.SaleInfo.OwnerDealer, Req.SaleInfo.TargetDealerCode},
			},
		}
	} else {
		if Req.SaleInfo.DealerCode != "" || Req.SaleInfo.SaleCode != "" {
			reqShopSearch.SearchList = []gshopsearch.SearchList{
				{
					Type:  "partnerCode",
					Value: []string{Req.SaleInfo.DealerCode, Req.SaleInfo.SaleCode},
				},
			}

		}

	}
	if len(reqShopSearch.SearchList) > 0 {
		serviceShopSearch := shopsearch.NewIServiceGetShopSearch(in.Config)
		respShopSearch, code, bizError, err := serviceShopSearch.ServiceGetShopSearch(reqShopSearch, LogModel)
		if err != nil {
			response.Code = code
			response.BizError = bizError
			response.Message = err.Error()
			response.System = util.System_GCS
			return false, response
		}
		respShopSearchUse = *respShopSearch
	}

	if len(Req.OrderInfo.OrderItems) > 0 {
		serviceGetPiminfo := piminfo.NewIServiceGetPiminfo(in.Config)
		respPim, code, bizError, err := serviceGetPiminfo.ServiceGetPiminfo(piminfo.RequestPiminfo{
			CorrelationID: Req.CorrelationID,
			OrderItems:    Req.OrderInfo.OrderItems,
		}, Claims, LogModel)
		if err != nil {
			response.Code = code
			response.BizError = bizError
			response.Message = err.Error()
			response.System = util.System_REALTIME_INVENTORY
			return false, response
		}
		Req = TranformProductInfo(Req, *respPim)
	}

	if OrderFlowid.IsPickup {
		err = serviceDaoSaveOrder.InsertOrder(DataInsertOrder(Req, Claims, &respShopSearchUse, OrderFlowid), LogModel)
	} else {
		err = serviceDaoSaveOrder.UpdateOrder(DataInsertOrder(Req, Claims, &respShopSearchUse, OrderFlowid), LogModel)
	}
	if err != nil {
		response.Code = util.Code500
		response.BizError = fiber.ErrInternalServerError.Message
		response.Message = err.Error()
		response.System = util.System_REALTIME_INVENTORY
		return false, response
	}

	if OrderFlowid.IsPickup {
		serviceGetSaveOmniTracking := inventory.NewIServiceGetSaveOmniTracking(in.Config)
		code, bizError, err := serviceGetSaveOmniTracking.ServiceGetSaveOmniTracking(Req, Claims, LogModel)
		if err != nil {
			response.Code = code
			response.BizError = bizError
			response.Message = err.Error()
			response.System = util.System_REALTIME_INVENTORY
			return false, response
		}
	}
	response = util.GetResponse200(response)
	return true, response
}

func DataInsertOrder(req models.RequestSaveOrder, claims util.Claims, shopsearch *gshopsearch.GShopSearchResponse, orderFlowid *flowconfig.OrderFlowidConfig) *order.Order {
	var orderItems []order.OrderItems
	orderStateCode := "WAITING_PRODUCT"
	paymentMethodInfos := []order.PaymentMethodInfo{}
	identificationCardInfos := &order.IdentificationCardInfo{}
	if req.CustomerInfo.IdentificationCardInfo != nil {
		identificationCardInfos = &order.IdentificationCardInfo{IssueDate: req.CustomerInfo.IdentificationCardInfo.IssueDate,
			IssuerPlace: req.CustomerInfo.IdentificationCardInfo.IssuerPlace,
			ExpireDate:  req.CustomerInfo.IdentificationCardInfo.ExpireDate}
	}
	if !orderFlowid.IsPickup {
		orderStateCode = req.OrderInfo.OrderStateCode
	}
	if req.OrderInfo.PaymentInfo.PaymentMethodInfo != nil {
		for _, data := range req.OrderInfo.PaymentInfo.PaymentMethodInfo {
			paymentMethodInfo := order.PaymentMethodInfo{
				PaymentMethod:            data.PaymentMethod,
				PaymentMethodDescription: data.PaymentMethodDescription,
				Amount:                   data.Amount,
				CreditCardNo:             data.CreditCardNo,
				CreditCardType:           data.CreditCardType,
				OwnerName:                data.OwnerName,
				ExpireDate:               data.ExpireDate,
				ApproveCode:              data.ApproveCode,
				BankNo:                   data.BankNo,
				BankName:                 data.BankName,
				BankObjectId:             data.BankObjectId,
				BinNum:                   data.BinNum,
				RepCompanyId:             data.RepCompanyId,
			}
			paymentMethodInfos = append(paymentMethodInfos, paymentMethodInfo)
		}
	}
	for _, d := range req.OrderInfo.OrderItems {
		orderItem := order.OrderItems{}
		aDiscount := []order.Discount{}
		if d.AmountInfo.DiscountInfo != nil {
			for _, discount := range d.AmountInfo.DiscountInfo.Discount {
				aDiscount = append(aDiscount, order.Discount{
					DiscountCode:   discount.DiscountCode,
					DiscountAmount: util.ConvertFloat64(discount.DiscountAmount),
					CounponSerial:  discount.CounponSerial,
				})
			}
		}
		orderItem = order.OrderItems{
			Sequence:    d.Sequence,
			ProductCode: d.ProductCode,
			ProductInfo: order.ProductInfo{
				Code:     d.ProductInfo.Code,
				Name:     d.ProductInfo.Name,
				Brand:    d.ProductInfo.Brand,
				Model:    d.ProductInfo.Model,
				Color:    d.ProductInfo.Color,
				Capacity: d.ProductInfo.Capacity,
			},
			ProductType:        d.ProductType,
			PromotionSet:       d.PromotionSet,
			PromotionType:      d.PromotionType,
			Proposition:        d.Proposition,
			GroupID:            d.GroupID,
			InventoryType:      d.InventoryType,
			StockShopCode:      d.StockShopCode,
			StockShopType:      util.GetStockShopType(d.StockShopCode),
			OrderItemStateCode: d.OrderItemStateCode,
		}
		if d.AmountInfo.Price != nil && d.AmountInfo.Qty != nil {
			orderItem.AmountInfo = order.AmountInfo{
				Price: *d.AmountInfo.Price,
				Qty:   *d.AmountInfo.Qty,
			}
		}
		if d.AmountInfo.DepositAmt != nil {
			orderItem.AmountInfo.DepositAmt = *d.AmountInfo.DepositAmt
		}
		if d.AmountInfo.DiscountInfo != nil {
			orderItem.AmountInfo.DiscountInfo = &order.DiscountInfo{
				TotalDiscountAmount: util.ConvertFloat64(d.AmountInfo.DiscountInfo.TotalDiscountAmount),
				Discount:            aDiscount,
			}
		}
		CampaignName := ""
		if d.CampaignInfo != nil {
			if d.CampaignInfo.CampaignName != nil {
				CampaignName = *d.CampaignInfo.CampaignName
			}
			orderItem.CampaignInfo = &order.CampaignInfo{
				CampaignCode: *d.CampaignInfo.CampaignCode,
				CampaignName: CampaignName,
				VerifyKey:    d.CampaignInfo.VerifyKey,
			}
		}
		orderItems = append(orderItems, orderItem)
	}

	now := time.Now()

	orderDB := order.Order{
		CorrelationID: req.CorrelationID,
		FlowID:        req.FlowID,
		Channel:       claims.ChannelSystem,
		SaleInfo: order.SaleInfo{
			DealerCode:       req.SaleInfo.DealerCode,
			DealerName:       GetShopNameTh(req.SaleInfo.DealerCode, shopsearch),
			OwnerDealer:      req.SaleInfo.OwnerDealer,
			OwnerSaleId:      req.SaleInfo.OwnerSaleId,
			OwnerDealerName:  GetShopNameTh(req.SaleInfo.OwnerDealer, shopsearch),
			WebMethodChannel: req.SaleInfo.WebMethodChannel,
			SalePlatform:     req.SaleInfo.SalePlatform,
			TargetDealerCode: req.SaleInfo.TargetDealerCode,
		},

		OrderInfo: order.OrderInfo{
			OrderNumber:    req.OrderInfo.OrderNumber,
			OrderStateCode: orderStateCode,
			CreatedDate:    primitive.NewDateTimeFromTime(now),
			CreatedBy:      claims.ChannelSystem,
			UpdatedDate:    primitive.NewDateTimeFromTime(now),
			UpdatedBy:      claims.ChannelSystem,
			PaymentInfo: order.PaymentInfo{
				CpuId:               req.OrderInfo.PaymentInfo.CpuId,
				RepCompanyObjectId:  req.OrderInfo.PaymentInfo.RepCompanyObjectId,
				OperCompanyObjectId: req.OrderInfo.PaymentInfo.OperCompanyObjectId,
				CashierInfo:         order.CashierInfo(req.OrderInfo.PaymentInfo.CashierInfo),
				PaymentMethodInfo:   &paymentMethodInfos,
				TotalAmount:         *req.OrderInfo.PaymentInfo.TotalAmount,
				PaymentMethod:       req.OrderInfo.PaymentInfo.PaymentMethod,
				CreditCardNo:        req.OrderInfo.PaymentInfo.CreditCardNo,
				OwnerName:           req.OrderInfo.PaymentInfo.OwnerName,
				BankName:            req.OrderInfo.PaymentInfo.BankName,
				BankNo:              req.OrderInfo.PaymentInfo.BankNo,
			},
			OrderItems: orderItems,
		},
		CustomerInfo: order.CustomerInfo{
			Title:                  req.CustomerInfo.Title,
			FirstName:              req.CustomerInfo.FirstName,
			LastName:               req.CustomerInfo.LastName,
			Identification:         req.CustomerInfo.Identification,
			IdentificationType:     req.CustomerInfo.IdentificationType,
			Gender:                 req.CustomerInfo.Gender,
			ContactNo:              req.CustomerInfo.ContactNo,
			ContactEmail:           req.CustomerInfo.ContactEmail,
			IdentificationCardInfo: identificationCardInfos,
		},
	}
	if req.OrderInfo.OrderDate != nil {
		orderDB.OrderInfo.OrderDate = primitive.NewDateTimeFromTime(*req.OrderInfo.OrderDate)
	}
	if req.OrderInfo.PaymentInfo.BillDiscountAmount != nil {
		orderDB.OrderInfo.PaymentInfo.BillDiscountAmount = *req.OrderInfo.PaymentInfo.BillDiscountAmount
	}
	if req.OrderInfo.PaymentInfo.BillOtherPaymentAmount != nil {
		orderDB.OrderInfo.PaymentInfo.BillOtherPaymentAmount = *req.OrderInfo.PaymentInfo.BillOtherPaymentAmount
	}
	if req.OrderInfo.PaymentInfo.PaymentAmount != nil {
		orderDB.OrderInfo.PaymentInfo.PaymentAmount = *req.OrderInfo.PaymentInfo.PaymentAmount
	}
	if req.OrderInfo.OrderDate != nil {
		orderDB.OrderInfo.OrderDate = primitive.NewDateTimeFromTime(*req.OrderInfo.OrderDate)
	}
	if req.OrderInfo.PaymentInfo.ExpireDate != nil {
		orderDB.OrderInfo.PaymentInfo.ExpireDate = primitive.NewDateTimeFromTime(*req.OrderInfo.PaymentInfo.ExpireDate)
	}
	if orderFlowid.IsPickup {
		orderDB.SaleInfo.TargetDealerName = GetShopNameTh(req.SaleInfo.TargetDealerCode, shopsearch)
	}

	if req.CustomerInfo.CustomerAddress != nil {
		orderDB.CustomerInfo.CustomerAddress = &order.CustomerAddress{}
		orderDB.CustomerInfo.CustomerAddress.HouseNo = req.CustomerInfo.CustomerAddress.HouseNo
		orderDB.CustomerInfo.CustomerAddress.BuildingName = req.CustomerInfo.CustomerAddress.BuildingName
		orderDB.CustomerInfo.CustomerAddress.Soi = req.CustomerInfo.CustomerAddress.Soi
		orderDB.CustomerInfo.CustomerAddress.Moo = req.CustomerInfo.CustomerAddress.Moo
		orderDB.CustomerInfo.CustomerAddress.RoomNo = req.CustomerInfo.CustomerAddress.RoomNo
		orderDB.CustomerInfo.CustomerAddress.StreetName = req.CustomerInfo.CustomerAddress.StreetName
		orderDB.CustomerInfo.CustomerAddress.Tumbon = req.CustomerInfo.CustomerAddress.Tumbon
		orderDB.CustomerInfo.CustomerAddress.Amphur = req.CustomerInfo.CustomerAddress.Amphur
		orderDB.CustomerInfo.CustomerAddress.City = req.CustomerInfo.CustomerAddress.City
		orderDB.CustomerInfo.CustomerAddress.Zip = req.CustomerInfo.CustomerAddress.Zip
	}
	if req.CustomerInfo.BirthDate != nil {
		orderDB.CustomerInfo.BirthDate = primitive.NewDateTimeFromTime(*req.CustomerInfo.BirthDate)
	}

	if req.ContactInfo != nil {
		orderDB.ContactInfo.Title = req.ContactInfo.Title
		orderDB.ContactInfo.FirstName = req.ContactInfo.FirstName
		orderDB.ContactInfo.LastName = req.ContactInfo.LastName
		orderDB.ContactInfo.Identification = req.ContactInfo.Identification
		orderDB.ContactInfo.IdentificationType = req.ContactInfo.IdentificationType
		orderDB.ContactInfo.Gender = req.ContactInfo.Gender
		orderDB.ContactInfo.ContactNo = req.ContactInfo.ContactNo
		orderDB.ContactInfo.ContactEmail = req.ContactInfo.ContactEmail
		if req.ContactInfo.CustomerAddress != nil {
			orderDB.ContactInfo.CustomerAddress = &order.CustomerAddress{}
			orderDB.ContactInfo.CustomerAddress.HouseNo = req.ContactInfo.CustomerAddress.HouseNo
			orderDB.ContactInfo.CustomerAddress.BuildingName = req.ContactInfo.CustomerAddress.BuildingName
			orderDB.ContactInfo.CustomerAddress.Soi = req.ContactInfo.CustomerAddress.Soi
			orderDB.ContactInfo.CustomerAddress.Moo = req.ContactInfo.CustomerAddress.Moo
			orderDB.ContactInfo.CustomerAddress.RoomNo = req.ContactInfo.CustomerAddress.RoomNo
			orderDB.ContactInfo.CustomerAddress.StreetName = req.ContactInfo.CustomerAddress.StreetName
			orderDB.ContactInfo.CustomerAddress.Tumbon = req.ContactInfo.CustomerAddress.Tumbon
			orderDB.ContactInfo.CustomerAddress.Amphur = req.ContactInfo.CustomerAddress.Amphur
			orderDB.ContactInfo.CustomerAddress.City = req.ContactInfo.CustomerAddress.City
			orderDB.ContactInfo.CustomerAddress.Zip = req.ContactInfo.CustomerAddress.Zip
		}
		if req.ContactInfo.BirthDate != nil {
			orderDB.ContactInfo.BirthDate = primitive.NewDateTimeFromTime(*req.ContactInfo.BirthDate)
		}
	}

	return &orderDB
}

func TranformProductInfo(req models.RequestSaveOrder, reqPim getpiminfo.GetPiminfoResponse) models.RequestSaveOrder {
	for _, p := range reqPim.Data.ProductList {
		for i, d := range req.OrderInfo.OrderItems {
			name := p.ProductName
			if p.MatCode == d.ProductCode {
				req.OrderInfo.OrderItems[i].ProductInfo.Code = p.MatCode
				if name == "" {
					name = p.MatDescription
				}
				req.OrderInfo.OrderItems[i].ProductInfo.Name = name
				req.OrderInfo.OrderItems[i].ProductInfo.Brand = p.Brand
				req.OrderInfo.OrderItems[i].ProductInfo.Model = p.Model
				req.OrderInfo.OrderItems[i].ProductInfo.Color = p.ColorTH
				req.OrderInfo.OrderItems[i].ProductInfo.Capacity = p.Capacity
			}
		}
	}
	return req
}

func GetShopNameTh(partnerCode string, shopsearch *gshopsearch.GShopSearchResponse) string {
	var shopNameTh string

	if shopsearch != nil && shopsearch.Data != nil {
		for _, d := range shopsearch.Data.ShopInfoList {
			if d.PartnerCode == partnerCode {
				shopNameTh = d.ShopNameTh
				break
			}
		}
	}

	return shopNameTh
}
