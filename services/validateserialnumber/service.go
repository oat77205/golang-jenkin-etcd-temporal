package validateserialnumber

import (
	"hl-order-api/configs"
	"hl-order-api/routes/models"
	"hl-order-api/services/flowconfig"
	"hl-order-api/services/gcs"
	"hl-order-api/services/order"
	"hl-order-api/services/psa"
	"hl-order-api/util"

	"github.com/gofiber/fiber/v2"
	"gitlab.com/true-itsd/iservicemax/omni/api/ms/psa/services/deviceserviceinfo"
	"gitlab.com/true-itsd/iservicemax/omni/pkg/common-utils-backend/services/logger"
	"gitlab.com/true-itsd/iservicemax/omni/pkg/common-utils-backend/utils"
)

type IServiceValidateSerialNumber interface {
	ServiceValidateSerialNumber(req models.ValidateSerialNumberResquest, claims util.Claims, flowid *flowconfig.OrderFlowidConfig, logModel logger.LogModel) (bool, utils.ResponseStandard)
}

type ServiceValidateSerialNumber struct {
}

func NewIServiceValidateSerialNumber() IServiceValidateSerialNumber {
	return &ServiceValidateSerialNumber{}
}

func (r *ServiceValidateSerialNumber) ServiceValidateSerialNumber(req models.ValidateSerialNumberResquest, claims util.Claims, flowid *flowconfig.OrderFlowidConfig, logModel logger.LogModel) (bool, utils.ResponseStandard) {
	var response utils.ResponseStandard

	serviceDaoSaveOrder := order.NewIServiceDaoOrder(configs.Conf.Database)
	err, orderData := serviceDaoSaveOrder.GetOrder(req.OrderInfo.OrderNumber, claims.ChannelSystem, logModel)
	if err != nil {
		response.Code = util.Code500
		response.BizError = fiber.ErrInternalServerError.Message
		response.Message = err.Error()
		response.System = util.System_HL_ORDER_API
		return false, response
	}
	if orderData == nil && err == nil {
		response.Code = util.Code404
		response.BizError = fiber.ErrNotFound.Message
		response.Message = fiber.ErrNotFound.Message
		response.System = util.System_HL_ORDER_API
		return false, response
	}

	if orderData.OrderInfo.OrderStateCode != "PAID" {
		response.Code = util.Code404
		response.BizError = fiber.ErrNotFound.Message
		response.Message = fiber.ErrNotFound.Message
		response.System = util.System_HL_ORDER_API
		return false, response
	}

	serviceGCS := gcs.NewIServiceGCS()
	subscriberNumber := ""
	if orderData.SubscriberInfo != nil {
		subscriberNumber = orderData.SubscriberInfo.SubscriberNumber
	}
	result, _ := serviceGCS.ServicePreverifyExisting(models.PreverifyExistingRequest{
		CorrelationID: req.CorrelationID,
		SubscriberInfo: models.SubscriberInfoPreverifyExisting{
			SubscriberNumber: subscriberNumber,
		},
		InquiryProfileInfo: models.InquiryProfileInfo{
			InquiryProfileFlag: true,
		},
	}, claims, logModel)
	if result.Code != util.Code200 {
		response.Code = result.Code
		response.BizError = result.Description
		response.Message = result.Description
		response.System = util.System_GCS
		return false, response
	}

	resPrivilegeSubscriberByMobile, code, bizError, err := serviceGCS.ServicePrivilegeSubscriberByMobileService(req, orderData, claims, logModel)
	if err != nil {
		response.Code = code
		response.BizError = bizError
		response.Message = err.Error()
		response.System = util.System_GCS
		return false, response
	}

	validateSerialNumberResponse := new(models.ValidateSerialNumberResponse)
	contractProposition := []order.ContractProposition{}
	if len(req.OrderInfo.OrderItem) > 0 {
		servicePsa := psa.NewIServicePSA()
		for n, d := range req.OrderInfo.OrderItem {
			respPsa, code, bizError, err := servicePsa.ServiceGetDeviceServiceInfo(deviceserviceinfo.GetDeviceServiceInfoRequest{
				ReqTrxId: req.CorrelationID,
				Key: []deviceserviceinfo.KeyInfo{
					{
						Name:  d.InventoryType,
						Value: d.Serial,
					},
				},
				User: deviceserviceinfo.UserInfo{
					AppUser:       "HEADLESS",
					ChannelAccess: "HL-TOS",
					ChannelCode:   orderData.SaleInfo.DealerCode,
					ChannelUSer:   claims.ChannelAlias,
					USerCode:      claims.EmployeeID,
					UserLogin:     claims.UserName,
				},
			}, logModel)
			if code != util.Code200 && code != util.Code404 {
				response.Code = code
				response.BizError = bizError
				response.Message = err.Error()
				response.System = util.System_PSA
				return false, response
			}
			errorMessage := ""
			foundFlag := true
			if code == util.Code404 {
				errorMessage = respPsa.Resp.Message
				foundFlag = false
			}
			validateSerialNumberResponse.OrderItem = append(validateSerialNumberResponse.OrderItem, models.OrderItemSN{
				Sequence:      n + 1,
				ProductCode:   d.ProductCode,
				Serial:        d.Serial,
				InventoryType: d.InventoryType,
				SerialStatus:  d.SerialStatus,
				FoundFlag:     foundFlag,
				ErrorMessage:  errorMessage,
			})
		}
	}

	for _, d := range resPrivilegeSubscriberByMobile.Data.SubscriberInfo.ContractPropositionList {
		contractProposition = append(contractProposition, order.ContractProposition{
			ContractStartDate:     d.ContractStartDate,
			ContractExpireDate:    d.ContractExpireDate,
			SocName:               d.SocName,
			Fee:                   d.Fee,
			SocDescription:        d.SocDescription,
			Term:                  d.Term,
			OfferInstanceID:       d.OfferInstanceID,
			OfferGroup:            d.OfferGroup,
			SocType:               d.SocType,
			ContractExpireDateStr: d.ContractExpireDateStr,
		})
	}

	err = serviceDaoSaveOrder.UpdateContractProposition(req.OrderInfo.OrderNumber, contractProposition, logModel)
	if err != nil {
		response.Code = util.Code500
		response.BizError = fiber.ErrInternalServerError.Message
		response.Message = err.Error()
		response.System = util.System_HL_ORDER_API
		return false, response
	}

	response = util.GetResponse200(response)
	response.Data = validateSerialNumberResponse
	listFalse := []int{}
	for _, d := range validateSerialNumberResponse.OrderItem {
		if !d.FoundFlag {
			listFalse = append(listFalse, 1)
		}
	}
	if len(listFalse) != 0 {
		response.Code = util.Code422
		if len(listFalse) == len(validateSerialNumberResponse.OrderItem) {
			response.BizError = fiber.ErrNotFound.Message
			response.Message = fiber.ErrNotFound.Message
		} else if len(listFalse) != len(validateSerialNumberResponse.OrderItem) {
			response.BizError = "Some of results found"
			response.Message = "Some of results found"
		}
	}

	return true, response
}
