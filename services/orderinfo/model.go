package orderinfo

type OrderList struct {
	TotalPage int     `json:"totalPage"`
	OrderList []Order `json:"orderList"`
}

type Order struct {
	OrderNumber        string             `json:"orderNumber"`
	OrderStatus        string             `json:"orderStatus"`
	OrderDate          string             `json:"orderDate"`
	TrackingList       []TrackingList     `json:"trackingList"`
	PaymentBalance     float64            `json:"paymentBalance"`
	CustomerFirstName  string             `json:"customerFirstName"`
	CustomerLastName   string             `json:"customerLastName"`
	CustomerContactNo  string             `json:"customerContactNo"`
	ContactFirstName   string             `json:"contactFirstName"`
	ContactLastName    string             `json:"contactLastName"`
	ContactPhoneNo     string             `json:"contactPhoneNo"`
	PickupCustomerInfo PickupCustomerInfo `json:"pickupCustomerInfo"`
	OwnerDealer        string             `json:"ownerDealer"`
	TargetDeliverDate  string             `json:"targetDeliverDate"`
	TargetDealer       string             `json:"targetDealer"`
	TargetPickUpDate   string             `json:"targetPickUpDate"`
	TrackingLastState  string             `json:"trackingLastState"`
	OrderLastUpdate    string             `json:"orderLastUpdate"`
}

type TrackingList struct {
	TrackingNumber    string `json:"trackingNumber"`
	TrackingStatus    string `json:"trackingStatus"`
	TrackingReason    string `json:"trackingReason"`
	TrackingReasonEng string `json:"trackingReasonEng"`
	WarningMSG        string `json:"warningMSG"`
}

type OrderDetails struct {
	OrderInfo          OrderInfo          `json:"orderInfo"`
	CustomerInfo       CustomerInfo       `json:"customerInfo"`
	ContactInfo        ContactInfo        `json:"contactInfo"`
	OwnerDealer        string             `json:"ownerDealer"`
	TargetDeliverDate  string             `json:"targetDeliverDate"`
	TargetPickUpDate   string             `json:"targetPickUpDate"`
	PickupCustomerInfo PickupCustomerInfo `json:"pickupCustomerInfo"`
	DocumentInfo       []DocumentInfo     `json:"documentInfo"`
	TargetDealer       string             `json:"targetDealer"`
	DisableFunction    bool               `json:"disableFunction"`
	RenderPickupAden   bool               `json:"renderPickupAden"`
	TrackingLastState  string             `json:"trackingLastState"`
}

type OrderInfo struct {
	OrderNumber             string                    `json:"orderNumber"`
	OrderStatus             string                    `json:"orderStatus"`
	OrderReasonName         string                    `json:"orderReasonName"`
	OrderDate               string                    `json:"orderDate"`
	PickupDate              string                    `json:"pickupDate"`
	PickupRemark            string                    `json:"pickupRemark"`
	PickupTransactionDate   string                    `json:"pickupTransactionDate"`
	PickupLaterFlag         bool                      `json:"pickupLaterFlag"`
	OrderItems              []OrderItems              `json:"orderItems"`
	TrackingList            []Tracking                `json:"trackingList"`
	CustomerPickUpTrackings []CustomerPickUpTrackings `json:"customerPickUpTrackings"`
	PaymentInfo             PaymentInfo               `json:"paymentInfo"`
}

type PickupCustomerInfo struct {
	FirstName  string      `json:"firstName,omitempty"`
	LastName   string      `json:"lastName,omitempty"`
	ContactNo  string      `json:"contactNo,omitempty"`
	VerifyInfo *VerifyInfo `json:"verifyInfo,omitempty"`
}

type VerifyInfo struct {
	SecondAuthenFlag   bool   `json:"secondAuthenFlag,omitempty"`
	SecondAuthenBy     string `json:"secondAuthenBy,omitempty"`
	SecondAuthenByName string `json:"secondAuthenByName,omitempty"`
	SecondAuthenReason string `json:"secondAuthenReason,omitempty"`
}

type DocumentInfo struct {
	DocumentType string `json:"documentType,omitempty"`
	RefId        string `json:"refId,omitempty"`
}

type OrderItems struct {
	Sequence    int         `json:"sequence"`
	ProductCode string      `json:"productCode"`
	TrackingId  string      `json:"trackingId"`
	Serial      string      `json:"serial"`
	ProductInfo ProductInfo `json:"productInfo"`
	AmountInfo  AmountInfo  `json:"amountInfo"`
}

type ProductInfo struct {
	Code     string `json:"code"`
	Name     string `json:"name"`
	Brand    string `json:"brand"`
	Model    string `json:"model"`
	Color    string `json:"color"`
	Capacity string `json:"capacity"`
}

type AmountInfo struct {
	Price        float64      `json:"price"`
	Qty          int          `json:"qty"`
	DiscountInfo DiscountInfo `json:"discountInfo"`
}

type DiscountInfo struct {
	TotalDiscountAmount float64    `json:"totalDiscountAmount"`
	Discount            []Discount `json:"discount"`
}

type Discount struct {
	DiscountCode   string  `json:"discountCode"`
	DiscountAmount float64 `json:"discountAmount"`
}

type Tracking struct {
	TrackingNumber    string      `json:"trackingNumber"`
	TrackingStatus    string      `json:"trackingStatus"`
	TrackingReason    string      `json:"trackingReason"`
	TrackingReasonEng string      `json:"trackingReasonEng"`
	GoodsReceiveDate  string      `json:"goodsReceiveDate"`
	ImagePathList     []ImagePath `json:"imagePathList"`
	SaleName          string      `json:"saleName"`
	AdenStatus        string      `json:"adenStatus"`
	AdenStatusDate    string      `json:"adenStatusDate"`
}

type ImagePath struct {
	Type      string `json:"type"`
	RefID     string `json:"refId"`
	ImagePath string `json:"imagePath"`
}

type CustomerPickUpTrackings struct {
	TrackingNumber string      `json:"trackingNumber"`
	TrackingStatus string      `json:"trackingStatus"`
	PickupShop     string      `json:"pickupShop"`
	PickUpDate     string      `json:"pickUpDate"`
	SaleName       string      `json:"saleName"`
	ImagePathList  []ImagePath `json:"imagePathList"`
}
type PaymentInfo struct {
	PaymentMethod          string  `json:"paymentMethod"`
	PaymentAmount          float64 `json:"paymentAmount"`
	PaymentBalance         float64 `json:"paymentBalance"`
	BillDiscountAmount     float64 `json:"billDiscountAmount"`
	BillOtherPaymentAmount float64 `json:"billOtherPaymentAmount"`
}

type CustomerInfo struct {
	FirstName      string `json:"firstName"`
	LastName       string `json:"lastName"`
	ContactNo      string `json:"contactNo"`
	ContactEmail   string `json:"contactEmail"`
	Identification string `json:"identification"`
}

type ContactInfo struct {
	FirstName      string `json:"firstName"`
	LastName       string `json:"lastName"`
	ContactNo      string `json:"contactNo"`
	Identification string `json:"identification"`
}
