package orderinfo

import (
	"hl-order-api/configs"
	"hl-order-api/routes/models"
	"hl-order-api/services/order"
	"hl-order-api/util"
	"math"
	"time"

	"gitlab.com/true-itsd/iservicemax/omni/api/ms/inventory/services/gettrackingdetail"
	"gitlab.com/true-itsd/iservicemax/omni/api/ms/inventory/services/gettrackinghistory"
	"gitlab.com/true-itsd/iservicemax/omni/api/ms/sale-profile/services/gshopsearch"
	"gitlab.com/true-itsd/iservicemax/omni/pkg/common-utils-backend/services/logger"
	"gitlab.com/true-itsd/iservicemax/omni/pkg/common-utils-backend/utils"
)

type IOrderService interface {
	SearchOrderList(req models.RequestSearchOrderList, claims util.Claims, logModel logger.LogModel) (resp utils.ResponseStandard, valid bool)
	GetOrderDetails(req models.RequestGetOrderDetails, claims util.Claims, logModel logger.LogModel) (resp utils.ResponseStandard, valid bool)
}

type OrderService struct {
	trackingServiec     gettrackingdetail.IServiceGetTrackingDetail
	trackingHistService gettrackinghistory.IServiceGetTrackingHistory
	shopSearchService   gshopsearch.IServiceGShopSearch
	orderService        order.IServiceDaoOrder
}

func NewOrderService(confsInventory utils.EtcdApiConfig) IOrderService {
	confGetTrackingDetail := util.ConvertGetEndpointAuthentication(confsInventory, "getTrackingDetailPath", "getTrackingDetail")

	newCallGetTrackingDetail := gettrackingdetail.NewCallGetTrackingDetail(confGetTrackingDetail)
	serviceGetTrackingDetail := gettrackingdetail.NewServiceGetTrackingDetail(newCallGetTrackingDetail)

	confInven := util.ConvertGetEndpointAuthentication(confsInventory, "gShopSearch", "")
	callGShopSearch := gshopsearch.NewCallGShopSearch(confInven)
	gShopSearchService := gshopsearch.NewServiceGShopSearch(callGShopSearch)

	confGetTrackingHist := util.ConvertGetEndpointAuthentication(confsInventory, "getTrackingHistory", "getTrackingHistory")
	callTrackingHist := gettrackinghistory.NewCallGetTrackingHistory(confGetTrackingHist)
	serviceTrackingHist := gettrackinghistory.NewServiceGetTrackingHistory(callTrackingHist)

	return OrderService{
		trackingServiec:     serviceGetTrackingDetail,
		trackingHistService: serviceTrackingHist,
		shopSearchService:   gShopSearchService,
		orderService:        order.NewIServiceDaoOrder(configs.Conf.Database),
	}
}

func (i OrderService) SearchOrderList(req models.RequestSearchOrderList, claims util.Claims, logModel logger.LogModel) (resp utils.ResponseStandard, valid bool) {
	orderReq := order.SearchOrderRequest{
		CorrelationId:         req.CorrelationId,
		OrderNumber:           req.SearchValue,
		CustomerName:          req.SearchValue,
		CustomerLastName:      req.SearchValue,
		CustomerContactNumber: req.SearchValue,
		ShopCode:              req.ShopCode,
		OrderStatus:           req.OrderStatus,
		StockType:             req.StockType,
		PageNo:                req.PageNo,
		PageSize:              req.PageSize,
		FilterType:            "SearchOrderList",
		FindAll:               false,
	}
	if req.OrderStatus == "Ready to Pick Up" {
		orderReq.OrderStatus = "READY_TO_PICK_UP"
	} else if req.OrderStatus == "Completed Pick Up" {
		orderReq.OrderStatus = "COMPLETED"
	} else if req.OrderStatus == "Cancelled Order" {
		orderReq.OrderStatus = "CANCELLED_ORDER"
	} else if req.OrderStatus == "Rejected Order" || req.OrderStatus == "Ship to Shop" || req.OrderStatus == "Waiting Product" ||
		req.OrderStatus == "Delivered Failed" || req.OrderStatus == "Delivered Success" {
		orderReq.OrderStatus = "WAITING_PRODUCT"
		orderReq.FindAll = true
	}
	orderList, totalPag, err := i.orderService.GetOrderList(orderReq, logModel)
	if err != nil {
		resp.Error = utils.GetErrorResponse(err)
		return
	}
	if len(orderList) == 0 {
		targetOrder := ""
		targetOrderArr := []string{}
		trackingReq := gettrackingdetail.GetTrackingDetailRequest{
			CorrelationId:  req.CorrelationId,
			TrackingNumber: req.SearchValue,
		}
		trackingDetailResp := i.trackingServiec.GetTrackingDetailService(trackingReq, logModel, "OMNI - GetTrackingDetailResult")
		if trackingDetailResp != nil && trackingDetailResp.Code == "200" && trackingDetailResp.Data != nil && len(trackingDetailResp.Data.TrackingList) > 0 {
			for _, track := range trackingDetailResp.Data.TrackingList {
				targetOrder = track.OrderNumber
				targetOrderArr = append(targetOrderArr, track.OrderNumber)
			}
		}
		if targetOrder == "" {
			valid = true
			resp.Code = util.Code404
			resp.BizError = "NOTFOUND"
			resp.Message = "Data Not Found"
			return
		}
		orderReq.OrderNumber = targetOrder
		orderReq.OrderNumberArr = targetOrderArr
		orderList, totalPag, err = i.orderService.GetOrderList(orderReq, logModel)
		if err != nil {
			resp.Error = utils.GetErrorResponse(err)
			return
		}
		if len(orderList) == 0 {
			valid = true
			resp.Code = util.Code400
			resp.BizError = "NOTFOUND"
			resp.Message = "Data Not Found"
			return
		}
	}

	orderListResp := []Order{}
	for _, order := range orderList {
		targetOrder := Order{
			OrderNumber:       order.OrderInfo.OrderNumber,
			OrderStatus:       order.OrderInfo.OrderStatus,
			OrderDate:         util.ToThaiTime(order.OrderInfo.OrderDate.Time()).Format("02-01-2006 15:04"),
			PaymentBalance:    order.OrderInfo.PaymentInfo.TotalAmount - order.OrderInfo.PaymentInfo.BillDiscountAmount - order.OrderInfo.PaymentInfo.BillOtherPaymentAmount - order.OrderInfo.PaymentInfo.PaymentAmount,
			CustomerFirstName: order.CustomerInfo.FirstName,
			CustomerLastName:  order.CustomerInfo.LastName,
			CustomerContactNo: order.CustomerInfo.ContactNo,
			ContactFirstName:  order.ContactInfo.FirstName,
			ContactLastName:   order.ContactInfo.LastName,
			ContactPhoneNo:    order.ContactInfo.ContactNo,
			OwnerDealer:       order.SaleInfo.OwnerDealerName,
			TargetDealer:      order.SaleInfo.TargetDealerName,
			TargetPickUpDate:  "",
			OrderLastUpdate:   util.ToThaiTime(order.OrderInfo.UpdatedDate.Time()).Format("02-01-2006 15:04"),
		}
		if order.PickupCustomerInfo != nil {
			targetOrder.PickupCustomerInfo = PickupCustomerInfo{
				FirstName: order.PickupCustomerInfo.FirstName,
				LastName:  order.PickupCustomerInfo.LastName,
				ContactNo: order.PickupCustomerInfo.ContactNo,
			}
		}

		targetOrder.TrackingList = []TrackingList{}
		trackingHistList := []gettrackinghistory.Tracking{}
		trackingHistReq := gettrackinghistory.GetTrackingHistoryRequest{
			CorrelationID: req.CorrelationId,
			OrderNumber:   order.OrderInfo.OrderNumber,
			// TrackingStatus: "READY_TO_PICK_UP",
		}
		trackingHistResp := i.trackingHistService.GetTrackingHistoryService(trackingHistReq, logModel, "OMNI - GetTrackingHistoryResult")
		if trackingHistResp.Code == "200" && trackingHistResp.Data != nil && len(trackingHistResp.Data.TrackingList) > 0 {
			trackingHistList = trackingHistResp.Data.TrackingList
		}
		if order.OrderInfo.OrderStateCode == "COMPLETED" || (order.OrderInfo.OrderStateCode == "CANCELLED_ORDER" && order.PickupCustomerInfo != nil) {
			for _, track := range trackingHistList {
				if track.TrackingStatus == "READY_TO_PICK_UP" {
					engReason := "NA"
					if track.StatusReason == "" {
						if track.TrackingStatus == "PAID" || track.TrackingStatus == "PACKED" {
							engReason = "WaitingProduct"
						} else {
							engReason = "Unidentified"
						}
					} else if track.StatusReason == "กล่องสภาพสมบูรณ์" {
						engReason = "PerfectBox"
					} else if track.StatusReason == "กล่องชำรุด ไม่รับสินค้า" {
						engReason = "ImperfectBox"
					}
					targetOrder.TrackingList = append(targetOrder.TrackingList, TrackingList{
						TrackingNumber:    track.TrackingNumber,
						TrackingStatus:    track.TrackingStatus,
						TrackingReason:    track.StatusReason,
						TrackingReasonEng: engReason,
						WarningMSG:        track.WarningMSG,
					})
				}
			}
		}
		trackingReq := gettrackingdetail.GetTrackingDetailRequest{
			CorrelationId: req.CorrelationId,
			OrderNumber:   order.OrderInfo.OrderNumber,
		}
		trackingDetailResp := i.trackingServiec.GetTrackingDetailService(trackingReq, logModel, "OMNI - GetTrackingDetailResult")
		trackingDetailList := []gettrackingdetail.Tracking{}

		if trackingDetailResp != nil && trackingDetailResp.Code == "200" && trackingDetailResp.Data != nil && len(trackingDetailResp.Data.TrackingList) > 0 {
			trackingDetailList = trackingDetailResp.Data.TrackingList
			var targetPickUpDateStr string
			formatDateStr := "02-01-2006 15:04"
			for _, track := range trackingDetailResp.Data.TrackingList {
				targetPickUpDateStr = ""
				if !(order.OrderInfo.OrderStateCode == "COMPLETED" || (order.OrderInfo.OrderStateCode == "CANCELLED_ORDER" && order.PickupCustomerInfo != nil)) {
					engReason := "NA"
					if track.StatusReason == "" {
						if track.TrackingStatus == "PAID" || track.TrackingStatus == "PACKED" {
							engReason = "WaitingProduct"
						} else {
							engReason = "Unidentified"
						}
					} else if track.StatusReason == "กล่องสภาพสมบูรณ์" {
						engReason = "PerfectBox"
					} else if track.StatusReason == "กล่องชำรุด ไม่รับสินค้า" {
						engReason = "ImperfectBox"
					}
					targetOrder.TrackingList = append(targetOrder.TrackingList, TrackingList{
						TrackingNumber:    track.TrackingNumber,
						TrackingStatus:    track.TrackingStatus,
						TrackingReason:    track.StatusReason,
						TrackingReasonEng: engReason,
						WarningMSG:        track.WarningMSG,
					})
				}

				if order.OrderInfo.OrderStateCode == "COMPLETED" {
					if order.OrderInfo.PickupLaterFlag {
						formatDateStr = "02-01-2006"
						targetPickUpDateStr = util.ToThaiTime(order.OrderInfo.PickupDate.Time()).Format("02-01-2006 15:04")
					} else {
						targetPickUpDateStr = track.StatusDate
					}
				} else if order.OrderInfo.OrderStateCode == "CANCELLED_ORDER" {
					if order.OrderInfo.PickupLaterFlag {
						formatDateStr = "02-01-2006"
						targetPickUpDateStr = util.ToThaiTime(order.OrderInfo.PickupDate.Time()).Format("02-01-2006 15:04")
					} else {
						targetPickUpDateStr = track.StatusDate
					}
				} else if order.OrderInfo.OrderStateCode == "READY_TO_PICK_UP" {
					formatDateStr = "02-01-2006"
					targetPickUpDateStr = track.DueDate
				}
				if targetPickUpDateStr != "" && targetOrder.TargetPickUpDate != "" {
					timeNew, err := time.Parse("02-01-2006 15:04", targetPickUpDateStr)
					timeCurrent, err2 := time.Parse("02-01-2006 15:04", targetOrder.TargetPickUpDate)
					if err == nil && err2 == nil && timeNew.After(timeCurrent) {
						targetOrder.TargetPickUpDate = targetPickUpDateStr
					}
				} else if targetPickUpDateStr != "" && targetOrder.TargetPickUpDate == "" {
					targetOrder.TargetPickUpDate = targetPickUpDateStr
				}

			}
			timeCurrent, err2 := time.Parse("02-01-2006 15:04", targetOrder.TargetPickUpDate)
			if err2 == nil {
				targetOrder.TargetPickUpDate = timeCurrent.Format(formatDateStr)
			}

		}

		if order.OrderInfo.OrderStateCode == "READY_TO_PICK_UP" {
			targetOrder.OrderStatus = "Ready to Pick Up"
		} else if order.OrderInfo.OrderStateCode == "COMPLETED" {
			targetOrder.OrderStatus = "Completed Pick Up"
		} else if order.OrderInfo.OrderStateCode == "CANCELLED_ORDER" {
			targetOrder.OrderStatus = "Cancelled Order"
		} else if order.OrderInfo.OrderStateCode == "WAITING_PRODUCT" {
			haveRejectedTracking := false
			haveWaitingProduct := false
			haveDeliveredFailed := false
			haveDeliverySuccess := false
			haveShiptoShop := false

			for _, track := range targetOrder.TrackingList {
				if track.TrackingStatus == "REJECTED_TRACKING" {
					haveRejectedTracking = true
					break
				} else if track.TrackingStatus == "PAID" || track.TrackingStatus == "PACKED" {
					haveWaitingProduct = true
				} else if track.TrackingStatus == "DELIVERED_FAILED" {
					haveDeliveredFailed = true
				} else if track.TrackingStatus == "SHIPPED" {
					haveShiptoShop = true
				} else if track.TrackingStatus == "DELIVERED_SUCCESS" {
					haveDeliverySuccess = true
				}
			}

			if req.OrderStatus == "Rejected Order" && !haveRejectedTracking {
				continue
			} else if req.OrderStatus == "Waiting Product" && (haveRejectedTracking || !haveWaitingProduct) {
				continue
			} else if req.OrderStatus == "Delivered Failed" && (haveRejectedTracking || haveWaitingProduct || !haveDeliveredFailed) {
				continue
			} else if req.OrderStatus == "Ship to Shop" && (haveRejectedTracking || haveWaitingProduct || haveDeliveredFailed || !haveShiptoShop) {
				continue
			} else if req.OrderStatus == "Delivered Success" && (haveRejectedTracking || haveShiptoShop || haveWaitingProduct || haveDeliveredFailed || !haveDeliverySuccess) {
				continue
			}

			if haveRejectedTracking {
				targetOrder.OrderStatus = "Rejected Order"
			} else if haveWaitingProduct {
				targetOrder.OrderStatus = "Waiting Product"
			} else if haveDeliveredFailed {
				targetOrder.OrderStatus = "Delivered Failed"
			} else if haveShiptoShop {
				targetOrder.OrderStatus = "Ship to Shop"
			} else if haveDeliverySuccess {
				targetOrder.OrderStatus = "Delivered Success"
			}
		}

		targetOrder.TargetDeliverDate, targetOrder.TrackingLastState = calTargetDeliverDateAndTrackingLastState(targetOrder.OrderStatus, order.AirwayBills, trackingDetailList, trackingHistList)

		orderListResp = append(orderListResp, targetOrder)
	}
	if len(orderListResp) == 0 {
		valid = true
		resp.Code = util.Code404
		resp.BizError = "NOTFOUND"
		resp.Message = "Data Not Found"
		return
	}
	if orderReq.FindAll {
		totalPag = int(math.Ceil(float64(len(orderListResp)) / float64(req.PageSize)))
		startIndex := ((req.PageNo - 1) * req.PageSize)
		endIndex := req.PageNo * req.PageSize
		if (len(orderListResp)) < endIndex {
			endIndex = len(orderListResp)
		}
		orderListResp = orderListResp[startIndex:endIndex]
	}

	resp.Code = util.Code200
	resp.Message = "Success"
	resp.Data = OrderList{
		TotalPage: totalPag,
		OrderList: orderListResp,
	}
	valid = true
	return
}

func (i OrderService) GetOrderDetails(req models.RequestGetOrderDetails, claims util.Claims, logModel logger.LogModel) (resp utils.ResponseStandard, valid bool) {

	orderReq := order.SearchOrderRequest{
		CorrelationId: req.CorrelationId,
		OrderNumber:   req.SearchValue,
		ShopCode:      req.ShopCode,
		Channel:       req.Channel,
		UserID:        req.UserID,
	}
	orderList, _, err := i.orderService.GetOrderList(orderReq, logModel)
	if err != nil {
		resp.Error = utils.GetErrorResponse(err)
		return
	}
	if len(orderList) == 0 {
		targetOrder := ""
		trackingReq := gettrackingdetail.GetTrackingDetailRequest{
			CorrelationId:  req.CorrelationId,
			TrackingNumber: req.SearchValue,
		}
		trackingDetailResp := i.trackingServiec.GetTrackingDetailService(trackingReq, logModel, "OMNI - GetTrackingDetailResult")
		if trackingDetailResp != nil && trackingDetailResp.Code == "200" && trackingDetailResp.Data != nil && len(trackingDetailResp.Data.TrackingList) > 0 {
			for _, track := range trackingDetailResp.Data.TrackingList {
				targetOrder = track.OrderNumber
				break
			}
		}
		if targetOrder == "" {
			valid = true
			resp.Code = util.Code404
			resp.BizError = "NOTFOUND"
			resp.Message = "Data Not Found"
			return
		}
		orderReq.OrderNumber = targetOrder
		orderList, _, err = i.orderService.GetOrderList(orderReq, logModel)
		if err != nil {
			resp.Error = utils.GetErrorResponse(err)
			return
		}
		if len(orderList) == 0 {
			valid = true
			resp.Code = util.Code400
			resp.BizError = "NOTFOUND"
			resp.Message = "Data Not Found"
			return
		}
	}

	order := orderList[0]
	orderResp := convertOrderToOrderDetail(order)

	orderResp.OrderInfo.TrackingList = []Tracking{}
	orderResp.OrderInfo.CustomerPickUpTrackings = []CustomerPickUpTrackings{}

	trackingHistReq := gettrackinghistory.GetTrackingHistoryRequest{
		CorrelationID: req.CorrelationId,
		OrderNumber:   order.OrderInfo.OrderNumber,
	}

	trackingList := []gettrackinghistory.Tracking{}
	trackingHistResp := i.trackingHistService.GetTrackingHistoryService(trackingHistReq, logModel, "OMNI - GetTrackingHistoryResult")
	if trackingHistResp.Code == "200" && trackingHistResp.Data != nil && len(trackingHistResp.Data.TrackingList) > 0 {
		trackingList = trackingHistResp.Data.TrackingList
	}

	if order.OrderInfo.OrderStateCode == "COMPLETED" || (order.OrderInfo.OrderStateCode == "CANCELLED_ORDER" && order.PickupCustomerInfo != nil) {
		if len(trackingList) > 0 {
			orderResp.OrderInfo.TrackingList = prepareOrderTrackingHist(trackingList)
		}
	}

	if req.ShopCode != "" && order.SaleInfo.TargetDealerCode != req.ShopCode {
		orderResp.DisableFunction = true
	}

	trackingReq := gettrackingdetail.GetTrackingDetailRequest{
		CorrelationId: req.CorrelationId,
		OrderNumber:   order.OrderInfo.OrderNumber,
	}
	trackingDetailResp := i.trackingServiec.GetTrackingDetailService(trackingReq, logModel, "OMNI - GetTrackingDetailResult")
	trackingDetailList := []gettrackingdetail.Tracking{}
	if trackingDetailResp != nil && trackingDetailResp.Code == "200" && trackingDetailResp.Data != nil && len(trackingDetailResp.Data.TrackingList) > 0 {
		trackingDetailList = trackingDetailResp.Data.TrackingList
		if order.OrderInfo.OrderStateCode == "COMPLETED" || (order.OrderInfo.OrderStateCode == "CANCELLED_ORDER" && order.PickupCustomerInfo != nil) {
			orderResp.OrderInfo.CustomerPickUpTrackings = prepareOrderTrackingDetailCP(trackingDetailResp.Data.TrackingList)
		} else {
			orderResp.OrderInfo.TrackingList = prepareOrderTrackingDetail(order.OrderInfo.OrderStateCode, trackingDetailResp.Data.TrackingList, trackingList)
		}

		var targetPickUpDateStr string
		formatDateStr := "02-01-2006 15:04"
		for _, track := range trackingDetailResp.Data.TrackingList {
			targetPickUpDateStr = ""
			if order.OrderInfo.OrderStateCode == "COMPLETED" {
				if order.OrderInfo.PickupLaterFlag {
					formatDateStr = "02-01-2006"
					targetPickUpDateStr = util.ToThaiTime(order.OrderInfo.PickupDate.Time()).Format("02-01-2006 15:04")
				} else {
					targetPickUpDateStr = track.StatusDate
				}
			} else if order.OrderInfo.OrderStateCode == "CANCELLED_ORDER" {
				if order.OrderInfo.PickupLaterFlag {
					formatDateStr = "02-01-2006"
					targetPickUpDateStr = util.ToThaiTime(order.OrderInfo.PickupDate.Time()).Format("02-01-2006 15:04")
				} else {
					targetPickUpDateStr = track.StatusDate
				}
			} else if order.OrderInfo.OrderStateCode == "READY_TO_PICK_UP" {
				formatDateStr = "02-01-2006"
				targetPickUpDateStr = track.DueDate
			}

			if targetPickUpDateStr != "" && orderResp.TargetPickUpDate != "" {
				timeNew, err := time.Parse("02-01-2006 15:04", targetPickUpDateStr)
				timeCurrent, err2 := time.Parse("02-01-2006 15:04", orderResp.TargetPickUpDate)
				if err == nil && err2 == nil && timeNew.After(timeCurrent) {
					orderResp.TargetPickUpDate = targetPickUpDateStr
				}
			} else if targetPickUpDateStr != "" && orderResp.TargetPickUpDate == "" {
				orderResp.TargetPickUpDate = targetPickUpDateStr
			}

		}
		timeCurrent, err2 := time.Parse("02-01-2006 15:04", orderResp.TargetPickUpDate)
		if err2 == nil {
			orderResp.TargetPickUpDate = timeCurrent.Format(formatDateStr)
		}
	}

	if order.OrderInfo.OrderStateCode == "READY_TO_PICK_UP" {
		orderResp.OrderInfo.OrderStatus = "Ready to Pick Up"
	} else if order.OrderInfo.OrderStateCode == "COMPLETED" {
		orderResp.OrderInfo.OrderStatus = "Completed Pick Up"
	} else if order.OrderInfo.OrderStateCode == "CANCELLED_ORDER" {
		orderResp.OrderInfo.OrderStatus = "Cancelled Order"
	} else if order.OrderInfo.OrderStateCode == "WAITING_PRODUCT" {
		haveRejectedTracking := false
		haveWaitingProduct := false
		haveDeliveredFailed := false
		haveDeliverySuccess := false
		haveShiptoShop := false

		for _, track := range orderResp.OrderInfo.TrackingList {
			if track.TrackingStatus == "REJECTED_TRACKING" {
				haveRejectedTracking = true
				break
			} else if track.TrackingStatus == "PAID" || track.TrackingStatus == "PACKED" {
				haveWaitingProduct = true
			} else if track.TrackingStatus == "SHIPPED" {
				haveShiptoShop = true
			} else if track.TrackingStatus == "DELIVERED_FAILED" {
				haveDeliveredFailed = true
			} else if track.TrackingStatus == "DELIVERED_SUCCESS" {
				haveDeliverySuccess = true
			}
		}
		if haveRejectedTracking {
			orderResp.OrderInfo.OrderStatus = "Rejected Order"
		} else if haveWaitingProduct {
			orderResp.OrderInfo.OrderStatus = "Waiting Product"
		} else if haveDeliveredFailed {
			orderResp.OrderInfo.OrderStatus = "Delivered Failed"
		} else if haveShiptoShop {
			orderResp.OrderInfo.OrderStatus = "Ship to Shop"
		} else if haveDeliverySuccess {
			orderResp.OrderInfo.OrderStatus = "Delivered Success"
		}
	}

	orderResp.TargetDeliverDate, orderResp.TrackingLastState = calTargetDeliverDateAndTrackingLastState(orderResp.OrderInfo.OrderStatus, order.AirwayBills, trackingDetailList, trackingList)

	orderResp.RenderPickupAden = false
	if orderResp.OrderInfo.OrderStatus == "Completed Pick Up" || orderResp.OrderInfo.OrderStatus == "Ready to Pick Up" ||
		orderResp.OrderInfo.OrderStatus == "Rejected Order" || orderResp.OrderInfo.OrderStatus == "Delivered Failed" ||
		orderResp.OrderInfo.OrderStatus == "Delivered Success" {
		orderResp.RenderPickupAden = true
	} else if orderResp.OrderInfo.OrderStatus == "Cancelled Order" {
		if order.PickupCustomerInfo != nil {
			orderResp.RenderPickupAden = true
		} else if len(orderResp.OrderInfo.TrackingList) > 0 {
			for _, track := range orderResp.OrderInfo.TrackingList {
				if track.TrackingReason != "" || track.AdenStatus != "" ||
					track.TrackingStatus == "DELIVERED_FAILED" || track.TrackingStatus == "DELIVERED_SUCCESS" {
					orderResp.RenderPickupAden = true
					break
				}
			}
		}
	}
	resp.Code = util.Code200
	resp.Message = "Success"
	resp.Data = orderResp
	valid = true
	return
}

func convertOrderToOrderDetail(ord order.Order) OrderDetails {

	orderItems := []OrderItems{}

	for _, item := range ord.OrderInfo.OrderItems {

		discountInfo := DiscountInfo{}
		if item.AmountInfo.DiscountInfo != nil {
			discountInfo.TotalDiscountAmount = item.AmountInfo.DiscountInfo.TotalDiscountAmount
			discountInfo.Discount = []Discount{}
			for _, v := range item.AmountInfo.DiscountInfo.Discount {
				discountInfo.Discount = append(discountInfo.Discount, Discount{
					DiscountCode:   v.DiscountCode,
					DiscountAmount: v.DiscountAmount,
				})
			}
		}
		orderItems = append(orderItems, OrderItems{
			Sequence:    item.Sequence,
			ProductCode: item.ProductCode,
			TrackingId:  item.TrackingId,
			Serial:      item.Serial,
			ProductInfo: ProductInfo{
				Code:     item.ProductInfo.Code,
				Name:     item.ProductInfo.Name,
				Brand:    item.ProductInfo.Brand,
				Model:    item.ProductInfo.Model,
				Color:    item.ProductInfo.Color,
				Capacity: item.ProductInfo.Capacity,
			},
			AmountInfo: AmountInfo{
				Price:        item.AmountInfo.Price,
				Qty:          int(item.AmountInfo.Qty),
				DiscountInfo: discountInfo,
			},
		})
	}

	var pickupCustomerInfo PickupCustomerInfo
	if ord.PickupCustomerInfo != nil {
		pickupCustomerInfo = PickupCustomerInfo{
			FirstName: ord.PickupCustomerInfo.FirstName,
			LastName:  ord.PickupCustomerInfo.LastName,
			ContactNo: ord.PickupCustomerInfo.ContactNo,
		}
		if ord.PickupCustomerInfo.VerifyInfo != nil {
			pickupCustomerInfo.VerifyInfo = &VerifyInfo{
				SecondAuthenFlag:   ord.PickupCustomerInfo.VerifyInfo.SecondAuthenFlag,
				SecondAuthenBy:     ord.PickupCustomerInfo.VerifyInfo.SecondAuthenBy,
				SecondAuthenByName: ord.PickupCustomerInfo.VerifyInfo.SecondAuthenByName,
				SecondAuthenReason: ord.PickupCustomerInfo.VerifyInfo.SecondAuthenReason,
			}
		}
	}

	var documentInfoList []DocumentInfo
	for _, doc := range ord.DocumentInfo {
		documentInfo := DocumentInfo{
			DocumentType: doc.DocumentType,
			RefId:        doc.RefID,
		}
		documentInfoList = append(documentInfoList, documentInfo)
	}

	orderDate := util.ToThaiTime(ord.OrderInfo.OrderDate.Time()).Format("02-01-2006")
	if orderDate == "01-01-1970" {
		orderDate = ""
	}
	pickupDate := util.ToThaiTime(ord.OrderInfo.PickupDate.Time()).Format("02-01-2006")
	if pickupDate == "01-01-1970" {
		pickupDate = ""
	}
	pickupTransactionDate := util.ToThaiTime(ord.OrderInfo.PickupTransactionDate.Time()).Format("02-01-2006 15:04")
	if pickupTransactionDate == "01-01-1970 07:00" {
		pickupTransactionDate = ""
	}
	return OrderDetails{
		OrderInfo: OrderInfo{
			OrderNumber:           ord.OrderInfo.OrderNumber,
			OrderStatus:           ord.OrderInfo.OrderStatus,
			OrderReasonName:       ord.OrderInfo.OrderReasonName,
			OrderDate:             orderDate,
			PickupDate:            pickupDate,
			PickupRemark:          ord.OrderInfo.PickupRemark,
			PickupTransactionDate: pickupTransactionDate,
			PickupLaterFlag:       ord.OrderInfo.PickupLaterFlag,
			OrderItems:            orderItems,
			PaymentInfo: PaymentInfo{
				PaymentMethod:          ord.OrderInfo.PaymentInfo.PaymentMethod,
				PaymentAmount:          ord.OrderInfo.PaymentInfo.PaymentAmount,
				PaymentBalance:         ord.OrderInfo.PaymentInfo.TotalAmount - ord.OrderInfo.PaymentInfo.BillDiscountAmount - ord.OrderInfo.PaymentInfo.BillOtherPaymentAmount - ord.OrderInfo.PaymentInfo.PaymentAmount,
				BillDiscountAmount:     ord.OrderInfo.PaymentInfo.BillDiscountAmount,
				BillOtherPaymentAmount: ord.OrderInfo.PaymentInfo.BillOtherPaymentAmount,
			},
		},
		CustomerInfo: CustomerInfo{
			FirstName:      ord.CustomerInfo.FirstName,
			LastName:       ord.CustomerInfo.LastName,
			ContactNo:      ord.CustomerInfo.ContactNo,
			ContactEmail:   ord.CustomerInfo.ContactEmail,
			Identification: ord.CustomerInfo.Identification,
		},
		ContactInfo: ContactInfo{
			FirstName:      ord.ContactInfo.FirstName,
			LastName:       ord.ContactInfo.LastName,
			ContactNo:      ord.ContactInfo.ContactNo,
			Identification: ord.ContactInfo.Identification,
		},
		PickupCustomerInfo: pickupCustomerInfo,
		DocumentInfo:       documentInfoList,
		OwnerDealer:        ord.SaleInfo.OwnerDealerName,
		TargetPickUpDate:   "",
		TargetDealer:       ord.SaleInfo.TargetDealerName,
		DisableFunction:    false,
	}
}

func prepareOrderTrackingDetail(orderStatus string, trackingIn []gettrackingdetail.Tracking, trackingHist []gettrackinghistory.Tracking) (trackingOut []Tracking) {

	for _, track := range trackingIn {
		engReason := "NA"
		if track.StatusReason == "" {
			if track.TrackingStatus == "PAID" || track.TrackingStatus == "PACKED" {
				engReason = "WaitingProduct"
			} else {
				engReason = "Unidentified"
			}
		} else if track.StatusReason == "กล่องสภาพสมบูรณ์" {
			engReason = "PerfectBox"
		} else if track.StatusReason == "กล่องชำรุด ไม่รับสินค้า" {
			engReason = "ImperfectBox"
		}
		tracking := Tracking{
			TrackingNumber:    track.TrackingNumber,
			TrackingStatus:    track.TrackingStatus,
			TrackingReason:    track.StatusReason,
			TrackingReasonEng: engReason,
			SaleName:          track.PickupName,
		}
		if orderStatus == "CANCELLED_ORDER" {
			tracking.ImagePathList = getImagePathList(trackingHist, track.StatusReason)
		} else {
			tracking.ImagePathList = []ImagePath{}
			for _, image := range track.ImagePathList {
				tracking.ImagePathList = append(tracking.ImagePathList, ImagePath{
					Type:      image.Type,
					RefID:     image.RefId,
					ImagePath: image.ImagePath,
				})
			}
		}

		if track.TrackingStatus == "READY_TO_PICK_UP" || track.TrackingStatus == "REJECTED_TRACKING" {
			tracking.GoodsReceiveDate = track.StatusDate
		} else if track.TrackingStatus == "CANCELLED_ORDER" {
			for _, track2 := range trackingHist {
				if (track.StatusReason == "กล่องสภาพสมบูรณ์" && track2.TrackingStatus == "READY_TO_PICK_UP") || (track.StatusReason == "กล่องชำรุด ไม่รับสินค้า" && track2.TrackingStatus == "REJECTED_TRACKING") {
					tracking.GoodsReceiveDate = track2.StatusDate
					break
				}
			}
		}
		if track.TrackingStatus == "DELIVERED_SUCCESS" || track.TrackingStatus == "DELIVERED_FAILED" {
			tracking.AdenStatus = track.TrackingStatus
			tracking.AdenStatusDate = track.StatusDate
		} else if track.TrackingStatus == "READY_TO_PICK_UP" || track.TrackingStatus == "REJECTED_TRACKING" || track.TrackingStatus == "CANCELLED_ORDER" {
			tracking.AdenStatus, tracking.AdenStatusDate = getAdenStatus(trackingHist, "DELIVERED_SUCCESS", "")
			if tracking.AdenStatus == "" || tracking.AdenStatusDate == "" {
				tracking.AdenStatus, tracking.AdenStatusDate = getAdenStatus(trackingHist, "READY_TO_PICK_UP", track.StatusReason)
			}
			if tracking.AdenStatus == "" || tracking.AdenStatusDate == "" {
				tracking.AdenStatus, tracking.AdenStatusDate = getAdenStatus(trackingHist, "REJECTED_TRACKING", track.StatusReason)
			}
			if (tracking.AdenStatus == "" || tracking.AdenStatusDate == "") && track.TrackingStatus == "CANCELLED_ORDER" {
				tracking.AdenStatus, tracking.AdenStatusDate = getAdenStatus(trackingHist, "DELIVERED_FAILED", "")
				if tracking.AdenStatus == "" || tracking.AdenStatusDate == "" {
					tracking.AdenStatus, tracking.AdenStatusDate = getAdenStatus(trackingHist, "SHIPPED", "")
				}
			}
		}

		trackingOut = append(trackingOut, tracking)
	}

	return
}
func getAdenStatus(trackingHist []gettrackinghistory.Tracking, trackingStatus string, statusReason string) (adenStatus string, adenStatusDate string) {
	for _, track2 := range trackingHist {
		if track2.TrackingStatus == trackingStatus && statusReason == "" {
			adenStatus = track2.TrackingStatus
			adenStatusDate = track2.StatusDate
			break
		} else if (statusReason == "กล่องสภาพสมบูรณ์" && track2.TrackingStatus == trackingStatus) ||
			(statusReason == "กล่องชำรุด ไม่รับสินค้า" && track2.TrackingStatus == trackingStatus) {
			adenStatus = "DELIVERED_SUCCESS"
			adenStatusDate = track2.StatusDate
			break
		}
	}
	return
}
func getImagePathList(trackingHist []gettrackinghistory.Tracking, statusReason string) (imagePathList []ImagePath) {
	for _, track2 := range trackingHist {
		if statusReason == "กล่องสภาพสมบูรณ์" && track2.TrackingStatus == "READY_TO_PICK_UP" {
			imagePathList = []ImagePath{}
			for _, image := range track2.ImagePathList {
				imagePathList = append(imagePathList, ImagePath{
					Type:      image.Type,
					RefID:     image.RefId,
					ImagePath: image.ImagePath,
				})
			}
			break
		} else if statusReason == "กล่องชำรุด ไม่รับสินค้า" && track2.TrackingStatus == "REJECTED_TRACKING" {
			imagePathList = []ImagePath{}
			for _, image := range track2.ImagePathList {
				imagePathList = append(imagePathList, ImagePath{
					Type:      image.Type,
					RefID:     image.RefId,
					ImagePath: image.ImagePath,
				})
			}
		}
	}
	return
}
func prepareOrderTrackingDetailCP(trackingIn []gettrackingdetail.Tracking) (trackingOut []CustomerPickUpTrackings) {

	for _, track := range trackingIn {
		imagePathList := []ImagePath{}
		for _, image := range track.ImagePathList {
			imagePathList = append(imagePathList, ImagePath{
				Type:      image.Type,
				RefID:     image.RefId,
				ImagePath: image.ImagePath,
			})
		}
		trackingOut = append(trackingOut, CustomerPickUpTrackings{
			TrackingNumber: track.TrackingNumber,
			TrackingStatus: track.TrackingStatus,
			ImagePathList:  imagePathList,
			SaleName:       track.PickupName,
			PickupShop:     track.PickupShopCode,
			PickUpDate:     track.StatusDate,
		})
	}

	return
}

func prepareOrderTrackingHist(trackingIn []gettrackinghistory.Tracking) (trackingOut []Tracking) {

	for _, track := range trackingIn {
		if track.TrackingStatus == "READY_TO_PICK_UP" {
			imagePathList := []ImagePath{}
			for _, image := range track.ImagePathList {
				imagePathList = append(imagePathList, ImagePath{
					Type:      image.Type,
					RefID:     image.RefId,
					ImagePath: image.ImagePath,
				})
			}
			engReason := "NA"
			if track.StatusReason == "" {
				if track.TrackingStatus == "PAID" || track.TrackingStatus == "PACKED" {
					engReason = "WaitingProduct"
				} else {
					engReason = "Unidentified"
				}
			} else if track.StatusReason == "กล่องสภาพสมบูรณ์" {
				engReason = "PerfectBox"
			} else if track.StatusReason == "กล่องชำรุด ไม่รับสินค้า" {
				engReason = "ImperfectBox"
			}
			tracking := Tracking{
				TrackingNumber:    track.TrackingNumber,
				TrackingStatus:    track.TrackingStatus,
				TrackingReason:    track.StatusReason,
				TrackingReasonEng: engReason,
				GoodsReceiveDate:  track.StatusDate,
				ImagePathList:     imagePathList,
				SaleName:          track.PickupName,
				AdenStatus:        "DELIVERED_SUCCESS",
				AdenStatusDate:    track.StatusDate,
			}
			for _, track2 := range trackingIn {
				if track2.TrackingStatus == "DELIVERED_SUCCESS" {
					tracking.AdenStatusDate = track2.StatusDate
					break
				}
			}

			trackingOut = append(trackingOut, tracking)
		}
	}

	return
}

func calTargetDeliverDateAndTrackingLastState(orderStatus string, airwayBills []order.AirwayBills, trackingDetail []gettrackingdetail.Tracking, trackingHist []gettrackinghistory.Tracking) (targetDeliverDate string, trackingLastState string) {
	trackingStatus := ""
	if orderStatus == "Waiting Product" {
		targetDeliverDate = ""
		trackingLastState = ""
	} else if orderStatus == "Ship to Shop" {
		var deliverDate *time.Time = nil
		for _, aw := range airwayBills {
			if deliverDate == nil || deliverDate.Before(aw.TargetDeliverEndDate) {
				deliverDate = &aw.TargetDeliverEndDate
			}
		}
		targetDeliverDateStr := ""
		if deliverDate != nil {
			targetDeliverDateStr = util.ToThaiTime(*deliverDate).Format("02-01-2006")
		}
		targetDeliverDate = targetDeliverDateStr
		trackingLastState = "SHIPPED"
	} else if orderStatus == "Delivered Success" {
		trackingStatus = "DELIVERED_SUCCESS"
		trackingLastState = "DELILVERED"
	} else if orderStatus == "Delivered Failed" {
		trackingStatus = "DELIVERED_FAILED"
		trackingLastState = "DELILVEREDFAILED"
	} else if orderStatus == "Ready to Pick Up" {
		trackingStatus = "READY_TO_PICK_UP"
		trackingLastState = "READYTOPICKUP"
	} else if orderStatus == "Rejected Order" {
		trackingStatus = "REJECTED_TRACKING"
		trackingLastState = "REJECTED"
	} else if orderStatus == "Completed Pick Up" {
		trackingLastState = "READYTOPICKUP"
		targetDeliverDate = ""
		for _, track := range trackingHist {
			if track.TrackingStatus == "READY_TO_PICK_UP" && track.StatusDate != "" {
				if targetDeliverDate == "" {
					targetDeliverDate = track.StatusDate

				} else {
					timeNew, err := time.Parse("02-01-2006 15:04", track.StatusDate)
					timeCurrent, err2 := time.Parse("02-01-2006 15:04", targetDeliverDate)
					if err == nil && err2 == nil && timeNew.After(timeCurrent) {
						targetDeliverDate = track.StatusDate
					}
				}
			}
		}
	} else if orderStatus == "Cancelled Order" {
		readytoPickUpDate := ""
		rejectedTrackingDate := ""
		deliverySuccessDate := ""
		deliveryFailedDate := ""
		shipped := ""

		for _, track := range trackingHist {
			if track.StatusDate != "" {
				if track.TrackingStatus == "READY_TO_PICK_UP" {
					if readytoPickUpDate == "" {
						readytoPickUpDate = track.StatusDate
					} else {
						timeNew, err := time.Parse("02-01-2006 15:04", track.StatusDate)
						timeCurrent, err2 := time.Parse("02-01-2006 15:04", readytoPickUpDate)
						if err == nil && err2 == nil && timeNew.After(timeCurrent) {
							readytoPickUpDate = track.StatusDate
						}
					}
				} else if track.TrackingStatus == "REJECTED_TRACKING" {
					if rejectedTrackingDate == "" {
						rejectedTrackingDate = track.StatusDate
					} else {
						timeNew, err := time.Parse("02-01-2006 15:04", track.StatusDate)
						timeCurrent, err2 := time.Parse("02-01-2006 15:04", rejectedTrackingDate)
						if err == nil && err2 == nil && timeNew.After(timeCurrent) {
							rejectedTrackingDate = track.StatusDate
						}
					}
				} else if track.TrackingStatus == "DELIVERED_SUCCESS" {
					if deliverySuccessDate == "" {
						deliverySuccessDate = track.StatusDate
					} else {
						timeNew, err := time.Parse("02-01-2006 15:04", track.StatusDate)
						timeCurrent, err2 := time.Parse("02-01-2006 15:04", deliverySuccessDate)
						if err == nil && err2 == nil && timeNew.After(timeCurrent) {
							deliverySuccessDate = track.StatusDate
						}
					}
				} else if track.TrackingStatus == "DELIVERED_FAILED" {
					if deliveryFailedDate == "" {
						deliveryFailedDate = track.StatusDate
					} else {
						timeNew, err := time.Parse("02-01-2006 15:04", track.StatusDate)
						timeCurrent, err2 := time.Parse("02-01-2006 15:04", deliveryFailedDate)
						if err == nil && err2 == nil && timeNew.After(timeCurrent) {
							deliveryFailedDate = track.StatusDate
						}
					}
				} else if track.TrackingStatus == "SHIPPED" {
					if shipped == "" {
						timeNew, err := time.Parse("02-01-2006 15:04", track.StatusDate)
						if err == nil {
							shipped = timeNew.Format("02-01-2006")
						}

					} else {
						timeNew, err := time.Parse("02-01-2006 15:04", track.StatusDate)
						timeCurrent, err2 := time.Parse("02-01-2006", shipped)
						if err == nil && err2 == nil && timeNew.After(timeCurrent) {
							shipped = timeNew.Format("02-01-2006")
						}
					}
				}
			}
		}
		if readytoPickUpDate != "" {
			targetDeliverDate = readytoPickUpDate
			trackingLastState = "READYTOPICKUP"
		} else if rejectedTrackingDate != "" {
			targetDeliverDate = rejectedTrackingDate
			trackingLastState = "REJECTED"
		} else if deliverySuccessDate != "" {
			targetDeliverDate = deliverySuccessDate
			trackingLastState = "DELILVERED"
		} else if deliveryFailedDate != "" {
			targetDeliverDate = deliveryFailedDate
			trackingLastState = "DELILVEREDFAILED"
		} else if shipped != "" {
			targetDeliverDate = shipped
			trackingLastState = "SHIPPED"
		}
	}

	if trackingStatus != "" {
		for _, track := range trackingDetail {
			if track.TrackingStatus == trackingStatus && track.StatusDate != "" {
				if targetDeliverDate == "" {
					targetDeliverDate = track.StatusDate
				} else {
					timeNew, err := time.Parse("02-01-2006 15:04", track.StatusDate)
					timeCurrent, err2 := time.Parse("02-01-2006 15:04", targetDeliverDate)
					if err == nil && err2 == nil && timeNew.After(timeCurrent) {
						targetDeliverDate = track.StatusDate
					}
				}
			}
		}
	}
	return
}
