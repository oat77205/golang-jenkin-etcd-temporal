package validateotp

import (
	"hl-order-api/configs"
	"hl-order-api/routes/models"
	"hl-order-api/services/order"
	"hl-order-api/services/orderhistory"
	"time"

	b64 "encoding/base64"
	"encoding/json"
	"hl-order-api/util"

	validateOTP "gitlab.com/true-itsd/iservicemax/omni/api/ms/hl-otp/services/validateotp"
	"gitlab.com/true-itsd/iservicemax/omni/pkg/common-utils-backend/services/logger"
	"gitlab.com/true-itsd/iservicemax/omni/pkg/common-utils-backend/utils"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type IServiceValidateOTP interface {
	ServiceValidateOTP(models.RequestValidateOTP, util.Claims, logger.LogModel) (*utils.ResponseStandard, error)
}

type ServiceValidateOTP struct {
	config    utils.EtcdApiConfig
	Databases utils.SystemDatabase
}

func NewIServiceValidateOTP(config utils.EtcdApiConfig, db utils.SystemDatabase) IServiceValidateOTP {
	return &ServiceValidateOTP{
		config:    config,
		Databases: db,
	}
}

func (confs *ServiceValidateOTP) ServiceValidateOTP(req models.RequestValidateOTP, claims util.Claims, logModel logger.LogModel) (*utils.ResponseStandard, error) {
	var response utils.ResponseStandard

	if req.FunctionName != "VERIFY_CUSTOMER_PICKUP" {
		response.Code = "422"
		response.BizError = "FunctionName invalid"
		response.Message = "FunctionName must be VERIFY_CUSTOMER_PICKUP"
		return &response, nil
	}

	serviceDaoGetOrder := order.NewIServiceDaoOrder(configs.Conf.Database)
	err, orderData := serviceDaoGetOrder.GetOrderByOrderNumber(req.OrderNumber, logModel)
	if err != nil {
		response.Code = "500"
		response.Message = "Internal server error"
		return &response, err
	}

	if orderData == nil {
		response.Code = "400"
		response.BizError = "NOTFOUND"
		response.Message = "Data not found"
		return &response, nil
	}

	if orderData.OrderInfo.OrderStateCode != "READY_TO_PICK_UP" {
		response.Code = "422"
		response.BizError = "OrderStateCode invalid"
		response.Message = "OrderStateCode is not equal READY_TO_PICK_UP"
		return &response, nil
	}

	confValidateOTP := util.ConvertGetEndpointAuthentication(confs.config, "validateOTP", "validateOTP")
	callValidateOTP := validateOTP.NewCallValidateOTP(confValidateOTP)
	service := validateOTP.NewServiceValidateOTP(callValidateOTP)
	newContactNumber := "66" + orderData.CustomerInfo.ContactNo[1:]
	reqData := models.ValidateOTPData{
		Function: req.FunctionName,
		Msisdn:   newContactNumber,
		Language: "TH",
		OTPCode:  req.OtpCode,
		RefID:    req.RefID,
	}
	reqOTPJson, err := json.Marshal(reqData)
	if err != nil {
		response.Code = "500"
		response.Message = err.Error()
		return &response, nil
	}
	reqOTPBase64 := b64.URLEncoding.EncodeToString([]byte(reqOTPJson))
	newReq := validateOTP.ValidateOTPRequest{
		Data: reqOTPBase64,
	}
	newReqHeader := validateOTP.ValidateHeaderOTPRequest{
		CorrelationId: req.CorrelationID,
		ChannelName:   "headless",
	}
	validateOTPResponse, err := service.ValidateOTPService(newReq, newReqHeader, logModel, logModel.StepName)
	if err != nil {
		response.Code = "500"
		response.Message = "Internal server error"
		return &response, err
	}

	if validateOTPResponse.Code != "200" {
		response.Code = "400"
		response.BizError = validateOTPResponse.BizCode
		response.Message = validateOTPResponse.Message
		return &response, nil
	}

	serviceDaoOrderHistory := orderhistory.NewIServiceDaoOrderHistory(confs.Databases)
	err = serviceDaoOrderHistory.InsertOrderHistory(orderhistory.OrderHistory{
		CorrelationID:       req.CorrelationID,
		OrderNumber:         orderData.OrderInfo.OrderNumber,
		UserID:              claims.ChannelSystem,
		ActivityName:        "ValidateOTP",
		ActivityDescription: "ValidateOTP " + orderData.OrderInfo.OrderStateCode,
		CreatedDate:         primitive.NewDateTimeFromTime(time.Now()),
		CreatedBy:           claims.ChannelSystem,
	}, logModel)

	if err != nil {
		response.Code = "500"
		response.Message = "Internal server error"
		return &response, err
	}

	response = utils.ResponseStandard{
		Code:         "200",
		BizError:     "Success",
		Message:      "Success",
		TranID:       "",
		ApiCode:      "",
		HlTrackingId: "",
		Data:         nil,
	}

	return &response, nil
}
