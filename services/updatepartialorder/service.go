package updatepartialorder

import (
	"hl-order-api/configs"
	"hl-order-api/routes/models"
	"hl-order-api/services/flowconfig"
	"hl-order-api/services/order"
	"hl-order-api/services/orderhistory"
	"hl-order-api/services/psa"
	"hl-order-api/services/tsmpayment"
	"hl-order-api/services/tsmsale"
	"hl-order-api/util"
	"time"

	"github.com/gofiber/fiber/v2"
	"gitlab.com/true-itsd/iservicemax/omni/api/ms/psa/services/deviceserviceinfo"
	"gitlab.com/true-itsd/iservicemax/omni/pkg/common-utils-backend/services/logger"
	"gitlab.com/true-itsd/iservicemax/omni/pkg/common-utils-backend/utils"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type IServiceUpdatePartialOrder interface {
	ServiceUpdatePartialOrder(req models.UpdatePartialOrderRequest, claims util.Claims, flowid *flowconfig.OrderFlowidConfig, logModel logger.LogModel) (bool, utils.ResponseStandard)
}

type ServiceUpdatePartialOrder struct {
	Databases utils.SystemDatabase
}

func NewIServiceUpdatePartialOrder(db utils.SystemDatabase) IServiceUpdatePartialOrder {
	return &ServiceUpdatePartialOrder{
		Databases: db,
	}
}

func (r *ServiceUpdatePartialOrder) ServiceUpdatePartialOrder(req models.UpdatePartialOrderRequest, claims util.Claims, flowid *flowconfig.OrderFlowidConfig, logModel logger.LogModel) (bool, utils.ResponseStandard) {
	var response utils.ResponseStandard

	serviceDaoSaveOrder := order.NewIServiceDaoOrder(configs.Conf.Database)
	err, orderData := serviceDaoSaveOrder.GetOrder(req.OrderInfo.OrderNumber, claims.ChannelSystem, logModel)
	if err != nil {
		response.Code = util.Code500
		response.BizError = fiber.ErrInternalServerError.Message
		response.Message = err.Error()
		response.System = util.System_HL_ORDER_API
		return false, response
	}
	if orderData == nil && err == nil {
		response.Code = util.Code404
		response.BizError = fiber.ErrNotFound.Message
		response.Message = fiber.ErrNotFound.Message
		response.System = util.System_HL_ORDER_API
		return false, response
	}

	if orderData.OrderInfo.OrderStateCode != "PAID" {
		response.Code = util.Code404
		response.BizError = fiber.ErrNotFound.Message
		response.Message = fiber.ErrNotFound.Message
		response.System = util.System_HL_ORDER_API
		return false, response
	}

	defer func() {
		serviceDaoOrderHistory := orderhistory.NewIServiceDaoOrderHistory(r.Databases)
		serviceDaoOrderHistory.InsertOrderHistory(orderhistory.OrderHistory{
			CorrelationID:       req.CorrelationID,
			OrderNumber:         req.OrderInfo.OrderNumber,
			UserID:              claims.ChannelSystem,
			ActivityName:        "UpdatePartialOrder",
			ActivityDescription: "UpdatePartialOrder " + orderData.OrderInfo.OrderStateCode,
			CreatedDate:         primitive.NewDateTimeFromTime(time.Now()),
			CreatedBy:           claims.ChannelSystem,
		}, logModel)
	}()

	servicePsa := psa.NewIServicePSA()
	updatePartialOrderResponse := new(models.UpdatePartialOrderResponse)
	if len(req.OrderInfo.OrderItem) > 0 {
		for n, d := range req.OrderInfo.OrderItem {
			respPsa, code, bizError, err := servicePsa.ServiceGetDeviceServiceInfo(deviceserviceinfo.GetDeviceServiceInfoRequest{
				ReqTrxId: req.CorrelationID,
				Key: []deviceserviceinfo.KeyInfo{
					{
						Name:  d.InventoryType,
						Value: d.Serial,
					},
				},
				User: deviceserviceinfo.UserInfo{
					AppUser:       "HEADLESS",
					ChannelAccess: "HL-TOS",
					ChannelCode:   orderData.SaleInfo.DealerCode,
					ChannelUSer:   claims.ChannelAlias,
					USerCode:      claims.EmployeeID,
					UserLogin:     claims.UserName,
				},
			}, logModel)
			if code != "200" && code != "404" {
				response.Code = code
				response.BizError = bizError
				response.Message = err.Error()
				response.System = util.System_PSA
				return false, response
			}
			errorMessage := ""
			foundFlag := true
			if code == "404" {
				errorMessage = respPsa.Resp.Message
				foundFlag = false
			}
			updatePartialOrderResponse.OrderItem = append(updatePartialOrderResponse.OrderItem, models.OrderItemResponse{
				Sequence:      n + 1,
				ProductCode:   d.ProductCode,
				Serial:        d.Serial,
				InventoryType: d.InventoryType,
				SerialStatus:  d.SerialStatus,
				FoundFlag:     foundFlag,
				ErrorMessage:  errorMessage,
			})
		}
	}

	serviceTsmSale := tsmsale.NewIServiceTsmSale()
	_, code, bizError, err := serviceTsmSale.ServiceSubmitOrderOnline(req, logModel)
	if err != nil {
		response.Code = code
		response.BizError = bizError
		response.Message = err.Error()
		response.System = util.System_TSM_SALE
		return false, response
	}

	serviceTsmPayment := tsmpayment.NewIServiceTsmPayment()
	_, code, bizError, err = serviceTsmPayment.ServiceAddPayment(req, orderData, logModel)
	if err != nil {
		response.Code = code
		response.BizError = bizError
		response.Message = err.Error()
		response.System = util.System_TSM_PAYMENT
		return false, response
	}

	for _, d := range req.OrderInfo.OrderItem {
		err := serviceDaoSaveOrder.UpdateOrderItems(req.OrderInfo.OrderNumber, order.OrderItems{
			Sequence:      d.Sequence,
			ProductCode:   d.ProductCode,
			Serial:        d.Serial,
			InventoryType: d.InventoryType,
			SerialStatus:  d.SerialStatus,
		}, logModel)
		if err != nil {
			response.Code = util.Code500
			response.BizError = fiber.ErrInternalServerError.Message
			response.Message = err.Error()
			response.System = util.System_HL_ORDER_API
			return false, response
		}
	}

	response = util.GetResponse200(response)
	response.Data = updatePartialOrderResponse
	listFalse := []int{}
	for _, d := range updatePartialOrderResponse.OrderItem {
		if !d.FoundFlag {
			listFalse = append(listFalse, 1)
		}
	}
	if len(listFalse) != 0 {
		response.Code = util.Code422
		response.System = util.System_HL_ORDER_API
		if len(listFalse) == len(updatePartialOrderResponse.OrderItem) {
			response.BizError = fiber.ErrNotFound.Message
			response.Message = fiber.ErrNotFound.Message
		} else if len(listFalse) != len(updatePartialOrderResponse.OrderItem) {
			response.BizError = "Some of results found"
			response.Message = "Some of results found"
		}
	}

	return true, response
}
