package submitorder

import (
	"hl-order-api/configs"
	"hl-order-api/routes/models"
	"hl-order-api/services/flowconfig"
	"hl-order-api/services/intx"
	"hl-order-api/services/omx"
	"hl-order-api/services/order"
	"hl-order-api/services/orderhistory"
	"hl-order-api/services/psa"
	"hl-order-api/util"
	"time"

	"github.com/gofiber/fiber/v2"
	"gitlab.com/true-itsd/iservicemax/omni/api/ms/omx/services/submitorder"
	"gitlab.com/true-itsd/iservicemax/omni/pkg/common-utils-backend/services/logger"
	"gitlab.com/true-itsd/iservicemax/omni/pkg/common-utils-backend/utils"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type IServiceSubmitOrder interface {
	ServiceSubmitOrder(req models.SubmitOrderRequest, claims util.Claims, flowid *flowconfig.OrderFlowidConfig, logModel logger.LogModel) (bool, utils.ResponseStandard)
}

type ServiceSubmitOrder struct {
	Databases utils.SystemDatabase
}

func NewIServiceSubmitOrder(db utils.SystemDatabase) IServiceSubmitOrder {
	return &ServiceSubmitOrder{
		Databases: db,
	}
}

func (r *ServiceSubmitOrder) ServiceSubmitOrder(req models.SubmitOrderRequest, claims util.Claims, flowid *flowconfig.OrderFlowidConfig, logModel logger.LogModel) (bool, utils.ResponseStandard) {
	var response utils.ResponseStandard

	serviceDaoSaveOrder := order.NewIServiceDaoOrder(configs.Conf.Database)
	err, orderData := serviceDaoSaveOrder.GetOrder(req.OrderInfo.OrderNumber, claims.ChannelSystem, logModel)
	if err != nil {
		response.Code = util.Code500
		response.BizError = fiber.ErrInternalServerError.Message
		response.Message = err.Error()
		response.System = util.System_HL_ORDER_API
		return false, response
	}
	if orderData == nil && err == nil {
		response.Code = util.Code404
		response.BizError = fiber.ErrNotFound.Message
		response.Message = fiber.ErrNotFound.Message
		response.System = util.System_HL_ORDER_API
		return false, response
	}

	if orderData.OrderInfo.OrderStateCode != "PAID" {
		response.Code = util.Code404
		response.BizError = fiber.ErrNotFound.Message
		response.Message = fiber.ErrNotFound.Message
		response.System = util.System_HL_ORDER_API
		return false, response
	}

	defer func() {
		serviceDaoOrderHistory := orderhistory.NewIServiceDaoOrderHistory(r.Databases)
		serviceDaoOrderHistory.InsertOrderHistory(orderhistory.OrderHistory{
			CorrelationID:       req.CorrelationID,
			OrderNumber:         req.OrderInfo.OrderNumber,
			UserID:              claims.ChannelSystem,
			ActivityName:        "SubmitOrder",
			ActivityDescription: "SubmitOrder " + orderData.OrderInfo.OrderStateCode,
			CreatedDate:         primitive.NewDateTimeFromTime(time.Now()),
			CreatedBy:           claims.ChannelSystem,
		}, logModel)
	}()

	servicePsa := psa.NewIServicePSA()
	_, code, bizError, err := servicePsa.ServiceUpdateWarrantyDevice(req, claims, flowid, orderData, logModel)
	if err != nil {
		response.Code = code
		response.BizError = bizError
		response.Message = err.Error()
		response.System = util.System_PSA
		return false, response
	}

	respSubmitOMX := new(submitorder.SubmitOMXResponse)
	if orderData.SubscriberInfo.Priceplan != "" {
		serviceINTX := intx.NewIServiceINTX()
		respGetPromotion, code, bizError, err := serviceINTX.ServiceGetPromotionListByBusinessLine(req, orderData, logModel)
		if err != nil {
			response.Code = code
			response.BizError = bizError
			response.Message = err.Error()
			response.System = util.System_INTX
			return false, response
		}

		serviceOMX := omx.NewIServiceOMX()
		respSubmitOMX, code, bizError, err = serviceOMX.ServiceSubmitOMX(req, orderData, respGetPromotion, logModel)
		if err != nil {
			response.Code = code
			response.BizError = bizError
			response.Message = err.Error()
			response.System = util.System_OMX
			return false, response
		}
	}

	err = serviceDaoSaveOrder.UpdateForSubmitOrder(req.OrderInfo.OrderNumber, "COMPLETED", *req.OrderInfo.SubmitOrderDate.Time, logModel)
	if err != nil {
		response.Code = util.Code500
		response.BizError = fiber.ErrInternalServerError.Message
		response.Message = err.Error()
		response.System = util.System_HL_ORDER_API
		return false, response
	}

	response = util.GetResponse200(response)
	response.OmxTrackingId = respSubmitOMX.SubmitOrderResponse.OMXTrackingID
	return true, response
}
