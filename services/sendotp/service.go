package sendotp

import (
	"hl-order-api/configs"
	"hl-order-api/routes/models"
	"hl-order-api/services/order"
	"hl-order-api/services/orderhistory"
	"time"

	b64 "encoding/base64"
	"encoding/json"
	"hl-order-api/util"

	requestOTP "gitlab.com/true-itsd/iservicemax/omni/api/ms/hl-otp/services/requestotp"
	"gitlab.com/true-itsd/iservicemax/omni/pkg/common-utils-backend/services/logger"
	"gitlab.com/true-itsd/iservicemax/omni/pkg/common-utils-backend/utils"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type IServiceSendOTP interface {
	ServiceSendOTP(models.RequestSendOTP, util.Claims, logger.LogModel) (*utils.ResponseStandard, error)
}

type ServiceSendOTP struct {
	config    utils.EtcdApiConfig
	Databases utils.SystemDatabase
}

func NewIServiceSendOTP(config utils.EtcdApiConfig, db utils.SystemDatabase) IServiceSendOTP {
	return &ServiceSendOTP{
		config:    config,
		Databases: db,
	}
}

func (confs *ServiceSendOTP) ServiceSendOTP(req models.RequestSendOTP, claims util.Claims, logModel logger.LogModel) (*utils.ResponseStandard, error) {
	var response utils.ResponseStandard

	if req.FunctionName != "VERIFY_CUSTOMER_PICKUP" {
		response.Code = "422"
		response.BizError = "FunctionName invalid"
		response.Message = "FunctionName must be VERIFY_CUSTOMER_PICKUP"
		return &response, nil
	}

	serviceDaoGetOrder := order.NewIServiceDaoOrder(configs.Conf.Database)
	err, orderData := serviceDaoGetOrder.GetOrderByOrderNumber(req.OrderNumber, logModel)
	if err != nil {
		response.Code = "500"
		response.Message = "Internal server error"
		return &response, err
	}

	if orderData == nil {
		response.Code = "400"
		response.BizError = "NOTFOUND"
		response.Message = "Data not found"
		return &response, nil
	}

	if orderData.OrderInfo.OrderStateCode != "READY_TO_PICK_UP" {
		response.Code = "422"
		response.BizError = "OrderStateCode invalid"
		response.Message = "OrderStateCode is not equal READY_TO_PICK_UP"
		return &response, nil
	}

	confRequestOTP := util.ConvertGetEndpointAuthentication(confs.config, "requestOTP", "requestOTP")
	callRequestOTP := requestOTP.NewCallRequestOTP(confRequestOTP)
	service := requestOTP.NewServiceRequestOTP(callRequestOTP)
	newContactNumber := "66" + orderData.CustomerInfo.ContactNo[1:]
	reqData := models.RequestOTPData{
		Function: req.FunctionName,
		Msisdn:   newContactNumber,
		Language: "TH",
	}
	reqOTPJson, err := json.Marshal(reqData)
	if err != nil {
		response.Code = "500"
		response.Message = err.Error()
		return &response, nil
	}
	reqOTPBase64 := b64.URLEncoding.EncodeToString([]byte(reqOTPJson))
	newReq := requestOTP.RequestOTPRequest{
		Data: reqOTPBase64,
	}
	newReqHeader := requestOTP.RequestHeaderOTPRequest{
		CorrelationId: req.CorrelationID,
		ChannelName:   "headless",
	}
	requestOTPResponse, err := service.RequestOTPService(newReq, newReqHeader, logModel, logModel.StepName)
	if err != nil {
		response.Code = "500"
		response.Message = "Internal server error"
		return &response, err
	}

	if requestOTPResponse.Code != "200" {
		response.Code = "400"
		response.BizError = requestOTPResponse.BizError
		response.Message = requestOTPResponse.Message
		return &response, nil
	}

	sendOTPString, err := b64.URLEncoding.DecodeString(requestOTPResponse.Data)
	if err != nil {
		response.Code = "500"
		response.Message = err.Error()
		return &response, nil
	}
	var sendOTPData models.ResponseSendOTPData
	err = json.Unmarshal(sendOTPString, &sendOTPData)
	if err != nil {
		response.Code = "500"
		response.Message = err.Error()
		return &response, nil
	}

	userID := claims.EmployeeID
	createdBy := claims.ThaiName
	if userID == "" {
		userID = claims.ChannelSystem
	}
	if createdBy == "" {
		createdBy = claims.ChannelSystem
	}
	serviceDaoOrderHistory := orderhistory.NewIServiceDaoOrderHistory(confs.Databases)
	err = serviceDaoOrderHistory.InsertOrderHistory(orderhistory.OrderHistory{
		CorrelationID:       req.CorrelationID,
		OrderNumber:         orderData.OrderInfo.OrderNumber,
		UserID:              userID,
		ActivityName:        "SendOTP",
		ActivityDescription: "SendOTP " + orderData.OrderInfo.OrderStateCode,
		CreatedDate:         primitive.NewDateTimeFromTime(time.Now()),
		CreatedBy:           createdBy,
	}, logModel)

	if err != nil {
		response.Code = "500"
		response.Message = "Internal server error"
		return &response, err
	}

	response = utils.ResponseStandard{
		Code:         "200",
		BizError:     "Success",
		Message:      "Success",
		TranID:       "",
		ApiCode:      "",
		HlTrackingId: "",
		Data:         sendOTPData,
	}

	return &response, nil
}
