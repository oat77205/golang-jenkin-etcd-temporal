package goodsreceive

import (
	"encoding/json"
	"hl-order-api/configs"
	"hl-order-api/routes/models"
	"hl-order-api/services/flowconfig"
	"hl-order-api/services/order"
	"hl-order-api/services/orderhistory"
	"hl-order-api/util"
	"strings"
	"time"

	"gitlab.com/true-itsd/iservicemax/omni/api/ms/sale-profile/services/gshopsearch"

	"go.mongodb.org/mongo-driver/bson/primitive"

	"github.com/google/uuid"
	"gitlab.com/true-itsd/iservicemax/omni/api/ms/com-platform/services/whatup"
	gettrackingdetail "gitlab.com/true-itsd/iservicemax/omni/api/ms/inventory/services/gettrackingdetail"
	"gitlab.com/true-itsd/iservicemax/omni/api/ms/inventory/services/saveomnitracking"
	tostoken "gitlab.com/true-itsd/iservicemax/omni/api/ms/partner/services/token"
	updateorderstatus "gitlab.com/true-itsd/iservicemax/omni/api/ms/partner/services/updateorderstatus"
	"gitlab.com/true-itsd/iservicemax/omni/pkg/common-utils-backend/services/logger"
	"gitlab.com/true-itsd/iservicemax/omni/pkg/common-utils-backend/utils"
)

type IServiceGoodsReceive interface {
	GetInstance() *ServiceGoodsReceive
	ServiceSaveGoodsReceive(models.RequestGoodsReceive, util.Claims, logger.LogModel) (bool, utils.ResponseStandard)
	IsOrderReadyToPickup(logModel logger.LogModel, trackingDetail gettrackingdetail.GetTrackingDetailResponse, orderData *order.Order) (bool, error)
}

type ServiceGoodsReceive struct {
	ServiceGetTrackingDetail gettrackingdetail.IServiceGetTrackingDetail
	ServiceSaveOmniTracking  saveomnitracking.IServiceSaveOmniTracking
	ServiceToken             tostoken.IServiceGetToken
	ServiceUpdateOrderStatus updateorderstatus.IServiceUpdateOrderStatus
	ServiceComplat           whatup.IServiceWhatUpSendSms
	ServiceGShopSearch       gshopsearch.IServiceGShopSearch
	Databases                utils.SystemDatabase
}

func NewIServiceGoodsReceive(confs utils.EtcdApiConfig, confsToken utils.EtcdApiConfig, confsPlatform utils.EtcdApiConfig, confComplatForm utils.EtcdApiConfig, db utils.SystemDatabase) IServiceGoodsReceive {

	confGetTrackingDetail := util.ConvertGetEndpointAuthentication(confs, "getTrackingDetailPath", "getTrackingDetail")
	newCallGetTrackingDetail := gettrackingdetail.NewCallGetTrackingDetail(confGetTrackingDetail)
	serviceGetTrackingDetail := gettrackingdetail.NewServiceGetTrackingDetail(newCallGetTrackingDetail)

	confGetSaveOmniTracking := util.ConvertGetEndpointAuthentication(confs, "saveOmniTrackingEndpoint", "saveOmniTracking")
	newCallSaveOmniTracking := saveomnitracking.NewCallSaveOmniTracking(confGetSaveOmniTracking)
	serviceSaveOmniTracking := saveomnitracking.NewServiceSaveOmniTracking(newCallSaveOmniTracking)

	confToken := util.ConvertGetEndpointAuthentication(confsToken, "tokenEndpoint", "getToken")
	newCallGetToken := tostoken.NewCallGetToken(confToken)
	serviceToken := tostoken.NewServiceGetToken(newCallGetToken)

	confUpdateOrderStatus := util.ConvertGetEndpointAuthentication(confsPlatform, "updateOrderStatusEndpoint", "updateOrderStatus")
	newCallUpdateOrderStatus := updateorderstatus.NewCallUpdateOrderStatus(confUpdateOrderStatus)
	serviceUpdateOrderStatus := updateorderstatus.NewServiceUpdateOrderStatus(newCallUpdateOrderStatus)

	confWhatup := util.ConvertGetEndpointAuthentication(confComplatForm, "smsgTemplate", "smsgTemplate")
	newCalComplat := whatup.NewCalWhatUpSendSms(confWhatup)
	serviceComplat := whatup.NewServiceWhatUpSendSms(newCalComplat)

	confGShopSearch := util.ConvertGetEndpointAuthentication(confs, "gShopSearch", "")
	callGShopSearch := gshopsearch.NewCallGShopSearch(confGShopSearch)
	gShopSearchService := gshopsearch.NewServiceGShopSearch(callGShopSearch)

	return &ServiceGoodsReceive{
		ServiceGetTrackingDetail: serviceGetTrackingDetail,
		ServiceSaveOmniTracking:  serviceSaveOmniTracking,
		ServiceToken:             serviceToken,
		ServiceUpdateOrderStatus: serviceUpdateOrderStatus,
		ServiceComplat:           serviceComplat,
		ServiceGShopSearch:       gShopSearchService,
		Databases:                db,
	}
}

func (in *ServiceGoodsReceive) GetInstance() *ServiceGoodsReceive {
	return in
}

func (in *ServiceGoodsReceive) ServiceSaveGoodsReceive(req models.RequestGoodsReceive, claims util.Claims, logModel logger.LogModel) (bool, utils.ResponseStandard) {
	var response utils.ResponseStandard

	//get omni tracking (return all tracking in order id)
	//--check tracking list status
	//----if is DELIVERED
	//------check ready to pickup? (status = "READY_TO_PICK_UP")
	//------save omni tracking (status = "READY_TO_PICK_UP")
	//------send sms
	//------update order status(status = "READY_TO_PICK_UP")
	//--------if channel == TOS > update order status (TOS API)
	//------save order history

	serviceDaoSaveOrder := order.NewIServiceDaoOrder(configs.Conf.Database)

	serviceDaoOrderFlowidConfig := flowconfig.NewIServiceDaoOrderFlowidConfig(configs.Conf.Database)

	serviceDaoOrderHistory := orderhistory.NewIServiceDaoOrderHistory(in.Databases)

	var orderFlowId *flowconfig.OrderFlowidConfig
	var responseGoodsReceiveList []models.ResponseGoodsReceive
	for _, receive := range req.GoodsReceive {
		goodsReceiveResult := models.ResponseGoodsReceive{TrackingId: receive.TrackingId}

		//get tracking
		getTrackingDetailReq := BuildGetTrackingDetail(receive.OrderNumber)
		trackingDetailResp := in.ServiceGetTrackingDetail.GetTrackingDetailService(getTrackingDetailReq, logModel, "OMNI - GetTrackingDetailResult")
		if trackingDetailResp.Code != "200" {
			goodsReceiveResult.Status = "FAILED"
			goodsReceiveResult.Message = trackingDetailResp.Message
			responseGoodsReceiveList = append(responseGoodsReceiveList, goodsReceiveResult)
			continue
		}

		if trackingDetailResp.Code == "200" && trackingDetailResp.Data == nil {
			goodsReceiveResult.Status = "FAILED"
			goodsReceiveResult.Message = trackingDetailResp.Message
			responseGoodsReceiveList = append(responseGoodsReceiveList, goodsReceiveResult)
			continue
		}

		//@TODO is ready to pickup response ???

		err, orderData := serviceDaoSaveOrder.GetOrderByOrderNumber(receive.OrderNumber, logModel)
		if err != nil {
			goodsReceiveResult.Status = "FAILED"
			goodsReceiveResult.Message = err.Error()
			responseGoodsReceiveList = append(responseGoodsReceiveList, goodsReceiveResult)
			continue
		}

		if orderData == nil || (orderData != nil && orderData.CorrelationID == "") {
			goodsReceiveResult.Status = "FAILED"
			goodsReceiveResult.Message = "Order not found"
			responseGoodsReceiveList = append(responseGoodsReceiveList, goodsReceiveResult)
			continue
		}

		//get flow config
		err, orderFlowId = serviceDaoOrderFlowidConfig.GetOrderFlowidConfig(orderData.FlowID, logModel)
		if err != nil {
			goodsReceiveResult.Status = "FAILED"
			goodsReceiveResult.Message = "Flow id not found"
			responseGoodsReceiveList = append(responseGoodsReceiveList, goodsReceiveResult)
			continue
		}

		if orderFlowId == nil && err == nil {
			goodsReceiveResult.Status = "FAILED"
			goodsReceiveResult.Message = "Flow id not found"
			responseGoodsReceiveList = append(responseGoodsReceiveList, goodsReceiveResult)
			continue
		}

		//save omni tracking
		saveOmniTrackingReq := BuildSaveOmniTrackingRequest(req.CorrelationId, receive, claims, orderFlowId)
		saveOmniTrackingRes := in.ServiceSaveOmniTracking.SaveOmniTrackingService(saveOmniTrackingReq, logModel)
		if saveOmniTrackingRes.Code != "200" {
			goodsReceiveResult.Status = "FAILED"
			goodsReceiveResult.Message = saveOmniTrackingRes.Message
			responseGoodsReceiveList = append(responseGoodsReceiveList, goodsReceiveResult)
			continue
		}

		goodsReceiveResult = models.ResponseGoodsReceive{TrackingId: receive.TrackingId}
		goodsReceiveResult.Status = "SUCCESS"
		responseGoodsReceiveList = append(responseGoodsReceiveList, goodsReceiveResult)
	}

	orderNumberList := GetOrderNumber(req.GoodsReceive)
	for _, orderNumber := range orderNumberList {
		getTrackingDetailReq := BuildGetTrackingDetail(orderNumber)
		trackingDetailResp := in.ServiceGetTrackingDetail.GetTrackingDetailService(getTrackingDetailReq, logModel, "OMNI - GetTrackingDetialByOrderForCheckReadyToPickup")
		if trackingDetailResp.Code != "200" {
			continue
		}

		if trackingDetailResp.Code == "200" && trackingDetailResp.Data == nil {
			continue
		}

		err, orderData := serviceDaoSaveOrder.GetOrderByOrderNumber(orderNumber, logModel)
		if err != nil {
			continue
		}

		//isReady
		isReadyToPickUp, err := in.IsOrderReadyToPickup(logModel, *trackingDetailResp, orderData)
		if err != nil {
			continue
		}

		if !isReadyToPickUp {
			continue
		}

		orderStatus := "READY_TO_PICK_UP"

		orderData.OrderInfo.OrderStateCode = orderStatus
		serviceDaoSaveOrder.UpdateOrderState(orderData, logModel)
		if err != nil {
			continue
		}

		serviceDaoOrderHistory.InsertOrderHistory(orderhistory.OrderHistory{
			CorrelationID:       req.CorrelationId,
			OrderNumber:         orderNumber,
			UserID:              claims.ChannelSystem,
			ActivityName:        "GoodsReceive",
			ActivityDescription: "GoodsReceive " + orderStatus,
			CreatedDate:         primitive.NewDateTimeFromTime(time.Now()),
			CreatedBy:           claims.ChannelSystem,
		}, logModel)

		//send sms
		if orderFlowId.GoodsReceive.SmsToCustomer.Action == true {
			currentDate := util.ToThaiTime(time.Now())
			currentDateStr := currentDate.Format("2006-01-02T15:04:05+07:00")
			//"01095830220007.1,{'orderid':'[orderid]', 'destinationshopname':'[destinationshopname]', 'startdate':'[startdate]','enddate':'[enddate]'}"
			destinationShopName := orderData.SaleInfo.TargetDealerName
			startDateStr := currentDate.Format(orderFlowId.GoodsReceive.SmsToCustomer.Sms.DateFormat)
			endDate := currentDate.AddDate(0, 0, orderFlowId.PickupDueDate)
			endDateStr := endDate.Format(orderFlowId.GoodsReceive.SmsToCustomer.Sms.DateFormat)
			templateContent := orderFlowId.GoodsReceive.SmsToCustomer.Sms.TemplateContent
			templateContent = strings.ReplaceAll(templateContent, "[orderid]", orderNumber)
			templateContent = strings.ReplaceAll(templateContent, "[destinationshopname]", destinationShopName)
			templateContent = strings.ReplaceAll(templateContent, "[startdate]", startDateStr)
			templateContent = strings.ReplaceAll(templateContent, "[enddate]", endDateStr)

			msisdn := orderData.CustomerInfo.ContactNo
			if strings.HasPrefix(msisdn, "0") {
				msisdn = "66" + msisdn[1:]
			}
			// get shop name
			whatupReq := whatup.WhatupSendSmsRequest{
				DateTime:          currentDateStr,
				ServiceId:         orderFlowId.GoodsReceive.SmsToCustomer.Sms.ServiceId,
				Msisdn:            msisdn,
				SourceAddressInfo: orderFlowId.GoodsReceive.SmsToCustomer.Sms.ShortCode,
				TemplateContent:   templateContent,
				Language:          "TH",
			}
			in.ServiceComplat.WhatUpSendSmsService(whatupReq, logModel)
		}

		if orderData.Channel == "TOS" {
			//update order status to tos
			tokenRes := in.ServiceToken.GetTokenService(logModel)
			if tokenRes.Code != "200" {
				continue
			}

			tosOrderStatus := "GOODS_RECEIVED"
			updateOrderStatus := updateorderstatus.UpdateOrderStatusRequest{
				Token:       tokenRes.Data.AccessToken,
				OrderNumber: orderNumber,
				OrderStatus: tosOrderStatus,
			}

			in.ServiceUpdateOrderStatus.UpdateOrderStatusService(updateOrderStatus, logModel)
			//if updateOrderResp.Code != "200" {
			//	continue
			//}
		}
	}

	response = util.GetResponse200(response)
	response.Data = models.DataGoodsReceive{GoodsReceive: responseGoodsReceiveList}
	return true, response
}

func (in *ServiceGoodsReceive) IsOrderReadyToPickup(logModel logger.LogModel, trackingDetail gettrackingdetail.GetTrackingDetailResponse, orderData *order.Order) (bool, error) {
	startStep := time.Now()

	reqByte, _ := json.Marshal(trackingDetail)

	logStepRequest := logger.LogStepRequest{
		StepName:    "HL - IsOrderReadyToPickup",
		StartDate:   utils.ConvDatetimeFormatLog(startStep),
		StepRequest: string(reqByte),
		Endpoint:    "",
		Method:      "POST",
		System:      "HL",
	}

	isReady := false

	cntTracking := 0
	for _, orderItem := range orderData.OrderInfo.OrderItems {
		if len(strings.TrimSpace(orderItem.TrackingId)) > 0 {
			cntTracking++
		}
	}

	if cntTracking == 0 || cntTracking != len(orderData.OrderInfo.OrderItems) {
		resByte, _ := json.Marshal(isReady)
		logStepRequest.StepResponse = string(resByte)
		logStepRequest.ResultCode = "200"
		logStepRequest.ResultDesc = "Success"
		logger.LogStep(logStepRequest, logModel, startStep)
		return isReady, nil
	}

	cntReadyToPickUp := 0
	for _, tracking := range trackingDetail.Data.TrackingList {
		if tracking.TrackingStatus == "READY_TO_PICK_UP" {
			cntReadyToPickUp++
		}
	}

	if cntReadyToPickUp > 0 && cntReadyToPickUp == len(trackingDetail.Data.TrackingList) {
		isReady = true
	}

	resByte, _ := json.Marshal(isReady)
	logStepRequest.StepResponse = string(resByte)
	logStepRequest.ResultCode = "200"
	logStepRequest.ResultDesc = "Success"
	logger.LogStep(logStepRequest, logModel, startStep)

	return isReady, nil
}

func BuildGetTrackingDetail(orderNumber string) gettrackingdetail.GetTrackingDetailRequest {
	return gettrackingdetail.GetTrackingDetailRequest{
		CorrelationId:  uuid.New().String(),
		OrderNumber:    orderNumber,
		PickupShopCode: "",
		StockType:      "",
		TrackingStatus: "",
		TrackingNumber: "",
		StartDate:      "",
		EndDate:        "",
	}
}

func BuildSaveOmniTrackingRequest(correlationId string, req models.GoodsReceive, claims util.Claims, flowId *flowconfig.OrderFlowidConfig) saveomnitracking.SaveOmniTrackingRequest {

	//READY_TO_PICK_UP due date > 14
	//REJECTED_TRACKING due date null
	userId := claims.EmployeeID
	if len(userId) == 0 {
		userId = claims.ChannelSystem
	}

	pickupName := claims.ThaiName
	if len(pickupName) == 0 {
		pickupName = claims.ChannelSystem
	}

	saveOmniTrackingReq := saveomnitracking.SaveOmniTrackingRequest{
		CorrelationID:  correlationId,
		OrderNumber:    req.OrderNumber,
		Channel:        claims.ChannelSystem, //jwt
		TrackingNumber: req.TrackingId,
		TrackingStatus: req.TrackingStatus,
		StatusReason:   req.TrackingReason,
		PickupName:     pickupName, //jwt ชื่อ นามสกุล
		UserID:         userId,     //jwt id
	}

	var imageList []saveomnitracking.ImagePathList
	for _, image := range req.ImageList {
		imageList = append(imageList, saveomnitracking.ImagePathList{
			RefID:     image.RefId,
			ImagePath: image.ImagePath,
			Type:      "image",
		})
	}
	if len(imageList) > 0 {
		saveOmniTrackingReq.ImagePathList = imageList
	}

	if req.TrackingStatus == "READY_TO_PICK_UP" {
		saveOmniTrackingReq.DueDate = flowId.PickupDueDate
	}

	return saveOmniTrackingReq
}

func GetOrderNumber(goodsReceive []models.GoodsReceive) []string {
	unique := make(map[string]bool, len(goodsReceive))
	us := make([]string, len(unique))
	for _, elem := range goodsReceive {
		orderNumber := elem.OrderNumber
		if elem.OrderNumber != "" && !unique[orderNumber] {
			us = append(us, orderNumber)
			unique[orderNumber] = true
		}
	}
	return us
}
