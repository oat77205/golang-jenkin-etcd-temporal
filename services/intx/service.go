package intx

import (
	"errors"
	"hl-order-api/configs"
	"hl-order-api/routes/models"
	"hl-order-api/services/order"
	"hl-order-api/util"

	"github.com/gofiber/fiber/v2"
	"gitlab.com/true-itsd/iservicemax/omni/api/ms/intx/services/getofferdetail"
	"gitlab.com/true-itsd/iservicemax/omni/api/ms/intx/services/promotionList"
	"gitlab.com/true-itsd/iservicemax/omni/pkg/common-utils-backend/services/logger"
)

type IServiceINTX interface {
	ServiceGetPromotionListByBusinessLine(req models.SubmitOrderRequest, orderData *order.Order, logModel logger.LogModel) (response *promotionList.DataPromotionListByBusinessLineResponse, code string, bizError string, err error)
	ServiceGetOfferDetail(req models.SubmitOrderRequest, classify string, orderData *order.Order, logModel logger.LogModel) (response *getofferdetail.ResponseOfferDetailList, code string, bizError string, err error)
}

type ServiceINTX struct {
}

func NewIServiceINTX() IServiceINTX {
	return &ServiceINTX{}
}

func (r *ServiceINTX) ServiceGetPromotionListByBusinessLine(req models.SubmitOrderRequest, orderData *order.Order, logModel logger.LogModel) (response *promotionList.DataPromotionListByBusinessLineResponse, code string, bizError string, err error) {
	serviceGetPromotionList := promotionList.NewGetPromotionList(configs.Conf.Endpoint.Intx)
	resp, err := serviceGetPromotionList.CallGetPromotionListByBusinessLine(promotionList.PromotionListByBusinessLineRequest{
		BusinessLine:    "MOBILE",
		PrimResourceVal: util.ConvertNumber(orderData.SubscriberInfo.SubscriberNumber),
		CorrelatedId:    req.CorrelationID,
	}, logModel)

	if err != nil {
		return nil, "500", fiber.ErrInternalServerError.Message, err
	}

	if len(resp.Resp.Return.AgreementOffer.AgreementOfferArr) == 0 {
		return nil, "404", fiber.ErrNotFound.Message, errors.New(fiber.ErrNotFound.Message)
	}

	return &resp, "200", "Success", nil
}

func (r *ServiceINTX) ServiceGetOfferDetail(req models.SubmitOrderRequest, classify string, orderData *order.Order, logModel logger.LogModel) (response *getofferdetail.ResponseOfferDetailList, code string, bizError string, err error) {
	serviceGetOfferDetail := getofferdetail.NewCallGetOfferDetail(configs.Conf.Endpoint.Intx)
	callGetOfferDetail := getofferdetail.NewServiceGetOfferDetail(serviceGetOfferDetail)
	result, resp := callGetOfferDetail.GetOfferDetailService(getofferdetail.RequestOfferDetailList{
		CorrelatedID: req.CorrelationID,
		SearchList: struct {
			SearchInfoArray []getofferdetail.SearchInfoArray "json:\"searchInfoArray\""
		}{
			SearchInfoArray: []getofferdetail.SearchInfoArray{
				{
					Type:  "OFFERNAME",
					Value: orderData.SubscriberInfo.Priceplan,
				},
			},
		},
		Classify: classify,
	}, logModel)

	if result.Code == "500" {
		return nil, result.Code, fiber.ErrInternalServerError.Message, errors.New(result.Description)
	}

	if result.Code == "900" {
		return nil, result.Code, result.Description, errors.New(result.Description)
	}

	return resp, "200", "Success", nil
}
