package inventory

import (
	"errors"
	"fmt"
	"hl-order-api/configs"
	"hl-order-api/routes/models"
	"hl-order-api/services/order"
	"hl-order-api/util"
	"strings"
	"time"

	"gitlab.com/true-itsd/iservicemax/omni/api/ms/inventory/services/gettrackingdetail"
	"gitlab.com/true-itsd/iservicemax/omni/api/ms/inventory/services/saveomnitracking"
	"gitlab.com/true-itsd/iservicemax/omni/api/ms/inventory/services/saveomnitrackinghistory"
	"gitlab.com/true-itsd/iservicemax/omni/api/ms/sale-profile/services/gshopsearch"
	"gitlab.com/true-itsd/iservicemax/omni/pkg/common-utils-backend/services/logger"
	"gitlab.com/true-itsd/iservicemax/omni/pkg/common-utils-backend/utils"
)

type IServiceServiceGetSaveOmniTracking interface {
	ServiceGetSaveOmniTracking(req models.RequestSaveOrder, claims util.Claims, logModel logger.LogModel) (string, string, error)
}

type ServiceServiceGetSaveOmniTracking struct {
	config utils.EtcdApiConfig
}

func NewIServiceGetSaveOmniTracking(config utils.EtcdApiConfig) IServiceServiceGetSaveOmniTracking {
	return &ServiceServiceGetSaveOmniTracking{config: config}
}

func (confs *ServiceServiceGetSaveOmniTracking) ServiceGetSaveOmniTracking(req models.RequestSaveOrder, claims util.Claims, logModel logger.LogModel) (string, string, error) {
	var err error
	var code string
	var bizError string

	var confGetSaveOmniTracking utils.EtcdApiConfig
	var newCallSaveOmniTracking saveomnitracking.ICallSaveOmniTracking
	var servcieSaveOmniTracking saveomnitracking.IServiceSaveOmniTracking

	totalItems := len(req.OrderInfo.OrderItems)
	stockShopCodeSlice := []string{}
	for _, d := range req.OrderInfo.OrderItems {
		stockShopCodeSlice = append(stockShopCodeSlice, d.StockShopCode)
	}
	stockShopCodeDup := util.RemoveDuplicateStr(stockShopCodeSlice)
	if len(stockShopCodeDup) > 0 {
		confGetSaveOmniTracking = util.ConvertGetEndpointAuthentication(confs.config, "saveOmniTrackingEndpoint", "saveOmniTracking")
		newCallSaveOmniTracking = saveomnitracking.NewCallSaveOmniTracking(confGetSaveOmniTracking)
		servcieSaveOmniTracking = saveomnitracking.NewServiceSaveOmniTracking(newCallSaveOmniTracking)
	}
	for _, d := range stockShopCodeDup {
		reqSaveOmniTracking := saveomnitracking.SaveOmniTrackingRequest{
			CorrelationID:  req.CorrelationID,
			OrderNumber:    req.OrderInfo.OrderNumber,
			OrderShopCode:  req.SaleInfo.DealerCode,
			PickupShopCode: req.SaleInfo.TargetDealerCode,
			StockShopCode:  d,
			StockType:      util.GetStockShopType(d),
			Channel:        claims.ChannelSystem,
			TotalItems:     totalItems,
			TrackingStatus: "PAID",
			UserID:         claims.ChannelSystem,
		}

		resp := servcieSaveOmniTracking.SaveOmniTrackingService(reqSaveOmniTracking, logModel)
		if resp.Code != "200" {
			code = resp.Code
			bizError = resp.BizError
			err = errors.New(resp.Message)
		}
	}

	if err != nil {
		return code, bizError, err
	}
	return "200", "", nil
}

type IServiceValidateTracking interface {
	ServiceValidateTracking(models.RequestValidateTracking, util.Claims, logger.LogModel) (*utils.ResponseStandard, error)
}

type ServiceValidateTracking struct {
	config utils.EtcdApiConfig
}

func NewIServiceValidateTracking(config utils.EtcdApiConfig) IServiceValidateTracking {
	return &ServiceValidateTracking{config: config}
}

func (in *ServiceValidateTracking) ServiceValidateTracking(req models.RequestValidateTracking, claims util.Claims, logModel logger.LogModel) (*utils.ResponseStandard, error) {
	var response utils.ResponseStandard
	var validateTrackingResponse models.ResponseValidateTracking
	confGetTrackingDetail := util.ConvertGetEndpointAuthentication(in.config, "getTrackingDetailPath", "getTrackingDetail")

	callGetTrackingDetail := gettrackingdetail.NewCallGetTrackingDetail(confGetTrackingDetail)
	serviceDaoGetOrder := order.NewIServiceDaoOrder(configs.Conf.Database)

	getTrackingDetailService := gettrackingdetail.NewServiceGetTrackingDetail(callGetTrackingDetail)
	getTrackingDetailReq := gettrackingdetail.GetTrackingDetailRequest{
		CorrelationId:  req.CorrelationID,
		TrackingNumber: req.TrackingNumber,
	}

	getTrackingDetailResp := getTrackingDetailService.GetTrackingDetailService(getTrackingDetailReq, logModel, "")

	if getTrackingDetailResp.Code != "200" {
		validateTrackingResponse = BuildValidateTrackingResponse("01", "")
		response.Code = "200"
		response.BizError = "SUCCESS"
		response.Message = "SUCCESS"
		response.Data = validateTrackingResponse
		return &response, nil
	}

	if getTrackingDetailResp.Data == nil || getTrackingDetailResp.Data.TrackingList == nil {
		tmpGetTrackingDetailReq := gettrackingdetail.GetTrackingDetailRequest{
			CorrelationId: req.CorrelationID,
			OrderNumber:   req.TrackingNumber,
		}

		tmpGetTrackingDetailResp := getTrackingDetailService.GetTrackingDetailService(tmpGetTrackingDetailReq, logModel, "")
		if tmpGetTrackingDetailResp.Code != "200" {
			validateTrackingResponse = BuildValidateTrackingResponse("01", "")
			response.Code = "200"
			response.BizError = "SUCCESS"
			response.Message = "SUCCESS"
			response.Data = validateTrackingResponse
			return &response, nil
		}

		if tmpGetTrackingDetailResp.Data == nil {
			validateTrackingResponse = BuildValidateTrackingResponse("01", "")
			response.Code = "200"
			response.BizError = "SUCCESS"
			response.Message = "SUCCESS"
			response.Data = validateTrackingResponse
			return &response, nil
		}

		if len(tmpGetTrackingDetailResp.Data.TrackingList) == 0 {
			validateTrackingResponse = BuildValidateTrackingResponse("01", "")
			response.Code = "200"
			response.BizError = "SUCCESS"
			response.Message = "SUCCESS"
			response.Data = validateTrackingResponse
			return &response, nil
		}

		if tmpGetTrackingDetailResp.Data.TrackingList[0].TrackingStatus == "PAID" || tmpGetTrackingDetailResp.Data.TrackingList[0].TrackingStatus == "PACKED" {
			err, tmpOrderData := serviceDaoGetOrder.GetOrderByOrderNumber(req.TrackingNumber, logModel)
			if err != nil {
				response.Code = "500"
				response.Message = err.Error()
				return &response, nil
			}

			if tmpOrderData == nil {
				validateTrackingResponse = BuildValidateTrackingResponse("01", "")
				response.Code = "200"
				response.BizError = "SUCCESS"
				response.Message = "SUCCESS"
				response.Data = validateTrackingResponse
				return &response, nil
			}

			if tmpOrderData.OrderInfo.OrderStateCode != "WAITING_PRODUCT" {
				validateTrackingResponse = BuildValidateTrackingResponse("01", "")
				response.Code = "200"
				response.BizError = "SUCCESS"
				response.Message = "SUCCESS"
				response.Data = validateTrackingResponse
				return &response, nil
			}

			validateTrackingResponse = BuildValidateTrackingResponse("04", tmpOrderData.OrderInfo.OrderNumber)
			response.Code = "200"
			response.BizError = "SUCCESS"
			response.Message = "SUCCESS"
			response.Data = validateTrackingResponse
			return &response, nil
		}

		validateTrackingResponse = BuildValidateTrackingResponse("01", "")
		response.Code = "200"
		response.BizError = "SUCCESS"
		response.Message = "SUCCESS"
		response.Data = validateTrackingResponse
		return &response, nil
	}

	if getTrackingDetailResp.Data.TrackingList[0].TrackingStatus == "DELIVERED_FAILED" {
		validateTrackingResponse = BuildValidateTrackingResponse("04", getTrackingDetailResp.Data.TrackingList[0].TrackingNumber)
		response.Code = "200"
		response.BizError = "SUCCESS"
		response.Message = "SUCCESS"
		response.Data = validateTrackingResponse
		return &response, nil
	}

	err, orderData := serviceDaoGetOrder.GetOrderByOrderNumber(getTrackingDetailResp.Data.TrackingList[0].OrderNumber, logModel)
	if err != nil {
		response.Code = "500"
		response.Message = err.Error()
		return &response, nil
	}

	if orderData == nil {
		validateTrackingResponse = BuildValidateTrackingResponse("01", "")
		response.Code = "200"
		response.BizError = "SUCCESS"
		response.Message = "SUCCESS"
		response.Data = validateTrackingResponse
		return &response, nil
	}

	logModel.SearchKey = logModel.SearchKey + orderData.OrderInfo.OrderNumber
	if req.ShopCode != orderData.SaleInfo.TargetDealerCode {
		confGShopSearch := util.ConvertGetEndpointAuthentication(in.config, "gShopSearch", "")
		callGShopSearch := gshopsearch.NewCallGShopSearch(confGShopSearch)
		gShopSearchService := gshopsearch.NewServiceGShopSearch(callGShopSearch)
		gShopSearchReq := gshopsearch.GShopSearchRequest{
			Channel:       req.Channel,
			CorrelationID: req.CorrelationID,
			UserID:        req.UserID,
			SearchList: []gshopsearch.SearchList{
				{
					Type:  "partnerCode",
					Value: []string{req.ShopCode},
				},
			},
		}

		gShopSearchResponse, err := gShopSearchService.GShopSearchService(gShopSearchReq, logModel)
		if err != nil {
			response.Code = "500"
			response.Message = err.Error()
			response.BizError = err.Error()
			return &response, err
		}

		if gShopSearchResponse.Data == nil {
			response.Code = "400"
			response.Message = "Data not found"
			return &response, nil
		}

		if len(gShopSearchResponse.Data.ShopInfoList) == 0 {
			response.Code = "400"
			response.Message = "Data not found"
			return &response, nil
		}

		currentDate := time.Now().Format("02-01-2006")
		currentDate = strings.Replace(currentDate, "-", "/", -1)

		confSaveOmniTrackingingHist := util.ConvertGetEndpointAuthentication(in.config, "saveOmniTrackingHistory", "saveOmniTrackingHistory")
		callSaveOmniTrackingingHist := saveomnitrackinghistory.NewCallSaveOmniTrackingHistory(confSaveOmniTrackingingHist)
		saveOmniTrackingingHistService := saveomnitrackinghistory.NewServiceSaveOmniTrackingHistory(callSaveOmniTrackingingHist)

		statusReason := fmt.Sprintf("**สินค้าจัดส่งไปที่ %s เมื่อวันที่ %v \nTracking Number %s กรุณาติดต่อ Aden หรือสาขา เพื่อนำส่งคืนสินค้า**", gShopSearchResponse.Data.ShopInfoList[0].ShopNameTh, currentDate, getTrackingDetailResp.Data.TrackingList[0].TrackingNumber)
		saveOmniTrackingingHistReq := saveomnitrackinghistory.SaveOmniTrackingHistoryRequest{
			CorrelationID:  req.CorrelationID,
			TrackingID:     getTrackingDetailResp.Data.TrackingList[0].TrackingID,
			OrderNumber:    getTrackingDetailResp.Data.TrackingList[0].OrderNumber,
			Channel:        req.Channel,
			TrackingNumber: getTrackingDetailResp.Data.TrackingList[0].TrackingNumber,
			TrackingStatus: "RECEIVE_FAIL",
			StatusReason:   statusReason,
			PickupName:     claims.ThaiName,
			UserID:         req.UserID,
		}

		saveOmniTrackingingHistResp := saveOmniTrackingingHistService.SaveOmniTrackingHistoryService(saveOmniTrackingingHistReq, logModel)
		if saveOmniTrackingingHistResp.Code != "200" {
			validateTrackingResponse = BuildValidateTrackingResponse("01", "")
			response.Code = "200"
			response.BizError = "SUCCESS"
			response.Message = "SUCCESS"
			response.Data = validateTrackingResponse
			return &response, nil
		}

		gShopSearchReq2 := gshopsearch.GShopSearchRequest{
			Channel:       req.Channel,
			CorrelationID: req.CorrelationID,
			UserID:        req.UserID,
			SearchList: []gshopsearch.SearchList{
				{
					Type:  "partnerCode",
					Value: []string{orderData.SaleInfo.TargetDealerCode},
				},
			},
		}

		gShopSearchResponse2, err := gShopSearchService.GShopSearchService(gShopSearchReq2, logModel)
		if err != nil {
			response.Code = "500"
			response.Message = err.Error()
			response.BizError = err.Error()
			return &response, err
		}

		if gShopSearchResponse2.Data == nil {
			response.Code = "400"
			response.Message = "Data not found"
			return &response, nil
		}

		if len(gShopSearchResponse2.Data.ShopInfoList) == 0 {
			response.Code = "400"
			response.Message = "Data not found"
			return &response, nil
		}

		validateTrackingResponse = BuildValidateTrackingResponse("02", "")
		validateTrackingResponse.OrderInfo = &models.OrderInfo{
			OrderNumber: orderData.OrderInfo.OrderNumber,
		}
		validateTrackingResponse.TargetDealer = gShopSearchResponse2.Data.ShopInfoList[0].ShopNameTh
		response.Code = "200"
		response.BizError = "SUCCESS"
		response.Message = "SUCCESS"
		response.Data = validateTrackingResponse
		return &response, nil
	}

	trackingStatus := getTrackingDetailResp.Data.TrackingList[0].TrackingStatus
	if trackingStatus == "CANCELLED_ORDER" || trackingStatus == "COMPLETED_PICK_UP" {
		validateTrackingResponse = BuildValidateTrackingResponse("03", getTrackingDetailResp.Data.TrackingList[0].TrackingNumber)
		response.Code = "200"
		response.BizError = "SUCCESS"
		response.Message = "SUCCESS"
		response.Data = validateTrackingResponse
		return &response, nil
	}

	if trackingStatus == "READY_TO_PICK_UP" {
		validateTrackingResponse = BuildValidateTrackingResponse("03", getTrackingDetailResp.Data.TrackingList[0].TrackingNumber)
		response.Code = "200"
		response.BizError = "SUCCESS"
		response.Message = "SUCCESS"
		response.Data = validateTrackingResponse
		return &response, nil

	} else if trackingStatus == "REJECTED_TRACKING" {
		trackingTime, err := time.Parse("02-01-2006 15:05", getTrackingDetailResp.Data.TrackingList[0].StatusDate)
		if err != nil {
			response.Code = "500"
			response.Message = err.Error()
			return &response, err
		}
		equal := DateEqual(time.Now(), trackingTime)
		if !equal {
			validateTrackingResponse = BuildValidateTrackingResponse("03", getTrackingDetailResp.Data.TrackingList[0].TrackingNumber)
			response.Code = "200"
			response.BizError = "SUCCESS"
			response.Message = "SUCCESS"
			response.Data = validateTrackingResponse
			return &response, nil
		}
	} else if getTrackingDetailResp.Data.TrackingList[0].StatusReason != "" {
		validateTrackingResponse = BuildValidateTrackingResponse("03", getTrackingDetailResp.Data.TrackingList[0].TrackingNumber)
		response.Code = "200"
		response.BizError = "SUCCESS"
		response.Message = "SUCCESS"
		response.Data = validateTrackingResponse
		return &response, nil
	}

	getTrackingDetailReq2 := gettrackingdetail.GetTrackingDetailRequest{
		CorrelationId: req.CorrelationID,
		OrderNumber:   orderData.OrderInfo.OrderNumber,
	}

	getTrackingDetailResp2 := getTrackingDetailService.GetTrackingDetailService(getTrackingDetailReq2, logModel, "")
	if getTrackingDetailResp2.Code != "200" {
		response.Code = "400"
		response.Message = getTrackingDetailResp.Message
		return &response, errors.New(getTrackingDetailResp.Message)
	}

	confGShopSearch := util.ConvertGetEndpointAuthentication(in.config, "gShopSearch", "")
	callGShopSearch := gshopsearch.NewCallGShopSearch(confGShopSearch)
	gShopSearchService := gshopsearch.NewServiceGShopSearch(callGShopSearch)
	gShopSearchReq := gshopsearch.GShopSearchRequest{
		Channel:       req.Channel,
		CorrelationID: req.CorrelationID,
		UserID:        req.UserID,
		SearchList: []gshopsearch.SearchList{
			{
				Type:  "partnerCode",
				Value: []string{orderData.SaleInfo.TargetDealerCode},
			},
		},
	}

	gShopSearchResponse, err := gShopSearchService.GShopSearchService(gShopSearchReq, logModel)
	if err != nil {
		response.Code = "500"
		response.Message = err.Error()
		response.BizError = err.Error()
		return &response, err
	}

	if gShopSearchResponse.Data == nil {
		response.Code = "400"
		response.Message = "Data not found"
		return &response, nil
	}

	if len(gShopSearchResponse.Data.ShopInfoList) == 0 {
		response.Code = "400"
		response.Message = "Data not found"
		return &response, nil
	}

	totalReceived := 0
	trackingList := []models.TrackingList{}
	for _, t := range getTrackingDetailResp2.Data.TrackingList {
		tracking := models.TrackingList{
			TrackingNumber: t.TrackingNumber,
			TrackingReason: t.StatusReason,
		}

		if t.StatusReason != "" && t.StatusReason == "กล่องสภาพสมบูรณ์" {
			totalReceived++
		}

		trackingList = append(trackingList, tracking)
	}

	validateTrackingResponse = models.ResponseValidateTracking{
		ValidateCode:    "00",
		ValidateMessage: "",
		OrderInfo: &models.OrderInfo{
			OrderNumber:           orderData.OrderInfo.OrderNumber,
			OrderStatus:           orderData.OrderInfo.OrderStatus,
			OrderDate:             orderData.OrderInfo.OrderDate.Time().Format("2006-01-02 00:00"),
			TrackingList:          trackingList,
			TotalTrackingReceived: &totalReceived,
		},
		CustomerInfo: &models.ValidateTrackingCustomerInfo{
			FirstName: orderData.CustomerInfo.FirstName,
			LastName:  orderData.CustomerInfo.LastName,
		},
		TargetDealer: gShopSearchResponse.Data.ShopInfoList[0].ShopNameTh,
	}

	response = utils.ResponseStandard{
		Code:         getTrackingDetailResp.Code,
		BizError:     getTrackingDetailResp.BizError,
		Message:      getTrackingDetailResp.Message,
		TranID:       req.CorrelationID,
		ApiCode:      "",
		HlTrackingId: "",
		Data:         validateTrackingResponse,
	}

	return &response, nil
}

func DateEqual(date1, date2 time.Time) bool {
	y1, m1, d1 := date1.Date()
	y2, m2, d2 := date2.Date()
	return y1 == y2 && m1 == m2 && d1 == d2
}

func BuildValidateTrackingResponse(code string, trackingNumber string) models.ResponseValidateTracking {
	var validateTrackingResponse models.ResponseValidateTracking
	validateTrackingResponse.ValidateCode = code

	switch code {
	case "01":
		validateTrackingResponse.ValidateMessage = "หมายเลข Tracking Number ไม่ถูกต้อง\nกรุณาตรวจสอบอีกครั้ง"
	case "02":
		validateTrackingResponse.ValidateMessage = "รายการสินค้าผิดสาขา\nกรุณาส่งคืนสินค้า และแจ้งเจ้าหน้าที่ขนส่ง"
	case "03":
		validateTrackingResponse.ValidateMessage = fmt.Sprintf("หมายเลข Tracking Number %s\nรับเข้าสาขาเรียบร้อยแล้ว\nกรุณาตรวจสอบอีกครั้ง", trackingNumber)
	case "04":
		validateTrackingResponse.ValidateMessage = fmt.Sprintf("หมายเลข Order No %s\nไม่สามารถรับสินค้าเข้าระบบได้ในขณะนี้\nกรุณาติดต่อ ITSC", trackingNumber)
	default:
		validateTrackingResponse.ValidateMessage = ""
	}

	return validateTrackingResponse
}
