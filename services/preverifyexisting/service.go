package preverifyexisting

import (
	"hl-order-api/routes/models"
	"hl-order-api/services/gcs"
	"hl-order-api/util"

	"gitlab.com/true-itsd/iservicemax/omni/pkg/common-utils-backend/services/logger"
	"gitlab.com/true-itsd/iservicemax/omni/pkg/common-utils-backend/utils"
)

type IServicePreverifyExisting interface {
	ServicePreverifyExisting(req models.PreverifyExistingRequest, claims util.Claims, logModel logger.LogModel) (bool, utils.ResponseStandard)
}

type ServicePreverifyExisting struct {
}

func NewIServicePreverifyExisting() IServicePreverifyExisting {
	return &ServicePreverifyExisting{}
}

func (r *ServicePreverifyExisting) ServicePreverifyExisting(req models.PreverifyExistingRequest, claims util.Claims, logModel logger.LogModel) (bool, utils.ResponseStandard) {
	var response utils.ResponseStandard

	serviceGCS := gcs.NewIServiceGCS()
	result, resp := serviceGCS.ServicePreverifyExisting(req, claims, logModel)
	if result.Code != "200" {
		response.Code = result.Code
		response.BizError = result.Description
		response.Message = result.Description
		response.System = util.System_GCS
		return false, response
	}

	response = util.GetResponse200(response)
	if req.InquiryProfileInfo.InquiryProfileFlag {
		response.Data = models.PreverifyExistingResponse{
			MobileNo:       resp.Data.ServiceId,
			Identification: resp.Data.IdNumber,
			CompanyCode:    resp.Data.CompanyCode,
			CustomerType:   resp.Data.CustomerType,
			ProductAging:   resp.Data.ProductAging,
			CurrentOffer: models.CurrentOffer{
				OfferInfo: models.OfferInfo{
					Code:         resp.Data.CurrentOffer.Offer.Code,
					Name:         resp.Data.CurrentOffer.Offer.Name,
					Description:  resp.Data.CurrentOffer.Offer.Description,
					Types:        resp.Data.CurrentOffer.Offer.Types,
					ServiceLevel: resp.Data.CurrentOffer.Offer.ServiceLevel,
					RcRate:       resp.Data.CurrentOffer.Offer.RcRate,
				},
				ServiceAgreementInfo: models.ServiceAgreementInfo{
					Code:         resp.Data.CurrentOffer.ServiceAgreement.Code,
					Name:         resp.Data.CurrentOffer.ServiceAgreement.Name,
					Description:  resp.Data.CurrentOffer.ServiceAgreement.Description,
					Types:        resp.Data.CurrentOffer.ServiceAgreement.Types,
					ServiceLevel: resp.Data.CurrentOffer.ServiceAgreement.ServiceLevel,
					RcRate:       resp.Data.CurrentOffer.ServiceAgreement.RcRate,
				},
			},
			ProductInfo: models.ProductInfo{
				SubscriberInfo: models.SubscriberInfo{
					ConvergenceCode:     resp.Data.ProductInfo.SubscriberInfo.ConvergenceCode,
					CreateDate:          resp.Data.ProductInfo.SubscriberInfo.CreateDate,
					EffectiveDate:       resp.Data.ProductInfo.SubscriberInfo.EffectiveDate,
					InstallationType:    resp.Data.ProductInfo.SubscriberInfo.InstallationType,
					MultiSIMLevel:       resp.Data.ProductInfo.SubscriberInfo.MultiSIMLevel,
					ResourceType:        resp.Data.ProductInfo.SubscriberInfo.ResourceType,
					ResourceValue:       resp.Data.ProductInfo.SubscriberInfo.ResourceValue,
					RelatedSubscriberId: resp.Data.ProductInfo.SubscriberInfo.RelatedSubscriberId,
					StartDate:           resp.Data.ProductInfo.SubscriberInfo.StartDate,
					SubscriberId:        resp.Data.ProductInfo.SubscriberInfo.SubscriberId,
					Status:              resp.Data.ProductInfo.SubscriberInfo.Status,
				},
			},
		}
	}
	return true, response
}
