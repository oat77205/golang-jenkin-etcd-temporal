package psa

import (
	"errors"
	"hl-order-api/configs"
	"hl-order-api/routes/models"
	"hl-order-api/services/flowconfig"
	"hl-order-api/services/order"
	"hl-order-api/util"

	"github.com/gofiber/fiber/v2"
	"gitlab.com/true-itsd/iservicemax/omni/api/ms/psa/services/deviceserviceinfo"
	"gitlab.com/true-itsd/iservicemax/omni/api/ms/psa/services/updatewarrantydevice"
	"gitlab.com/true-itsd/iservicemax/omni/pkg/common-utils-backend/services/logger"
)

type IServicePSA interface {
	ServiceGetDeviceServiceInfo(req deviceserviceinfo.GetDeviceServiceInfoRequest, logModel logger.LogModel) (response *deviceserviceinfo.GetDeviceServiceInfoResponse, code string, bizError string, err error)
	ServiceUpdateWarrantyDevice(req models.SubmitOrderRequest, claims util.Claims, flowid *flowconfig.OrderFlowidConfig, orderData *order.Order, logModel logger.LogModel) (response *updatewarrantydevice.UpdateWarrantyDeviceResponse, code string, bizError string, err error)
}

type ServicePSA struct {
}

func NewIServicePSA() IServicePSA {
	return &ServicePSA{}
}

func (r *ServicePSA) ServiceGetDeviceServiceInfo(req deviceserviceinfo.GetDeviceServiceInfoRequest, logModel logger.LogModel) (response *deviceserviceinfo.GetDeviceServiceInfoResponse, code string, bizError string, err error) {
	callGetDeviceServiceInfo := deviceserviceinfo.NewCallGetDeviceServiceInfo(configs.Conf.Endpoint.Psa)
	serviceGetDeviceServiceInfo := deviceserviceinfo.NewServiceGetDeviceServiceInfo(callGetDeviceServiceInfo)
	resp, result := serviceGetDeviceServiceInfo.GetDeviceServiceInfoServiceReturnResponse(req, logModel)
	if result.Code == "500" {
		return nil, result.Code, fiber.ErrInternalServerError.Message, errors.New(result.Description)
	}

	if result.Code == "404" {
		return resp, result.Code, fiber.ErrNotFound.Message, errors.New(fiber.ErrNotFound.Message)
	}

	if result.Code != "200" {
		return nil, result.Code, result.Description, errors.New(result.Description)
	}

	return resp, result.Code, result.Description, nil
}

func (r *ServicePSA) ServiceUpdateWarrantyDevice(req models.SubmitOrderRequest, claims util.Claims, flowid *flowconfig.OrderFlowidConfig, orderData *order.Order, logModel logger.LogModel) (response *updatewarrantydevice.UpdateWarrantyDeviceResponse, code string, bizError string, err error) {
	callUpdateWarrantyDevice := updatewarrantydevice.NewCallUpdateWarrantyDevice(configs.Conf.Endpoint.Psa)
	serviceUpdateWarrantyDevice := updatewarrantydevice.NewServiceGetDeviceServiceInfo(callUpdateWarrantyDevice)

	keyInfo := []updatewarrantydevice.KeyInfo{}
	for _, d := range orderData.OrderInfo.OrderItems {
		keyInfo = append(keyInfo, updatewarrantydevice.KeyInfo{
			Name:  d.Serial,
			Value: d.InventoryType,
		})
	}
	resp, result := serviceUpdateWarrantyDevice.UpdateWarrantyDeviceService(updatewarrantydevice.UpdateWarrantyDeviceRequest{
		ReqTrxId: req.CorrelationID,
		UserInfo: updatewarrantydevice.UserInfo{
			AppUser:       "HEADLESS",
			ChannelAccess: "HL-TOS",
			ChannelCode:   orderData.SaleInfo.DealerCode,
			ChannelUser:   claims.ChannelAlias,
			Role:          "",
			UserCode:      claims.EmployeeID,
			UserLogin:     claims.UserName,
			AppPassword:   "",
		},
		KeyInfo: keyInfo,
		Params: []updatewarrantydevice.Params{
			{
				ParamName: "ONLINE",
				ParamInfo: []updatewarrantydevice.ParamInfo{
					{
						Name:         "",
						Value:        "",
						Remark:       "",
						SaleDate:     req.OrderInfo.SubmitOrderDate.String(),
						SaleChannel:  "1",
						SaleOrderId:  req.OrderInfo.OrderNumber,
						CampaignCode: "",
					},
				},
			},
		},
	}, logModel)
	if result.Code == "500" {
		return nil, result.Code, fiber.ErrInternalServerError.Message, errors.New(result.Description)
	}

	if result.Code == "404" {
		return resp, result.Code, fiber.ErrNotFound.Message, errors.New(fiber.ErrNotFound.Message)
	}

	if result.Code != "200" {
		return nil, result.Code, result.Description, errors.New(result.Description)
	}

	return resp, result.Code, result.Description, nil
}
