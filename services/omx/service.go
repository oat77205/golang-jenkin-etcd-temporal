package omx

import (
	"errors"
	"hl-order-api/configs"
	"hl-order-api/routes/models"
	"hl-order-api/services/intx"
	"hl-order-api/services/order"
	"strings"
	"time"

	"github.com/gofiber/fiber/v2"
	"gitlab.com/true-itsd/iservicemax/omni/api/ms/intx/services/promotionList"
	"gitlab.com/true-itsd/iservicemax/omni/api/ms/omx/services/submitorder"
	"gitlab.com/true-itsd/iservicemax/omni/pkg/common-utils-backend/services/logger"
	"gitlab.com/true-itsd/iservicemax/omni/pkg/common-utils-backend/utils"
)

type IServiceOMX interface {
	ServiceSubmitOMX(req models.SubmitOrderRequest, orderData *order.Order, getPromotion *promotionList.DataPromotionListByBusinessLineResponse, logModel logger.LogModel) (response *submitorder.SubmitOMXResponse, code string, bizError string, err error)
}

type ServiceOMX struct {
}

func NewIServiceOMX() IServiceOMX {
	return &ServiceOMX{}
}

func (r *ServiceOMX) ServiceSubmitOMX(req models.SubmitOrderRequest, orderData *order.Order, getPromotion *promotionList.DataPromotionListByBusinessLineResponse, logModel logger.LogModel) (response *submitorder.SubmitOMXResponse, code string, bizError string, err error) {
	orderType := ""
	trContractTerm := ""
	trDefaultContractFee := ""
	soc := ""
	currentPriceplan := ""
	var contractExpireDate *time.Time
	var diff time.Duration

	for _, d := range getPromotion.Resp.Return.AgreementOffer.AgreementOfferArr {
		if d.Offer.SocType == "P" {
			currentPriceplan = d.Offer.Code
			break
		}
	}

	offers := []submitorder.Offers{}
	if orderData.SubscriberInfo.Priceplan == currentPriceplan {
		orderType = "3"
	} else if orderData.SubscriberInfo.Priceplan != currentPriceplan {
		orderType = "2"
		offers = append(offers, submitorder.Offers{
			EffectiveDate: time.Now().Format("2006-01-02T15:04:05Z"),
			OfferName:     orderData.SubscriberInfo.Priceplan,
			ServiceType:   "80",
		})
	}

	for _, d := range orderData.SubscriberInfo.AdditionPackage {
		serviceINTX := intx.NewIServiceINTX()
		respGetOffer, code, bizError, err := serviceINTX.ServiceGetOfferDetail(req, getClassify(getPromotion.Resp.Return.System), orderData, logModel)
		if err != nil {
			return nil, code, bizError, err
		}
		offerDetailInfoArray := respGetOffer.GetOfferDetailListResponse.Return.OfferDetailList.OfferDetailInfoArray
		if len(offerDetailInfoArray) > 0 {
			soc = offerDetailInfoArray[0].Code
			properties := strings.Split(offerDetailInfoArray[0].Properties, ";")
			for _, d := range properties {
				if strings.Contains(d, "TR_DEFAULT_CONTRACT_FEE") {
					trDefaultContractFee = strings.Split(d, "=")[1]
				} else if strings.Contains(d, "TR_CONTRACT_TERM") {
					trContractTerm = strings.Split(d, "=")[1]
				}
			}
		}

		for i, d := range orderData.SubscriberInfo.ContractProposition {
			if i+1 < len(orderData.SubscriberInfo.ContractProposition) {
				_, date1 := utils.SetDateFormatByDateStr(d.ContractExpireDate, "", "2006-01-02 15:04:05")
				_, date2 := utils.SetDateFormatByDateStr(orderData.SubscriberInfo.ContractProposition[i+1].ContractExpireDate, "", "2006-01-02 15:04:05")
				if date1.After(date2) {
					contractExpireDate = &date1
				} else {
					contractExpireDate = &date2
				}
			}
		}
		now := time.Now()
		if contractExpireDate != nil && contractExpireDate.After(now) {
			diff = contractExpireDate.Sub(now)
		}

		offer := submitorder.Offers{
			OfferName:   d.OfferName,
			ServiceType: d.ServiceType,
		}
		if d.ServiceType == "85" {
			offer.OfferParameterInfo = getOfferParameterInfo(trContractTerm, trDefaultContractFee, diff, now)
		}
		if orderType == "3" {
			offer.Soc = soc
		}
		offers = append(offers, offer)
	}

	reqOMX := submitorder.SubmitOMXRequest{
		Order: submitorder.OrderSubmitOMX{
			Channel:    "headless",
			OrderID:    req.OrderInfo.OrderNumber,
			OrderType:  orderType,
			DealerCode: orderData.SaleInfo.DealerCode,
			ExtendedInfo: []submitorder.ExtendedInfo{
				{
					Name:  "ORG_CHANNEL",
					Value: req.Channel,
				},
			},
		},
		Customer: submitorder.CustomerSubmitOMX{
			Ou: submitorder.Ou{
				Subscriber: submitorder.SubscriberSubmitOMX{
					ActivityInfo: submitorder.ActivityInfoSubmitOMX{
						ActivityReason: "CREQ",
					},
					Offers:           offers,
					SubscriberNumber: orderData.SubscriberInfo.SubscriberNumber,
				},
			},
		},
	}
	serviceSubmitOMX := submitorder.NewCallSubmitOMX(configs.Conf.Endpoint.Omx)
	callSubmitOMX := submitorder.NewServiceSubmitOMX(serviceSubmitOMX)
	result, resp := callSubmitOMX.SubmitOMXService(reqOMX, logModel)

	if result.Code == "500" {
		return nil, result.Code, fiber.ErrInternalServerError.Message, errors.New(result.Description)
	}

	if result.Code == "900" {
		return nil, "200", result.Description, errors.New(result.Description)
	}

	return resp, "200", "Success", nil
}

func getClassify(productType string) string {
	types := ""

	if productType == strings.ToUpper("PREPAY") {
		types = "pre"
	} else if productType == strings.ToUpper("CCBS") {
		types = "post"
	}

	return types
}

func getOfferParameterInfo(trContractTerm string, trDefaultContractFee string, diff time.Duration, now time.Time) []submitorder.OfferParameterInfo {
	startDate, expireDate := utils.GetDateOffer(trContractTerm)
	_, date := utils.SetDateFormatByDateStr(expireDate, "", "2006-01-02 00:00:00")
	contractExpireDate := date.Add(diff).Format("2006-01-02 00:00:00")
	return []submitorder.OfferParameterInfo{
		{
			ParamName:   "TR_ACTUAL_CONTRACT_START_DATE",
			ValuesArray: utils.DefaultValue(startDate),
		},
		{
			ParamName:   "TR_CONTRACT_FEE",
			ValuesArray: utils.DefaultValue(trDefaultContractFee),
		},
		{
			ParamName:   "TR_CONTRACT_NUMBER",
			ValuesArray: "-",
		},
		{
			ParamName:   "TR_CONTRACT_REMARK",
			ValuesArray: "-",
		},
		{
			ParamName:   "TR_CONTRACT_TERM",
			ValuesArray: utils.DefaultValue(trContractTerm),
		},
		{
			ParamName:   "TR_GENERATE_CHARGE_YES_NO",
			ValuesArray: "N",
		},
		{
			ParamName:   "TR_ORIG_CONTRACT_EXPIRE_DATE",
			ValuesArray: utils.DefaultValue(contractExpireDate),
		},
	}
}
