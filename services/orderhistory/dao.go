package orderhistory

import (
	"context"
	"time"

	"gitlab.com/true-itsd/iservicemax/omni/pkg/common-utils-backend/databases"
	"gitlab.com/true-itsd/iservicemax/omni/pkg/common-utils-backend/services/logger"
	"gitlab.com/true-itsd/iservicemax/omni/pkg/common-utils-backend/utils"
)

const CollectionDB = "order_history"

type IServiceDaoOrderHistory interface {
	InsertOrderHistory(reqDB OrderHistory, logModel logger.LogModel) error
}

type serviceDaoOrderHistory struct {
	conf utils.SystemDatabase
}

func NewIServiceDaoOrderHistory(conf utils.SystemDatabase) IServiceDaoOrderHistory {
	return &serviceDaoOrderHistory{
		conf: conf,
	}
}

func (in *serviceDaoOrderHistory) InsertOrderHistory(reqDB OrderHistory, logModel logger.LogModel) error {
	startStep := time.Now()

	logStepRequest := logger.LogStepRequest{
		StepName:    "Create - OrderHistory",
		StartDate:   utils.ConvDatetimeFormatLog(startStep),
		StepRequest: "", //Query params
		Endpoint:    "Mongo DB",
		Method:      "Insert",
		System:      "Order",
	}

	defer func() {
		logger.LogStep(logStepRequest, logModel, startStep)
	}()

	//Set timeout query 5 secs
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	client, err := databases.GetMongoDB(in.conf.MongoDB.Uri, in.conf.MongoDB.DbName, nil)
	if err != nil {
		logStepRequest.ResultDesc = "Error : GetMongoDB"
		logStepRequest.StepResponse = err.Error()
		logStepRequest.ResultCode = "500"
		return err
	}
	_, err = client.Collection(CollectionDB).InsertOne(ctx, reqDB)
	if err != nil {
		logStepRequest.ResultDesc = "Error : Insert"
		logStepRequest.StepResponse = err.Error()
		logStepRequest.ResultCode = "500"
		return err
	}
	logStepRequest.ResultDesc = "SUCCESS"
	logStepRequest.ResultCode = "200"
	return nil
}
