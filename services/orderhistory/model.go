package orderhistory

import "go.mongodb.org/mongo-driver/bson/primitive"

type OrderHistory struct {
	CorrelationID       string             `bson:"correlationId"`
	OrderNumber         string             `bson:"orderNumber"`
	UserID              string             `bson:"userId"`
	ActivityName        string             `bson:"activityName"`
	ActivityDescription string             `bson:"activityDescription"`
	CreatedDate         primitive.DateTime `bson:"createdDate"`
	CreatedBy           string             `bson:"createdBy"`
}
