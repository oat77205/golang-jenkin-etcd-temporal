package shopsearch

import (
	"errors"
	"hl-order-api/util"

	"github.com/gofiber/fiber/v2"
	"gitlab.com/true-itsd/iservicemax/omni/api/ms/sale-profile/services/gshopsearch"
	"gitlab.com/true-itsd/iservicemax/omni/pkg/common-utils-backend/services/logger"
	"gitlab.com/true-itsd/iservicemax/omni/pkg/common-utils-backend/utils"
)

type IServiceServiceGetShopSearch interface {
	ServiceGetShopSearch(req gshopsearch.GShopSearchRequest, logModel logger.LogModel) (*gshopsearch.GShopSearchResponse, string, string, error)
}

type ServiceServiceGetShopSearch struct {
	config utils.EtcdApiConfig
}

func NewIServiceGetShopSearch(config utils.EtcdApiConfig) IServiceServiceGetShopSearch {
	return &ServiceServiceGetShopSearch{
		config: config,
	}
}

func (in *ServiceServiceGetShopSearch) ServiceGetShopSearch(req gshopsearch.GShopSearchRequest, logModel logger.LogModel) (*gshopsearch.GShopSearchResponse, string, string, error) {
	confGShopSearch := util.ConvertGetEndpointAuthentication(in.config, "gShopSearch", "")

	callGShopSearch := gshopsearch.NewCallGShopSearch(confGShopSearch)
	gShopSearchService := gshopsearch.NewServiceGShopSearch(callGShopSearch)
	gShopSearchResponse, err := gShopSearchService.GShopSearchService(req, logModel)
	if err != nil {
		return nil, "500", fiber.ErrInternalServerError.Message, err
	}

	if gShopSearchResponse.Data == nil || len(gShopSearchResponse.Data.ShopInfoList) == 0 {
		return nil, "404", fiber.ErrNotFound.Message, err
	}

	dupCode := util.RemoveDuplicateStr(req.SearchList[0].Value)

	if len(dupCode) != len(gShopSearchResponse.Data.ShopInfoList) {
		msg := ""
		aPartnerCode := make([]string, len(gShopSearchResponse.Data.ShopInfoList))
		for i, d := range gShopSearchResponse.Data.ShopInfoList {
			aPartnerCode[i] = d.PartnerCode
		}

		for _, d := range req.SearchList[0].Value {
			if !util.Contains(aPartnerCode, d) {
				msg += d + ","
			}
		}
		return nil, "404", fiber.ErrNotFound.Message, errors.New(fiber.ErrNotFound.Message + " partnerCode (" + msg[0:len(msg)-1] + ")")
	}

	return gShopSearchResponse, "200", "", nil
}
