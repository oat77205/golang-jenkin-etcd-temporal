package getreceipt

import (
	"hl-order-api/routes/models"
	"hl-order-api/services/orderhistory"
	"hl-order-api/services/tsmpayment"
	"hl-order-api/util"
	"strings"
	"time"

	"gitlab.com/true-itsd/iservicemax/omni/api/ms/payment/services/getreceipt"
	"gitlab.com/true-itsd/iservicemax/omni/pkg/common-utils-backend/services/logger"
	"gitlab.com/true-itsd/iservicemax/omni/pkg/common-utils-backend/utils"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type IServiceGetReceipt interface {
	ServiceGetReceipt(req models.GetReceiptRequest, claims util.Claims, logModel logger.LogModel) (bool, utils.ResponseStandard)
}

type ServiceGetReceipt struct {
	Databases utils.SystemDatabase
}

func NewIServiceGetReceipt(db utils.SystemDatabase) IServiceGetReceipt {
	return &ServiceGetReceipt{
		Databases: db,
	}
}

func (r *ServiceGetReceipt) ServiceGetReceipt(req models.GetReceiptRequest, claims util.Claims, logModel logger.LogModel) (bool, utils.ResponseStandard) {
	var response utils.ResponseStandard

	serviceTsmPayment := tsmpayment.NewIServiceTsmPayment()
	respGetReceipt, code, bizError, err := serviceTsmPayment.ServiceGetReceipt(getreceipt.GetReceiptRequest{
		OrderId: req.OrderInfo.OrderNumber,
	}, logModel)

	if err != nil {
		response.Code = code
		response.BizError = bizError
		response.Message = err.Error()
		response.System = util.System_TSM_PAYMENT
		return false, response
	}

	defer func() {
		serviceDaoOrderHistory := orderhistory.NewIServiceDaoOrderHistory(r.Databases)
		serviceDaoOrderHistory.InsertOrderHistory(orderhistory.OrderHistory{
			CorrelationID:       req.CorrelationID,
			OrderNumber:         req.OrderInfo.OrderNumber,
			UserID:              claims.ChannelSystem,
			ActivityName:        "GetReceipt",
			ActivityDescription: "GetReceipt ",
			CreatedDate:         primitive.NewDateTimeFromTime(time.Now()),
			CreatedBy:           claims.ChannelSystem,
		}, logModel)
	}()

	var Data []string
	for _, d := range respGetReceipt.ResponseData.PrintData {
		s := strings.Split(d, "|")
		Data = append(Data, string(s[5]))
	}

	response = util.GetResponse200(response)
	response.Data = Data
	return true, response
}
