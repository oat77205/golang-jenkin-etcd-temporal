package gcs

import (
	"errors"
	"hl-order-api/configs"
	"hl-order-api/routes/models"
	"hl-order-api/services/order"
	"hl-order-api/services/validateexistingcampaign"
	"hl-order-api/util"

	"github.com/gofiber/fiber/v2"
	"gitlab.com/true-itsd/iservicemax/omni/api/ms/gcs/services/validate"
	"gitlab.com/true-itsd/iservicemax/omni/api/ms/gcs/services/validateprivilegesubscriberbymobile"
	"gitlab.com/true-itsd/iservicemax/omni/pkg/common-utils-backend/services/logger"
	"gitlab.com/true-itsd/iservicemax/omni/pkg/common-utils-backend/utils"
)

type IServiceGCS interface {
	ServicePreverifyExisting(req models.PreverifyExistingRequest, claims util.Claims, logModel logger.LogModel) (utils.Result, *validate.ValidatePreverifyExistingResponse)
	ServicePrivilegeSubscriberByMobileService(req models.ValidateSerialNumberResquest, orderData *order.Order, claims util.Claims, logModel logger.LogModel) (*validateprivilegesubscriberbymobile.ValidatePrivilegeSubscriberByMobileResponse, string, string, error)
}

type ServiceGCS struct {
}

func NewIServiceGCS() IServiceGCS {
	return &ServiceGCS{}
}

func (r *ServiceGCS) ServicePreverifyExisting(req models.PreverifyExistingRequest, claims util.Claims, logModel logger.LogModel) (utils.Result, *validate.ValidatePreverifyExistingResponse) {
	callPreverifyExisting := validate.NewCalValidateRegiserPrepaid(configs.Conf.Endpoint.GCS)
	servicePreverifyExisting := validate.NewServiceValidate(callPreverifyExisting)

	result, resp := servicePreverifyExisting.ValidatePreverifyExistingService(validate.ValidatePreverifyExistingRequest{
		CorrelationId: req.CorrelationID,
		Channel:       "headless",
		UserId:        claims.ChannelSystem,
		ServiceId:     req.SubscriberInfo.SubscriberNumber,
		RequireDetail: req.InquiryProfileInfo.InquiryProfileFlag,
	}, logModel)

	return result, resp
}

func (r *ServiceGCS) ServicePrivilegeSubscriberByMobileService(req models.ValidateSerialNumberResquest, orderData *order.Order, claims util.Claims, logModel logger.LogModel) (*validateprivilegesubscriberbymobile.ValidatePrivilegeSubscriberByMobileResponse, string, string, error) {
	callPrivilegeSubscriberByMobile := validateprivilegesubscriberbymobile.NewCalPrivilegeSubscriberByMobile(configs.Conf.Endpoint.GCS)
	servicePrivilegeSubscriberByMobile := validateprivilegesubscriberbymobile.NewServicePrivilegeSubscriberByMobile(callPrivilegeSubscriberByMobile)

	serviceID := ""
	companyCode := ""
	ccbsProposition := ""
	campaignCode := ""
	serviceCode := req.SubscriberInfo.ServiceCode

	if orderData.SubscriberInfo != nil {
		serviceID = orderData.SubscriberInfo.SubscriberNumber
		companyCode = orderData.SubscriberInfo.CompanyCode
		ccbsProposition = orderData.SubscriberInfo.PropositionName
	}

	if serviceCode == "" {
		if len(orderData.OrderInfo.OrderItems) > 0 {
			campaignCode = orderData.OrderInfo.OrderItems[0].CampaignInfo.CampaignCode
		}
		serviceValidExitCam := validateexistingcampaign.NewIServiceValidateExistingCampaign(configs.Conf.Endpoint.GCS, configs.Conf.Endpoint.Privilege)
		code, respGetCampaign := serviceValidExitCam.GetCampaignService(campaignCode, logModel)
		if respGetCampaign.Code != util.Code200 {
			return nil, respGetCampaign.Code, respGetCampaign.BizError, errors.New(respGetCampaign.Message)
		}
		serviceCode = code
	}

	resp := servicePrivilegeSubscriberByMobile.PrivilegeSubscriberByMobileService(validateprivilegesubscriberbymobile.ValidatePrivilegeSubscriberByMobileRequest{
		CorrelationID:   req.CorrelationID,
		Channel:         "headless",
		UserID:          claims.ChannelSystem,
		ServiceCode:     serviceCode,
		ServiceID:       serviceID,
		IDNumber:        orderData.ContactInfo.Identification,
		PartnerCode:     orderData.SaleInfo.DealerCode,
		CompanyCode:     companyCode,
		Proposition:     "", //db.proposition
		CcbsProposition: ccbsProposition,
		CustomerType:    orderData.CustomerInfo.CustomerType,
		CampaignCode:    "", //db.campaignCode
		CampaignType:    "", //db.campaignType
	}, logModel)

	if resp.Code == "500" {
		return nil, resp.Code, fiber.ErrInternalServerError.Message, errors.New(resp.Message)
	}

	return resp, resp.Code, resp.Message, nil
}
