package flowconfig

import (
	"context"
	"encoding/json"
	"fmt"
	"time"

	"gitlab.com/true-itsd/iservicemax/omni/pkg/common-utils-backend/databases"
	"gitlab.com/true-itsd/iservicemax/omni/pkg/common-utils-backend/services/logger"
	"gitlab.com/true-itsd/iservicemax/omni/pkg/common-utils-backend/utils"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

const CollectionDB = "order_flowid_config"

type IServiceDaoOrderFlowidConfig interface {
	GetOrderFlowidConfig(flowid string, logModel logger.LogModel) (error, *OrderFlowidConfig)
}

type serviceDaoOrderFlowidConfig struct {
	conf utils.SystemDatabase
}

func NewIServiceDaoOrderFlowidConfig(conf utils.SystemDatabase) IServiceDaoOrderFlowidConfig {
	return &serviceDaoOrderFlowidConfig{
		conf: conf,
	}
}

func (in *serviceDaoOrderFlowidConfig) GetOrderFlowidConfig(flowid string, logModel logger.LogModel) (error, *OrderFlowidConfig) {
	startStep := time.Now()
	var dataDB *OrderFlowidConfig
	var query primitive.D

	logStepRequest := logger.LogStepRequest{
		StepName:     "Get - OrderFlowidConfig",
		StartDate:    utils.ConvDatetimeFormatLog(startStep),
		StepRequest:  "",
		StepResponse: "Success",
		Endpoint:     "Mongo DB",
		Method:       "Select",
		System:       "Order",
	}

	defer func() {
		logStepRequest.StepRequest = fmt.Sprint(query)
		logger.LogStep(logStepRequest, logModel, startStep)
	}()

	//Set timeout query 5 secs
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	client, err := databases.GetMongoDB(in.conf.MongoDB.Uri, in.conf.MongoDB.DbName, nil)
	if err != nil {
		logStepRequest.ResultDesc = "Error : GetMongoDB"
		logStepRequest.StepResponse = err.Error()
		logStepRequest.ResultCode = "500"
		return err, nil
	}

	query = bson.D{{Key: "flowId", Value: flowid}}
	cursor, err := client.Collection(CollectionDB).Find(ctx, query)
	if err != nil {
		logStepRequest.ResultDesc = "Error : QueryOrderFlowidConfig"
		logStepRequest.StepResponse = err.Error()
		logStepRequest.ResultCode = "500"
		return err, nil
	}

	defer cursor.Close(ctx)
	for cursor.Next(ctx) {
		cursor.Decode(&dataDB)
	}
	if err = cursor.Err(); err != nil {
		return err, nil
	}

	if dataDB == nil {
		logStepRequest.ResultDesc = "Error : Data not found"
		logStepRequest.StepResponse = "Data not found"
		logStepRequest.ResultCode = "404"
		return nil, nil
	}

	logResponse, _ := json.Marshal(dataDB)
	logStepRequest.StepResponse = string(logResponse)
	logStepRequest.ResultDesc = "SUCCESS"
	logStepRequest.ResultCode = "200"
	return nil, dataDB
}
