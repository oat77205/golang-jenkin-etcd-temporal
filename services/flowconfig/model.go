package flowconfig

type OrderFlowidConfig struct {
	FlowID          string `bson:"flowId"`
	IsPickup        bool   `bson:"isPickup"`
	PickupDueDate   int    `bson:"pickupDueDate"`
	ApplicationCode string `bson:"applicationCode"`
	Destination     string `bson:"destination"`
	SaveOrder       struct {
		SaveOrderOnline  bool `bson:"saveOrderOnline"`
		SubmitOrder      bool `bson:"submitOrder"`
		AddPaymentMethod bool `bson:"addPaymentMethod"`
	} `bson:"saveOrder"`
	UpdateOrderState struct {
		IssueStock struct {
			SubmitOrder      bool `bson:"submitOrder"`
			AddPaymentMethod bool `bson:"addPaymentMethod"`
		} `bson:"issueStock"`
	} `bson:"updateOrderState"`
	GoodsReceive   GoodsReceive   `bson:"goodsReceive"`
	CustomerPickup CustomerPickup `bson:"customerPickup"`
	DocSystem      DocSystem      `bson:"docSystem"`
}

type GoodsReceive struct {
	SmsToCustomer SmsToCustomer `bson:"smsToCustomer"`
}

type SmsToCustomer struct {
	Action       bool `bson:"action"`
	Sms          Sms  `bson:"sms"`
	CompletedSms Sms  `bson:"completedSms"`
	CancelSms    Sms  `bson:"cancelSms"`
}

type Sms struct {
	TemplateId      string `bson:"templateId"`
	Version         string `bson:"version"`
	ServiceId       string `bson:"serviceId"`
	ShortCode       string `bson:"shortCode"`
	TemplateContent string `bson:"templateContent"`
	DateFormat      string `bson:"dateFormat"`
}

type CustomerPickup struct {
	SmsToCustomer   SmsToCustomer   `bson:"smsToCustomer"`
	EmailToCustomer EmailToCustomer `bson:"emailToCustomer"`
}

type EmailToCustomer struct {
	Action         bool  `bson:"action"`
	CompletedEmail Email `bson:"completedEmail"`
	// Email          Email `bson:"email"`
	// CancelEmail    Email `bson:"cancelEmail"`
}

type Email struct {
	TemplateID string `bson:"templateId"`
	Version    string `bson:"version"`
	Language   string `bson:"language"`
}

type DocSystem struct {
	UploadType      string `bson:"uploadType"`
	ApplicationCode string `bson:"applicationCode"`
	Destination     string `bson:"destination"`
	Overwrite       bool   `bson:"overwrite"`
}
