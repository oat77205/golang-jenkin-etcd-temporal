package tsmsale

import (
	"errors"
	"hl-order-api/configs"
	"hl-order-api/routes/models"

	"github.com/gofiber/fiber/v2"
	"gitlab.com/true-itsd/iservicemax/omni/api/ms/inventory/services/submitorder"
	"gitlab.com/true-itsd/iservicemax/omni/pkg/common-utils-backend/services/logger"
)

type IServiceTsmSale interface {
	ServiceSubmitOrderOnline(req models.UpdatePartialOrderRequest, logModel logger.LogModel) (response *submitorder.SubmitOrderResponse, code string, bizError string, err error)
}

type ServiceTsmSale struct {
}

func NewIServiceTsmSale() IServiceTsmSale {
	return &ServiceTsmSale{}
}

func (r *ServiceTsmSale) ServiceSubmitOrderOnline(req models.UpdatePartialOrderRequest, logModel logger.LogModel) (response *submitorder.SubmitOrderResponse, code string, bizError string, err error) {
	callSubmitOrder := submitorder.NewCallSubmitOrder(configs.Conf.Endpoint.TsmSale)
	serviceSubmitOrder := submitorder.NewServiceSubmitOrder(callSubmitOrder)

	var serials []submitorder.Serials
	var serialsRange []submitorder.SerialsRange
	if len(req.OrderInfo.OrderItem) > 0 {
		for i, d := range req.OrderInfo.OrderItem {
			sequence := i + 1
			serials = append(serials, submitorder.Serials{
				OrderID:  req.OrderInfo.OrderNumber,
				Sequence: sequence,
				Serial:   d.Serial,
			})
			serialsRange = append(serialsRange, submitorder.SerialsRange{
				OrderID:  req.OrderInfo.OrderNumber,
				Sequence: sequence,
				Serial:   d.Serial,
			})
		}
	}

	resp, result := serviceSubmitOrder.SubmitOrderOnlineService(submitorder.SubmitOrderRequest{
		OrderID:      req.OrderInfo.OrderNumber,
		Serials:      serials,
		SerialsRange: serialsRange,
	}, logModel)
	if result.Code == "500" {
		return nil, result.Code, fiber.ErrInternalServerError.Message, errors.New(result.Description)
	}

	if result.Code != "200" {
		return nil, result.Code, result.Description, errors.New(result.Description)
	}

	return resp, result.Code, result.Description, nil
}
