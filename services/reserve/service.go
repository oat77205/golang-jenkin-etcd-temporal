package reserve

import (
	"bytes"
	"fmt"
	"hl-order-api/configs"
	"hl-order-api/routes/models"
	"hl-order-api/services/flowconfig"
	"hl-order-api/services/order"
	"hl-order-api/services/orderhistory"
	"hl-order-api/services/piminfo"
	"hl-order-api/services/shopsearch"
	"strings"
	"time"

	"hl-order-api/util"

	"github.com/gofiber/fiber/v2"
	saveOrderOnline "gitlab.com/true-itsd/iservicemax/omni/api/ms/inventory/services/saveorder"
	"gitlab.com/true-itsd/iservicemax/omni/api/ms/pim/services/getpiminfo"
	"gitlab.com/true-itsd/iservicemax/omni/api/ms/sale-profile/services/gshopsearch"
	"gitlab.com/true-itsd/iservicemax/omni/pkg/common-utils-backend/services/logger"
	"gitlab.com/true-itsd/iservicemax/omni/pkg/common-utils-backend/utils"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type IServiceReserve interface {
	ServiceReserve(models.RequestReserve, util.Claims, *flowconfig.OrderFlowidConfig, logger.LogModel) (bool, utils.ResponseStandard)
	SetStructToSaveOrderOnline(req models.RequestReserve) saveOrderOnline.SaveOrderRequest
	SaveReserve(req saveOrderOnline.SaveOrderRequest, logModel logger.LogModel) (*saveOrderOnline.SaveOrderResponse, utils.Result)
}

type ServiceReserve struct {
	configSMU       utils.EtcdApiConfig
	configInventory utils.EtcdApiConfig
	Databases       utils.SystemDatabase
}

func NewIServiceReserve(configSMU utils.EtcdApiConfig, configInventory utils.EtcdApiConfig, db utils.SystemDatabase) IServiceReserve {
	return &ServiceReserve{
		configSMU:       configSMU,
		configInventory: configInventory,
		Databases:       db,
	}
}

func (in *ServiceReserve) ServiceReserve(req models.RequestReserve, claims util.Claims, orderFlowid *flowconfig.OrderFlowidConfig, logModel logger.LogModel) (bool, utils.ResponseStandard) {
	var response utils.ResponseStandard
	response.Code = util.Code200
	response.BizError = "Success"
	response.Message = "Success"
	serviceDaoSaveOrder := order.NewIServiceDaoOrder(configs.Conf.Database)

	err, orderData := serviceDaoSaveOrder.GetOrder(req.OrderInfo.OrderNumber, claims.ChannelSystem, logModel)
	if err != nil {
		response.Code = util.Code500
		response.BizError = fiber.ErrInternalServerError.Message
		response.Message = err.Error()
		response.System = util.System_HL_ORDER_API
		return false, response
	}
	if orderData != nil {
		response.Code = util.Code422
		response.BizError = "Duplicate order number"
		response.Message = "Duplicate order number"
		response.System = util.System_HL_ORDER_API
		return false, response
	}

	defer func() {
		if response.Code == "200" {
			serviceDaoOrderHistory := orderhistory.NewIServiceDaoOrderHistory(in.Databases)
			serviceDaoOrderHistory.InsertOrderHistory(orderhistory.OrderHistory{
				CorrelationID:       req.CorrelationID,
				OrderNumber:         req.OrderInfo.OrderNumber,
				UserID:              claims.ChannelSystem,
				ActivityName:        "Reserve",
				ActivityDescription: "Reserve " + req.OrderInfo.OrderNumber,
				CreatedDate:         primitive.NewDateTimeFromTime(time.Now()),
				CreatedBy:           claims.ChannelSystem,
			}, logModel)
		}
	}()

	reqShopSearch := gshopsearch.GShopSearchRequest{}
	reqShopSearch.Channel = claims.ChannelSystem
	reqShopSearch.CorrelationID = req.CorrelationID
	reqShopSearch.UserID = claims.ChannelSystem
	searchList := []string{req.SaleInfo.DealerCode}
	if req.SaleInfo.OwnerDealer != "" {
		searchList = append(searchList, req.SaleInfo.OwnerDealer)
	}
	reqShopSearch.SearchList = []gshopsearch.SearchList{
		{
			Type:  "partnerCode",
			Value: searchList,
		},
	}
	if orderFlowid.IsPickup {
		reqShopSearch.SearchList = []gshopsearch.SearchList{
			{
				Type:  "partnerCode",
				Value: []string{req.SaleInfo.DealerCode, req.SaleInfo.OwnerDealer},
			},
		}
	}
	serviceShopSearch := shopsearch.NewIServiceGetShopSearch(in.configInventory)
	respShopSearch, code, bizError, err := serviceShopSearch.ServiceGetShopSearch(reqShopSearch, logModel)

	if err != nil {
		response.Code = code
		response.BizError = bizError
		response.Message = err.Error()
		response.System = util.System_GCS
		return false, response
	}

	serviceGetPiminfo := piminfo.NewIServiceGetPiminfo(in.configInventory)
	respPim, code, bizError, err := serviceGetPiminfo.ServiceGetPiminfo(piminfo.RequestPiminfo{
		CorrelationID: req.CorrelationID,
		OrderItems:    req.OrderInfo.OrderItems,
	}, claims, logModel)

	if err != nil {
		response.Code = code
		response.BizError = bizError
		response.Message = err.Error()
		response.System = util.System_REALTIME_INVENTORY
		return false, response
	}
	req = TranformProductInfo(req, *respPim)

	_, resp := in.SaveReserve(in.SetStructToSaveOrderOnline(req), logModel)
	if resp.Code != util.Code200 {
		response.Code = resp.Code
		response.BizError = "Fail"
		response.Message = resp.Description
		response.System = util.System_TSM_SALE
		return false, response
	}

	err = serviceDaoSaveOrder.InsertOrder(DataInsertOrder(req, claims, respShopSearch, orderFlowid), logModel)
	if err != nil {
		response.Code = util.Code500
		response.BizError = fiber.ErrInternalServerError.Message
		response.Message = err.Error()
		response.System = util.System_HL_ORDER_API
		return false, response
	}

	return true, response
}

func (in *ServiceReserve) SetStructToSaveOrderOnline(req models.RequestReserve) saveOrderOnline.SaveOrderRequest {
	var response saveOrderOnline.SaveOrderRequest
	Details := []saveOrderOnline.Details{}
	response.OrderID = req.OrderInfo.OrderNumber
	response.ThaiID = req.CustomerInfo.Identification
	response.CustomerFullName = req.CustomerInfo.FirstName + " " + req.CustomerInfo.LastName
	response.CustomerAddress = req.CustomerInfo.CustomerAddress.HouseNo + " " + req.CustomerInfo.CustomerAddress.BuildingName + " " + req.CustomerInfo.CustomerAddress.Tumbon + " " + req.CustomerInfo.CustomerAddress.Amphur + " " + req.CustomerInfo.CustomerAddress.City + " " + req.CustomerInfo.CustomerAddress.Zip
	response.ShopCode = req.SaleInfo.DealerCode
	response.SaleCode = req.SaleInfo.SaleCode
	response.SaleName = req.SaleInfo.SaleName
	response.Total = *req.OrderInfo.PaymentInfo.TotalAmount
	response.BillDiscountAmt = *req.OrderInfo.PaymentInfo.BillDiscountAmount
	response.TotalAfterDiscount = 0
	response.BillOtherPaymentAmt = *req.OrderInfo.PaymentInfo.BillOtherPaymentAmount
	response.GrandTotal = 0
	response.GrandTotal = 0
	response.BillDiscount = []string{}
	response.BillOtherPayments = []string{}
	verifyKey := []string{}
	PrivilgeRequiredValue := ""
	TotalOtherPaymentAmt := 0.00
	DepositAmt := 0.00
	NetAmt := 0.00
	OtherPaymentAmt := 0.00
	InstallmentAmt := 0.00
	TotalDiscountAmt := 0.00
	TotalNetAmt := 0.00
	Discounts := []saveOrderOnline.Discounts{}
	OtherPayments := []saveOrderOnline.OtherPayments{}
	CampaignCode := ""
	CampaignName := ""
	if len(req.OrderInfo.OrderItems) > 0 {
		for i, d := range req.OrderInfo.OrderItems {
			qty := int(*d.AmountInfo.Qty)
			Price := *d.AmountInfo.Price
			Total := Price * float64(qty)
			if d.AmountInfo.DiscountInfo != nil {
				TotalDiscountAmt = *d.AmountInfo.DiscountInfo.TotalDiscountAmount
				for j, valueDiscount := range d.AmountInfo.DiscountInfo.Discount {
					Discount := saveOrderOnline.Discounts{
						OrderID:        req.OrderInfo.OrderNumber,
						Sequence:       i + 1,
						No:             j + 1,
						DiscountCode:   valueDiscount.DiscountCode,
						DiscountAmount: *valueDiscount.DiscountAmount,
					}
					Discounts = append(Discounts, Discount)
				}
			}
			if d.AmountInfo.OtherPaymentInfo != nil {
				OtherPaymentAmt = *d.AmountInfo.OtherPaymentInfo.TotalOtherPaymentAmount
				TotalOtherPaymentAmt = TotalOtherPaymentAmt + OtherPaymentAmt
				for k, valueOtherPayment := range d.AmountInfo.OtherPaymentInfo.OtherPayments {
					OtherPayment := saveOrderOnline.OtherPayments{
						OrderID:            req.OrderInfo.OrderNumber,
						Sequence:           i + 1,
						No:                 k + 1,
						OtherPaymentCode:   valueOtherPayment.OtherPaymentCode,
						OtherPaymentAmount: int(*valueOtherPayment.OtherPaymentAmount),
					}
					OtherPayments = append(OtherPayments, OtherPayment)
				}
			}
			if d.CampaignInfo != nil {
				CampaignCode = *d.CampaignInfo.CampaignCode
				if d.CampaignInfo.CampaignName != nil {
					CampaignName = *d.CampaignInfo.CampaignName
				}
				if len(d.CampaignInfo.VerifyKey) > 0 {
					for _, value := range d.CampaignInfo.VerifyKey {
						verifyKey = append(verifyKey, CreateKeyValuePairs(value))
					}
					PrivilgeRequiredValue = strings.Join(verifyKey, ",")
				}
			}
			NetAmt = Total - TotalDiscountAmt
			TotalNetAmt = TotalNetAmt + NetAmt
			Detail := saveOrderOnline.Details{
				OrderID:                req.OrderInfo.OrderNumber,
				Sequence:               i + 1,
				Campaign:               CampaignCode,
				CampaignName:           CampaignName,
				Proposition:            d.Proposition,
				PromotionSet:           d.PromotionSet,
				PromotionType:          d.PromotionType,
				GroupID:                d.GroupID,
				ProductType:            d.ProductType,
				ProductCode:            d.ProductCode,
				ServiceRegisterType:    "",
				MobileNumber:           req.SubscriberInfo.SubscriberNumber,
				Price:                  d.AmountInfo.Price,
				Qty:                    &qty,
				Total:                  &Total,
				DiscountAmount:         &TotalDiscountAmt,
				DepositAmount:          &DepositAmt,
				NetAmount:              &NetAmt,
				OtherPaymentAmount:     &OtherPaymentAmt,
				InstallmentAmount:      &InstallmentAmt,
				PrivilegeRequiredValue: PrivilgeRequiredValue,
				Discounts:              Discounts,
				OtherPayments:          OtherPayments,
			}
			Details = append(Details, Detail)
			response.Details = Details
		}
	}
	response.Total = TotalNetAmt
	response.TotalAfterDiscount = response.Total - response.BillDiscountAmt
	response.GrandTotal = response.TotalAfterDiscount - response.BillOtherPaymentAmt - TotalOtherPaymentAmt
	return response

}

func (confs *ServiceReserve) SaveReserve(req saveOrderOnline.SaveOrderRequest, logModel logger.LogModel) (*saveOrderOnline.SaveOrderResponse, utils.Result) {
	confGetSaveOrderOnline := util.ConvertGetEndpointAuthentication(confs.configSMU, "saveOrderOnline", "saveOrderOnline")
	newCallSaveOrderOnline := saveOrderOnline.NewCallSaveOrder(confGetSaveOrderOnline)
	servcieSaveOrderOnline := saveOrderOnline.NewServiceSaveOrder(newCallSaveOrderOnline)
	data, resp := servcieSaveOrderOnline.SaveOrderOnlineService(req, logModel)
	return data, resp
}
func CreateKeyValuePairs(m map[string]string) string {
	b := new(bytes.Buffer)
	for key, value := range m {
		fmt.Fprintf(b, "%s=%s", key, value)
	}
	return b.String()
}
func TranformProductInfo(req models.RequestReserve, reqPim getpiminfo.GetPiminfoResponse) models.RequestReserve {
	for _, p := range reqPim.Data.ProductList {
		for i, d := range req.OrderInfo.OrderItems {
			if p.MatCode == d.ProductCode {
				req.OrderInfo.OrderItems[i].ProductInfo.Code = p.MatCode
				req.OrderInfo.OrderItems[i].ProductInfo.Name = p.ProductName
				req.OrderInfo.OrderItems[i].ProductInfo.Brand = p.Brand
				req.OrderInfo.OrderItems[i].ProductInfo.Model = p.Model
				req.OrderInfo.OrderItems[i].ProductInfo.Color = p.ColorTH
				req.OrderInfo.OrderItems[i].ProductInfo.Capacity = p.Capacity
			}
		}
	}
	return req
}
func DataInsertOrder(req models.RequestReserve, claims util.Claims, shopsearch *gshopsearch.GShopSearchResponse, orderFlowid *flowconfig.OrderFlowidConfig) *order.Order {
	var orderItems []order.OrderItems
	var billDiscountInfos []order.BillDiscountInfo
	var billOtherPaymentInfos []order.BillOtherPaymentInfo
	var additionPackage []order.AdditionPackage
	for _, d := range req.OrderInfo.OrderItems {
		orderItem := order.OrderItems{}
		aDiscount := []order.Discount{}
		aOtherPayments := []order.OtherPayments{}
		if d.AmountInfo.DiscountInfo != nil {
			for _, discount := range d.AmountInfo.DiscountInfo.Discount {
				aDiscount = append(aDiscount, order.Discount{
					DiscountCode:   discount.DiscountCode,
					DiscountAmount: util.ConvertFloat64(discount.DiscountAmount),
					CounponSerial:  discount.CounponSerial,
				})

			}
		}

		orderItem = order.OrderItems{
			Sequence:    d.Sequence,
			ProductCode: d.ProductCode,
			ProductInfo: order.ProductInfo{
				Code:     d.ProductInfo.Code,
				Name:     d.ProductInfo.Name,
				Brand:    d.ProductInfo.Brand,
				Model:    d.ProductInfo.Model,
				Color:    d.ProductInfo.Color,
				Capacity: d.ProductInfo.Capacity,
			},
			ProductType:        d.ProductType,
			PromotionSet:       d.PromotionSet,
			PromotionType:      d.PromotionType,
			Proposition:        d.Proposition,
			GroupID:            d.GroupID,
			InventoryType:      d.InventoryType,
			StockShopCode:      d.StockShopCode,
			StockShopType:      util.GetStockShopType(d.StockShopCode),
			OrderItemStateCode: d.OrderItemStateCode,
			AmountInfo: order.AmountInfo{
				Price: *d.AmountInfo.Price,
				Qty:   *d.AmountInfo.Qty,
			},
		}
		if d.AmountInfo.DepositAmt != nil {
			orderItem.AmountInfo.DepositAmt = *d.AmountInfo.DepositAmt
		}
		if d.AmountInfo.OtherPaymentInfo != nil {
			for _, otherpayment := range d.AmountInfo.OtherPaymentInfo.OtherPayments {
				aOtherPayments = append(aOtherPayments, order.OtherPayments{
					OtherPaymentCode:   otherpayment.OtherPaymentCode,
					OtherPaymentAmount: otherpayment.OtherPaymentAmount,
					CounponSerial:      otherpayment.CounponSerial,
				})
			}
			orderItem.AmountInfo.OtherPaymentInfo = &order.OtherPaymentInfo{
				TotalOtherPaymentAmount: d.AmountInfo.OtherPaymentInfo.TotalOtherPaymentAmount,
				OtherPayments:           &aOtherPayments,
			}
		}
		if d.AmountInfo.DiscountInfo != nil {
			orderItem.AmountInfo.DiscountInfo = &order.DiscountInfo{
				TotalDiscountAmount: util.ConvertFloat64(d.AmountInfo.DiscountInfo.TotalDiscountAmount),
				Discount:            aDiscount,
			}
		}

		if d.CampaignInfo != nil {
			orderItem.CampaignInfo = &order.CampaignInfo{
				CampaignCode: utils.GetValueStr(d.CampaignInfo.CampaignCode),
				CampaignName: utils.GetValueStr(d.CampaignInfo.CampaignName),
				VerifyKey:    d.CampaignInfo.VerifyKey,
			}
		}
		orderItems = append(orderItems, orderItem)
	}
	if req.OrderInfo.PaymentInfo.BillDiscountInfo != nil {
		for _, d := range *req.OrderInfo.PaymentInfo.BillDiscountInfo {
			billDiscountInfo := order.BillDiscountInfo{
				DiscountCode:   d.DiscountCode,
				DiscountAmount: d.DiscountAmount,
				CouponSerial:   d.CouponSerial,
			}
			billDiscountInfos = append(billDiscountInfos, billDiscountInfo)
		}

	}
	if req.OrderInfo.PaymentInfo.BillOtherPaymentInfo != nil {
		for _, d := range *req.OrderInfo.PaymentInfo.BillOtherPaymentInfo {
			billOtherPaymentInfo := order.BillOtherPaymentInfo{
				OtherPaymentCode:   d.OtherPaymentCode,
				OtherPaymentAmount: d.OtherPaymentAmount,
				CouponSerial:       d.CouponSerial,
			}
			billOtherPaymentInfos = append(billOtherPaymentInfos, billOtherPaymentInfo)
		}
	}
	if req.SubscriberInfo.AdditionPackage != nil {
		for _, d := range *req.SubscriberInfo.AdditionPackage {
			additionPackage = append(additionPackage, order.AdditionPackage{
				OfferName:   d.OfferName,
				ServiceType: d.ServiceType,
			})
		}
	}

	now := time.Now()
	orderDB := order.Order{
		CorrelationID: req.CorrelationID,
		FlowID:        req.FlowID,
		Channel:       req.Channel,
		SaleInfo: order.SaleInfo{
			DealerCode:      req.SaleInfo.DealerCode,
			DealerName:      order.GetShopNameTh(req.SaleInfo.DealerCode, shopsearch),
			OwnerDealer:     req.SaleInfo.OwnerDealer,
			SaleCode:        req.SaleInfo.SaleCode,
			OwnerDealerName: order.GetShopNameTh(req.SaleInfo.OwnerDealer, shopsearch),
			SaleName:        req.SaleInfo.SaleName,
		},
		SubscriberInfo: &order.SubscriberInfo{
			SubscriberNumber: req.SubscriberInfo.SubscriberNumber,
			CompanyCode:      req.SubscriberInfo.CompanyCode,
			Priceplan:        req.SubscriberInfo.Priceplan,
			AdditionPackage:  additionPackage,
		},
		OrderInfo: order.OrderInfo{
			OrderNumber:    req.OrderInfo.OrderNumber,
			OrderDate:      primitive.NewDateTimeFromTime(*req.OrderInfo.OrderDate.Time),
			OrderStateCode: "RESERVED",
			CreatedDate:    primitive.NewDateTimeFromTime(now),
			CreatedBy:      claims.ChannelSystem,
			UpdatedDate:    primitive.NewDateTimeFromTime(now),
			UpdatedBy:      claims.ChannelSystem,
			OrderItems:     orderItems,
			PaymentInfo: order.PaymentInfo{
				TotalAmount:            *req.OrderInfo.PaymentInfo.TotalAmount,
				BillDiscountAmount:     *req.OrderInfo.PaymentInfo.BillDiscountAmount,
				BillOtherPaymentAmount: *req.OrderInfo.PaymentInfo.BillOtherPaymentAmount,
				BillDiscountInfo:       &billDiscountInfos,
				BillOtherPaymentInfo:   &billOtherPaymentInfos,
			},
		},

		CustomerInfo: order.CustomerInfo{
			Title:              req.CustomerInfo.Title,
			FirstName:          req.CustomerInfo.FirstName,
			LastName:           req.CustomerInfo.LastName,
			Identification:     req.CustomerInfo.Identification,
			IdentificationType: req.CustomerInfo.IdentificationType,
		},
	}

	if req.OrderInfo.PaymentInfo.ExpireDate.Time != nil {
		orderDB.OrderInfo.PaymentInfo.ExpireDate = primitive.NewDateTimeFromTime(*req.OrderInfo.PaymentInfo.ExpireDate.Time)
	}

	if req.CustomerInfo.CustomerAddress != nil {
		orderDB.CustomerInfo.CustomerAddress = &order.CustomerAddress{}
		orderDB.CustomerInfo.CustomerAddress.HouseNo = req.CustomerInfo.CustomerAddress.HouseNo
		orderDB.CustomerInfo.CustomerAddress.Tumbon = req.CustomerInfo.CustomerAddress.Tumbon
		orderDB.CustomerInfo.CustomerAddress.Amphur = req.CustomerInfo.CustomerAddress.Amphur
		orderDB.CustomerInfo.CustomerAddress.City = req.CustomerInfo.CustomerAddress.City
		orderDB.CustomerInfo.CustomerAddress.Zip = req.CustomerInfo.CustomerAddress.Zip
	}
	if req.CustomerInfo.BirthDate.Time != nil {
		orderDB.CustomerInfo.BirthDate = primitive.NewDateTimeFromTime(*req.CustomerInfo.BirthDate.Time)
	}

	if req.ContactInfo != nil {
		orderDB.ContactInfo.Title = req.ContactInfo.Title
		orderDB.ContactInfo.FirstName = req.ContactInfo.FirstName
		orderDB.ContactInfo.LastName = req.ContactInfo.LastName
		orderDB.ContactInfo.Identification = req.ContactInfo.Identification
		orderDB.ContactInfo.IdentificationType = req.ContactInfo.IdentificationType
		orderDB.ContactInfo.Gender = req.ContactInfo.Gender
		orderDB.ContactInfo.ContactNo = req.ContactInfo.ContactNo
		orderDB.ContactInfo.ContactEmail = req.ContactInfo.ContactEmail
		if req.ContactInfo.CustomerAddress != nil {
			orderDB.ContactInfo.CustomerAddress = &order.CustomerAddress{}
			orderDB.ContactInfo.CustomerAddress.HouseNo = req.ContactInfo.CustomerAddress.HouseNo
			orderDB.ContactInfo.CustomerAddress.Tumbon = req.ContactInfo.CustomerAddress.Tumbon
			orderDB.ContactInfo.CustomerAddress.Amphur = req.ContactInfo.CustomerAddress.Amphur
			orderDB.ContactInfo.CustomerAddress.City = req.ContactInfo.CustomerAddress.City
			orderDB.ContactInfo.CustomerAddress.Zip = req.ContactInfo.CustomerAddress.Zip
		}
		if req.ContactInfo.BirthDate.Time != nil {
			orderDB.ContactInfo.BirthDate = primitive.NewDateTimeFromTime(*req.ContactInfo.BirthDate.Time)
		}
	}

	return &orderDB
}
