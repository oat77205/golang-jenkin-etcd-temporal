package tsmpayment

import (
	"errors"
	"hl-order-api/configs"
	"hl-order-api/routes/models"
	"hl-order-api/services/order"

	"github.com/gofiber/fiber/v2"
	"gitlab.com/true-itsd/iservicemax/omni/api/ms/payment/services/addpayment"
	"gitlab.com/true-itsd/iservicemax/omni/api/ms/payment/services/getreceipt"
	"gitlab.com/true-itsd/iservicemax/omni/pkg/common-utils-backend/services/logger"
)

type IServiceTsmPayment interface {
	ServiceAddPayment(req models.UpdatePartialOrderRequest, orderData *order.Order, logModel logger.LogModel) (response *addpayment.AddPaymentResponse, code string, bizError string, err error)
	ServiceGetReceipt(req getreceipt.GetReceiptRequest, logModel logger.LogModel) (response *getreceipt.GetReceiptResponse, code string, bizError string, err error)
}

type ServiceTsmPayment struct {
}

func NewIServiceTsmPayment() IServiceTsmPayment {
	return &ServiceTsmPayment{}
}

func (r *ServiceTsmPayment) ServiceAddPayment(req models.UpdatePartialOrderRequest, orderData *order.Order, logModel logger.LogModel) (response *addpayment.AddPaymentResponse, code string, bizError string, err error) {
	callAddPayment := addpayment.NewCallAddPaymentService(configs.Conf.Endpoint.TsmPayment)
	serviceAddPayment := addpayment.NewServiceAddPaymentService(callAddPayment)

	var installmentPaymentList []addpayment.InstallmentPaymentList
	var creditcardPaymentList []addpayment.CreditcardPaymentList
	var creditcardPaymentFullList []addpayment.CreditcardPaymentFullList
	var chequePaymentList []addpayment.ChequePaymentList
	var withholdingTaxPaymentList []addpayment.WithholdingTaxPaymentList
	changeDueAmount := int(0)
	cash := int(0)
	total := int(orderData.OrderInfo.PaymentInfo.TotalAmount)
	taxID := ""
	if orderData.CustomerInfo.CustomerType == "I" {
		taxID = orderData.CustomerInfo.Identification
	}

	if orderData.OrderInfo.PaymentInfo.PaymentMethodInfo != nil {
		for _, d := range *orderData.OrderInfo.PaymentInfo.PaymentMethodInfo {
			creditcardPaymentFullList = append(creditcardPaymentFullList, addpayment.CreditcardPaymentFullList{
				Amount:         int(*d.Amount),
				ApprovalCode:   d.ApproveCode,
				BankID:         d.BankNo,
				BankObjectID:   d.BankObjectId,
				Binnum:         d.BinNum,
				CreditcardNo:   d.CreditCardNo,
				CreditcardType: d.CreditCardType,
				ExpiryDate:     d.ExpireDate,
				RepCompanyID:   d.RepCompanyId,
				PaymentCode:    d.PaymentMethod,
			})
			creditcardPaymentFullList = append(creditcardPaymentFullList)
		}
	}

	respstandard, resp := serviceAddPayment.AddPaymentServiceService(addpayment.AddPaymentRequest{
		OrderNo:                   req.OrderInfo.OrderNumber,
		CashierID:                 orderData.OrderInfo.PaymentInfo.CashierInfo.CashierId,
		CashierName:               orderData.OrderInfo.PaymentInfo.CashierInfo.CashierName,
		CashierLanUser:            orderData.OrderInfo.PaymentInfo.CashierInfo.CashierLanUser,
		CustomerType:              orderData.CustomerInfo.CustomerType,
		InstallmentPaymentList:    installmentPaymentList,
		CreditcardPaymentList:     creditcardPaymentList,
		CreditcardPaymentFullList: creditcardPaymentFullList,
		ChequePaymentList:         chequePaymentList,
		WithholdingTaxPaymentList: withholdingTaxPaymentList,
		RepCompanyObjectID:        orderData.OrderInfo.PaymentInfo.RepCompanyObjectId,
		OperCompanyObjectID:       orderData.OrderInfo.PaymentInfo.OperCompanyObjectId,
		CPUID:                     orderData.OrderInfo.PaymentInfo.CpuId,
		ReceiveCash:               "0",
		ChangeDueAmount:           &changeDueAmount,
		Cash:                      &cash,
		Total:                     &total,
		TrxType:                   "PC",
		FullFormFlag:              "0",
		CustomerInfoPaymentModel: addpayment.CustomerInfoPaymentModel{
			CertificateID:       orderData.CustomerInfo.Identification,
			CustomerType:        orderData.CustomerInfo.CustomerType,
			TitleName:           orderData.CustomerInfo.Title,
			FirstName:           orderData.CustomerInfo.FirstName,
			LastName:            orderData.CustomerInfo.LastName,
			AddressNo:           orderData.CustomerInfo.CustomerAddress.HouseNo,
			Moo:                 orderData.CustomerInfo.CustomerAddress.Moo,
			Soi:                 orderData.CustomerInfo.CustomerAddress.Soi,
			Road:                orderData.CustomerInfo.CustomerAddress.StreetName,
			Tumbol:              orderData.CustomerInfo.CustomerAddress.Tumbon,
			Amphur:              orderData.CustomerInfo.CustomerAddress.Amphur,
			Province:            orderData.CustomerInfo.CustomerAddress.City,
			Postcode:            orderData.CustomerInfo.CustomerAddress.Zip,
			TaxID:               taxID,
			BranchInfo:          "00000",
			CustomerFullName:    orderData.CustomerInfo.Title + " " + orderData.CustomerInfo.FirstName + " " + orderData.CustomerInfo.LastName,
			CustomerFullAddress: orderData.CustomerInfo.CustomerAddress.HouseNo + " " + orderData.CustomerInfo.CustomerAddress.BuildingName + " " + orderData.CustomerInfo.CustomerAddress.Tumbon + " " + orderData.CustomerInfo.CustomerAddress.Amphur + " " + orderData.CustomerInfo.CustomerAddress.City + " " + orderData.CustomerInfo.CustomerAddress.Zip,
		},
	}, logModel)
	if respstandard.Code == "500" {
		return nil, respstandard.Code, fiber.ErrInternalServerError.Message, errors.New(respstandard.Message)
	}

	return resp, respstandard.Code, respstandard.Message, nil
}

func (r *ServiceTsmPayment) ServiceGetReceipt(req getreceipt.GetReceiptRequest, logModel logger.LogModel) (response *getreceipt.GetReceiptResponse, code string, bizError string, err error) {
	callGetReceipt := getreceipt.NewCallGetReceiptService(configs.Conf.Endpoint.TsmPayment)
	serviceGetReceipt := getreceipt.NewServiceGetReceiptService(callGetReceipt)
	respstandard, resp := serviceGetReceipt.GetReceiptServiceService(req, logModel)
	if respstandard.Code == "500" {
		return nil, respstandard.Code, fiber.ErrInternalServerError.Message, errors.New(respstandard.Message)
	}

	if len(resp.ResponseData.PrintData) == 0 && resp.Fault != nil {
		return resp, "422", "ALREADY PRINT", errors.New(resp.Fault.Message)
	}

	return resp, respstandard.Code, respstandard.Message, nil
}
