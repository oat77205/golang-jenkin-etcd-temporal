package util

const (
	Code200                   = "200"
	Code400                   = "400"
	Code401                   = "401"
	Code404                   = "404"
	Code422                   = "422"
	Code500                   = "500"
	FormatDate                = "2006-01-02T15:04:05.000+07:00"
	ParameterRequired         = "Parameter is required."
	System_GCS                = "GCS"
	System_PSA                = "PSA"
	System_HL_ORDER_API       = "HL-OREDER-API"
	System_PIM                = "PIM"
	System_TSM_SALE           = "TSM-SALE"
	System_REALTIME_INVENTORY = "REALTIME-INVENTORY"
	System_TSM_PAYMENT        = "TSM-PAYMENT"
	System_INTX               = "INTX"
	System_OMX                = "OMX"
	System_DOC_SYSTEM         = "DOC-SYSTEM"
	System_HL_REPORT          = "HL-REPORT"
	System_PRIVILEGE          = "HL-REPORT"
)
