package util

import (
	"gitlab.com/true-itsd/iservicemax/omni/pkg/common-utils-backend/utils"
)

func ConvertGetEndpointAuthentication(conf utils.EtcdApiConfig, url string, urlEndpoint string) utils.EtcdApiConfig {
	var config utils.EtcdApiConfig

	if urlEndpoint != "" {
		config.Url = conf.Url
		config.Endpoints = map[string]string{urlEndpoint: conf.Endpoints[url]}
	} else {
		config.Url = conf.Url + conf.Endpoints[url]
	}

	config.InsecureSkipVerify = conf.InsecureSkipVerify
	config.Authentication = utils.Authentication{
		Basic: &utils.BasicAuthentication{
			Username: conf.Authentication.Basic.Username,
			Password: conf.Authentication.Basic.Password,
		},
	}

	return config
}
