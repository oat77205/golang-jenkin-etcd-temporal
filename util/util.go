package util

import (
	"hl-order-api/configs"
	"hl-order-api/routes/models"
	"time"

	"github.com/go-playground/validator/v10"
	"github.com/gofiber/fiber/v2"
	"gitlab.com/true-itsd/iservicemax/omni/pkg/common-utils-backend/utils"
)

var (
	OrderStateRequireSequence = []string{"SHIPPED"}
)

func IsValidCorrelation(req models.ValidCorrelationRequest) (bool, *models.ValidCorrelationResponse) {
	ignoreChannel := []string{"USSD"}

	if len(req.Channel) > 0 && Contains(ignoreChannel, req.Channel) {
		return true, nil
	}

	if len(req.CorrelationID) > 0 && len(req.Channel) > 0 && (req.CorrelationID == req.HCorID && req.Channel == req.HChannel) {
		return true, nil
	}

	r := &models.ValidCorrelationResponse{
		Code:        "400",
		Description: "Bad Request (x-correlation-id, x-consumer-username)",
	}

	return false, r
}

func Contains(s []string, str string) bool {
	for _, v := range s {
		if v == str {
			return true
		}
	}

	return false
}

func GetHttpsStatus(code string) int {
	switch code {
	case "200":
		return fiber.StatusOK
	case "900":
		return fiber.StatusOK
	case "400":
		return fiber.StatusBadRequest
	case "404":
		return fiber.StatusNotFound
	case "405":
		return fiber.StatusMethodNotAllowed
	case "422":
		return fiber.StatusUnprocessableEntity
	case "500":
		return fiber.StatusInternalServerError
	case "502":
		return fiber.StatusBadGateway
	case "503":
		return fiber.StatusServiceUnavailable
	case "504":
		return fiber.StatusGatewayTimeout
	case "512":
		return 512
	case "513":
		return 513
	default:
		return 200
	}
}

func ConvertNumber(number string) string {
	if number[0:1] == "6" {
		number = "0" + number[2:11]
	}
	return number
}

func GetResponse200(response utils.ResponseStandard) utils.ResponseStandard {
	response.Code = "200"
	response.BizError = "SUCCESS"
	response.Message = "Success"
	return response
}

func RemoveDuplicateStr(strSlice []string) []string {
	allKeys := make(map[string]bool)
	list := []string{}
	for _, item := range strSlice {
		if _, value := allKeys[item]; !value {
			allKeys[item] = true
			list = append(list, item)
		}
	}
	return list
}

func GetStockShopType(stockShopCode string) string {
	if Contains(configs.Conf.StockShopCode, stockShopCode) {
		return "ONLINE"
	} else {
		return "SHOP"
	}
}

func GetArrayErrorResponse(err []error) *[]utils.ErrorParam {
	var response []utils.ErrorParam
	for _, d := range err {
		if d != nil {
			for _, e := range d.(validator.ValidationErrors) {
				if e.Param() != "" {
					response = append(response, utils.ErrorParam{Param: e.Namespace(), Message: e.Tag() + " -> " + e.Param()})
				} else {
					response = append(response, utils.ErrorParam{Param: e.Namespace(), Message: e.Tag()})
				}
			}
		}
	}

	return &response
}

func ConvertFloat64(data *float64) float64 {
	if data == nil {
		return 0
	}
	return *data
}

func ToThaiTime(t time.Time) (result time.Time) {
	loc, _ := time.LoadLocation("Asia/Bangkok")
	return t.In(loc)
}
