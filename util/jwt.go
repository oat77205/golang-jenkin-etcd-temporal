package util

import (
	"errors"
	"strings"

	"github.com/golang-jwt/jwt/v4"
)

type Claims struct {
	UserType        string   `json:"userType,omitempty"`
	UserName        string   `json:"userName,omitempty"`
	FirstName       string   `json:"firstName,omitempty"`
	SurName         string   `json:"surName,omitempty"`
	ThaiName        string   `json:"thaiName,omitempty"`
	EngName         string   `json:"engName,omitempty"`
	EmployeeID      string   `json:"employeeID,omitempty"`
	ChannelSystem   string   `json:"channelSystem,omitempty"`
	ChannelAlias    string   `json:"channelAlias,omitempty"`
	ChannelName     string   `json:"channelName,omitempty"`
	PartnerTypeName string   `json:"partnerTypeName,omitempty"`
	SaleCode        string   `json:"saleCode,omitempty"`
	ShopCodeList    []string `json:"shopCodeList,omitempty"`
	UserGroupList   []string `json:"userGroupList,omitempty"`
	RefID           string   `json:"refID,omitempty"`
	jwt.RegisteredClaims
}

func decodeJWT(tokenString string) (error, *Claims) {
	token, _, err := new(jwt.Parser).ParseUnverified(tokenString, &Claims{})
	if err != nil {
		return err, nil
	}

	claims, ok := token.Claims.(*Claims)

	if !ok {
		err = errors.New("token is invalid")
		return err, nil
	}
	return nil, claims
}

func GetJWT(authorization string) (error, *Claims) {
	if authorization != "" {
		tokenstring := strings.Replace(authorization, "Bearer ", "", -1)
		err, claims := decodeJWT(tokenstring)
		if err != nil {
			return err, nil
		}
		return nil, claims
	} else {
		err := errors.New("token is invalid")
		return err, nil
	}
}
