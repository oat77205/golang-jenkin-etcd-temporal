package test

import (
	b64 "encoding/base64"
	"hl-order-api/configs"
	"testing"
)

/*func Test_GoodsReceive_IsReadyToPickUp(t *testing.T) {
	configs.FetchEtcd()
	start := time.Now()

	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	getTrackingMock := mock.NewMockIServiceGetTrackingDetail(ctrl)

	//getTrackingDetailResponse := gettrackingdetail.GetTrackingDetailResponse{Code: "200"}
	getTrackingDetailResponse := MockGetTrackingDetailResp()
	getTrackingDetailResponse.Data.TrackingList[0].TrackingStatus = "READY_TO_PICK_UP"
	getTrackingMock.EXPECT().GetTrackingDetailService(gomock.Any(), gomock.Any()).Return(&getTrackingDetailResponse)

	saveOmniMock := mock.NewMockIServiceSaveOmniTracking(ctrl)
	//saveOmniMock.EXPECT().SaveOmniTrackingService(gomock.Any(), gomock.Any()).Return(utils.ResponseStandard{Code: "200"})

	conf := utils.EtcdApiConfig{}
	goodsReceive := goodsreceive.NewIServiceGoodsReceive()
	goodsReceive.GetInstance().ServiceGetTrackingDetail = getTrackingMock
	goodsReceive.GetInstance().ServiceSaveOmniTracking = saveOmniMock

	var request models.RequestGoodsReceive
	var logModel logger.LogModel

	getLogModelRequest := logger.LogModelRequest{
		ServiceType:   configs.Conf.Log.ServiceType.GoodsReceive,
		CorrelationID: request.CorrelationId,
		Method:        fiber.MethodPost,
		StepName:      configs.Conf.Log.ServiceType.GoodsReceive,
		Start:         start,
		TrackingID:    request.GoodsReceive[0].TrackingId,
		//SubscriberNumber: request.CustomerInfo.ContractNo,
		Suffix:  configs.Conf.Log.Suffix,
		Product: configs.Conf.Log.Product,
	}
	logModel = logger.GetLogModel(getLogModelRequest)

	err, claims := util.GetJWT(string("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjaGFubmVsU3lzdGVtIjoiVE9TIiwiZXhwIjoxNjY3OTgzMTU2LCJpc3MiOiJJU00ifQ.A4Jt2xpv4LON_2QxuYonkmr3Gbdod5pELSX95k5BCEs"))

	if err != nil {
		t.Error(err)
	}
	result, _ := goodsReceive.ServiceSaveGoodsReceive(request, *claims, logModel)

	assert.Equal(t, false, result)
}

func Test_GoodsReceive_Success(t *testing.T) {
	configs.FetchEtcd()
	start := time.Now()

	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	getTrackingMock := mock.NewMockIServiceGetTrackingDetail(ctrl)

	getTrackingDetailResponse := MockGetTrackingDetailResp()
	getTrackingMock.EXPECT().GetTrackingDetailService(gomock.Any(), gomock.Any()).Return(&getTrackingDetailResponse)

	saveOmniMock := mock.NewMockIServiceSaveOmniTracking(ctrl)
	saveOmniMock.EXPECT().SaveOmniTrackingService(gomock.Any(), gomock.Any()).Return(utils.ResponseStandard{Code: "200"})

	goodsReceive := goodsreceive.NewIServiceGoodsReceive(utils.EtcdApiConfig{})
	goodsReceive.GetInstance().ServiceGetTrackingDetail = getTrackingMock
	goodsReceive.GetInstance().ServiceSaveOmniTracking = saveOmniMock

	var request models.RequestGoodsReceive
	var logModel logger.LogModel

	getLogModelRequest := logger.LogModelRequest{
		ServiceType:   configs.Conf.Log.ServiceType.GoodsReceive,
		CorrelationID: request.CorrelationId,
		Method:        fiber.MethodPost,
		StepName:      configs.Conf.Log.ServiceType.GoodsReceive,
		Start:         start,
		TrackingID:    request.GoodsReceive[0].TrackingId,
		//SubscriberNumber: request.CustomerInfo.ContractNo,
		Suffix:  configs.Conf.Log.Suffix,
		Product: configs.Conf.Log.Product,
	}
	logModel = logger.GetLogModel(getLogModelRequest)

	err, claims := util.GetJWT(string("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjaGFubmVsU3lzdGVtIjoiVE9TIiwiZXhwIjoxNjY3OTgzMTU2LCJpc3MiOiJJU00ifQ.A4Jt2xpv4LON_2QxuYonkmr3Gbdod5pELSX95k5BCEs"))

	if err != nil {
		t.Error(err)
	}
	result, _ := goodsReceive.ServiceSaveGoodsReceive(request, *claims, logModel)

	assert.Equal(t, true, result)
}

func MockGetTrackingDetailResp() gettrackingdetail.GetTrackingDetailResponse {
	var resp gettrackingdetail.GetTrackingDetailResponse
	s := `{"code":"200","bizError":"SUCCESS","message":"SUCCESS","data":{"trackingList":[{"orderNumber":"NOPTest12345633","orderShopCode":"80100964","pickupShopCode":"80000011","stockShopCode":"80100964","stockType":"ONLINE","channel":"TOS","trackingNumber":"12354123","totalItems":"2","totalItemsbyTracking":"2","trackingStatus":"SHIPED","statusReason":"","dueDate":"","pickupName":"","customerPickupName":"","imagePathList":[]}]}}`
	json.Unmarshal([]byte(s), &resp)
	return resp
}*/

func Test_Config(t *testing.T) {

	raw := `{
  "endpoint": "hl-order-api-complat",
  "url": "http://172.19.103.111:18084",
  "username": "2307441101",
  "password": "KgCv8L3p",
  "timeout": 30,
  "insecureSkipVerify": true,
  "paramList": [
    {
      "name": "smsgTemplate",
      "value": "/api/smsg-template"
    }
  ]
}`
	rawb64 := b64.StdEncoding.EncodeToString([]byte(raw))

	encrypt := configs.Encrypt([]byte("teDMCPtJM8Qsiy_PxM6TzxtIuoI01Gbj"), rawb64)
	t.Log(encrypt)

	data := configs.Decrypt([]byte("teDMCPtJM8Qsiy_PxM6TzxtIuoI01Gbj"), encrypt)
	t.Log(data)
	r, _ := b64.StdEncoding.DecodeString(data)
	t.Log(string(r))
}

func Test_Decode_Config(t *testing.T) {
	data := configs.Decrypt([]byte("teDMCPtJM8Qsiy_PxM6TzxtIuoI01Gbj"), "_sjEVCAQQjBEnpaNaXZOEfBXCk8ruzUiYnr0hUe_-pCcgnSx4KazPtSJYwfsOV59n8Y6fGluIVnypMzXOwfrLkMuS9Pd8e0seDfogfstJvTwVd9JrvNiZgMYIKVVgBS418UcAigW6ObhVIZF6z3QBoM1Hect90VFH_TguRZaGPS8BFk0CTS-Oi6o1JvY32vpt2csVZXg-eA1Z6oilIFeSeUwJYiXOqfD97LcMuDQx-7czZvNy57TVpi1HqnkZTJjDDDE2BA3Bqt1I2R7M-gwyQAZRkmcryksqRtkevzFUR94ziT1p9lkUsS12xxBufCaCh0tZD0QA4HXxR1qWq5zmPlcoLECPwC8qYqBuAlGAvNCgZK0oZCn6-CpD7dDd856-vj1j8wk-CnaYtiZIzcVa-CYezCq583V7Bj44wJ6WJnSNUIULa5wgIzvP2N44md_Y9X-7GtpQ2eI9x6lFV748U2grOc8dNJ1A2xes-4yKoHkko75eDw68wcnv1X8zm-NGa7UQHd0vdfD8olVge4N8TlTUDLDKACkgzm6ZxrfpI4OOSHX5bPZuiaWvFH0z70SGjNNH0guelkoyTw6j2I6cUzEevwD75hi3MH_AtfRLBPkKCKfJnti5q4fFuwSY2Ipnn46ChgisigzpGLD_1RO2sk4ug3FOPXN")
	t.Log(data)
	r, _ := b64.StdEncoding.DecodeString(data)
	t.Log(string(r))
}
